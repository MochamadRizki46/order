FROM trainingad1/java11
RUN java -version
RUN cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime && echo "Asia/Jakarta" > /etc/timezone
EXPOSE 1234
COPY ad1-partner-order-0.0.1-SNAPSHOT.jar ad1-partner-order-0.0.1-SNAPSHOT.jar
COPY . .
RUN pwd
ENTRYPOINT ["java", "-javaagent:/elastic-apm-agent-1.28.1.jar", "-Delastic.apm.service_name=dev-ad1-partner-order", "-Delastic.apm.server_urls=http://apm-server-apm-server.logging.svc.cluster.local:8200", "-Delastic.apm.application_packages=id.co.adira.partner" ,"-Xms512m" , "-Xmx1024m", "-jar", "ad1-partner-order-0.0.1-SNAPSHOT.jar"]
#ENTRYPOINT java -Xms512m -Xmx1024m -jar ad1-partner-order-0.0.1-SNAPSHOT.jar