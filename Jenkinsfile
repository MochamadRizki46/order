pipeline {

  environment {
    
    Version_Major = 1
    Version_Minor = 0
    Version_Patch = 0
    IMAGE_NAME = "adira-ent-registry-registry-vpc.ap-southeast-5.cr.aliyuncs.com/partner/dev-ad1-partner-order"
    IMAGE_TAG = "${Version_Major}.${Version_Minor}.${Version_Patch}-${BUILD_TIMESTAMP}-${env.BUILD_NUMBER}"
    Cluster_Context = credentials('cluster-context')
    Cluster_User = credentials('cluster-user')
	String SonarProjectKey = 'ad1-partner-order'
	String SonarHost = 'http://10.161.17.144:9000/dashboard?id='    
    String SPRING_ACTIVE_PROFILE = 'dev'
  }

  agent {
    kubernetes {
      
      defaultContainer 'jnlp'
      yaml """
apiVersion: v1
kind: Pod
metadata:
labels:
  component: ci
spec:
  # Use service account that can deploy to all namespaces
  
  containers: 
  - name: maven
    image: maven:3.6.3-openjdk-11
    imagePullPolicy: IfNotPresent
    command:
    - cat
    tty: true 
  - name: helm
    image: trainingad1/helm3
    imagePullPolicy: IfNotPresent
    command:
    - cat
    tty: true
  - name: kaniko
    image: gcr.io/kaniko-project/executor:debug-539ddefcae3fd6b411a95982a830d987f4214251
    imagePullPolicy: IfNotPresent
    command:
    - /busybox/cat
    tty: true
    volumeMounts:
      - name: docker-config
        mountPath: /kaniko/.docker
  volumes:
  - name: docker-config
    projected:
      sources:
      - secret:
          name: regcred1
          items:
            - key: .dockerconfigjson
              path: config.json
    
"""
}
  }
  stages {
	stage('Unit Test') {
      steps {
        container('maven') {
          sh """
            echo "******** currently executing Build stage ********"
            mvn clean test
          """
        }
      }
    }
    stage('build') {
      steps {
        container('maven') {
          sh """
            echo "******** currently executing Build stage ********"
            mvn clean package -DskipTest
          """
        }
      }
    }
    stage('sonarqube') {
      steps {
          withSonarQubeEnv('SonarQube') {
        container('maven') {
          sh """
            echo "******** currently executing sonarqube stage ********"
           mvn sonar:sonar \
                -Dsonar.projectKey=ad1-partner-order \
                -Dsonar.host.url=http://10.161.17.144:9000 \
                -Dsonar.login=97c87409342b7a6a0994a79eec8178410f5fcac5
          """
        }
      }
    }
  }
    stage("Quality gate") {
            steps {
                echo "******** currently executing Quality gate stage ********"
                waitForQualityGate abortPipeline: false
            }
        }
    stage('kaniko and push image to ali cloud private registry') {
      steps {
        container('kaniko') {
          sh """
             echo "******** currently executing kaniko stage ********"
             cp $workspace/target/ad1-partner-order-0.0.1-SNAPSHOT.jar ./ad1-partner-order-0.0.1-SNAPSHOT.jar
              ls -la
            /kaniko/executor --dockerfile=Dockerfile -f `pwd`/Dockerfile --context `pwd` --destination="${IMAGE_NAME}:${IMAGE_TAG}"
            """
        }
      }
    }
    stage('helm') {
      steps {
        container('helm') {
          withCredentials([file(credentialsId: 'kubeconfig', variable: 'KUBECONFIG')]) {
          sh """
            echo "******** currently executing deployment stage ********"
            kubectl config set-context ${Cluster_Context} --cluster=kubernetes --user=${Cluster_User}
            kubectl config use-context ${Cluster_Context}
            kubectl get nodes
            helm upgrade -i ad1-partner-order  helm/ad1-partner-order -f helm/ad1-partner-order/values-dev.yaml -n partner --set=image.tag=${IMAGE_TAG}
            kubectl rollout status deployment/ad1-partner-order -n partner
			kubectl get pods -n partner
			kubectl get svc -n partner
            helm ls -n partner
             """
         }
        }
      }
    post {
        failure {
              mail to: 'sonarandkatalon@adira.co.id,syam.arifin@adira.co.id,diki.kusnendar@adira.co.id,v.sandrotua.gajah@adira.co.id,v.dery.cahyadi@adira.co.id',
                subject: "FAILED: Build ${env.JOB_NAME} in Development Branch",
                body: "Build failed ${env.JOB_NAME} build no: ${env.BUILD_NUMBER}.\n\nView the log at:\n ${env.BUILD_URL}\n\nSee The SonarQube Report : ${SonarHost}${SonarProjectKey}"
        }
    success{
            mail to: 'sonarandkatalon@adira.co.id,syam.arifin@adira.co.id,diki.kusnendar@adira.co.id,v.sandrotua.gajah@adira.co.id,v.dery.cahyadi@adira.co.id',
                subject: "SUCCESSFUL: Build ${env.JOB_NAME} in Development Branch",
                body: "Build Successful ${env.JOB_NAME} build no: ${env.BUILD_NUMBER}\n\nView the log at:\n ${env.BUILD_URL}\n\nSee The SonarQube Report : ${SonarHost}${SonarProjectKey}"
        }
        aborted{
            mail to: 'sonarandkatalon@adira.co.id,syam.arifin@adira.co.id,diki.kusnendar@adira.co.id,v.sandrotua.gajah@adira.co.id,v.dery.cahyadi@adira.co.id',
                subject: "ABORTED: Build ${env.JOB_NAME} in Development Branch",
                body: "Build was aborted ${env.JOB_NAME} build no: ${env.BUILD_NUMBER}\n\nView the log at:\n ${env.BUILD_URL}\n\nSee The SonarQube Report : ${SonarHost}${SonarProjectKey}"
        }
       } 
    }
  }
}