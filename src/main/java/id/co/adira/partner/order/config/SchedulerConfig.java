package id.co.adira.partner.order.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import id.co.adira.partner.order.service.RetryMsgService;

@Configuration
@EnableScheduling
public class SchedulerConfig {

	@Autowired
	private RetryMsgService retryMsgService;
	
	
	@Scheduled(cron = "*/60 * * * * ?") //sv3
	public void retryCustomerDistribute() {
		retryMsgService.retryCustomer();
	}
	
	@Scheduled(cron = "*/60 * * * * ?") //sv3
	public void retryTrackingDistribute() {
		retryMsgService.retryTracking();
	}
	
	@Scheduled(cron = "*/60 * * * * ?") //sv3
	public void retryApplicationDistribute() {
		retryMsgService.retryApplication();
	}
}