package id.co.adira.partner.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AppConst {
    DRAFT("Draft"),
    CONTENT_TYPE("Content-Type"),
    MULTIPART_FORM_DATA("multipart/form-data"),
    ACCEPT("Accept"),
    X_API_KEY("x-api-key"),
    DOCUMENT_TYPE("documentType"),
    NO_NPWP("NoNPWP"),
    NPWP_CUSTOMER("NPWPCustomer"),
    KTP_CUSTOMER("KTPCustomer"),
    STNK("STNK"),
    NO_KTP("NoKTP"),
    FILE("file"),
    DOCUMENT_TITLE("documentTitle"),
    REGION("Region"),
    APPLICATION("application"),
    A1PARTNER("A1PARTNER"),
    OBJECT_STORE("objectStore"),
    ADIRA_OS("ADIRAOS"),
    REQUEST_ID("requestId"),
    ONE_TO_FIVE("12345"),
    SALES_OFFICER("salesOfficer"),
    APPL_NO_UNIT("applNoUnit"),
    ACCTION("ACCTION"),
    ID("id"),
    TANGGAL_SURVEY("tanggalSurvey"),
    JAM_SURVEY("jamSurvey"),
    FILES("files"),
    GROUPSTATUS_LE_TRACK("GS0001"),
    STATUS_LE_TRACK("ST0001"),
	PARA_TRACKING_UPDATE_LE("system-ad1Partner"),
	CUST_PERSONAL("PER"),
	REQ_IDP("requestId"),
    CURRENT_ORDER_NO("Current order no: {}"),
    AUTHORIZATION("Authorization"),
    SUBMIT_DRAFT_FOR_ORDER("Submit Draft for Order"),
    SUBMIT_DRAFT_FOR_CUSTOMER("Submit Draft for Customer"),
    SUBMIT_DRAFT_FOR_DOMISILI("Submit Draft for Domisili"),
    SUBMIT_DRAFT_FOR_UNIT("Submit Draft for Unit"),
    SUBMIT_DRAFT_FOR_UNIT_V2("Submit Draft for Unit v2"),
    SUBMIT_ORDER_TO_LE("Submit Order to LE"),
    SUBMIT_FOR_DOCUMENT("Submit Document"),
    CALCULATE_CREDIT_SIMULATION_FROM("Calculate Credit Simulation from User: "),
    ADMIN_CASH_FEE("adminCashFee"),
    ADMIN_CREDIT_FEE("adminCreditFee"),
    OTR("otr"),
    ASURANSI_CASH_AMT("asuransiCashAmt"),
    ASURANSI_CREDIT_AMT("asuransiCreditAmt"),
    EFFECTIVE_OR_LANDING_RATE("effectiveOrLandingRate"),
    UPLOAD_ECM("Upload File to ECM");
    
	private String message;

}
