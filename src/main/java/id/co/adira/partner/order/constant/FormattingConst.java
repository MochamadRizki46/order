package id.co.adira.partner.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum FormattingConst {
    FIRST_SIMPLE_DATE_FORMAT("dd-MM-yyyy"),
    SECOND_SIMPLE_DATE_FORMAT("dd MMM yyyy"),
    THIRD_SIMPLE_DATE_FORMAT("yyyy-MM-dd"),
    FOURTH_SIMPLE_DATE_FORMAT("dd-MMM-yyyy"),
    TIME_FORMAT("HH:mm"),
	DATE_TIME_FORMAT("dd MMM yyyy HH:mm");


    private String message;
}
