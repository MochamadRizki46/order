package id.co.adira.partner.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RequestAndResponseEnum {
    MOTOR_BARU_CREDIT_REQUEST_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"fin_type\":\"1\"," +
            "\"program\":\"\",\"obj_code\":\"001\",\"obj_brand_code\":\"519\"," +
            "\"obj_type_code\":\"021\",\"obj_model_code\":\"1FA\",\"obj_mfg_year\":2022," +
            "\"otr\":15000000,\"tenor\":24,\"payment_type\":\"02\",\"insr_type\":1,\"adm_insr_fin\":1," +
            "\"acuan_hitung\":1,\"nominal_hitung\":760964}"),

    MOTOR_BARU_CASH_REQUEST_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"fin_type\":\"1\",\"program\":\"\",\"obj_code\":\"001\",\"obj_brand_code\":\"519\"," +
            "\"obj_type_code\":\"021\",\"obj_model_code\":\"1FA\",\"obj_mfg_year\":2022,\"otr\":15000000,\"tenor\":24," +
            "\"payment_type\":\"02\",\"insr_type\":1,\"adm_insr_fin\":0,\"acuan_hitung\":2,\"nominal_hitung\":7000000}"),

    MOTOR_BEKAS_REQUEST_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"fin_type\":\"1\"," +
            "\"program\":\"\",\"obj_code\":\"002\",\"obj_brand_code\":\"519\"," +
            "\"obj_type_code\":\"021\",\"obj_model_code\":\"1FA\"," +
            "\"obj_mfg_year\":2018,\"otr\":7000000,\"tenor\":24," +
            "\"payment_type\":\"02\",\"insr_type\":1," +
            "\"adm_insr_fin\":0,\"acuan_hitung\":1,\"nominal_hitung\":300000}"),

    MOTOR_BARU_RESPONSE_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"otr\":15000000,\"tenor\":24,\"dp_amount\":7000000," +
            "\"installment_amount\":760964,\"biaya_administrasi_format\":\"Nominal\",\"biaya_administrasi_credit\":0," +
            "\"biaya_administrasi_cash\":800000,\"provisi_format\":\"Percentage\",\"provisi_credit\":0," +
            "\"provisi_cash\":1.15,\"effective_rate_format\":\"Percentage\",\"effective_rate\":30.12345,\"insr_type\":1," +
            "\"asuransi_basic_format\":\"Percentage\",\"asuransi_basic_credit\":0,\"asuransi_basic_cash\":1.60," +
            "\"asuransi_pa_format\":\"Percentage\",\"asuransi_pa_credit\":0,\"asuransi_pa_cash\":1.50," +
            "\"asuransi_life_format\":\"Percentage\",\"asuransi_life_credit\":0,\"asuransi_life_cash\":1.26," +
            "\"others_fee_format\":\"Nominal\",\"others_fee_credit\":0,\"others_fee_cash\":100000}"),

    MOTOR_BEKAS_RESPONSE_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"otr\":7000000,\"tenor\":24,\"dp_amount\":300000," +
            "\"installment_amount\":560964,\"biaya_administrasi_format\":\"Nominal\",\"biaya_administrasi_credit\":0," +
            "\"biaya_administrasi_cash\":800000,\"provisi_format\":\"Percentage\",\"provisi_credit\":0," +
            "\"provisi_cash\":1.15,\"effective_rate_format\":\"Percentage\",\"effective_rate\":28.12345,\"insr_type\":1," +
            "\"asuransi_basic_format\":\"Percentage\",\"asuransi_basic_credit\":0,\"asuransi_basic_cash\":1.60," +
            "\"asuransi_pa_format\":\"Percentage\",\"asuransi_pa_credit\":0,\"asuransi_pa_cash\":1.50," +
            "\"asuransi_life_format\":\"Percentage\",\"asuransi_life_credit\":0,\"asuransi_life_cash\":1.26," +
            "\"others_fee_format\":\"Nominal\",\"others_fee_credit\":0,\"others_fee_cash\":100000}"),

    MOBIL_BEKAS_REQUEST_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"fin_type\":\"1\"," +
            "\"program\":\"\",\"obj_code\":\"004\",\"obj_brand_code\":\"474\"," +
            "\"obj_type_code\":\"007\",\"obj_model_code\":\"084\",\"obj_mfg_year\":2016," +
            "\"otr\":75000000,\"tenor\":12,\"payment_type\":\"02\",\"insr_type\":1,\"adm_insr_fin\":1," +
            "\"acuan_hitung\":1,\"nominal_hitung\":1000000}"),

    MOBIL_BEKAS_RESPONSE_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"otr\":75000000," +
            "\"tenor\":12,\"dp_amount\":1000000,\"installment_amount\":211000," +
            "\"biaya_administrasi_format\":\"Nominal\",\"biaya_administrasi_credit\":0," +
            "\"biaya_administrasi_cash\":800000,\"provisi_format\":\"Percentage\",\"provisi_credit\":0," +
            "\"provisi_cash\":1.15,\"effective_rate_format\":\"Percentage\",\"effective_rate\":28.12345," +
            "\"insr_type\":1,\"asuransi_basic_format\":\"Percentage\",\"asuransi_basic_credit\":0," +
            "\"asuransi_basic_cash\":1.60,\"asuransi_pa_format\":\"Percentage\",\"asuransi_pa_credit\":0," +
            "\"asuransi_pa_cash\":1.50,\"asuransi_life_format\":\"Percentage\",\"asuransi_life_credit\":0," +
            "\"asuransi_life_cash\":1.26,\"others_fee_format\":\"Nominal\",\"others_fee_credit\":0," +
            "\"others_fee_cash\":100000}"),

    MOBIL_BARU_REQUEST_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"fin_type\":\"1\",\"program\":\"\",\"obj_code\":\"003\"," +
            "\"obj_brand_code\":\"474\",\"obj_type_code\":\"023\",\"obj_model_code\":\"U01\"," +
            "\"obj_mfg_year\":2022,\"otr\":400000000,\"tenor\":12,\"payment_type\":\"02\",\"insr_type\":1," +
            "\"adm_insr_fin\":0,\"acuan_hitung\":1,\"nominal_hitung\":50000000}"),

    MOBIL_BARU_RESPONSE_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"otr\":400000000,\"tenor\":12,\"dp_amount\":5000000," +
            "\"installment_amount\":211000,\"biaya_administrasi_format\":\"Nominal\",\"biaya_administrasi_credit\":0,\"biaya_administrasi_cash\":2851300," +
            "\"provisi_format\":\"Percentage\",\"provisi_credit\":0,\"provisi_cash\":1.15,\"effective_rate_format\":\"Percentage\",\"effective_rate\":0.3," +
            "\"insr_type\":1,\"asuransi_basic_format\":\"Percentage\",\"asuransi_basic_credit\":0,\"asuransi_basic_cash\":0," +
            "\"asuransi_pa_format\":\"Percentage\",\"asuransi_pa_credit\":0,\"asuransi_pa_cash\":0,\"asuransi_life_format\":\"Percentage\"," +
            "\"asuransi_life_credit\":0,\"asuransi_life_cash\":0,\"others_fee_format\":\"Nominal\",\"others_fee_credit\":0,\"others_fee_cash\":100000}"),

    DURABLE_REQUEST_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"fin_type\":\"1\",\"program\":\"\"," +
            "\"obj_code\":\"005\",\"obj_brand_code\":\"630\",\"obj_type_code\":\"124\"," +
            "\"obj_model_code\":\"23I\",\"obj_mfg_year\":0,\"otr\":3000000,\"tenor\":24," +
            "\"payment_type\":\"02\",\"insr_type\":1,\"adm_insr_fin\":1,\"acuan_hitung\":1,\"nominal_hitung\":1000000}"),

    DURABLE_RESPONSE_BODY_FOR_CREDSIM("{\"dlc\":\"1707080019\",\"otr\":3000000,\"tenor\":24,\"dp_amount\":1000000,\"installment_amount\":211000," +
            "\"biaya_administrasi_format\":\"Nominal\",\"biaya_administrasi_credit\":0,\"biaya_administrasi_cash\":800000," +
            "\"provisi_format\":\"Percentage\",\"provisi_credit\":0,\"provisi_cash\":1.15,\"effective_rate_format\":\"Percentage\"," +
            "\"effective_rate\":28.12345,\"insr_type\":1,\"asuransi_basic_format\":\"Percentage\",\"asuransi_basic_credit\":0," +
            "\"asuransi_basic_cash\":1.60,\"asuransi_pa_format\":\"Percentage\",\"asuransi_pa_credit\":0,\"asuransi_pa_cash\":1.50," +
            "\"asuransi_life_format\":\"Percentage\",\"asuransi_life_credit\":0,\"asuransi_life_cash\":1.26," +
            "\"others_fee_format\":\"Nominal\",\"others_fee_credit\":0,\"others_fee_cash\":100000}");



    private String message;
}
