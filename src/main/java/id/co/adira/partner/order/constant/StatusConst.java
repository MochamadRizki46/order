package id.co.adira.partner.order.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusConst {

	OK("Ok"), SUCCESS("Success"), CANNOT_FIND_SUBMIT_ORDER_WITH_ID("Can't find submit order with id: "), APPL_NO(""),
	SISTEM_MENERIMA_ORDER("Sistem Menerima Order"), ORDER_TERKIRIM("Order Terkirim"), SVR("SVR"),
	THERE_IS_SOMETHING_ERROR_IN_OUR_SYSTEM("There's something error in our system!"),
	VERIFIKASI_SEDANG_DILAKUKAN("Verifikasi sedang dilakukan"),
	ERROR_SENDING_ORDER_TO_LEAD_ENGINE("Error Send order to Lead Engine"), AD1PARTNER("Ad1partner"),
	FAILED_GET_KECAMATAN("Failed Get Kecamatan "), FAILED_GET_MODEL("Failed Get Model "),
	FAILED_GET_KABKOT("Failed Get Kabkot "), FAILED_TO_GET_TRACKING_HEADER("Failed Get Tracking Header "),
	FAILED_TO_DELETE_DRAFT("Failed to delete draft: "),
	INSERT_TRACKING_HISTORY_ORDER_TERKIRIM("Insert Tracking History Order Terkirim"),
	VERIFIKASI_ORDER("Verifikasi Order"), GET_DATA_FROM_TABLE_SUBMIT_ORDER("Get Data from table submit order"),
	INSERT_TRACKING_HISTORY_VERIFIKASI_ORDER("Insert Tracking History Verifikasi Order"),
	GET_TRACKING_HEADER("Get Tracking Header"), FIRST_TRACKING_HOME("tracking home 1"),
	SECOND_TRACKING_HOME("tracking home 2 {}"), THIRD_TRACKING_HOME("tracking home 3"),
	DATA_TRACKING_NOT_FOUND_ON_LE("Data Tracking Detail Not Found From LE"),
	CANNOT_FIND_SUBMIT_ORDER_WITH_APP_NO_UNIT("Can't find submit order with app no unit: "),
	CANNOT_FIND_SUBMIT_CUSTOMER_WITH_CURRENT_SUBMIT_ORDER("Can't find submit customer with current submit order. Cause: {}"),
	CANNOT_FIND_SUBMIT_ORDER_WITH_ORDER_NO("Can't find submit order with order no: %s"),
	FAILED_TO_SEND_NOTIF("Failed to send notif: {}"),
	FAILED_TO_SAVE_DATA_TO_DB("Failed to save current data to db: {}"), CANNOT_PARSE_DATE("Can't parse date: "),
	FAILED_TO_SAVEDATA_TO_DB("Failed to save current data to db: "),
	ERROR_WHILE_MAPPING_TRACKING_ORDER_DETAIL("Error while mapping tracking order detail: {}"),
	FAILED_TO_CONVERT_FILE("Failed to convert file: {}"),
	ERROR_WHILE_HITTING_UPLOAD_ECM("Error while hitting Upload ECM Endpoint: "),
	ERROR_WHILE_HITTING_DELETE_DRAFT("Error while hitting Delete Draft Endpoint: "),
	ERROR_WHILE_HITTING_URL_APPL_NO("Error while hitting URL Appl No: "),
	ERROR_WHILE_HITTING_EP_TRACKING("Error while hitting URL EP Tracking: "),
	ERROR_WHILE_HITTING_URL_HISTORY("Error while hitting URL History: "),
	ERROR_WHILE_HITTING_URL_EP_HISTORY("Error while hitting URL EP History: "),
	ERROR_WHILE_HITTING_DP_OR_PMT_API("Error while hitting DP or Pmt API: {}"),
	ERROR_WHILE_SAVING_SUBMIT_ORDER_TO_DB("Error while saving submit order to DB: {}"),
	ERROR_WHILE_TRYING_TO_SAVE_DRAFT_CUSTOMER("Error while trying to save submit customer: {}"),
	ERROR_WHILE_TRYING_TO_SAVE_DRAFT_DOMISILI("Error while trying to save submit domisili: (}"),
	ERROR_WHILE_TRYING_TO_SAVE_SUBMIT_UNIT("Error while trying to save submit unit: {}"),
	SUCCESSFULLY_DELETE_SUBMIT_ORDER_WITH_ORDER_NO("Successfully delete submit order with order no: {}"),
	SUCCESSFULLY_SAVE_SUBMIT_ORDER_WITH_ORDER_NO("Successfully save submit order with order no: {}"),
	SUCCESSFULLY_SAVE_SUBMIT_DOMISILI_WITH_ORDER_NO("Successfully save submit domisili with order no: {}"),
	SUCCESSFULLY_SAVE_SUBMIT_UNIT_WITH_ORDER_NO("Successfully save submit unit with order no: {}"),
	SUCCESSFULLY_SAVE_SUBMIT_CUSTOMER_WITH_ORDER_NO("Successfully save submit customer with order no: {}"),
	SUCCESSFULLY_SAVE_DRAFT("Successfully save data "),
	SUCCESSFULLY_SUBMIT_LE("Successfully submit order to LE "),
	FAILED_TO_CREATE_FILE("Failed to convert file: {}"),
	FAILED_TO_HIT_GET_API_KEY_COMMON_SERVICE("Failed to hit get API key from common service: {}"),
	FAILED_TO_HIT_API_DP_OR_PMT("Failed to hit API for get DP or PMT: {}"),
	BOTH_ADMIN_CASH_AND_CREDIT_CANNOT_BE_ZERO("Value of Admin cash and credit can't be zero!"),
	BOTH_ASURANSI_CASH_AND_CREDIT_CANNOT_BE_ZERO("Value of Asuransi Cash and Credit can't be zero!"),
	DATA_NOT_FOUND("Data Not Found"),
	SUCCESSFULLY_SAVE_OR_UPDATE_SUBMIT_UNIT_WITH_ORDER_NO("Successfully, save submit unit with order no: {}"),
	SUCCESSFULLY_SAVE_NEW_SUBMIT_UNIT("Successfully save new submit unit!"),
	SUCCESSFULLY_UPDATE_AN_EXISTING_SUBMIT_UNIT("Successfully update an existing submit unit!"),
	INVALID_REQUEST_DATA_FROM_CREDSIM("Invalid request data from credit simulation. Please adjust your request!");

	private String message;

}
