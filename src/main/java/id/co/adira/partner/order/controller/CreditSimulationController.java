package id.co.adira.partner.order.controller;

import id.co.adira.partner.order.dto.DpOrPmtRequestDTOV2;
import id.co.adira.partner.order.dto.creditsimulation.request.CreditSimulationNewReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.CreditSimulationDetailReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.DpOrPaymentRequestDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPmtHeadResDTO;
import id.co.adira.partner.order.dto.response.BaseResponse;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.service.CreditSimulationService;
import id.co.adira.partner.order.service.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("api/order")
public class CreditSimulationController {

	final CreditSimulationService creditSimulationService;

	final LogService logService;

	private static final Logger LOGGER = LoggerFactory.getLogger(CreditSimulationController.class.getName());

	@Autowired
	public CreditSimulationController(CreditSimulationService creditSimulationService, LogService logService) {
		this.creditSimulationService = creditSimulationService;
		this.logService = logService;
	}

//	@PostMapping("/v1/calculateCreditSimulation")
//	public ResponseEntity<DataResponse> calculateCreditSimulation(
//			@RequestBody CreditSimulationDetailReqDTO creditSimulationDetailReqDTO,
//			@RequestHeader(name = "Authorization") String authorization) {
//		DataResponse dataResponse = creditSimulationService.calculateCreditSimulation(creditSimulationDetailReqDTO,
//				authorization);
//		return new ResponseEntity<>(dataResponse, dataResponse.getHttpStatus());
//	}

	@PostMapping("/v1/dpOrPmt")
	public ResponseEntity<Object> getDataDpOrPmt(@RequestBody DpOrPaymentRequestDTO dpOrPaymentRequestDTO) {
		try {
			StopWatch stopWatch = new StopWatch();

			stopWatch.start();
			DpOrPmtHeadResDTO response = creditSimulationService.getDpOrPmtCalculationData(dpOrPaymentRequestDTO);
			stopWatch.stop();

//            logService.insertCredSimToLog(dpOrPaymentRequestDTO, response, CALCULATE_CREDIT_SIMULATION_FROM.getMessage() + dpOrPaymentRequestDTO, dpOrPaymentRequestDTO.getPic(), stopWatch.getTotalTimeMillis());
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@PostMapping("/hitungCreditSimulation")
	public ResponseEntity<Object> getDpOrPmtV2(@RequestBody DpOrPmtRequestDTOV2 dpOrPmtRequestDTOV2) {
		DataResponse response = creditSimulationService.getDpOrPmtCalculationDataV2(dpOrPmtRequestDTOV2);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@PostMapping("/v1/calculateCreditSimulation")
//	@CrossOrigin
	public ResponseEntity<DataResponse> calculateCreditSimulationNew(
			@RequestBody CreditSimulationNewReqDTO creditSimulationNewReqDTO) throws JsonProcessingException {
		DataResponse dataResponse = creditSimulationService.calculateCreditSimulationNew(creditSimulationNewReqDTO);
		return new ResponseEntity<>(dataResponse, dataResponse.getHttpStatus());
	}
}
