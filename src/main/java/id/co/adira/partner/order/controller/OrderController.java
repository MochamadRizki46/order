package id.co.adira.partner.order.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.co.adira.partner.order.dto.*;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.service.DraftSubmitPartialService;
import id.co.adira.partner.order.service.DraftSubmitPartialServiceV2;
import id.co.adira.partner.order.service.LogService;
import id.co.adira.partner.order.service.OrderService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import static id.co.adira.partner.order.constant.AppConst.*;
import static id.co.adira.partner.order.constant.StatusConst.ERROR_WHILE_HITTING_DELETE_DRAFT;
import static id.co.adira.partner.order.constant.StatusConst.ERROR_WHILE_HITTING_UPLOAD_ECM;

@RestController
@RequestMapping("api/order")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private DraftSubmitPartialService draftSubmitPartialService;

	@Autowired
	private DraftSubmitPartialServiceV2 draftSubmitPartialServiceV2;

	@Autowired
	private LogService logService;

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

	String data = "";
	DataResponse responseOrder;
	StandardResponseDTO stdResponse;

	@PostMapping(value = "/save-order", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Save Submit Order", nickname = "Save Submit Order")
	public ResponseEntity<Object> submitOrder(@RequestBody @Valid SubmitOrderReqDTO submitOrderReqDTO)
			throws JsonProcessingException {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		stdResponse = orderService.savePartialOrder(submitOrderReqDTO);
		stopWatch.stop();

		logService.insertToLogTable(submitOrderReqDTO, stdResponse, SUBMIT_DRAFT_FOR_ORDER.getMessage(),
				stopWatch.getTotalTimeMillis(), submitOrderReqDTO.getOrderNo());
		return new ResponseEntity<>(stdResponse, HttpStatus.OK);

	}

	@PostMapping("/save-customer")
	@ApiOperation(value = "Save Submit Customer", nickname = "Save Submit Customer")
	public ResponseEntity<Object> submitCustomer(@RequestBody @Valid SubmitCustomerReqDTO submitCustomerReqDTO)
			throws JsonProcessingException {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		stdResponse = orderService.savePartialCustomer(submitCustomerReqDTO);
		stopWatch.stop();

		logService.insertToLogTable(submitCustomerReqDTO, stdResponse, SUBMIT_DRAFT_FOR_CUSTOMER.getMessage(),
				stopWatch.getTotalTimeMillis(), submitCustomerReqDTO.getOrderNo());
		return new ResponseEntity<>(stdResponse, HttpStatus.OK);

	}

	@PostMapping("/save-domisili")
	@ApiOperation(value = "Save Submit Domisili", nickname = "Save Submit Domisili")
	public ResponseEntity<Object> submitDomisili(@RequestBody @Valid SubmitDomisiliReqDTO submitDomisiliReqDTO)
			throws JsonProcessingException {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		stdResponse = orderService.savePartialDomisili(submitDomisiliReqDTO);
		stopWatch.stop();

		logService.insertToLogTable(submitDomisiliReqDTO, stdResponse, SUBMIT_DRAFT_FOR_DOMISILI.getMessage(),
				stopWatch.getTotalTimeMillis(), submitDomisiliReqDTO.getOrderNo());
		return new ResponseEntity<>(stdResponse, HttpStatus.OK);

	}

	@PostMapping("/save-unit")
	@ApiOperation(value = "Save Submit Unit", nickname = "Save Submit Unit")
	public ResponseEntity<Object> submitUnit(@RequestBody @Valid SubmitUnitReqDTO submitUnitReqDTO)
			throws JsonProcessingException {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		data = orderService.savePartialUnit(submitUnitReqDTO);
		stopWatch.stop();

		logService.insertToLogTable(submitUnitReqDTO, data, SUBMIT_DRAFT_FOR_UNIT.getMessage(),
				stopWatch.getTotalTimeMillis(), submitUnitReqDTO.getOrderNo());
		return new ResponseEntity<>(data, HttpStatus.OK);

	}

	@PostMapping("/save-unit-v2")
	@ApiOperation(value = "Save Submit Unit V2", nickname = "Save Submit Unit V2")
	public ResponseEntity<Object> submitUnitV2(@RequestBody @Valid SubmitUnitV2ReqDTO submitUnitReqDTO)
			throws JsonProcessingException {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		stdResponse = orderService.savePartialUnitV2(submitUnitReqDTO);
		stopWatch.stop();

		logService.insertToLogTable(submitUnitReqDTO, stdResponse, SUBMIT_DRAFT_FOR_UNIT_V2.getMessage(),
				stopWatch.getTotalTimeMillis(), submitUnitReqDTO.getOrderNo());
		return new ResponseEntity<>(stdResponse, HttpStatus.OK);

	}

	@PostMapping("/generate/orderid")
	@ApiOperation(value = "Generate Order Id", nickname = "Generate Order Id")
	public ResponseEntity<Object> generateNewOrderId(@RequestBody @Valid GenerateOrderReq orderReq) {
		Map<String, Object> dataId = new HashMap<>();

		String strData = orderService.getOrderId(orderReq);
		dataId.put("orderNo", strData);

		return new ResponseEntity<>(dataId, HttpStatus.OK);
	}

	@PostMapping("/save-document")
	@ApiOperation(value = "Save Document", nickname = "Save Document")
	public ResponseEntity<Object> insertDocument(@RequestBody @Valid DocumentReqDTO documentRequest)
			throws JsonProcessingException {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		data = orderService.saveDocument(documentRequest);
		stopWatch.stop();
		logService.insertToLogTable(documentRequest, data, SUBMIT_FOR_DOCUMENT.getMessage(),
				stopWatch.getTotalTimeMillis(), documentRequest.getOrderNo());

		return new ResponseEntity<>(data, HttpStatus.OK);
	}

	@PostMapping("/uploadECM")
	@ApiOperation(value = "Upload ECM", nickname = "Upload ECM")
	public ResponseEntity<Object> uploadECM(@RequestParam("file") MultipartFile file,
			@RequestParam("orderNo") String orderNo, @RequestParam("docType") String docType,
			@RequestParam("idNo") String idNo) {

		Map<String, Object> response = new HashMap<>();

		try {
			response = orderService.uploadEcm(file, orderNo, docType, idNo);
		} catch (Exception e) {
			LOGGER.info(ERROR_WHILE_HITTING_UPLOAD_ECM.getMessage() + e.getMessage());
		}

		return new ResponseEntity<>(response.get("body"), (HttpStatus) response.get("status"));

	}

	@GetMapping("/getDraftSubmitPartialOrder")
	@ApiOperation(value = "Get Draft Submit Partial Order", nickname = "Get Draft Submit Partial Order")
	public ResponseEntity<Object> getSubmitPartialCustomer(@RequestParam String pic) {
		DataResponse getDraftResponse = draftSubmitPartialService.getDraftSubmitPartialOrderByPic(pic);
		return new ResponseEntity<>(getDraftResponse, getDraftResponse.getHttpStatus());
	}

	@GetMapping("/getDraftDetailSubmitPartial")
	@ApiOperation(value = "Get Draft Detail Submit Partial", nickname = "Get Draft Detail Submit Partial")
	// @CrossOrigin
	public ResponseEntity<Object> getDraftSubmitPartialDetail(@RequestParam String orderNo) {
		DataResponse getDraftDetailResponse = draftSubmitPartialService.getDraftSubmitPartialDetailByOrderNo(orderNo);
		return new ResponseEntity<>(getDraftDetailResponse, getDraftDetailResponse.getHttpStatus());
	}

	@PostMapping("/deleteDraft/{orderNo}")
	@ApiOperation(value = "Delete Draft", nickname = "Delete Draft")
	public ResponseEntity<Object> deleteDraft(@PathVariable(value = "orderNo") String orderNo) {

		DataResponse response = new DataResponse();
		try {
			response = draftSubmitPartialService.deleteDraft(orderNo);
		} catch (Exception e) {
			LOGGER.info(ERROR_WHILE_HITTING_DELETE_DRAFT.getMessage() + e.getMessage());
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("/ocrIdp")
	@ApiOperation(value = "Get OCR IDP", nickname = "Get OCR IDP")
	public ResponseEntity<Object> getOcr(@RequestParam("file") MultipartFile file, @RequestParam("flag") String flag) {

		DataResponse response = new DataResponse();
		try {
			response = orderService.getOCR(file, flag);
		} catch (Exception e) {
			LOGGER.info(ERROR_WHILE_HITTING_DELETE_DRAFT.getMessage() + e.getMessage());
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("/ocrIdpWeb")
	@ApiOperation(value = "Get OCR IDP WEB", nickname = "Get OCR IDP WEB")
	public ResponseEntity<Object> getOcrWeb(@RequestBody IdpWebReqDTO idpWebReqDTO) {

		var response = new DataResponse();
		try {
			response = orderService.getWebOCR(idpWebReqDTO);
		} catch (Exception e) {
			LOGGER.info(ERROR_WHILE_HITTING_DELETE_DRAFT.getMessage() + e.getMessage());
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/v2/getDraftSubmitPartialOrder")
	public ResponseEntity<Object> getDraftSubmitPartialOrder(@RequestParam Integer numberOfPrevDay, @RequestParam String pic,
			@RequestParam int sort) throws ParseException {
		DataResponse dataResponse = draftSubmitPartialServiceV2.getDraftSubmitPartialOrderByPic(numberOfPrevDay, pic,
				sort);
		return new ResponseEntity<>(dataResponse, dataResponse.getHttpStatus());
	}
}
