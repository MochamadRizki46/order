package id.co.adira.partner.order.controller;

import static id.co.adira.partner.order.constant.AppConst.SUBMIT_ORDER_TO_LE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import id.co.adira.partner.order.dto.SendOrderToLeadReqDTO;
import id.co.adira.partner.order.dto.SendOrderToLeadV2ReqDTO;
import id.co.adira.partner.order.dto.StandardResponseDTO;
import id.co.adira.partner.order.dto.kafka.ApplicationDistributedDTO;
import id.co.adira.partner.order.service.LogService;
import id.co.adira.partner.order.service.OrderService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("api/submit")
public class OrderSubmitController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private LogService logService;

	String data = "";
	
	@PostMapping("/submitorder")
	@ApiOperation(value="Submit Order to Lead", nickname = "Submit Order to Lead")
	//@CrossOrigin
	public ResponseEntity<Object> submitOrderToLead(@RequestBody SendOrderToLeadReqDTO submitOrderReq) {
		try {
			data = orderService.submitOrderToLead(submitOrderReq);
		} catch (Exception e) {
			return new ResponseEntity<>(e.toString(), HttpStatus.NOT_ACCEPTABLE);
		}
			return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@PostMapping("/submitorderV2")
	@ApiOperation(value="Submit Order to Lead", nickname = "Submit Order to Lead")
	//@CrossOrigin
	public ResponseEntity<Object> submitOrderToLeadV2(@RequestBody SendOrderToLeadV2ReqDTO submitOrderReq) throws JsonProcessingException {
		StandardResponseDTO stdResponse;
		StopWatch stopWatch = new StopWatch();
		try {
			stopWatch.start();
			stdResponse = orderService.submitOrdertoLeadV2(submitOrderReq);
			stopWatch.stop();

		} catch (Exception e) {
			return new ResponseEntity<>(e.toString(), HttpStatus.NOT_ACCEPTABLE);
		}
		logService.insertToLogTable(submitOrderReq, stdResponse, SUBMIT_ORDER_TO_LE.getMessage(), stopWatch.getTotalTimeMillis(), submitOrderReq.getCredOrderNo());
		return new ResponseEntity<>(stdResponse, HttpStatus.OK);
	}
	
//	@PostMapping("/test")
//	@ApiOperation(value="Submit Order to Lead", nickname = "Submit Order to Lead")
//	//@CrossOrigin
//	public ResponseEntity<Object> test(@RequestBody ApplicationDistributedDTO request) throws JsonProcessingException {
//		StopWatch stopWatch = new StopWatch();
//		try {
//			stopWatch.start();
//			orderService.distributedApplication(request);
//			stopWatch.stop();
//
//		} catch (Exception e) {
//			return new ResponseEntity<>(e.toString(), HttpStatus.NOT_ACCEPTABLE);
//		}
//	
//		return new ResponseEntity<>(data, HttpStatus.OK);
//	}
	
}
