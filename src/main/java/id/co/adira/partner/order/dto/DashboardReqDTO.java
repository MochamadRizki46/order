package id.co.adira.partner.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import id.co.adira.partner.order.utils.Generated;

/**
 * @author Cris
 *
 */

@Generated
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class DashboardReqDTO {
	
	@ApiModelProperty(position = 1)
	@NotEmpty
	@Email
	String userLogin;
	@ApiModelProperty(position = 2)
	String paramDate;


}
