/**
 * 
 */
package id.co.adira.partner.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author Vionza
 *
 */

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class DocumentReqDTO {

	@ApiModelProperty(position = 1)
	@NotEmpty
	@Size(min = 20, max = 20, message = "Invalid order number. Maximum 20 characters!")
	String orderNo;
	@ApiModelProperty(position = 2)
	@NotEmpty
	String docId;
	@ApiModelProperty(position = 3)
	String docPath;
	@ApiModelProperty(position = 4)
	String docType;
	@ApiModelProperty(position = 5)
	@Email
	String userLogin;

}
