package id.co.adira.partner.order.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DpOrPmtRequestDTOV2 {
    @JsonProperty("dlc")
    private String dlc;
    @JsonProperty("fin_type")
    private String finType;
    @JsonProperty("program")
    private String program;
    @JsonProperty("obj_code")
    private String objectCode;
    @JsonProperty("obj_brand_code")
    private String objectBrandCode;
    @JsonProperty("obj_type_code")
    private String objectTypeCode;
    @JsonProperty("obj_model_code")
    private String objectModelCode;
    @JsonProperty("obj_mfg_year")
    private BigDecimal objectManufacturerYear;
    @JsonProperty("otr")
    private BigDecimal otr;
    @JsonProperty("tenor")
    private Integer tenor;
    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("insr_type")
    private Integer insrType;
    @JsonProperty("adm_insr_fin")
    private Integer adminInsrFin;
    @JsonProperty("acuan_hitung")
    private Integer acuanHitung;
    @JsonProperty("nominal_hitung")
    private BigDecimal nominalHitung;

}
