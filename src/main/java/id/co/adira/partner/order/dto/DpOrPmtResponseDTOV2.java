package id.co.adira.partner.order.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DpOrPmtResponseDTOV2 {
    @JsonProperty("dlc")
    private String dlc;

    @JsonProperty("otr")
    private BigDecimal otr;

    @JsonProperty("tenor")
    private Integer tenor;

    @JsonProperty("dp_amount")
    private BigDecimal dpAmount;

    @JsonProperty("installment_amount")
    private BigDecimal installmentAmount;

    @JsonProperty("biaya_administrasi_format")
    private String biayaAdministrasiFormat;

    @JsonProperty("biaya_administrasi_credit")
    private BigDecimal biayaAdministrasiCredit;

    @JsonProperty("biaya_administrasi_cash")
    private BigDecimal biayaAdministrasiCash;

    @JsonProperty("provisi_format")
    private String provinsiFormat;

    @JsonProperty("provisi_credit")
    private BigDecimal provinsiCredit;

    @JsonProperty("provisi_cash")
    private BigDecimal provinsiCash;

    @JsonProperty("effective_rate_format")
    private String effectiveRateFormat;

    @JsonProperty("effective_rate")
    private BigDecimal effectiveRate;

    @JsonProperty("asuransi_basic_format")
    private String asuransiBasicFormat;

    @JsonProperty("asuransi_basic_credit")
    private BigDecimal asuransiBasicCredit;

    @JsonProperty("asuransi_basic_cash")
    private BigDecimal asuransiBasicCash;

    @JsonProperty("asuransi_pa_format")
    private String asuransiPaFormat;

    @JsonProperty("asuransi_pa_credit")
    private BigDecimal asuransiPaCredit;

    @JsonProperty("asuransi_pa_cash")
    private BigDecimal asuransiPaCash;

    @JsonProperty("asuransi_life_format")
    private String asuransiLifeFormat;

    @JsonProperty("asuransi_life_credit")
    private BigDecimal asuransiLifeCredit;

    @JsonProperty("asuransi_life_cash")
    private BigDecimal asuransiLifeCash;

    @JsonProperty("others_fee_format")
    private String otherFeeFormat;

    @JsonProperty("others_fee_credit")
    private BigDecimal otherFeeCredit;

    @JsonProperty("others_fee_cash")
    private BigDecimal othersFeeCash;

}
