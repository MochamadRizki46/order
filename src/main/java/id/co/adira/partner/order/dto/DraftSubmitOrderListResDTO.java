package id.co.adira.partner.order.dto;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Generated
@Getter
@Setter
public class DraftSubmitOrderListResDTO {
    private List<DraftSubmitOrderResDTO> draftSubmitOrders;
}
