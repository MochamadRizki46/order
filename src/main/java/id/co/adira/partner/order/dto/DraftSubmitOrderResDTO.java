package id.co.adira.partner.order.dto;



import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DraftSubmitOrderResDTO {

	@ApiModelProperty(position = 1)
	String orderNo;
	@ApiModelProperty(position = 2)
    String custName;
	@ApiModelProperty(position = 3)
    String orderDate;
	@ApiModelProperty(position = 4)
    String objectDesc;
	@ApiModelProperty(position = 5)
    String objectModelDesc;
	@ApiModelProperty(position = 6)
    String status;
}
