/**
 * 
 */
package id.co.adira.partner.order.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author 10999943
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class GenerateOrderReq {
	@NotEmpty(message = "Role id must be filled!")
	@Size(min = 3, max = 3, message = "Invalid role id. Maximum 3 characters!")
	String roleId;
}
