/**
 * 
 */
package id.co.adira.partner.order.dto;

import lombok.Data;
import lombok.Generated;

import javax.validation.constraints.NotNull;

/**
 * @author Vionza
 *
 */
@Generated
@Data
public class IdpWebReqDTO {

	String file;
	@NotNull
	String flag;
}
