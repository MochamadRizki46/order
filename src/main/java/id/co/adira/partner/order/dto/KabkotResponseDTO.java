package id.co.adira.partner.order.dto;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.adira.partner.order.dto.revampms2.KabkotResDTO;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class KabkotResponseDTO {
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("message")
	private String message;
	
	@JsonProperty("data")
	private List<KabkotResDTO> data;
}