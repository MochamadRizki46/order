/**
 * 
 */
package id.co.adira.partner.order.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.adira.partner.order.utils.Generated;
import lombok.Data;

/**
 * @author Vionza
 *
 */

@Data
@Generated
public class OcrKtpResponsesDTO {

	@JsonProperty("commonResponse")
	private CommonResponse commonResponse;
	@JsonProperty("ktpData")
	private KtpData ktpData;
	
	@Generated
	static class CommonResponse {
		@JsonProperty("responseCode")
		private String responseCode;
		@JsonProperty("type")
		private String type;
		@JsonProperty("message")
		private String message;

	}
	
	@Generated
	static class KtpData {
		@JsonProperty("provinsi")
		private Detail provinsi;
		@JsonProperty("kota")
		private Detail kota;
		@JsonProperty("nik")
		private Detail nik;
		@JsonProperty("nama")
		private Detail nama;
		@JsonProperty("tempatLahir")
		private Detail tempatLahir;
		@JsonProperty("tanggalLahir")
		private Detail tanggalLahir;
		@JsonProperty("jenisKelamin")
		private Detail jenisKelamin;
		@JsonProperty("alamat")
		private Detail alamat;
		@JsonProperty("rt")
		private Detail rt;
		@JsonProperty("rw")
		private Detail rw;
		@JsonProperty("kelDesa")
		private Detail kelDesa;
		@JsonProperty("kecamatan")
		private Detail kecamatan;
		@JsonProperty("agama")
		private Detail agama;
		@JsonProperty("statusPerkawinan")
		private Detail statusPerkawinan;
		@JsonProperty("pekerjaan")
		private Detail pekerjaan;
		@JsonProperty("tanggalPembuatan")
		private Detail tanggalPembuatan;
		@JsonProperty("rawData")
		private String[] rawData;
	}

	static class Detail {
		@JsonProperty("value")
		private String value;
		@JsonProperty("confidenceLevel")
		private String confidenceLevel;
	}

}
