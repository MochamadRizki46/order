/**
 * 
 */
package id.co.adira.partner.order.dto;



import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.adira.partner.order.utils.Generated;
import lombok.Data;

/**
 * @author Vionza
 *
 */

@Data
@Generated
public class OcrNpwpResponsesDTO {

	@JsonProperty("commonResponse")
	private CommonResponse commonResponse;
	@JsonProperty("npwpData")
	private NpwpData npwpData;

	@Generated
	static class CommonResponse {
		@JsonProperty("responseCode")
		private String responseCode;
		@JsonProperty("type")
		private String type;
		@JsonProperty("message")
		private String message;

	}

	@Generated
	static class NpwpData {
		@JsonProperty("noNpwp")
		private Detail noNpwp;
		@JsonProperty("nama")
		private Detail nama;
		@JsonProperty("nik")
		private Detail nik;
		@JsonProperty("alamat")
		private Detail alamat;
		@JsonProperty("terdaftar")
		private Detail terdaftar;
		@JsonProperty("rawData")
		private String[] rawData;

	}

	static class Detail {
		@JsonProperty("value")
		private String value;
		@JsonProperty("confidenceLevel")
		private String confidenceLevel;

	}
}
