/**
 * 
 */
package id.co.adira.partner.order.dto;

import java.util.Date;

import id.co.adira.partner.order.utils.Generated;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 10999943
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class SendOrderToLeadReqDTO {
	@ApiModelProperty(position = 1)
	String credOrderNo;
	@ApiModelProperty(position = 2)
	String credIdNo;
	@ApiModelProperty(position = 3)
	String credName;
	@ApiModelProperty(position = 4)
	String credBirthPlace;
	@ApiModelProperty(position = 5)
	Date   credBirthDate;
	@ApiModelProperty(position = 6)
	String credCompPicName;
	@ApiModelProperty(position = 7)
	String credSex;
	@ApiModelProperty(position = 8)
	String credKtpAddr;
	@ApiModelProperty(position = 9)
	String credRtKtp;
	@ApiModelProperty(position = 10)
	String credRwKtp;
	@ApiModelProperty(position = 11)
	String credKelurahanId;
	@ApiModelProperty(position = 12)
	String credKecamatanId;
	@ApiModelProperty(position = 13)
	String credKabKotaId;
	@ApiModelProperty(position = 14)
	String credProvinceId;
	@ApiModelProperty(position = 15)
	String credZipCode;
	@ApiModelProperty(position = 16)
	Date   credNpwpDate;
	@ApiModelProperty(position = 17)
	String credMarriageStatus;
	@ApiModelProperty(position = 18)
	Date   credIdDate;
	@ApiModelProperty(position = 19)
	String credSidName;
	@ApiModelProperty(position = 20)
	String credFlagAddr;
	@ApiModelProperty(position = 21)
	String credSurveyAddr;
	@ApiModelProperty(position = 22)
	String credSvyRt;
	@ApiModelProperty(position = 23)
	String credSvyRw;
	@ApiModelProperty(position = 24)
	String credSvyKecamatanId;
	@ApiModelProperty(position = 25)
	String credSvyKelurahanId;
	@ApiModelProperty(position = 26)
	String credSvyZipCode;
	@ApiModelProperty(position = 27)
	String credSvyKabKotaId;
	@ApiModelProperty(position = 28)
	String credSvyProvinceId;
	@ApiModelProperty(position = 29)
	String credTelpNo;
	@ApiModelProperty(position = 30)
	String credMobilePhNo;
	@ApiModelProperty(position = 31)
	String credOccupation;
	@ApiModelProperty(position = 32)
	String credEconomySector;
	@ApiModelProperty(position = 33)
	String credJenisUsaha;
	@ApiModelProperty(position = 34)
	String credObjtCode;
	@ApiModelProperty(position = 35)
	String credObbrCode;
	@ApiModelProperty(position = 36)
	String credObtyCode;
	@ApiModelProperty(position = 37)
	String credObmoCode;
	@ApiModelProperty(position = 38)
	String credModelDetail;
	@ApiModelProperty(position = 39)
	String credObjtYear;
	@ApiModelProperty(position = 40)
	String credNameOnBpkb;
	@ApiModelProperty(position = 41)
	String credFotoStnk;
	@ApiModelProperty(position = 42)
	String credObjtFotoSisi4;
	@ApiModelProperty(position = 43)
	String credVoucherCode;
	@ApiModelProperty(position = 44)
	int    credFinType;
	@ApiModelProperty(position = 45)
	String credTypeBarang;
	@ApiModelProperty(position = 46)
	String credFinProduct;
	@ApiModelProperty(position = 47)
	String credInsuranceType;
	@ApiModelProperty(position = 48)
	int    credObjtPrice;
	@ApiModelProperty(position = 49)
	int    credTop;
	@ApiModelProperty(position = 50)
	int    credNetDp;
	@ApiModelProperty(position = 51)
	int    credInstAmt;
	@ApiModelProperty(position = 52)
	int    credPrincipalAmt;
	@ApiModelProperty(position = 53)
	String credSvyType;
	@ApiModelProperty(position = 54)
	String credMktNik;
	@ApiModelProperty(position = 55)
	Date   credDateSurvey;
	@ApiModelProperty(position = 56)
	String credTimeSurvey;
	@ApiModelProperty(position = 57)
	String credTimeMinuteSurvey;
	@ApiModelProperty(position = 58)
	String credDealNotes;
	@ApiModelProperty(position = 59)
	String credMaidenName;
	@ApiModelProperty(position = 60)
	String credBrId = null;
	@ApiModelProperty(position = 61)
	String credApplSourceCode;
	@ApiModelProperty(position = 62)
	String createdBy;
	@ApiModelProperty(position = 63)
	String credApplFlagSource;
	@ApiModelProperty(position = 64)
	String credCustType;
	@ApiModelProperty(position = 65)
	String credFotoKtpNpwp;
	@ApiModelProperty(position = 66)
	String credObjtFotoSisi42;
	@ApiModelProperty(position = 67)
	String credObjtFotoSisi43;
	@ApiModelProperty(position = 68)
	String credObjtFotoSisi44;
	@ApiModelProperty(position = 69)
	String credEmail;
	@ApiModelProperty(position = 70)
	String credFlagWa;
	@ApiModelProperty(position = 71)
	String credWaNo;
	@ApiModelProperty(position = 72)
	String credCollType;
	@ApiModelProperty(position = 73)
	String credColor;
	@ApiModelProperty(position = 74)
	String credDealCode;
	@ApiModelProperty(position = 75)
	int credNoOfYearWork;
	@ApiModelProperty(position = 76)
	int credIncome;
	@ApiModelProperty(position = 77)
	double credEffRate;
	@ApiModelProperty(position = 78)
	String credAdvArr;
	@ApiModelProperty(position = 79)
	String credApplNoPayung;
	@ApiModelProperty(position = 80)
	String credApplNoUnit;
	@ApiModelProperty(position = 81)
	String credReligion;
	@ApiModelProperty(position = 82)
	String credMobilePhNo2;
	@ApiModelProperty(position = 83)
	Date   credEstablishDate;
	@ApiModelProperty(position = 84)
	String credObjtGroup;
	@ApiModelProperty(position = 85)
	int    credDpBranch;
	@ApiModelProperty(position = 86)
	String credProductType;
	@ApiModelProperty(position = 87)
	String credBussActivity;
	@ApiModelProperty(position = 88)
	String credBussActivityType;
	@ApiModelProperty(position = 89)
	String credSoJob;
	@ApiModelProperty(position = 90)
	String credSoNik;
	@ApiModelProperty(position = 91)
	String credApi1;
	@ApiModelProperty(position = 92)
	String credApi2;
	@ApiModelProperty(position = 93)
	String credOrderDate;
	@ApiModelProperty(position = 94)
	String credPromoCode;
	@ApiModelProperty(position = 95)
	String credChannelType;
	@ApiModelProperty(position = 96)
	String credChannelCode;
	@ApiModelProperty(position = 97)
	Date   credobjtFotoDocSTNKDate;
	@ApiModelProperty(position = 98)
	Date   credobjtFotoDocKTPDate;

}
