package id.co.adira.partner.order.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated
public class StandardResponseDTO {

	@Setter
	@Getter
	@JsonProperty("Status")
	@ApiModelProperty(position = 1)
	private int status;
	
	@Setter
	@Getter
	@JsonProperty("Message")
	@ApiModelProperty(position = 2)
	private String message;
	
	public StandardResponseDTO() {	
	}
	
	public StandardResponseDTO(int status, String message) {
        this.status = status;
        this.message = message;
    }
}
