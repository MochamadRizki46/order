/**
 * 
 */
package id.co.adira.partner.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author 10999943
 *
 */

@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class SubmitCustomerReqDTO {
	@ApiModelProperty(position = 1)
	@NotEmpty
	@Size(min = 20, max = 20, message = "Invalid order number. Must 20 characters!")
	String orderNo;
	@ApiModelProperty(position = 2)
	@NotEmpty(message = "Id number must be filled!")
	@Size(min = 15, max = 16, message = "Invalid ID Number. Must have 15 or 16 characters!")
	String idNo;
	@ApiModelProperty(position = 3)
	@NotEmpty(message = "Customer name must be filled!")
	@Size(max = 100, message = "Maximum length size is 100!")
	String customerName;
	@ApiModelProperty(position = 4)
	@Size(max = 3, message = "Maximum length size is 3!")
	@NotEmpty(message = "Customer type must be filled!")
	String custType;
	@ApiModelProperty(position = 5)
	@Size(max = 100, message = "Maximum length size is 100!")
	String pic;
	@ApiModelProperty(position = 6)
	@Size(max = 15, message = "Maximum length size is 15!")
	String handphoneNo;
	@ApiModelProperty(position = 7)
	@Size(max = 15, message = "Maximum length size is 15!")
	String waNo;
	@ApiModelProperty(position = 8)
	Integer flagWa;
	@ApiModelProperty(position = 9)
	@Size(max = 100, message = "Maximum length size is 100!")
	String idName;
	@ApiModelProperty(position = 10)
	Date birthDate;
	@ApiModelProperty(position = 11)
	@Size(max = 50, message = "Maximum length size is 50!")
	String birthPlace;
	@ApiModelProperty(position = 12)
	@Size(max = 150, message = "Maximum length size is 150!")
	String idAddress;
	@ApiModelProperty(position = 13)
	@Size(max = 6, message = "Maximum length size is 6!")
	String idRtrw;
	@ApiModelProperty(position = 14)
	@Size(max = 3, message = "Maximum length size is 3!")
	String idType;
	@ApiModelProperty(position = 15)
	@Size(max = 5, message = "Maximum length size is 5!")
	String idKelurahan;
	@ApiModelProperty(position = 16)
	@Size(max = 50, message = "Maximum length size is 50!")
	String idKelurahanDesc;
	@ApiModelProperty(position = 17)
	@Size(max = 5, message = "Maximum length size is 5!")
	String idKecamatan;
	@ApiModelProperty(position = 18)
	@Size(max = 50, message = "Maximum length size is 50!")
	String idKecamatanDesc;
	@ApiModelProperty(position = 19)
	@Size(max = 4, message = "Maximum length size is 4!")
	String idKabkot;
	@ApiModelProperty(position = 20)
	@Size(max = 50, message = "Maximum length size is 50!")
	String idKabkotDesc;
	@ApiModelProperty(position = 21)
	@Size(max = 2, message = "Maximum length size is 2!")
	String idProvinsi;
	@ApiModelProperty(position = 22)
	@Size(max = 50, message = "Maximum length size is 50!")
	String idProvinsiDesc;
	@ApiModelProperty(position = 23)
	@Size(max = 5, message = "Maximum length size is 5!")
	String zipcode;
	@ApiModelProperty(position = 24)
	String finType;
	@ApiModelProperty(position = 25)
	String jsonIdp;
	@ApiModelProperty(position = 26)
	Date createdDate;
	@ApiModelProperty(position = 27)
	String createdBy;
	@ApiModelProperty(position = 28)
	Date modifiedDate;
	@ApiModelProperty(position = 29)
	String modifiedBy;
	@ApiModelProperty(position = 30)
	Integer active;
}
