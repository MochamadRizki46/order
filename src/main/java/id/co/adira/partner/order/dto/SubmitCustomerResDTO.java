package id.co.adira.partner.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SubmitCustomerResDTO {
    @ApiModelProperty(position = 1)
    String orderNo;
    @ApiModelProperty(position = 2)
    String idNo;
    @ApiModelProperty(position = 3)
    String customerName;
    @ApiModelProperty(position = 4)
    String custType;
    @ApiModelProperty(position = 5)
    String handphoneNo;
    @ApiModelProperty(position = 6)
    String waNo;
    @ApiModelProperty(position = 7)
    Integer flagWa;
    @ApiModelProperty(position = 8)
    String idName;
    @ApiModelProperty(position = 9)
    Date birthDate;
    @ApiModelProperty(position = 10)
    String birthPlace;
    @ApiModelProperty(position = 11)
    String idAddress;
    @ApiModelProperty(position = 12)
    String idRtrw;
    @ApiModelProperty(position = 13)
    String idType;
    @ApiModelProperty(position = 14)
    String idKelurahan;
    @ApiModelProperty(position = 15)
    String idKelurahanDesc;
    @ApiModelProperty(position = 16)
    String idKecamatan;
    @ApiModelProperty(position = 17)
    String idKecamatanDesc;
    @ApiModelProperty(position = 18)
    String idKabkot;
    @ApiModelProperty(position = 19)
    String idKabkotDesc;
    @ApiModelProperty(position = 20)
    String idProvinsi;
    @ApiModelProperty(position = 21)
    String idProvinsiDesc;
    @ApiModelProperty(position = 22)
    String zipcode;
    @ApiModelProperty(position = 23)
    String finType;
    @ApiModelProperty(position = 24)
    String jsonIdp;
    @ApiModelProperty(position = 25)
    Date createdDate;
    @ApiModelProperty(position = 26)
    String createdBy;
    @ApiModelProperty(position = 27)
    Date modifiedDate;
    @ApiModelProperty(position = 28)
    String modifiedBy;
    @ApiModelProperty(position = 29)
    Integer active;
    @ApiModelProperty(position = 30)
    String pic;
}
