package id.co.adira.partner.order.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import id.co.adira.partner.order.utils.Generated;

@Generated
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class SubmitDomisiliReqDTO {
	@ApiModelProperty(position = 1)
	@NotEmpty(message = "Order number must be filled!")
	@Size(min = 20, max = 20, message = "Invalid order number. Must have 20 characters!")
	String orderNo;
	@ApiModelProperty(position = 2)
	Integer flagAddressId;
	@ApiModelProperty(position = 3)
	@Size(max = 150, message = "Maximum character for address domisili is 150 characters!")
	String addressDomisili;
	@ApiModelProperty(position = 4)
	Date surveyDate;
	@ApiModelProperty(position = 5)
	String surveyTime;
	@ApiModelProperty(position = 6)
	@Size(max = 10, message = "Maximum character of rtrw is 10 characters!")
	String rtrw;
	@ApiModelProperty(position = 7)
	@Size(max = 5, message = "Maximum character of kelurahan is 5 characters!")
	String kelurahan;
	@ApiModelProperty(position = 8)
	@Size(max = 50, message = "Maximum character of kelurahan description is 50 characters!")
	String kelurahanDesc;
	@ApiModelProperty(position = 9)
	@Size(max = 5, message = "Maximum character of kecamatan is 5 characters!")
	String kecamatan;
	@ApiModelProperty(position = 10)
	@Size(max = 50, message = "Maximum character of kecamatan description is 50 characters!")
	String kecamatanDesc;
	@ApiModelProperty(position = 11)
	@Size(max = 4, message = "Maximum character of kabkot is 4 characters!")
	String kabkot;
	@ApiModelProperty(position = 12)
	@Size(max = 50, message = "Maximum character of kabkot description is 50 characters!")
	String kabkotDesc;
	@ApiModelProperty(position = 13)
	@Size(max = 2, message = "Maximum character of provinsi is 2 characters!")
	String provinsi;
	@ApiModelProperty(position = 14)
	@Size(max = 50, message = "Maximum character of provinsi description is 50 characters!")
	String provinsiDesc;
	@ApiModelProperty(position = 15)
	@Size(max = 5, message = "Invalid zipcode. Must have 5 characters!")
	String zipcode;
	@ApiModelProperty(position = 16)
	Date createdDate;
	@ApiModelProperty(position = 17)
	String createdBy;
	@ApiModelProperty(position = 18)
	Date modifiedDate;
	@ApiModelProperty(position = 19)
	String modifiedBy;
	@ApiModelProperty(position = 20)
	Integer active;
	@ApiModelProperty(position = 21)
	Integer flagKelurahan;
	@ApiModelProperty(position = 22)
	@Size(max = 200, message = "Maximum character of provinsi description is 200 characters!")
	String salesNotes;
}