package id.co.adira.partner.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

import id.co.adira.partner.order.utils.Generated;

import java.util.Date;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SubmitDomisiliResDTO {
    @ApiModelProperty(position = 1)
    String orderNo;
    @ApiModelProperty(position = 2)
    Integer flagAddressId;
    @ApiModelProperty(position = 3)
    String addressDomisili;
    @ApiModelProperty(position = 4)
    Date surveyDate;
    @ApiModelProperty(position = 5)
    String surveyTime;
    @ApiModelProperty(position = 6)
    String rtrw;
    @ApiModelProperty(position = 7)
    String kelurahan;
    @ApiModelProperty(position = 8)
    String kelurahanDesc;
    @ApiModelProperty(position = 9)
    String kecamatan;
    @ApiModelProperty(position = 10)
    String kecamatanDesc;
    @ApiModelProperty(position = 11)
    String kabkot;
    @ApiModelProperty(position = 12)
    String kabkotDesc;
    @ApiModelProperty(position = 13)
    String provinsi;
    @ApiModelProperty(position = 14)
    String provinsiDesc;
    @ApiModelProperty(position = 15)
    String zipcode;
    @ApiModelProperty(position = 16)
    String createdBy;
    @ApiModelProperty(position = 17)
    String modifiedBy;
    @ApiModelProperty(position = 18)
    Integer active;
    @ApiModelProperty(position = 19)
    Integer flagKelurahan;
    @ApiModelProperty(position = 20)
    String salesNotes;
}
