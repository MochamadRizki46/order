package id.co.adira.partner.order.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import id.co.adira.partner.order.utils.Generated;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;
import java.util.Date;

@Generated
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class SubmitOrderReqDTO {
	@ApiModelProperty(position = 1)
	@NotEmpty(message = "Order number must be filled!")
	@Size(min = 20, max = 20, message = "Invalid order number. Maximum 20 characters!")
	String orderNo;
	@ApiModelProperty(position = 2)
	@Size(max = 20, message = "Maximum of appl no payung size is 20!")
	String applNoPayung;
	@ApiModelProperty(position = 3)
	@Size(max = 20, message = "Maximum of appl no unit size is 20!")
	String applNoUnit;
	@ApiModelProperty(position = 4)
	@JsonFormat(timezone = "GMT+7")
	Date orderDate;
	@ApiModelProperty(position = 5)
	@Size(min = 3, max = 3, message = "Partner type field must have 3 characters!")
	String partnerType;
	@ApiModelProperty(position = 6)
	@Email(message = "Pic field must be a valid email!")
	String pic;
	@ApiModelProperty(position = 7)
	Integer idx;
	@ApiModelProperty(position = 8)
	@NotEmpty(message = "Status must be filled!")
	String status;
	@ApiModelProperty(position = 9)
	Date createdDate;
	@ApiModelProperty(position = 10)
	String createdBy;
	@ApiModelProperty(position = 11)
	Date modifiedDate;
	@ApiModelProperty(position = 12)
	String modifiedBy;
	@ApiModelProperty(position = 13)
	Integer active;

}
