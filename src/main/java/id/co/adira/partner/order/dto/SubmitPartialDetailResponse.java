package id.co.adira.partner.order.dto;

import id.co.adira.partner.order.utils.Generated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SubmitPartialDetailResponse {
    SubmitCustomerResDTO draftDataKonsumen;
    SubmitDomisiliResDTO draftDataDomisili;
    SubmitUnitResDTO draftDataUnit;
    String status;
}
