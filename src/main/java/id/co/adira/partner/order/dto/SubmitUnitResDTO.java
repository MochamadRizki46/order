package id.co.adira.partner.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

import id.co.adira.partner.order.utils.Generated;

@Generated
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubmitUnitResDTO {

	@ApiModelProperty(position = 1)
	String orderNo;
	@ApiModelProperty(position = 2)
	String objectBrand;
	@ApiModelProperty(position = 3)
	String objectBrandDesc;
	@ApiModelProperty(position = 4)
	String objectType;
	@ApiModelProperty(position = 5)
	String objectTypeDesc;
	@ApiModelProperty(position = 6)
	String objectModel;
	@ApiModelProperty(position = 7)
	String objectModelDesc;
	@ApiModelProperty(position = 8)
	String object;
	@ApiModelProperty(position = 9)
	String objectDesc;
	@ApiModelProperty(position = 10)
	String objectYear;
	@ApiModelProperty(position = 11)
	String tipeJaminan;
	@ApiModelProperty(position = 12)
	String jenisAsuransi;
	@ApiModelProperty(position = 13)
	String kotaDomisili;
	@ApiModelProperty(position = 14)
	BigDecimal objectPrice;
	@ApiModelProperty(position = 15)
	Integer tenor;
	@ApiModelProperty(position = 16)
	BigDecimal rate;
	@ApiModelProperty(position = 17)
	BigDecimal dp;
	@ApiModelProperty(position = 18)
	BigDecimal installmentAmt;
	@ApiModelProperty(position = 19)
	String promo;
	@ApiModelProperty(position = 20)
	String modelDetail;
	@ApiModelProperty(position = 21)
	String notes;
	@ApiModelProperty(position = 22)
	String createdBy;
	@ApiModelProperty(position = 23)
	String modifiedBy;
	@ApiModelProperty(position = 24)
	Integer active;
	@ApiModelProperty(position = 25)
	Integer flagCredsim;
	@ApiModelProperty(position = 26)
	String installmentTypeId;
	@ApiModelProperty(position = 27)
	String installmentTypeDesc;
	@ApiModelProperty(position = 28)
	String programId;
	@ApiModelProperty(position = 29)
	String programDesc;
	@ApiModelProperty(position = 30)
	String paymentTypeId;
	@ApiModelProperty(position = 31)
	String paymentTypeDesc;
	@ApiModelProperty(position = 32)
	Integer flagInsrAdmin;
	@ApiModelProperty(position = 33)
	Integer acuanHitung;
	@ApiModelProperty(position = 34)
	String kotaDomisiliId;
	@ApiModelProperty(position = 35)
	String modelDetailId;
	@ApiModelProperty(position = 36)
	Integer insrTloCoverage;
	@ApiModelProperty(position = 37)
	Integer insrCompreCoverage;
	@ApiModelProperty(position = 38)
	String insrTypeId;
}
