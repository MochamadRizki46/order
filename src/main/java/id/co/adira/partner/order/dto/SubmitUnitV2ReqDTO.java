/**
 * 
 */
package id.co.adira.partner.order.dto;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import id.co.adira.partner.order.utils.Generated;

@Generated
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class SubmitUnitV2ReqDTO {
	@ApiModelProperty(position = 1)
	@NotEmpty(message = "Order number must be filled!")
	@Size(min = 20, max = 20, message = "Invalid order number. Must have 20 characters!")
	String orderNo;
	@ApiModelProperty(position = 2)
	@Size(min = 3, max = 6, message = "Object brand invalid. Must have 3-6 characters!")
	String objectBrand;
	@ApiModelProperty(position = 3)
	@Size(max = 50, message = "Maximum object brand description size id 50!")
	String objectBrandDesc;
	@ApiModelProperty(position = 4)
	@Size(min = 3, max = 6, message = "Invalid object type. Must have 3-6 characters!")
	String objectType;
	@ApiModelProperty(position = 5)
	@Size(max = 50, message = "Maximum object type description size is 50!")
	String objectTypeDesc;
	@ApiModelProperty(position = 6)
	@Size(min = 3, max = 7, message = "Invalid object model. Must have 3-6 characters!")
	String objectModel;
	@ApiModelProperty(position = 7)
	@Size(max = 50, message = "Maximum object model description size is 50!")
	String objectModelDesc;
	@ApiModelProperty(position = 8)
	@Size(max = 3, min = 3, message = "Invalid object. Must have 3 characters!")
	String object;
	@ApiModelProperty(position = 9)
	@Size(max = 50, message = "Maximum object description size is 50!")
	String objectDesc;
	@ApiModelProperty(position = 10)
	String objectYear;
	@ApiModelProperty(position = 11)
	String tipeJaminan;
	@ApiModelProperty(position = 12)
	String jenisAsuransi;
	@ApiModelProperty(position = 13)
	String tipeKombinasi;
	@ApiModelProperty(position = 14)
	String kotaDomisili;
	@ApiModelProperty(position = 15)
	BigDecimal objectPrice;
	@ApiModelProperty(position = 16)
	Integer tenor;
	@ApiModelProperty(position = 17)
	BigDecimal rate;
	@ApiModelProperty(position = 18)
	BigDecimal dp;
	@ApiModelProperty(position = 19)
	BigDecimal installmentAmt;
	@ApiModelProperty(position = 20)
	String promo;
	@ApiModelProperty(position = 21)
	String modelDetail;
	@ApiModelProperty(position = 22)
	String notes;
	@ApiModelProperty(position = 23)
	Date createdDate;
	@ApiModelProperty(position = 24)
	String createdBy;
	@ApiModelProperty(position = 25)
	Date modifiedDate;
	@ApiModelProperty(position = 26)
	String modifiedBy;
	@ApiModelProperty(position = 27)
	Integer active;
	@ApiModelProperty(position = 28)
	Integer flagCredsim;
	@Size(max = 3, message = "Maximum length size is 3!")
	@ApiModelProperty(position = 29)
	String installmentTypeId;
	@Size(max = 100, message = "Maximum length size is 100!")
	@ApiModelProperty(position = 30)
	String installmentTypeDesc;
	@Size(max = 5, message = "Maximum length size is 5!")
	@ApiModelProperty(position = 31)
	String programId;
	@Size(max = 100, message = "Maximum length size is 100!")
	@ApiModelProperty(position = 32)
	String programDesc;
	@Size(max = 2, message = "Maximum length size is 2!")
	@ApiModelProperty(position = 33)
	String paymentTypeId;
	@Size(max = 100, message = "Maximum length size is 100!")
	@ApiModelProperty(position = 34)
	String paymentTypeDesc;
	@ApiModelProperty(position = 35)
	Integer flagInsrAdmin;
	@ApiModelProperty(position = 36)
	Integer acuanHitung;
	@Size(max = 4, message = "Maximum length size is 4!")
	@ApiModelProperty(position = 37)
	String kotaDomisiliId;
	@Size(max = 10, message = "Maximum length size is 10!")
	@ApiModelProperty(position = 38)
	String modelDetailId;
	@ApiModelProperty(position = 39)
	Integer insrTloCoverage;
	@ApiModelProperty(position = 40)
	Integer insrCompreCoverage;
	@Size(max = 2, message = "Maximum length size is 2!")
	@ApiModelProperty(position = 41)
	String insrTypeId;

}
