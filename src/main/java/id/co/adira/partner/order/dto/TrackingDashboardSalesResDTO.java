package id.co.adira.partner.order.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dak
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Generated
public class TrackingDashboardSalesResDTO {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(position = 1)
	private long orderTerkirim;
	@ApiModelProperty(position = 2)
	private long berlangsung;
	@ApiModelProperty(position = 3)
	private long po;
	@ApiModelProperty(position = 4)
	private int ditolak;
	@ApiModelProperty(position = 5)
	private int batal;
}
