/**
 * 
 */
package id.co.adira.partner.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import id.co.adira.partner.order.utils.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingOrderDetailReqDTO {
	@ApiModelProperty(position = 1)
	@NotEmpty(message = "Order number must be filled!")
	@Size(min = 20, max = 20, message = "Order number size must be 20 characters!")
	String orderNo;
	@ApiModelProperty(position = 2)
	@NotEmpty(message = "appl number must be filled!")
	String applNo;
}
