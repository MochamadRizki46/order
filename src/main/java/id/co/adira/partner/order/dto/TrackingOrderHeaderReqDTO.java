package id.co.adira.partner.order.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

import id.co.adira.partner.order.utils.Generated;

/**
 * @author 10999943
 *
 */
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class TrackingOrderHeaderReqDTO {
	@NotEmpty(message = "Appl number unit must be filled!")
	private String applNoUnit;


}