package id.co.adira.partner.order.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

import id.co.adira.partner.order.utils.Generated;

/**
 * @author 10999943
 *
 */
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class TrackingOrderHeaderResDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(position = 1)
	private String orderNo;
	@ApiModelProperty(position = 2)
	private String applNoUnit;
	@ApiModelProperty(position = 3)
	private String custName;
	@ApiModelProperty(position = 4)
	private String orderDate;
	@ApiModelProperty(position = 5)
	private String status;
	@ApiModelProperty(position = 6)
	private String object;
	@ApiModelProperty(position = 7)
	private String objectDesc;
	@ApiModelProperty(position = 8)
	private String objectModel;
	@ApiModelProperty(position = 9)
	private String objectModelDesc;
	@ApiModelProperty(position = 10)
	private String namaSo;
	@ApiModelProperty(position = 11)
	private String surveyDate;
	@ApiModelProperty(position = 12)
	private String surveyTime;

}