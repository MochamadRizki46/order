/**
 * 
 */
package id.co.adira.partner.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

import id.co.adira.partner.order.utils.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingOrderHistoryReqDTO {
	@ApiModelProperty(position = 1)
	@NotEmpty(message = "Appl number must be filled!")
	String applNo;
	@ApiModelProperty(position = 2)
	@NotEmpty(message = "Type user field must be filled!")
	String typeUser;
}
