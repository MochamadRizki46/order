package id.co.adira.partner.order.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dak
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Generated
public class TrackingOrderHistoryResDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(position = 1)
	private String applNoUnit;
	@ApiModelProperty(position = 2)
	private String orderNo;
	@ApiModelProperty(position = 3)
	private String statusDate;
	@ApiModelProperty(position = 4)
	private String statusTime;
	@ApiModelProperty(position = 5)
	private String status;
	@ApiModelProperty(position = 6)
	private String substatus;
	@ApiModelProperty(position = 7)
	private int sequence;
	

}
