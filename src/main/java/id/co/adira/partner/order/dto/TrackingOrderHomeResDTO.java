/**
 * 
 */
package id.co.adira.partner.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author vionza
 *
 */

@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingOrderHomeResDTO {
	@ApiModelProperty(position = 1)
	private String orderNo;
	@ApiModelProperty(position = 2)
	private String applNo;
	@ApiModelProperty(position = 3)
	private String custName;
	@ApiModelProperty(position = 4)
	private String orderDate;
	@ApiModelProperty(position = 5)
	private String status;
	@ApiModelProperty(position = 6)
	private String objectDesc;
	@ApiModelProperty(position = 7)
	private String objectModelDesc;
	@ApiModelProperty(position = 8)
	private String groupStatus;

}
