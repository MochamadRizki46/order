/**
 * 
 */
package id.co.adira.partner.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

import id.co.adira.partner.order.utils.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingOrderListReqDTO {
	@ApiModelProperty(position = 1)
	@NotEmpty(message = "User Id must be filled!")
	String userId;
	@ApiModelProperty(position = 2)
	@NotEmpty(message = "User Type must be filled!")
	String userType;
	@ApiModelProperty(position = 3)
	@NotEmpty(message = "Flag must be filled!")
	int flag;
	@ApiModelProperty(position = 4)
	@NotEmpty(message = "Sort must be filled!")
	int sort;
}
