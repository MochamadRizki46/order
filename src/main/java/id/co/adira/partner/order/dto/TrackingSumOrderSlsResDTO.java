package id.co.adira.partner.order.dto;

import java.io.Serializable;

import id.co.adira.partner.order.utils.Generated;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dak
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingSumOrderSlsResDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(position = 1)
	private long orderTerkirim;
	@ApiModelProperty(position = 2)
	private long berlangsung;
	@ApiModelProperty(position = 3)
	private long po;
	@ApiModelProperty(position = 4)
	private long ditolak;
	@ApiModelProperty(position = 5)
	private long batal;
	

}
