package id.co.adira.partner.order.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

import id.co.adira.partner.order.utils.Generated;

@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UserDetailReq {
	@NotEmpty(message = "User Id must be filled!")
	String userId;

}
