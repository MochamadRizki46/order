package id.co.adira.partner.order.dto.ad1gate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class KecamatanAd1gateDetailResDTO {
	@JsonProperty("PARA_KELURAHAN_ZIP_CODE")
	private String paraKelurahanZipCode;
	@JsonProperty("PARA_KELURAHAN_ID")
	private String paraKelurahanId;
	@JsonProperty("PARA_KELURAHAN_DESC")
	private String paraKelurahanDesc;
	@JsonProperty("PARA_KELURAHAN_ID_KEC")
	private String paraKelurahanIdKec;
	@JsonProperty("PARA_KECAMATAN_DESC")
	private String paraKecamatanDesc;
	@JsonProperty("KAB_KOT_ID")
	private String kabkotId;
	@JsonProperty("KAB_KOT_DESC")
	private String kabkotDesc;
	@JsonProperty("PROVINSI_DESC")
	private String provinsiDesc;
	@JsonProperty("KAB_KOT_ID_PROV")
	private String kabkotIdProv;
}
