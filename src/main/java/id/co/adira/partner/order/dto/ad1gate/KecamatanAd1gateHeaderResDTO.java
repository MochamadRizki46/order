package id.co.adira.partner.order.dto.ad1gate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class KecamatanAd1gateHeaderResDTO {
	
	@JsonProperty("Status")
	private int status;
	
	@JsonProperty("Message")
	private String message;
	
	@JsonProperty("Data")
	private List<KecamatanAd1gateDetailResDTO> data;
}