package id.co.adira.partner.order.dto.ad1gate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ModelAd1gateDetailResDTO {
	@JsonProperty("OBMO_CODE")
	private String obmoCode;
	@JsonProperty("OBMO_DESC")
	private String obmoDesc;
	@JsonProperty("OBBR_CODE")
	private String ObbrCode;
	@JsonProperty("OBBR_DESC")
	private String ObbrDesc;
	@JsonProperty("OBTY_CODE")
	private String ObtyCode;
	@JsonProperty("OBTY_DESC")
	private String ObtyDesc;
}
