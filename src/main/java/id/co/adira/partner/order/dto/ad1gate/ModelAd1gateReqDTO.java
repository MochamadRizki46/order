package id.co.adira.partner.order.dto.ad1gate;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ModelAd1gateReqDTO {
	@ApiModelProperty(position = 1)
	String search;
	@ApiModelProperty(position = 2)
	String branchid;
	@ApiModelProperty(position = 3)
	String groupcode;
	@ApiModelProperty(position = 4)
	String dlc;
}
