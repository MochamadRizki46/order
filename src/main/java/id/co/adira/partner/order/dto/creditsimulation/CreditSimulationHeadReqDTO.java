package id.co.adira.partner.order.dto.creditsimulation;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Generated
public abstract class CreditSimulationHeadReqDTO {
    private String installmentType;

    private int creditType;

    private double effectiveRate;

    private int flatRate;

    private double fv;

    private int rounded;

    private int seasonal;

    private double tenor;

    private double totalPrinciple;

}
