package id.co.adira.partner.order.dto.creditsimulation.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Generated
public class CreditSimulationNewReqDTO {
	@ApiModelProperty(position = 1)
    private String bp_ext;
	
	@ApiModelProperty(position = 2)
    private String kelurahan_id;

	@ApiModelProperty(position = 3)
    private String kotakab_id;

	@ApiModelProperty(position = 4)
    private String area_id;

	@ApiModelProperty(position = 5)
    private String installment_type;

	@ApiModelProperty(position = 6)
    private int fin_type;

	@ApiModelProperty(position = 7)
    private String program;

	@ApiModelProperty(position = 8)
    private String obj_code;

	@ApiModelProperty(position = 9)
    private String obj_brand_code;
    
	@ApiModelProperty(position = 10)
    private String obj_type_code;
    
	@ApiModelProperty(position = 11)
    private String obj_model_code;
    
	@ApiModelProperty(position = 12)
    private int obj_mfg_year;
    
	@ApiModelProperty(position = 13)
    private double otr;
    
	@ApiModelProperty(position = 14)
    private int tenor;
    
	@ApiModelProperty(position = 15)
    private String payment_type;
    
	@ApiModelProperty(position = 16)
    private double adm_fee;
    
	@ApiModelProperty(position = 17)
    private int insr_com_coverage;
    
	@ApiModelProperty(position = 18)
    private int insr_tlo_coverage;
    
	@ApiModelProperty(position = 19)
    private int adm_insr_fin;
    
	@ApiModelProperty(position = 20)
    private int acuan_hitung;
    
	@ApiModelProperty(position = 21)
    private double nominal_hitung;

}
