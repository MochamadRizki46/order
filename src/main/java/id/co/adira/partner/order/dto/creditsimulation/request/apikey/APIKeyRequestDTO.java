package id.co.adira.partner.order.dto.creditsimulation.request.apikey;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Generated
public class APIKeyRequestDTO {
    private String userName;
}
