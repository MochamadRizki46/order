package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import id.co.adira.partner.order.utils.Generated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AsuransiCashAndCreditReqDTO {
    private String objectCode;

    private String objectModelId;

    private String tenor;

    private String programId;

    private Double otr;
}
