package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated
@Getter
@Setter
public class BallonParam {
    private int ballonBy;

    private int ballonRate;

    private int ballonValue;

    private int ballonValueAol;
}
