package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import id.co.adira.partner.order.utils.Generated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.math.BigDecimal;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BiayaAdminReqDTO {
    String objectCode;

    String programId;

    String objectModelId;

    String tenor;

    BigDecimal otr;

}
