package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import id.co.adira.partner.order.dto.creditsimulation.CreditSimulationHeadReqDTO;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreditSimulationDetailReqDTO extends CreditSimulationHeadReqDTO {
    private BallonParam ballonParam;

    private List<FloatingParam> floatingParam;

    private GracePeriodeParam gracePeriodeParam;

    private IrregularParam irregularParam;

    private SteppingParam steppingParam;

}
