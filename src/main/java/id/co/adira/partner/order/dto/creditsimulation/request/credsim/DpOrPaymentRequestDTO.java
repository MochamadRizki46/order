package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

import id.co.adira.partner.order.utils.Generated;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DpOrPaymentRequestDTO {

    private String objectCode;

    private String programId;

    private BigDecimal angsuran;

    private String pic;

    private String objectModelId;

    private String objectTypeId;

    private Double otr;

    private Integer tenor;

    private Integer creditType;

    private Integer discount;

    private Double dpGross;

    private Double dpSystem;

    private Integer cashOrCredit;
}
