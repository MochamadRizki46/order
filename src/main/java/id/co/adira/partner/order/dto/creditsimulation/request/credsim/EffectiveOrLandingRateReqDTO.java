package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

import id.co.adira.partner.order.utils.Generated;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EffectiveOrLandingRateReqDTO {
    private String objectId;

    private String objectModelId;

    private String objectTypeId;

    private String pic;

    private BigDecimal tenor;

    private String programId;

}
