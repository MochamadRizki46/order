package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated
@Getter
@Setter
public class FloatingParam {
    private int effectiveRate;

    private int installmentNo;
}
