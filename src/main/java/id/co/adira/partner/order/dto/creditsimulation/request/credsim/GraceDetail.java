package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated
@Getter
@Setter
public class GraceDetail {
    private int installmentNo;
}
