package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Generated
@Getter
@Setter
public class GracePeriodeParam {
    private List<GraceDetail> graceDetail;

    private int gracePeriodeType;
}
