package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class InstallmentScheduleReqDTO {
    private Integer instGp;

    private Integer instSs;

    private Integer installmentNo;

    private Double interestRate;

    private Long interestToPay;

    private Long principalToPay;

    private Long remainingPrinciple;

    private Long totalPayment;
}
