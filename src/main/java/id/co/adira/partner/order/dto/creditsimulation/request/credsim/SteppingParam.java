package id.co.adira.partner.order.dto.creditsimulation.request.credsim;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Generated
@Getter
@Setter
public class SteppingParam {
    private List<SteppingDetail> steppingDetail;

    private int totalStep;
}
