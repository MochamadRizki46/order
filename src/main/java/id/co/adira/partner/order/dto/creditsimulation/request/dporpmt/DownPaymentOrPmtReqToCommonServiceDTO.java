package id.co.adira.partner.order.dto.creditsimulation.request.dporpmt;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

import id.co.adira.partner.order.utils.Generated;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DownPaymentOrPmtReqToCommonServiceDTO {

    private Double adminCash;

    private Double adminCredit;

    private BigDecimal angsuran;

    private BigDecimal asuransiCashRate;

    private BigDecimal asuransiCreditRate;

    private Integer creditType;

    private Double discount;

    private Double dpGross;

    private Double dpSystem;

    private Double effectiveRate;

//    private Double flatRate;

    private Double otr;

    private Integer tenor;

    @NotEmpty(message = "Please Insert the Username!")
    private String username;

    public void validateDownPaymentOrPmtValue(){
        if(getAdminCash() == null)
            this.adminCash = 0.0;
        if(getAdminCredit() == null)
            this.adminCredit = 0.0;
//        if(getAngsuran() == null)
//            this.angsuran = 0.0;
//        if(getAsuransiCreditRate() == null)
//            this.asuransiCreditRate = 0.0;
//        if(getAsuransiCashRate() == null)
//            this.asuransiCashRate = 0.0;
        if(getCreditType() == null)
            this.creditType = 0;
        if(getDiscount() == null)
            this.discount = 0.0;
        if(getDiscount() == null)
            this.discount = 0.0;
        if(getDpGross() == null)
            this.dpGross = 0.0;
        if(getDpSystem() == null)
            this.dpSystem = 0.0;
        if(getEffectiveRate() == null)
            this.effectiveRate = 0.0;
//        if(getFlatRate() == null)
//            this.flatRate = 0.0;
        if(getOtr() == null)
            this.otr = 0.0;
        if(getTenor() == null)
            this.tenor = 0;
    }

}
