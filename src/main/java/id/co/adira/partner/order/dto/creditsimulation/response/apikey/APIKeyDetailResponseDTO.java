package id.co.adira.partner.order.dto.creditsimulation.response.apikey;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.adira.partner.order.utils.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated
@Getter
@Setter
public class APIKeyDetailResponseDTO {

    @JsonProperty("apiKey")
    private String apiKey;

    @JsonProperty("apiKeyExpDate")
    private String apiKeyExpDate;
}
