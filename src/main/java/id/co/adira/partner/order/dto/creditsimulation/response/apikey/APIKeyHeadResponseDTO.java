package id.co.adira.partner.order.dto.creditsimulation.response.apikey;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.adira.partner.order.utils.Generated;
import lombok.Getter;
import lombok.Setter;

@Generated
@Getter
@Setter
public class APIKeyHeadResponseDTO {

    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("data")
    private APIKeyDetailResponseDTO data;
}
