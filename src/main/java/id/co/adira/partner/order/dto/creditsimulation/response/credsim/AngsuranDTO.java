package id.co.adira.partner.order.dto.creditsimulation.response.credsim;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AngsuranDTO {
    private String angs_ke;

    private Long sisa_angs;

    private Long angs_per_bulan;
}
