package id.co.adira.partner.order.dto.creditsimulation.response.credsim;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.adira.partner.order.utils.Generated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponseDTO {

    @JsonProperty("status")
    private HttpStatus httpStatus;

    @JsonProperty("message")
    private String message;

    @JsonProperty("data")
    private Object data;


}
