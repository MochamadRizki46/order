package id.co.adira.partner.order.dto.creditsimulation.response.credsim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

import id.co.adira.partner.order.utils.Generated;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BiayaAdminResDTO {
    BigDecimal adminCashFee;

    BigDecimal adminCreditFee;

}
