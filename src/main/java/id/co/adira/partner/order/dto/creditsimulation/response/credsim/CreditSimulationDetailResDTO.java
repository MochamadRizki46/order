package id.co.adira.partner.order.dto.creditsimulation.response.credsim;

import id.co.adira.partner.order.dto.creditsimulation.request.credsim.InstallmentScheduleReqDTO;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Generated
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreditSimulationDetailResDTO {
    private Integer creditType;

    private Double effectiveRate;

    private Double flatRate;

    private List<InstallmentScheduleReqDTO> installmentSchedule;

    private String installmentType;

    private Long lastInstallment;

    private Double pmt;

    private Long pmtRounded;

    private Integer tenor;

    private Long totalPrinciple;

    private Long totalRateAfter;

    private Long totalRateByFlatRate;
}
