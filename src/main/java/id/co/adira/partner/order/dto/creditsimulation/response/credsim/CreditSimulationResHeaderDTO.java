package id.co.adira.partner.order.dto.creditsimulation.response.credsim;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreditSimulationResHeaderDTO {
	private String message;
	private CreditSimulationResNewDTO data;

}
