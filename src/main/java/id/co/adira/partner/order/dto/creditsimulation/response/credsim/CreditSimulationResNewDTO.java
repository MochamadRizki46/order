package id.co.adira.partner.order.dto.creditsimulation.response.credsim;

import java.util.List;

import id.co.adira.partner.order.dto.creditsimulation.request.credsim.InstallmentScheduleReqDTO;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreditSimulationResNewDTO {
	private Double otr;

    private int tenor;

    private Double dp_amount;

    private Double maks_pencairan;

    private Double pencairan;

    private Double installment_amount;

    private String biaya_administrasi_format;

    private Double biaya_administrasi_credit;

    private Double biaya_administrasi_cash;

    private String provisi_format;

    private Double provisi_credit;

    private Double provisi_cash;

    private String effective_rate_format;

    private Double effective_rate;

    private String asuransi_basic_format;

    private Double asuransi_basic_credit;

    private Double asuransi_basic_cash;

    private String asuransi_pa_format;

    private Double asuransi_pa_credit;

    private Double asuransi_pa_cash;

    private String asuransi_life_format;

    private Double asuransi_life_credit;

    private Double asuransi_life_cash;

    private String others_fee_format;

    private Double others_fee_credit;

    private Double others_fee_cash;

    private int insr_com_coverage;

    private int insr_tlo_coverage;

    private List<AngsuranDTO> angsuran;

}

