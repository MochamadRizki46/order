package id.co.adira.partner.order.dto.creditsimulation.response.credsim;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.adira.partner.order.utils.Generated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LovBrandTipeModelDetailResDTO {
    @JsonProperty("objectBrandId")
    private String objectBrandId;

    @JsonProperty("objectBrandName")
    private String objectBrandName;

    @JsonProperty("objectTypeId")
    private String objectTypeId;

    @JsonProperty("objectTypeName")
    private String objectTypeName;

    @JsonProperty("objectModelId")
    private String objectModelId;

    @JsonProperty("objectModelName")
    private String objectModelName;

    @JsonProperty("objectName")
    private String objectName;
}
