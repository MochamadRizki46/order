package id.co.adira.partner.order.dto.creditsimulation.response.credsim;

import java.util.List;

import id.co.adira.partner.order.utils.Generated;

@Generated
public class LovBrandTipeModelHeadResDTO {
    private List<LovBrandTipeModelDetailResDTO> brandTipeDetails;
}
