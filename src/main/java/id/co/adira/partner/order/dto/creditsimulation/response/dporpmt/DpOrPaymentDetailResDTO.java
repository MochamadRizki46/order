package id.co.adira.partner.order.dto.creditsimulation.response.dporpmt;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DpOrPaymentDetailResDTO {

    @JsonProperty("adminCash")
    private Double adminCash;

    @JsonProperty("adminCredit")
    private Double adminCredit;

    @JsonProperty("angsuran")
    private Double angsuran;

    @JsonProperty("angsuran1")
    private Double angsuran1;

    @JsonProperty("asuransiCashRate")
    private Double asuransiCashRate;

    @JsonProperty("asuransiCreditRate")
    private Double asuransiCreditRate;

    @JsonProperty("creditType")
    private Integer creditType;

    @JsonProperty("discount")
    private Double discount;

    @JsonProperty("discountAngsuran")
    private Double discountAngsuran;

    @JsonProperty("discountEffectiveRate")
    private Double discountEffectiveRate;

    @JsonProperty("discountFlatRate")
    private Double discountFlatRate;

    @JsonProperty("dpGross")
    private Double dpGross;

    @JsonProperty("dpSystem")
    private Double dpSystem;

    @JsonProperty("effectiveRate")
    private Double effectiveRate;

    @JsonProperty("flatRate")
    private Double flatRate;

    @JsonProperty("otr")
    private Double otr;

    @JsonProperty("pokokHutangUnit")
    private Double pokokHutangUnit;

    @JsonProperty("tenor")
    private Integer tenor;

    @JsonProperty("totalPokokHutangUnit")
    private Double totalPokokHutangUnit;
}
