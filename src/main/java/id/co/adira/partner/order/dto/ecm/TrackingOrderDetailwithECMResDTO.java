package id.co.adira.partner.order.dto.ecm;




import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author vionza
 *
 */

@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingOrderDetailwithECMResDTO {

	@ApiModelProperty(position = 1)
	String orderNo;
	@ApiModelProperty(position = 2)
	String idNo;
	@ApiModelProperty(position = 3)
	String customerName;
	@ApiModelProperty(position = 4)
	String handphoneNo;
	@ApiModelProperty(position = 5)
	String waNo;
	@ApiModelProperty(position = 6)
	String addressDomisili;
	@ApiModelProperty(position = 7)
	String addressDetail;
	@ApiModelProperty(position = 8)
	String kelurahan;
	@ApiModelProperty(position = 9)
	String surveyDateTime;
	@ApiModelProperty(position = 10)
	String objectDetail;
	@ApiModelProperty(position = 11)
	BigDecimal objectPrice;
	@ApiModelProperty(position = 12)
	BigDecimal dp;
	@ApiModelProperty(position = 13)
	Integer tenor;
	@ApiModelProperty(position = 14)
	BigDecimal installmentAmt;
	@ApiModelProperty(position = 15)
	String promo;
	@ApiModelProperty(position = 16)
	String objectCode;
	@ApiModelProperty(position = 17)
	BigDecimal rate;
	@ApiModelProperty(position = 18)
	String objectYear;
	@ApiModelProperty(position = 19)
	String tipeJaminan;
	@ApiModelProperty(position = 20)
	String jenisAsuransi;
	@ApiModelProperty(position = 21)
	String fotoCustomer;
	@ApiModelProperty(position = 22)
	String fotoSTNK;
	@ApiModelProperty
	String merk;

}
