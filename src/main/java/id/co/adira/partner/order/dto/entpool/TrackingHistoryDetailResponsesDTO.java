/**
 * 
 */
package id.co.adira.partner.order.dto.entpool;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */

@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingHistoryDetailResponsesDTO {

	@JsonProperty("NO_APPL_UNIT")
	private String noApplUnit;
	@JsonProperty("TANGGAL")
    private Date tanggal;
	@JsonProperty("JAM")
    private String jam;
	@JsonProperty("STATUS")
    private String status;
	@JsonProperty("SUB_STATUS")
    private String subStatus;
	@JsonProperty("Sequence")
    private String sequence;
	@JsonProperty("Flag")
    private String flag;
}
