/**
 * 
 */
package id.co.adira.partner.order.dto.entpool;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingLastStatusDtlResDTO {
	

	@JsonProperty("NO_APLIKASI")
    private String noAplikasi;
	@JsonProperty("STATUS")
    private String status;
	@JsonProperty("NAMA")
    private String nama;
	@JsonProperty("OBJECT_GROUP_CODE")
    private String objectGroupCode;
	@JsonProperty("OBJECT_GROUP_NAME")
    private String objectGroupName;
	@JsonProperty("GROUPING_STATUS")
    private String groupingStatus;
	@JsonProperty("MODEL_CODE")
    private String modelCode;
	@JsonProperty("MODEL_NAME")
    private String modelName;
	@JsonProperty("Flag")
    private String flag;

}
