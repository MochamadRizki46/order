/**
 * 
 */
package id.co.adira.partner.order.dto.entpool;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingLastStatusHeaderResDTO {

	@JsonProperty("status")
	private String status;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("data")
	private List<TrackingLastStatusDtlResDTO> data;
		
}
