/**
 * 
 */
package id.co.adira.partner.order.dto.fcm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FcmNotificationRequestDTO {

	private String userId;
	private String to;
    private Notification notification;
    private Data data;
    private String priority = "high";
    private String sound = "default";
    
    
 
    @AllArgsConstructor
    @Setter
    @Getter
    public class Notification {
    	private String title;
        private String body;
       
    }
    
    @AllArgsConstructor
    @Setter
    @Getter
    public class Data {
        private String orderNo;
        private String applicationNo;
        private String applicationUnitNo;
        private String applicationStatus;
        private String isShow;
    }
}
