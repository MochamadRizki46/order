/**
 * 
 */
package id.co.adira.partner.order.dto.fcm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Vionza
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LatesTrackingForFCMResponsesDTO {
	String userId;
	String orderNo;
	String applicationNo;
	String applicationUnitNo;
	String groupStatus;
	String status;
}
