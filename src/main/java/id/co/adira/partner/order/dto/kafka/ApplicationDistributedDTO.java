/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
public class ApplicationDistributedDTO {

	private String skApplication;
    private String applicationNo;
    private String fkCustomer;
    private String sourceApplication;
    
    @JsonFormat(timezone = "GMT+7",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date orderDate;
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date applicationDate;
    @JsonFormat(timezone = "GMT+7",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date surveyAppointmentDate;
    
    private String orderType;
    private boolean flagAccount;
    private String accountNoFom;
    private String branchSurvey;
    private String branchSales;
    private String initialRecomendation;
    private String finalRecomendation;
    private int signPk;
    private String konsepType;
    private int objectQty;
    private String propInsrType;
    private int unit;
    private String applicationContractNo;
    private String applicationLastGroupStatus;
    private String applicationLastStatus;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    
    private String createdBy;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifiedDate;
    
    private String lastModifiedBy;
    private boolean active;
    private boolean flagIA;
    private String dealerNote;
    private String orderNo;
    private String typeSurvey;
    private ApplicationObjectDistributedDTO[] applicationObject;
    private ApplicationDocumentDistributedDTO[] applicationDocument;
    private ApplicationNoteDistributedDTO[] applicationNote;
}