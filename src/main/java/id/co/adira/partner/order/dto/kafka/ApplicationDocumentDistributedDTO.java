/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
public class ApplicationDocumentDistributedDTO {
	private String skApplicationDocument;
	private String paraDocumentTypeId;
	private boolean flagMandatory;
	private boolean flagDisplay;
	private boolean flagUnit;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd")
	private Date receivedDate;
	
	private String docName;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd")
	private Date uploadDate;
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;
	
	private String createdBy;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
	
	private String lastModifiedBy;
	private boolean active;
	private String fkApplication;
	private boolean flagMandatoryCa;
	private String fkApplicationObject;
	private String inputSource;
	private String objectIdEcm;
	private boolean flagFamily;
	private boolean flagMpic;
	private boolean flagPemegangSahamInd;
	private boolean flagPemegangSahamComp;
	private boolean flagPenjamin;
	private String ocrEncrypt;
	private String stnkEncrypt;
	private String stnkMetadataEcm;
	private String bpkbEncrypt;
	private String bpkbMetadataEcm;
	private String fkFamily;
	private String fkGuarantorIndividu;
	private String fkGuarantorCompany;
	private String fkShareholderIndividu;
	private String fkShareHolderCompany;
}