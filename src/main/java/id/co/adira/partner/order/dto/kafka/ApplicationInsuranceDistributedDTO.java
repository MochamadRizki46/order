/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 * 
 *
 */
@Generated
@Data
public class ApplicationInsuranceDistributedDTO {

	private String skApplicationInsurance;
    private String insuranceType;
    private String companyId;
    private String product;
    private int totalTenorInsurance;
    private String tenorInsurance1;
    private String insuranceType1;
    private String insuranceType2;
    private String subType;
    private String coverageType;
    private BigDecimal coverageAMT;
    private BigDecimal upperLimitPct;
    private BigDecimal lowerLimitPct;
    private BigDecimal upperLimitAmt;
    private BigDecimal lowerLimitAmt;
    private BigDecimal cashAmt;
    private BigDecimal creaditAmt;
    private BigDecimal totalSplit;
    private BigDecimal totalPCT;
    private BigDecimal totalAmt;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    private String createdBy;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifiedDate;
    
    private String lastModifiedBy;
    private boolean active;
    private String fkApplicationObject;
    
    private int jaminanKe;
    private BigDecimal rateAmount;
}
