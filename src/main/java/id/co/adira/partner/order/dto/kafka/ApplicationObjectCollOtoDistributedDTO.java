/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.math.BigDecimal;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationObjectCollOtoDistributedDTO {
	
	private String skApplicationObjtCollOto;
	private boolean flagMultiColl;
	private boolean flagCollIsUnit;
	private String identityType;
	private String identityNo;
	private String collaName;
	private Date dateOfBirth;
	private String placeOfBirth;
	private String placeOfBirthCity;
	private String groupObject;
	private String object;
	private String brandObject;
	private String objectType;
	private String objectModel;
	private String objectPurpose;
	private int mfgYear;
	private int registrationYear;
	private int yellowPlate;
	private int builtUp;
	private String bpkbNo;
	private String frameNo;
	private String engineNo;
	private String policeNo;
	private String gradeUnit;
	private String utjFacilities;
	private String bidderName;
	private BigDecimal showroomPrice;
	private String addEquip;
	private BigDecimal mpAdira;
	private BigDecimal rekondisiFisik;
	private int result;
	private BigDecimal ltv;
	private BigDecimal dpJaminan;
	private BigDecimal maxPh;
	private BigDecimal taksasiPrice;
	private String colaPurpose;
	private int capacity;
	private String color;
	private String madeIn;
	private String serialNo;
	private String bpkbAddress;
	private boolean active;
	
	 @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;
	 
	private String createdBy;
	
	 @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
	 
	private String lastModifiedBy;
	private String fkApplicationObject;
	private boolean flagCollNameIsAppl;
	private BigDecimal mpAdiraUpld;
	private String bpkbIDType;
	private boolean flagColorSameBpkbStnk;
	private Date stnkValidDate;
	private BigDecimal sellingPrice;
	private BigDecimal marketPrice;
}




