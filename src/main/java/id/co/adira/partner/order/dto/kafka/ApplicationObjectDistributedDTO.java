package id.co.adira.partner.order.dto.kafka;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

@Generated
@Data
public class ApplicationObjectDistributedDTO {
	private String skApplicationObject;
	private String applicationNoUnit;
	private String financingType;
	private String bussActivities;
	private String bussActivitiesType;
	private String groupObject;
	private String object;
	private String productType;
	private String brandObject;
	private String objectType;
	private String objectModel;
	private String objectUsed;
	private String objectPurpose;
	private String groupSales;
	private String orderSource;
	private String orderSourceName;
	private boolean flagThirdParty;
	private String thirdPartyType;
	private String thirdPartyId;
	private String workType;
	private String sentraId;
	private String unitId;
	private String program;
	private String rehabType;
	private String referenceNo;
	private String applicationContractNo;
	private String installmentType;
	private long applicationTop;
	private String paymentMethod;
	private BigDecimal effRate;
	private BigDecimal flatRate;
	private BigDecimal objectPrice;
	private BigDecimal karoseriPrice;
	private BigDecimal totalAmt;
	private BigDecimal dpNet;
	private BigDecimal installmentDeclineN;
	private BigDecimal paymentPerYear;
	private BigDecimal dpBranch;
	private BigDecimal dpGross;
	private BigDecimal principalAmt;
	private BigDecimal installmentAmt;
	private BigDecimal ltv;
	private BigDecimal dsr;
	private BigDecimal dir;
	private BigDecimal dsc;
	private BigDecimal irr;
	private String gpType;
	private long newTenor;
	private BigDecimal totalStepping;
	private String ballonType;
	private BigDecimal lastInstallmentAmt;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;
	
	private String createdBy;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
	
	private String lastModifiedBy;
	private boolean active;
	private String fkApplication;
	private BigDecimal tacAct;
	private BigDecimal tacMax;
	private String applSendFlag;
	private int isWithoutColla;
	private BigDecimal firstInstallment;
	private String applWithdrawalFlag;
	private String applIdGlosary;
	private BigDecimal insentifSales;
	private BigDecimal insentifPayment;
	private String nikCdo;
	private String namaCdo;
	private String jabatan;
	private String dealerMatrix;
	private String groupId;
	private String outletId;
	private BigDecimal dpChasisNet;
	private BigDecimal dpChasisGross;
	private BigDecimal totalDpKrs;
	private ApplicationInsuranceDistributedDTO[] applicationInsurance;
	private List<Map<String, Object>> applicationFee;
	private ApplicationLoanDistributedDTO[] applicationLoan;
	private ApplicationObjectCollOtoDistributedDTO applicationObjectCollOto;
	private Map<String, Object> applicationObjectCollProp;
	private List<Map<String, Object>> applicationRefund;
	private ApplicationSalesInfoDistributedDTO[] applicationSalesInfo;
	private List<Map<String, Object>> applicationKaroseri;
	private List<Map<String, Object>> applicationWmp;
	private long interestAmt;
	private BigDecimal realDsr;
}
