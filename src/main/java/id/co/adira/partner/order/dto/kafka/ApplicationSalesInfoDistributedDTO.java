/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
public class ApplicationSalesInfoDistributedDTO {
    private String skObjekSales;
    private String employeeId;
    private String salesType;
    private String employeeHeadId;
    private String employeeJob;
    private boolean active;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    
    private String createdBy;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifiedDate;
    
    private String lastModifiedBy;
    private String fkApplicationObject;
    private String employeeHeadJob;
    private String employeeHeadName;
    private String employeeName;
}
