/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
public class CustomerAddressDistributedDTO {

	private String skCustomerAddress;
	private boolean flagCorrespondence;
	private String matrixAddress;
	private String addressType;
	private String address;
	private String rt;
	private String rw;
	private String provinsi;
	private String kabupatenkota;
	private String kecamatan;
	private String kelurahan;
	private String zipCode;
	private boolean flagIdentity;
	private String latitude;
	private String longitude;
	private String locationNote;
	private String priority;
	private String fkApplicationObjectCollProp;
	private String fkGuarantorCompany;
	private String fkGuarantorIndividu;
	private String fkShareHolderIndividu;
	private String fkShareHolderCompany;
	private String fkCustomer;
	private boolean active;

	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;

	private String createdBy;

	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;

	private String lastModifiedBy;
}