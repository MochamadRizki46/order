/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
public class CustomerCompanyDistributedDTO {
	private String skCustomerCompany;
	private String companyType;
	private String profile;
	private String companyName;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd")
	private Date establishDate;
	
	private String natureOfBusiness;
	private String locationStatus;
	private String businessLocation;
	private int totalEmployee;
	private int noOfYearWork;
	private int noOfYearBusiness;
	private String sectorEconomic;
	private String fkCustomer;
	private boolean active;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;
	
	private String createdBy;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
	
	private String lastModifiedBy;
	private List<Map<String, Object>> customerShareholderCompany;
	private List<Map<String, Object>> customerShareholderIndividu;
	private ManagementPicDistributedDTO managementPIC;
}
