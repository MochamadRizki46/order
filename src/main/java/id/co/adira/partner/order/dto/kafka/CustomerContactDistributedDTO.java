/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
public class CustomerContactDistributedDTO {

	private String skCustomerContact;
    private String matrixContact;
    private String phoneAreaCode;
    private String phoneNo;
    private boolean flagHpIsWhatsapp;
    private String contactType;
    private String priority;
    private String fkCustomer;
    private String fkCustomerFamily;
    private String fkGuarantorCompany;
    private String fkGuarantorIndividu;
    private String fkShareHolderCompany;
    private String fkShareHolderIndividu;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;
    
    private String createdBy;
    
    @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifiedDate;
    
    private String lastModifiedBy;
    private boolean active;
}
