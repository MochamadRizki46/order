/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
public class CustomerPersonalDistributedDTO {
	private String skCustomerPersonal;
	private long noOfLiability;
	private String customerGroup;
	private String education;
	private String identityType;
	private String identityNo;
	private String identityFullName;
	private String aliasName;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd")
	private Date dateOfBirth;
	
	private String placeOfBirth;
	private String placeOfBirthCity;
	private String gender;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd")
	private Date identityDate;
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd")
	private Date identityExpireDate;
	
	private boolean flagIdentityLifetime;
	private String religion;
	private String maritalStatus;
	private String email;
	private String fkCustomer;
	private boolean active;
	
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;
	
	private String createdBy;
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
	
	private String lastModifiedBy;
	private String relationStatusEmergency;
	private String fullNameIdEmergency;
	private List<Map<String, Object>> customerFamily;
	private Map<String, Object> customerOccupation;
}