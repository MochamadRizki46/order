/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class GenerateApplNoResDTO {

	@ApiModelProperty(position = 1)
	private String orderNo;
	@ApiModelProperty(position = 2)
	private String applNo;
	@ApiModelProperty(position = 3)
	private String payungNo;
	@ApiModelProperty(position = 4)
	private String flagUpdate;
	@ApiModelProperty(position = 5)
	private String flagMs2;
	@ApiModelProperty(position = 6)
	private String sendDate;
}