/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class GenerateApplNoV2ResDTO {
	
	@ApiModelProperty(position = 1)
	private String orderNo;
	
	@ApiModelProperty(position = 2)
	private String applicationPayung;
	
	@ApiModelProperty(position = 3)
	private String applicationUnit;
	
}
