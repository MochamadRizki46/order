/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ManagementPicDistributedDTO {

	private String skManagementPic;
	private String identityType;
	private String identityNo;
	private String fullName;
	private String placeOfBirth;
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd")
	private Date dateOfBirth;
	private String placeOfBirthCity;
	private String level;
	private String email;
	private Boolean active;
	private String fkCustomerCompany;
	private String createdBy;
	 @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;
	private String lastModifiedBy;
	 @JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
	
}
