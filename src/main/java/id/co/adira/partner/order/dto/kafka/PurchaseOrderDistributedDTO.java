/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */

@Generated
@Data
public class PurchaseOrderDistributedDTO {
	private String skPo;
	private String poId;
	private String fkApplication;
	private String draftPoNo;
	private String poNo;
	private int noOfUnit;
	private int printCount;
	private Date poDate;
	private boolean flagSipBpkb;
	private String sppdNo;
	private int flagDasPps;
	private int flagUpload;
	private String claimBy;
	private String documentLink;

	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;

	private String createdBy;

	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
	private String lastModifiedBy;
	private int active;

	private List<Map<String, Object>> poKaroseri;

	private List<Map<String, Object>> poUnit;
	private BigDecimal poAmount;

}
