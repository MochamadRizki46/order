/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
public class TrackingOrderDistributeDTO {
	
	private String applicationTrackId;
	private String groupStatus;
	private String status;
	private String processType;
	private String nextProcess;

	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date datetimeStatus;

	private String fkApplicationId;
	private String employeeId;
	private String employeeName;
	private String employeeJob;
	private int sequence;
	private Boolean active;
	private String createdBy;

	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;

	private String lastModifiedBy;

	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
	
}
