/**
 * 
 */
package id.co.adira.partner.order.dto.kafka;

import java.util.Date;

import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UpdateStatusTrackingDTO {

	private String groupStatus;
	private String status;
	private String processType;
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date datetimeStatus;
	private String fkApplicationId;
	private String employeeId;
	private String employeeName;
	private String employeeJob;
	private int sequence;
	private Boolean active;
	private String createdBy;
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdDate;
	private String lastModifiedBy;
	@JsonFormat(timezone = "GMT+7", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastModifiedDate;
}
