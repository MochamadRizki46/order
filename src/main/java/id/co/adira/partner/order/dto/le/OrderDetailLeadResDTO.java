/**
 * 
 */
package id.co.adira.partner.order.dto.le;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class OrderDetailLeadResDTO {
	@ApiModelProperty(position = 1)
	String applNo;
	@ApiModelProperty(position = 2)
	String acIdno;
	@ApiModelProperty(position = 3)
	String acFullNameId;
	@ApiModelProperty(position = 4)
	String acHpNo;
	@ApiModelProperty(position = 5)
	int acFlagWa;
	@ApiModelProperty(position = 6)
	String acNoWa;
	@ApiModelProperty(position = 7)
	String acBirthPlace;
	@ApiModelProperty(position = 8)
	String acBirthDate;
	@ApiModelProperty(position = 9)
	String acSex;
	@ApiModelProperty(position = 10)
	String acAddress;
	@ApiModelProperty(position = 11)
	String acRt;
	@ApiModelProperty(position = 12)
	String acRw;
	@ApiModelProperty(position = 13)
	String acKelurahan;
	@ApiModelProperty(position = 14)
	String acKelurahanDesc;
	@ApiModelProperty(position = 15)
	String acKecamatan;
	@ApiModelProperty(position = 16)
	String acKecamatanDesc;
	@ApiModelProperty(position = 17)
	String acProvinsi;
	@ApiModelProperty(position = 18)
	String acProvinsiDesc;
	@ApiModelProperty(position = 19)
	String acKota;
	@ApiModelProperty(position = 20)
	String acKotaDesc;
	@ApiModelProperty(position = 21)
	String acAgama;
	@ApiModelProperty(position = 22)
	String acAgamaDescs;
	@ApiModelProperty(position = 23)
	String acStatusPerkawinan;
	@ApiModelProperty(position = 24)
	String acPekerjaan;
	@ApiModelProperty(position = 25)
	String acPekerjaanDesc;
	@ApiModelProperty(position = 26)
	String acIdDate;
	@ApiModelProperty(position = 27)
	String acAlamatLengkapDomisili;
	@ApiModelProperty(position = 28)
	String acKelurahanId;
	@ApiModelProperty(position = 29)
	String acKelurahanIdDesc;
	@ApiModelProperty(position = 30)
	String acKecamatanId;
	@ApiModelProperty(position = 31)
	String acKecamatanIdDesc;
	@ApiModelProperty(position = 32)
	String acProvinsiId;
	@ApiModelProperty(position = 33)
	String acProvinsiIdDesc;
	@ApiModelProperty(position = 34)
	String acKotaId;
	@ApiModelProperty(position = 35)
	String acKotaIdDesc;
	@ApiModelProperty(position = 36)
	String acKodePos;
	@ApiModelProperty(position = 37)
	String acTanggalSurvey;
	@ApiModelProperty(position = 38)
	String acJamSurvey;
	@ApiModelProperty(position = 39)
	String acObjectPorto;
	@ApiModelProperty(position = 40)
	String acObjectPortoDesc;
	@ApiModelProperty(position = 41)
	String acBrand;
	@ApiModelProperty(position = 42)
	String acBrandDesc;
	@ApiModelProperty(position = 43)
	String acTipe;
	@ApiModelProperty(position = 44)
	String acTipeDesc;
	@ApiModelProperty(position = 45)
	String acModel;
	@ApiModelProperty(position = 46)
	String acModelDesc;
	@ApiModelProperty(position = 47)
	String acTahun;
	@ApiModelProperty(position = 48)
	String acTipeJaminan;
	@ApiModelProperty(position = 49)
	String acTipeAsuransi;
	@ApiModelProperty(position = 50)
	BigDecimal acOTRHargaBarang;
	@ApiModelProperty(position = 51)
	int acTenor;
	@ApiModelProperty(position = 52)
	BigDecimal acRate;
	@ApiModelProperty(position = 53)
	BigDecimal acDp;
	@ApiModelProperty(position = 54)
	BigDecimal acCicilan;
	@ApiModelProperty(position = 55)
	String acPromo;
	@ApiModelProperty(position = 56)
	String acCabang;
	@ApiModelProperty(position = 57)
	String acNoPP;
	@ApiModelProperty(position = 58)
	String flag;
}
