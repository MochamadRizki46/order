/**
 * 
 */
package id.co.adira.partner.order.dto.le;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author vionza
 *
 */
@Generated
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OrderHeaderLeadResDTO {
    private String namaCustomer;
    private String noApplUnit;
    private String objtGroup;
    private String objtGroupName;
    private String brand;
    private String brandName;
    private String model;
    private String modelName;
    private String salesOfficer;
    private String tanggalSurvey;
    private String jamSurvey;
    private String flag;
}
