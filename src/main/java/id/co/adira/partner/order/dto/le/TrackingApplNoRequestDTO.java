/**
 * 
 */
package id.co.adira.partner.order.dto.le;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingApplNoRequestDTO {
	private String partnerCode;
	private String partnerType;
	private Date starDate;  
}
