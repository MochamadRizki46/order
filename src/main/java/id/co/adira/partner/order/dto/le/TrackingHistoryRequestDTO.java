/**
 * 
 */
package id.co.adira.partner.order.dto.le;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author vionza
 *
 */
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class TrackingHistoryRequestDTO {
	private String applNo;
	private String flag;
	private String typeUser;
}
