/**
 * 
 */
package id.co.adira.partner.order.dto.le;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingHistoryResponseDTO {

	private String applNoUnit;

	private String dateStatus;

	private String timeStatus;
	private String kodeGroupStatus;
	private String status;

	private String substatus;
	private String flag;
	private String sequences;
}
