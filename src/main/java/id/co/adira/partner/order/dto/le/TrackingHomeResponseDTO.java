/**
 * 
 */
package id.co.adira.partner.order.dto.le;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Vionza
 *
 */
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TrackingHomeResponseDTO {
	private String noAplikasi;
	private String status;
	private String nama;
	private String objectGroupCode;
	private String objectGroupName;
	private String modelCode;
	private String modelName;
	private String flag;
	private String kodeGroupStatus;
	private String applDate;
}
