package id.co.adira.partner.order.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseResponse {

    @JsonIgnore
    HttpStatus httpStatus;

    @JsonProperty("status")
    Integer status;

    @JsonProperty("message")
    String message;
}
