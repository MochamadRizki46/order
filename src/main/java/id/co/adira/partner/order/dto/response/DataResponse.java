package id.co.adira.partner.order.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataResponse extends BaseResponse{

	@JsonProperty("data")
    Object data;

}
