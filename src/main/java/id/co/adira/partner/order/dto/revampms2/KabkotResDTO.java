package id.co.adira.partner.order.dto.revampms2;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Generated
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class KabkotResDTO {
	@JsonProperty("PARENT_KODE")
	private String parentKode;
	@JsonProperty("KODE")
	private String kode;
	@JsonProperty("DESKRIPSI")
	private String deskripsi;
}
