/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "application")
@Data
public class Application {

	@Id
	@Column(name = "sk_application")
	private String skApplication;
	@Column(name = "application_no")
	private String applicationNo;
	@Column(name = "fk_customer")
	private String fkCustomer;
	@Column(name = "source_application")
	private String sourceApplication;
	@Column(name = "order_date")
	private Date orderDate;
	@Column(name = "application_date")
	private Date applicationDate;
	@Column(name = "survey_appointment_date")
	private Date surveyAppointmentDate;
	@Column(name = "order_type")
	private String orderType;
	@Column(name = "flag_account")
	private boolean flagAccount;
	@Column(name = "account_no_form")
	private String accountNoFom;
	@Column(name = "branch_survey")
	private String branchSurvey;
	@Column(name = "branch_sales")
	private String branchSales;
	@Column(name = "initial_recomendation")
	private String initialRecomendation;
	@Column(name = "final_recomendation")
	private String finalRecomendation;
	@Column(name = "sign_pk")
	private int signPk;
	@Column(name = "konsep_type")
	private String konsepType;
	@Column(name = "object_qty")
	private int objectQty;
	@Column(name = "prop_insr_type")
	private String propInsrType;
	@Column(name = "unit")
	private int unit;
	@Column(name = "application_contract_no")
	private String applicationContractNo;
	@Column(name = "application_last_group_status")
	private String applicationLastGroupStatus;
	@Column(name = "application_last_status")
	private String applicationLastStatus;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;
	@Column(name = "active")
	private boolean active;
	@Column(name = "flag_ia")
	private boolean flagIA;
	@Column(name = "dealer_note")
	private String dealerNote;
	@Column(name = "order_no")
	private String orderNo;
	
}