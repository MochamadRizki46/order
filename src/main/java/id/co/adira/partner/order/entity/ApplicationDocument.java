/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Data
@Table(name = "application_document")
public class ApplicationDocument {
	@Id
	@Column(name = "sk_application_document")
	private String skApplicationDocument;
	@Column(name = "para_document_type_id")
	private String paraDocumentTypeId;
	@Column(name = "flag_mandatory")
	private boolean flagMandatory;
	@Column(name = "flag_display")
	private boolean flagDisplay;
	@Column(name = "flag_unit")
	private boolean flagUnit;
	@Column(name = "received_date")
	private Date receivedDate;
	@Column(name = "document_name")
	private String docName;
	@Column(name = "upload_date")
	private Date uploadDate;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;
	@Column(name = "active")
	private boolean active;
	@Column(name = "fk_application")
	private String fkApplication;
	@Column(name = "flag_mandatory_ca")
	private boolean flagMandatoryCa;
	@Column(name = "fk_application_object")
	private String fkApplicationObject;
	@Column(name = "input_source")
	private String inputSource;
	@Column(name = "object_id_ecm")
	private String objectIdEcm;
	@Column(name = "flag_family")
	private boolean flagFamily;
	@Column(name = "flag_mpic")
	private boolean flagMpic;
	@Column(name = "flag_pemegang_saham_ind")
	private boolean flagPemegangSahamInd;
	@Column(name = "flag_pemegang_saham_com")
	private boolean flagPemegangSahamComp;
	@Column(name = "flag_penjamin")
	private boolean flagPenjamin;
	@Column(name = "ocr_encrypt")
	private String ocrEncrypt;
	@Column(name = "stnk_encrypt")
	private String stnkEncrypt;
	@Column(name = "stnk_metadata_ecm")
	private String stnkMetadataEcm;
	@Column(name = "bpkb_encrypt")
	private String bpkbEncrypt;
	@Column(name = "bpkb_metadata_ecm")
	private String bpkbMetadataEcm;
	@Column(name = "fk_family")
	private String fkFamily;
	@Column(name = "fk_guarantor_individu")
	private String fkGuarantorIndividu;
	@Column(name = "fk_guarantor_company")
	private String fkGuarantorCompany;
	@Column(name = "fk_shareholder_individu")
	private String fkShareholderIndividu;
	@Column(name = "fk_shareholder_company")
	private String fkShareHolderCompany;
	
//	@ManyToOne
//	@JoinColumn(name = "fk_application", referencedColumnName = "sk_application")
//	private String fkApplication;
	
}
