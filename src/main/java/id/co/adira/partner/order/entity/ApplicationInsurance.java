/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "application_insurance")
@Data
public class ApplicationInsurance {
	
	@Id
	@Column(name = "sk_application_insurance")
	private String skApplicationInsurance;
	@Column(name = "insurance_type")
	private String insuranceType;
	@Column(name = "company_identity")
	private String companyID;
	@Column(name = "product")
	private String product;
	@Column(name = "total_tenor_insurance")
	private int totalTenorInsurance;
	@Column(name = "tenor_insurance1")
	private String tenorInsurance1;
	@Column(name = "insurance_type1")
	private String insuranceType1;
	@Column(name = "insurance_type2")
	private String insuranceType2;
	@Column(name = "sub_type")
	private String subType;
	@Column(name = "coverage_type")
	private String coverageType;
	@Column(name = "coverage_amount")
	private BigDecimal coverageAMT;
	@Column(name = "upper_limit_percentage")
	private BigDecimal upperLimitPct;
	@Column(name = "lower_limit_percentage")
	private BigDecimal lowerLimitPct;
	@Column(name = "upper_limit_amount")
	private BigDecimal upperLimitAmt;
	@Column(name = "lower_limit_amount")
	private BigDecimal lowerLimitAmt;
	@Column(name = "cash_amount")
	private BigDecimal cashAmt;
	@Column(name = "credit_amount")
	private BigDecimal creaditAmt;
	@Column(name = "total_split")
	private BigDecimal totalSplit;
	@Column(name = "total_percentage")
	private BigDecimal totalPCT;
	@Column(name = "total_amount")
	private BigDecimal totalAmt;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;
	@Column(name = "active")
	private boolean active;
	@Column(name = "jaminan_ke")
	private int jaminanKe;
	@Column(name = "rate")
	private BigDecimal rateAmount;
	@Column(name = "fk_application_object")
	private String fkApplicationObject;
//	@ManyToOne
//	@JoinColumn(name = "fk_application_object", referencedColumnName = "sk_application_object")
//	private String fkApplObjt;
}