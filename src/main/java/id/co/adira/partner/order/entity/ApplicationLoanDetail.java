/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "application_loan_detail")
@Data
public class ApplicationLoanDetail {
	@Id
	@Column(name = "sk_application_loan_detail")
	private String applId;
	@Column(name = "installment_no")
	private long installmentNo;
	@Column(name = "percentage")
	private BigDecimal percentage;
	@Column(name = "amount")
	private BigDecimal amount;
	@Column(name = "active")
	private boolean active;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;
	@Column(name = "fk_application_object")
	private String fkApplicationObject;
	
//	@ManyToOne
//	@JoinColumn(name = "fk_application_object", referencedColumnName = "sk_application_object")
//	private String fkApplicationObjt;
}
