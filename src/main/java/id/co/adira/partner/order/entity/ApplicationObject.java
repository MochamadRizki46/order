/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "application_object")
@Data
public class ApplicationObject {

	@Id
	@Column(name = "sk_application_object")
	private String skApplicationObject;
	@Column(name = "application_no_unit")
	private String applicationNoUnit;
	@Column(name = "financing_type")
	private String financingType;
	@Column(name = "buss_activities")
	private String bussActivities;
	@Column(name = "buss_activities_type")
	private String bussActivitiesType;
	@Column(name = "group_object")
	private String groupObject;
	@Column(name = "object")
	private String object;
	@Column(name = "product_type")
	private String productType;
	@Column(name = "brand_object")
	private String brandObject;
	@Column(name = "object_type")
	private String objectType;
	@Column(name = "object_model")
	private String objectModel;
	@Column(name = "object_used")
	private String objectUsed;
	@Column(name = "object_purpose")
	private String objectPurpose;
	@Column(name = "group_sales")
	private String groupSales;
	@Column(name = "order_source")
	private String orderSource;
	@Column(name = "order_source_name")
	private String orderSourceName;
	@Column(name = "flag_third_party")
	private boolean flagThirdParty;
	@Column(name = "third_party_type")
	private String thirdPartyType;
	@Column(name = "third_party_id")
	private String thirdPartyID;
	@Column(name = "work_type")
	private String workType;
	@Column(name = "sentra_d")
	private String sentraId;
	@Column(name = "unit_d")
	private String unitId;
	@Column(name = "program")
	private String program;
	@Column(name = "rehab_type")
	private String rehabType;
	@Column(name = "reference_no")
	private String referenceNo;
	@Column(name = "application_contract_no")
	private String applicationContractNo;
	@Column(name = "installment_type")
	private String installmentType;
	@Column(name = "application_top")
	private long applicationTop;
	@Column(name = "payment_method")
	private String paymentMethod;
	@Column(name = "eff_rate")
	private BigDecimal effRate;
	@Column(name = "flat_rate")
	private BigDecimal flatRate;
	@Column(name = "object_price")
	private BigDecimal objectPrice;
	@Column(name = "karoseri_price")
	private BigDecimal karoseriPrice;
	@Column(name = "total_amt")
	private BigDecimal totalAmt;
	@Column(name = "dp_net")
	private BigDecimal dpNet;
	@Column(name = "installment_decline_n")
	private BigDecimal installmentDeclineN;
	@Column(name = "payment_per_year")
	private BigDecimal paymentPerYear;
	@Column(name = "dp_branch")
	private BigDecimal dpBranch;
	@Column(name = "dp_gross")
	private BigDecimal dpGross;
	@Column(name = "principal_amt")
	private BigDecimal principalAmt;
	@Column(name = "installment_amt")
	private BigDecimal installmentAmt;
	@Column(name = "ltv")
	private BigDecimal ltv;
	@Column(name = "dsr")
	private BigDecimal dsr;
	@Column(name = "dir")
	private BigDecimal dir;
	@Column(name = "dsc")
	private BigDecimal dsc;
	@Column(name = "irr")
	private BigDecimal irr;
	// @Column(name = "new_tenor")
	// private String gpType;
	@Column(name = "new_tenor")
	private long newTenor;
	@Column(name = "total_stepping")
	private BigDecimal totalStepping;
	@Column(name = "balloon_type")
	private String ballonType;
	@Column(name = "last_installment_amt")
	private BigDecimal lastInstallmentAmt;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;
	@Column(name = "active")
	private boolean active;
	@Column(name = "tac_act")
	private BigDecimal tacAct;
	@Column(name = "tac_max")
	private BigDecimal tacMax;
	@Column(name = "appl_send_flag")
	private String applSendFlag;
	@Column(name = "is_without_colla")
	private int isWithoutColla;
	@Column(name = "first_installment")
	private BigDecimal firstInstallment;
	@Column(name = "appl_withdrawal_flag")
	private String applWithdrawalFlag;
	@Column(name = "appl_id_glosary")
	private String applIdGlosary;
	@Column(name = "insentif_sales")
	private BigDecimal insentifSales;
	@Column(name = "insentif_payment")
	private BigDecimal insentifPayment;
	@Column(name = "nik_cdo")
	private String nikCdo;
	@Column(name = "nama_cdo")
	private String namaCdo;
	@Column(name = "jabatan")
	private String jabatan;
	@Column(name = "dealer_matrix")
	private String dealerMatrix;
	@Column(name = "group_id")
	private String groupID;
	@Column(name = "outlet_id")
	private String outletID;
	@Column(name = "dp_chasis_net")
	private BigDecimal dpChasisNet;
	@Column(name = "dp_chasis_gross")
	private BigDecimal dpChasisGross;
	@Column(name = "total_dp_krs")
	private BigDecimal totalDpKrs;
	@Column(name = "real_dsr")
	private BigDecimal realDsr;

	@Column(name = "fk_application")
	private String fkApplication;

//	@OneToMany(targetEntity = ApplicationInsurance.class, cascade = CascadeType.ALL)
//	@JoinColumn(name = "fk_application_object", referencedColumnName = "sk_application_object")
//	private List<ApplicationInsurance> applicationInsurance;
//	
//	@OneToMany(targetEntity = ApplicationLoanDetail.class, cascade = CascadeType.ALL)
//	@JoinColumn(name = "fk_application_object", referencedColumnName = "sk_application_object")
//	private List<ApplicationLoanDetail> applicationLoan;
//	
//	@OneToMany(targetEntity = ApplicationSalesInfo.class, cascade = CascadeType.ALL)
//	@JoinColumn(name = "fk_application_object", referencedColumnName = "sk_application_object")
//	private List<ApplicationSalesInfo> applicationSalesInfo;
//	
//	@OneToOne(targetEntity = ApplicationObjectCollateralOtomotive.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinColumn(name = "sk_application_object", referencedColumnName = "fk_application_object")
//	private ApplicationObjectCollateralOtomotive applicationObjectCollOto;
	
}
