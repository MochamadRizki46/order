/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Vionza
 *
 */
@Data
@Table(name = "application_object_coll_oto")
@Entity
public class ApplicationObjectCollateralOtomotive {
	
	@Id
	@Column(name = "sk_application_object_coll_oto")
	private String skApplicationObjtCollOto;
	
	@Column(name = "flag_multi_coll")
	private boolean flagMultiColl;
	@Column(name = "flag_coll_is_unit")
	private boolean flagCollIsUnit;
	@Column(name = "identity_type")
	private String identityType;
	@Column(name = "identity_no")
	private String identityNo;
	@Column(name = "colla_name")
	private String collaName;
	@Column(name = "date_of_birth")
	private Date dateOfBirth;
	@Column(name = "place_of_birth")
	private String placeOfBirth;
	@Column(name = "place_of_birth_city")
	private String placeOfBirthCity;
	@Column(name = "group_object")
	private String groupObject;
	@Column(name = "object")
	private String object;
	@Column(name = "brand_object")
	private String brandObject;
	@Column(name = "object_type")
	private String objectType;
	@Column(name = "object_model")
	private String objectModel;
	@Column(name = "object_purpose")
	private String objectPurpose;
	@Column(name = "mfg_year")
	private int mfgYear;
	@Column(name = "registration_year")
	private int registrationYear;
	@Column(name = "yellow_plate")
	private int yellowPlate;
	@Column(name = "built_up")
	private int builtUp;
	@Column(name = "bpkb_no")
	private String bpkbNo;
	@Column(name = "frame_no")
	private String frameNo;
	@Column(name = "engine_no")
	private String engineNo;
	@Column(name = "police_no")
	private String policeNo;
	@Column(name = "grade_unit")
	private String gradeUnit;
	@Column(name = "utj_facilities")
	private String utjFacilities;
	@Column(name = "bidder_name")
	private String bidderName;
	@Column(name = "showroom_price")
	private BigDecimal showroomPrice;
	@Column(name = "add_equip")
	private String addEquip;
	@Column(name = "mp_adira")
	private BigDecimal mpAdira;
	@Column(name = "rekondisi_fisik")
	private BigDecimal rekondisiFisik;
	@Column(name = "result")
	private int result;
	@Column(name = "ltv")
	private BigDecimal ltv;
	@Column(name = "dp_jaminan")
	private BigDecimal dpJaminan;
	@Column(name = "max_ph")
	private BigDecimal maxPh;
	@Column(name = "taksasi_price")
	private BigDecimal taksasiPrice;
	@Column(name = "cola_purpose")
	private String colaPurpose;
	@Column(name = "capacity")
	private int capacity;
	@Column(name = "color")
	private String color;
	@Column(name = "made_in")
	private String madeIn;
	@Column(name = "serial_no")
	private String serialNo;
	@Column(name = "bpkb_address")
	private String bpkbAddress;
	@Column(name = "active")
	private boolean active;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;
	@Column(name = "fk_application_object")
	private String fkApplicationObject;
	@Column(name = "flag_coll_name_is_appl")
	private boolean flagCollNameIsAppl;
	@Column(name = "mp_adira_upld")
	private BigDecimal mpAdiraUpld;
	@Column(name = "bpkb_id_type")
	private String bpkbIDType;
	@Column(name = "flag_color_same_bpkb_stnk")
	private boolean flagColorSameBpkbStnk;
	@Column(name = "stnk_valid_date")
	private Date stnkValidDate;
	@Column(name = "selling_price")
	private BigDecimal sellingPrice;
	@Column(name = "market_price")
	private BigDecimal marketPrice;
}
