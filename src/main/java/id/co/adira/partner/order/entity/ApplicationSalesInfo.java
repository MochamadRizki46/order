/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "application_sales_info")
@Data
public class ApplicationSalesInfo {
	@Id
	@Column(name = "sk_objek_sales")
	private String skObjekSales;
	@Column(name = "employee_id")
	private String employeeId;
	@Column(name = "sales_type")
	private String salesType;
	@Column(name = "employee_head_id")
	private String employeeHeadId;
	@Column(name = "employee_job")
	private String employeeJob;
	@Column(name = "active")
	private boolean active;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;
	@Column(name = "employee_head_job")
	private String employeeHeadJob;
	@Column(name = "fk_application_object")
	private String fkApplicationObject;
	@Column(name = "employee_head_name")
	private String employeeHeadName;
	@Column(name = "employee_name")
	private String employeeName;
	
//	@ManyToOne
//	@JoinColumn(name = "fk_application_object", referencedColumnName = "sk_application_object")
//	private String fkApplicationObject;
}
