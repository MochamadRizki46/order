/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "application_tracking")
@Data
public class ApplicationTracking {
	
	@Id
	@Column(name = "application_track_id")
	private String applicationTrackId;
	 
	@Column(name = "group_status")
	private String groupStatus;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "process_type")
	private String processType;
	
	@Column(name = "next_process")
	private String nextProcess;
	
	@Column(name = "datetime_status")
	private Date datetimeStatus;
	
	@Column(name = "fk_application_id")
	private String fkApplicationId;
	
	@Column(name = "employee_id")
	private String employeeId;
	
	@Column(name = "employee_name")
	private String employeeName;
	
	@Column(name = "employee_job")
	private String employeeJob;
	
	@Column(name = "sequence")
	private int sequence;
	
	@Column(name = "active")
	private Boolean active;
	
	@Column(name = "created_by")
	private String createdBy;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "last_modified_by")
	private String lastModifiedBy;
	
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
}
