/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "customer")
@Data
public class Customer {
	@Id
	@Column(name = "sk_customer")
	private String skCustomer;
	@Column(name = "customer_id")
	private String customerId;
	@Column(name = "customer_oid")
	private String customerOid;
	@Column(name = "customer_type")
	private String customerType;
	@Column(name = "npwp_address")
	private String npwpAddress;
	@Column(name = "npwp_name")
	private String npwpName;
	@Column(name = "npwp_no")
	private String npwpNo;
	@Column(name = "npwp_type")
	private String npwpType;
	@Column(name = "flag_npwp")
	private boolean flagNpwp;
	@Column(name = "flag_pkp_sign")
	private String flagPkpSign;
	@Column(name = "flag_guarantor")
	private boolean flagGuarantor;
	@Column(name = "active")
	private boolean active;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;

//	@OneToOne(targetEntity = CustomerPersonal.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinTable(name = "customer_personal", joinColumns = {
//			@JoinColumn(name = "fk_customer", referencedColumnName = "sk_customer") }, inverseJoinColumns = {
//					@JoinColumn(name = "sk_customer", referencedColumnName = "fk_customer") }, foreignKey = @ForeignKey(value = ConstraintMode.CONSTRAINT, name = "customer_personal_fk"))
//	private CustomerPersonal customerPersonal;
//
//	@OneToOne(targetEntity = CustomerCompany.class, cascade = CascadeType.ALL)
//	@JoinColumn(name = "sk_customer", referencedColumnName = "fk_customer")
//	private CustomerCompany customerCompany;

//	@OneToMany(targetEntity = CustomerAddress.class, cascade = CascadeType.ALL)
//	@JoinColumn(name = "fk_customer", referencedColumnName = "sk_customer")
//	private List<CustomerAddress> customerAddress;
//
//	@OneToMany(targetEntity = CustomerContact.class, cascade = CascadeType.ALL)
//	@JoinColumn(name = "fk_customer", referencedColumnName = "sk_customer")
//	private List<CustomerContact> customerContact;


}
