/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "customer_address")
@Data
public class CustomerAddress {
	@Id
	@Column(name = "sk_customer_address")
	private String skCustomerAddress;
	@Column(name = "flag_correspondence")
	private boolean flagCorrespondence;
	@Column(name = "matrix_address")
	private String matrixAddress;
	@Column(name = "address_type")
	private String addressType;
	@Column(name = "address")
	private String address;
	@Column(name = "rt")
	private String rt;
	@Column(name = "rw")
	private String rw;
	@Column(name = "provinsi")
	private String provinsi;
	@Column(name = "kabupatenkota")
	private String kabupatenkota;
	@Column(name = "kecamatan")
	private String kecamatan;
	@Column(name = "kelurahan")
	private String kelurahan;
	@Column(name = "zip_code")
	private String zipCode;
	@Column(name = "flag_identity")
	private boolean flagIdentity;
	@Column(name = "latitude")
	private String latitude;
	@Column(name = "longitude")
	private String longitude;
	@Column(name = "location_note")
	private String locationNote;
	@Column(name = "priority")
	private String priority;
	@Column(name = "fk_application_object_coll_prop")
	private String fkApplicationObjectCollProp;
	@Column(name = "fk_guarantor_company")
	private String fkGuarantorCompany;
	@Column(name = "fk_guarantor_individu")
	private String fkGuarantorIndividu;
	@Column(name = "fk_shareholder_individu")
	private String fkShareHolderIndividu;
	@Column(name = "fk_shareholder_company")
	private String fkShareHolderCompany;
	@Column(name = "fk_customer")
	private String fkCustomer;
	@Column(name = "active")
	private boolean active;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;
}
