/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "customer_company")
@Data
public class CustomerCompany {
	@Id
	@Column(name = "sk_customer_company")
	private String skCustomerCompany;
	@Column(name = "company_type")
	private String companyType;
	@Column(name = "profile")
	private String profile;
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "establish_date")
	private Date establishDate;
	@Column(name = "nature_of_business")
	private String natureOfBusiness;
	@Column(name = "location_status")
	private String locationStatus;
	@Column(name = "business_location")
	private String businessLocation;
	@Column(name = "total_employee")
	private int totalEmployee;
	@Column(name = "no_of_year_work")
	private int noOfYearWork;
	@Column(name = "no_of_year_business")
	private int noOfYearBusiness;
	@Column(name = "sector_economic")
	private String sectorEconomic;
	@Column(name = "fk_customer")
	private String fkCustomer;
	@Column(name = "active")
	private boolean active;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;

}
