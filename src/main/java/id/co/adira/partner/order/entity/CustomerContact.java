/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
@Table(name = "CUSTOMER_CONTACT")
@Entity
public class CustomerContact {

	@Id
	@Column(name = "SK_CUSTOMER_CONTACT")
	private String skCustomerContact;
	@Column(name = "MATRIX_CONTACT")
	private String matrixContact;
	@Column(name = "PHONE_AREA_CODE")
	private String phoneAreaCode;
	@Column(name = "PHONE_NO")
	private String phoneNo;
	@Column(name = "FLAG_HP_IS_WHATSAPP")
	private boolean flagHpIsWhatsapp;
	@Column(name = "CONTACT_TYPE")
	private String contactType;
	@Column(name = "PRIORITY")
	private String priority;
	@Column(name = "FK_CUSTOMER")
	private String fkCustomer;
	@Column(name = "FK_CUSTOMER_FAMILY")
	private String fkCustomerFamily;
	@Column(name = "FK_GUARANTOR_COMPANY")
	private String fkGuarantorCompany;
	@Column(name = "FK_GUARANTOR_INDIVIDU")
	private String fkGuarantorIndividu;
	@Column(name = "FK_SHAREHOLDER_COMPANY")
	private String fkShareHolderCompany;
	@Column(name = "FK_SHAREHOLDER_INDIVIDU")
	private String fkShareHolderIndividu;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "LAST_MODIFIED_DATE")
	private Date lastModifiedDate;
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;
	@Column(name = "ACTIVE")
	private boolean active;

}
