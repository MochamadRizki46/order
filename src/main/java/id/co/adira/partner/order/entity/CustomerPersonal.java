/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "customer_personal")
@Data
public class CustomerPersonal {
	@Id
	@Column(name = "sk_customer_personal")
	private String skCustomerPersonal;
	@Column(name = "no_of_liability")
	private long noOfLiability;
	@Column(name = "customer_group")
	private String customerGroup;
	@Column(name = "education")
	private String education;
	@Column(name = "identity_type")
	private String identityType;
	@Column(name = "identity_no")
	private String identityNo;
	@Column(name = "identity_full_name")
	private String identityFullName;
	@Column(name = "alias_name")
	private String aliasName;
	@Column(name = "date_of_birth")
	private Date dateOfBirth;
	@Column(name = "place_of_birth")
	private String placeOfBirth;
	@Column(name = "place_of_birth_city")
	private String placeOfBirthCity;
	@Column(name = "gender")
	private String gender;
	@Column(name = "identity_date")
	private Date identityDate;
	@Column(name = "identity_expire_date")
	private Date identityExpireDate;
	@Column(name = "flag_identity_lifetime")
	private boolean flagIdentityLifetime;
	@Column(name = "religion")
	private String religion;
	@Column(name = "marital_status")
	private String maritalStatus;
	@Column(name = "email")
	private String email;
	@Column(name = "fk_customer")
	private String fkCustomer;
	@Column(name = "active")
	private boolean active;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "created_by")
	private String createdBy;
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "last_modified_by")
	private String lastModifiedBy;
	@Column(name = "relation_status_emergency")
	private String relationStatusEmergency;
	@Column(name = "full_name_id_emergency")
	private String fullNameIdEmergency;
}
