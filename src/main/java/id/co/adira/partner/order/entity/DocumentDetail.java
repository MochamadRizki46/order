/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "tbl_doc_dtl")
@Data
public class DocumentDetail {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "doc_detail_id")
	String docDtlId;

	@Column(name = "id")
	String id;

	@Column(name = "doc_id")
	String docId;
	
	@Column(name = "doc_path")
	String docPath;
	
	@Column(name = "doc_type")
	String docType;
	
	@CreationTimestamp
	@Column(name = "created_date")
	Date createdDate;
	
	@Column(name = "created_by")
	String createdBy;
	
	@UpdateTimestamp
	@Column(name = "modified_date")
	Date modifiedDate;
	
	@Column(name = "modified_by")
	String modifiedBy;
	
	@Column(name = "active")
	Integer active;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "submit_order_id")
	SubmitOrder submitOrder;
}
