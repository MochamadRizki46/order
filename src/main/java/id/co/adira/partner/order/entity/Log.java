package id.co.adira.partner.order.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Data;
import lombok.Generated;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Generated
@Entity
@Table(name = "tbl_logs")
@TypeDefs({ @TypeDef(name = "json", typeClass = JsonStringType.class), @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class) })
@Data
public class Log {

    @Id
    @Column(name = "log_id")
    String logId;

    @Column(name = "order_no")
    String orderNo;

    @Column(name = "log_name")
    String logName;

    @Type(type = "jsonb")
    @Column(name = "json_request")
    String jsonRequest;

    @Type(type = "jsonb")
    @Column(name = "json_responses")
    String jsonResponses;

    @Column(name = "process_date")
    Date processDate;

    @Column(name = "duration")
    Integer duration;

    @Column(name = "active")
    Integer active;

    @Column(name = "user_id")
    String userId;
}
