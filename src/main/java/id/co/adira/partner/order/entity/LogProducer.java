/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Vionza
 *
 */

@Table(name = "log_producer_partner")
@Data
@Entity
public class LogProducer {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "log_producer_partner_seq")
	@SequenceGenerator(name = "log_producer_partner_seq", sequenceName = "log_producer_partner_seq", schema = "public", allocationSize = 1)
	@Column(name = "id")
	int id;
	@Column(name = "json_file")
	String jsonFile;
	@Column(name = "topic")
	String topic;
	@Column(name = "status")
	String status;
	@Column(name = "flag_status")
	int flagStatus;
	@Column(name = "created_date")
	Date createdDate;
	@Column(name = "message")
	String message;
}
