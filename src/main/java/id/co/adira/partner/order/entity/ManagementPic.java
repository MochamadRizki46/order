
package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Vionza
 *
 */
@Entity
@Table(name = "management_pic")
@Data
public class ManagementPic {
	
	@Id
	@Column(name = "SK_MANAGEMENT_PIC")
	private String skManagementPic;
	
	@Column(name = "FK_CUSTOMER_COMPANY")
	private String fkCustomerCompany;
	
	@Column(name = "IDENTITY_TYPE")
	private String identityType;
	@Column(name = "IDENTITY_NO")
	private String identityNo;
	@Column(name = "FULL_NAME")
	private String fullName;
	@Column(name = "PLACE_OF_BIRTH")
	private String placeOfBirth;
	@Column(name = "DATE_OF_BIRTH")
	private Date dateOfBirth;
	@Column(name = "PLACE_OF_BIRTH_CITY")
	private String placeOfBirthCity;
	@Column(name = "LEVEL")
	private String level;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "ACTIVE")
	private Boolean active;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;
	@Column(name = "LAST_MODIFIED_DATE")
	private Date lastModifiedDate;
}
