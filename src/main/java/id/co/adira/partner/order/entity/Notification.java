/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Vionza
 *
 */
@Entity
@Table(name = "tbl_notification")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Notification {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "notif_id")
	private String id;
	
	@Column(name = "user_id")
	private String userId;
	@Column(name = "order_no")
	private String orderNo;
	@Column(name = "application_no")
	private String applicationNo;
	@Column(name = "application_no_unit")
	private String applicationNoUnit;
	@Column(name = "title")
	private String title;
	@Column(name = "body")
	private String body;
	@Column(name = "status")
	private String status;
	@Column(name = "created_date")
	private Date createdDate;
	@Column(name = "isread")
	private boolean isRead;
	@Column(name = "active")
	private boolean active;

}
