/**
 * 
 */
package id.co.adira.partner.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.adira.partner.order.utils.Generated;
import lombok.Data;

/**
 * @author Vionza
 *
 */
@Generated
@Entity
@Table(name = "para_appl_status")
@Data
public class ParaApplicationStatus {
	@Id
	@Column(name = "para_appl_status_sk_id")
	private String id;
	@Column(name = "para_appl_status_id")
	private String applStatusId;
	@Column(name = "para_appl_status_group_id")
	private String applGroupStatusId;
	@Column(name = "squad")
	private String squad;
	@Column(name = "level1")
	private String applGroupStatus;
	@Column(name = "level2")
	private String applStatus;
	@Column(name = "level3")
	private String applSubStatus;
	@Column(name = "active")
	private boolean active;

}
