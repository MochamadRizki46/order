/**
 * 
 */
package id.co.adira.partner.order.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
@Table(name = "PO")
@Entity
public class PurchaseOrder {

	@Id
	@Column(name = "SK_PO")
	private String skPo;
	@Column(name = "PO_ID")
	private String poId;
	@Column(name = "FK_APPLICATION")
	private String fkApplication;
	@Column(name = "DRAFT_PO_NO")
	private String draftPoNo;
	@Column(name = "PO_NO")
	private String poNo;
	@Column(name = "NO_OF_UNIT")
	private int noOfUnit;
	@Column(name = "PRINT_COUNT")
	private int printCount;
	@Column(name = "PO_DATE")
	private Date poDate;
	@Column(name = "FLAG_SIP_BPKB")
	private boolean flagSipBpkb;
	@Column(name = "SPPD_NO")
	private String sppdNo;
	@Column(name = "FLAG_DAS_PPS")
	private int flagDasPps;
	@Column(name = "FLAG_UPLOAD")
	private int flagUpload;
	@Column(name = "CLAIM_BY")
	private String claimBy;
	@Column(name = "DOCUMENT_LINK")
	private String documentLink;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "LAST_MODIFIED_DATE")
	private Date lastModifiedDate;
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;
	@Column(name = "ACTIVE")
	private int active;
	@Column(name = "PO_AMOUNT")
	private BigDecimal poAmount;
}
