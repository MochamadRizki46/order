package id.co.adira.partner.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

@Generated
@Entity
@Table(name = "tbl_status_order_detail")
@Data
public class StatusOrderDetail {
	
	@Id
	@Column(name = "id")
	String id;
	
	@Column(name = "order_no")
	String orderNo;
	@Column(name = "appl_no_unit")
	String applNoUnit;
	@Column(name = "id_seq")
	int idSeq;
	@Column(name = "status")
	String status;
	@Column(name = "substatus")
	String substatus;
	@Column(name = "groupstatus")
	String groupStatus;
	@Column(name = "status_date")
	Date statusDate;
	@Column(name = "status_time")
	String statusTime;
	@Column(name = "user_type")
	String userType;
	@Column(name = "active")
	int active;
	@Column(name = "create_by")
	String createBy;
	@Column(name = "create_date")
	Date createDate;

}
