package id.co.adira.partner.order.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.*;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 10999943
 *
 */
@Generated
@Entity
@Table(name = "tbl_order_customer")
@Setter
@Getter
@TypeDefs({ @TypeDef(name = "json", typeClass = JsonStringType.class),
		@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class) })
public class SubmitCustomer implements Serializable {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "order_customer_id")
	String submitCustomerId;

	@Column(name = "id_no")
	String idNo;
	@Column(name = "customer_name")
	String customerName;
	@Column(name = "pic")
	String pic;
	@Column(name = "cust_type")
	String custType;
	@Column(name = "handphone_no")
	String handphoneNo;
	@Column(name = "wa_no")
	String waNo;
	@Column(name = "flag_wa")
	Integer flagWa;
	@Column(name = "id_name")
	String idName;
	@Column(name = "birth_date")
	Date birthDate;
	@Column(name = "birth_place")
	String birthPlace;
	@Column(name = "id_address")
	String idAddress;
	@Column(name = "id_type")
	String idType;
	@Column(name = "id_rtrw")
	String idRtrw;
	@Column(name = "id_kelurahan")
	String idKelurahan;
	@Column(name = "id_kelurahan_desc")
	String idKelurahanDesc;
	@Column(name = "id_kecamatan")
	String idKecamatan;
	@Column(name = "id_kecamatan_desc")
	String idKecamatanDesc;
	@Column(name = "id_kabkot")
	String idKabkot;
	@Column(name = "id_kabkot_desc")
	String idKabkotDesc;
	@Column(name = "id_provinsi")
	String idProvinsi;
	@Column(name = "id_provinsi_desc")
	String idProvinsiDesc;
	@Column(name = "zipcode")
	String zipcode;
	@Column(name = "tipe_pembiayaan")
	Integer finType;
	@Column(name = "created_date")
	@CreationTimestamp
	Date createdDate;
	@Column(name = "created_by")
	String createdBy;
	@Column(name = "modified_date")
	@UpdateTimestamp
	Date modifiedDate;
	@Column(name = "modified_by")
	String modifiedBy;
	@Column(name = "active")
	Integer active;
	@Type(type = "jsonb")
	@Column(name = "json_idp", columnDefinition = "jsonb")
	String jsonIdp;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "submit_order_id")
	SubmitOrder submitOrder;

}
