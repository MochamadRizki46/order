package id.co.adira.partner.order.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;
import lombok.Generated;

import org.checkerframework.common.aliasing.qual.Unique;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Generated
@Entity
@Table(name = "tbl_order_domisili")
@Data
public class SubmitDomisili implements Serializable {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "order_domisili_id")
	String submitDomisiliId;

	@Column(name = "flag_address_id")
	Integer flagAddressId;
	@Column(name = "address_domisili")
	String addressDomisili;	
	@Column(name = "survey_date")
	Date surveyDate;
	@Column(name = "survey_time")
	String surveyTime;	
	@Column(name = "rtrw")
	String rtrw;
	@Column(name = "kelurahan")
	String kelurahan;	
	@Column(name = "kelurahan_desc")
	String kelurahanDesc;
	@Column(name = "kecamatan")
	String kecamatan;	
	@Column(name = "kecamatan_desc")
	String kecamatanDesc;
	@Column(name = "kabkot")
	String kabkot;	
	@Column(name = "kabkot_desc")
	String kabkotDesc;
	@Column(name = "provinsi")
	String provinsi;	
	@Column(name = "provinsi_desc")
	String provinsiDesc;
	@Column(name = "zipcode")
	String zipcode;
	@CreationTimestamp
	@Column(name = "created_date")
	Date createdDate;
	@Column(name = "created_by")
	String createdBy;
	@UpdateTimestamp
	@Column(name = "modified_date")
	Date modifiedDate;
	@Column(name = "modified_by")
	String modifiedBy;
	@Column(name = "active")
	Integer active;
	@Column(name = "nama_so")
	String namaSo;
	@Column(name = "flag_kelurahan")
	Integer flagKelurahan;
	@Column(name = "sales_notes")
	String salesNotes;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "submit_order_id")
	SubmitOrder submitOrder;

}