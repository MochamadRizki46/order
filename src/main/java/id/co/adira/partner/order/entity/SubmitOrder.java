package id.co.adira.partner.order.entity;

import lombok.Data;
import lombok.Generated;

import org.checkerframework.common.aliasing.qual.Unique;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Generated
@Entity
@Table(name = "tbl_submit_order")
@Data
public class SubmitOrder implements Serializable {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "submit_order_id")
	String submitOrderId;

	@Column(name = "order_no")
	String orderNo;
	@Column(name = "appl_no_payung")
	String applNoPayung;
	@Column(name = "order_date")
	Date orderDate;
	@Column(name = "partner_type")
	String partnerType;
	@Column(name = "pic")
	String pic;
	@Column(name = "active")
	Integer active;
	@Column(name = "idx")
	Integer idx;
	@Column(name = "status")
	String status;
	@Column(name = "group_status")
	String groupStatus;
	@Column(name = "created_date")
	@CreationTimestamp
	Date createdDate;
	@Column(name = "created_by")
	String createdBy;
	@Column(name = "modified_date")
	@UpdateTimestamp
	Date modifiedDate;
	@Column(name = "modified_by")
	String modifiedBy;
	@Column(name = "appl_no_unit")
	String applNoUnit;
	@Column(name = "send_date_status")
	Date sendDateStatus;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "submitOrder")
	SubmitDomisili submitDomisili;
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "submitOrder")
	SubmitUnit submitUnit;
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "submitOrder")
	SubmitCustomer submitCustomer;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "submitOrder")
	List<DocumentDetail> documentDetail;



}