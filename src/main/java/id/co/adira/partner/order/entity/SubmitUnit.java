/**
 * 
 */
package id.co.adira.partner.order.entity;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;
import lombok.Generated;

import org.checkerframework.common.aliasing.qual.Unique;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author 10999943
 *
 */
@Generated
@Entity
@Table(name = "tbl_order_unit")
@Data
public class SubmitUnit implements Serializable {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "order_unit_id")
	String submitUnitId;

	@Column(name ="object_brand")
	String objectBrand;
	@Column(name ="object_brand_desc")
	String objectBrandDesc;
	@Column(name ="object_type")
	String objectType;
	@Column(name ="object_type_desc")
	String objectTypeDesc;
	@Column(name ="object_model")
	String objectModel;
	@Column(name ="object_model_desc")
	String objectModelDesc;
	@Column(name ="object")
	String object;
	@Column(name ="object_desc")
	String objectDesc;
	@Column(name ="object_year")
	String objectYear;
	@Column(name ="tipe_jaminan")
	String tipeJaminan;
	@Column(name ="jenis_asuransi")
	String jenisAsuransi;
	@Column(name ="kota_domisili")
	String kotaDomisili;
	@Column(name ="object_price")
	BigDecimal objectPrice;
	@Column(name ="tenor")
	Integer tenor;
	@Column(name ="rate")
	BigDecimal rate;
	@Column(name ="dp")
	BigDecimal dp;
	@Column(name ="installment_amt")
	BigDecimal installmentAmt;
	@Column(name ="promo")
	String promo;
	@Column(name ="model_detail")
	String modelDetail;
	@Column(name ="notes")
	String notes;
	@Column(name = "tipe_kombinasi")
	String tipeKombinasi;
	@Column(name = "created_date")
	@CreationTimestamp
	Date createdDate;
	@Column(name = "created_by")
	String createdBy;
	@Column(name = "modified_date")
	@UpdateTimestamp
	Date modifiedDate;
	@Column(name = "modified_by")
	String modifiedBy;
	@Column(name = "active")
	Integer active;
	@Column(name = "flag_credsim")
	Integer flagCredsim;
	@Column(name = "installment_type_id")
	String installmentTypeId;
	@Column(name = "installment_type_desc")
	String installmentTypeDesc;
	@Column(name = "program_id")
	String programId;
	@Column(name = "program_desc")
	String programDesc;
	@Column(name = "payment_type_id")
	String paymentTypeId;
	@Column(name = "payment_type_desc")
	String paymentTypeDesc;
	@Column(name = "flag_insr_admin")
	Integer flagInsrAdmin;
	@Column(name = "acuan_hitung")
	Integer acuanHitung;
	@Column(name = "kota_domisili_id")
	String kotaDomisiliId;
	@Column(name = "model_detail_id")
	String modelDetailId;
	@Column(name = "insr_tlo_coverage")
	Integer insrTloCoverage;
	@Column(name = "insr_compre_coverage")
	Integer insrCompreCoverage;
	@Column(name = "insr_type_id")
	String insrTypeId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "submit_order_id")
	SubmitOrder submitOrder;

}