/**
 * 
 */
package id.co.adira.partner.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Generated;

/**
 * @author Vionza
 *
 */
@Generated
@Data
@Entity
@Table(name = "para_user_detail")
public class UserDetail {
	@Id
	@Column(name = "id")
	int id;
	@Column(name = "userid")
	String userId;
	@Column(name = "partner_code")
	String partnerCode;
}

