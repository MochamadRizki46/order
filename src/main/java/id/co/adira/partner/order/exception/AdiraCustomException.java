/**
 * 
 */
package id.co.adira.partner.order.exception;


/**
 * @author Jagadeesh Kumar B
 *
 */

public class AdiraCustomException extends Exception {

	private static final long serialVersionUID = -5928434483633910456L;

	public AdiraCustomException(String message, Throwable cause) {
		super(message, cause);
	}

	public AdiraCustomException(String message) {
		super(message);
	}

}
