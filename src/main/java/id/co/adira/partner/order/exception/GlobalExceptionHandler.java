package id.co.adira.partner.order.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author Jagadeesh Kumar B
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {
	
	private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * @param ex
     * @param request
     * @return logs exception and sends response related to all sub classes of exception class
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> globalExceptionHandler(Exception ex, WebRequest request) {
    	LOG.error("Error from the api: ", ex);
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    /**
     * @param ex
     * @param request
     * @return logs exception and sends response related to AdiraCustomException class
     */
    @ExceptionHandler(AdiraCustomException.class)
    public ResponseEntity<Object> adiraExceptionHandler(AdiraCustomException ex, WebRequest request) { 
    	LOG.error("Error from the api: ", ex);
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    /**
     * @param ex
     * @param request
     * @return logs exception and sends response related to JsonProcessingException and JsonMappingException class
     */
    @ExceptionHandler({JsonProcessingException.class, JsonMappingException.class})
    public ResponseEntity<Object> jsonExceptionHandler(AdiraCustomException ex, WebRequest request) { 
    	LOG.error("Error parsing the json from the api: ", ex);
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
