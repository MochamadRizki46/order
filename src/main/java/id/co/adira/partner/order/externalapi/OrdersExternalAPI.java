package id.co.adira.partner.order.externalapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @author Jagadeesh Kumar B
 *
 */
@Component
public class OrdersExternalAPI {

	@Autowired
	private RestTemplate template;  
	
	/**
	 * @param url
	 * @return external api get call
	 */
	public Object getApiCall(String url) {
		return template.getForEntity(url, Object.class);
	}
}
