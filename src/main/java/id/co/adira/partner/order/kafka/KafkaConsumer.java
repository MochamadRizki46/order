//package id.co.adira.partner.order.kafka;
//
//import java.util.HashMap;
//import java.util.List;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.kafka.support.KafkaHeaders;
//import org.springframework.messaging.handler.annotation.Header;
//import org.springframework.stereotype.Component;
//
//import id.co.adira.partner.order.config.BeanConfig;
//import id.co.adira.partner.order.dto.kafka.ApplicationDistributedDTO;
//import id.co.adira.partner.order.dto.kafka.CustomerDistributedDTO;
//import id.co.adira.partner.order.dto.kafka.GenerateApplNoResDTO;
//import id.co.adira.partner.order.dto.kafka.GenerateApplNoV2ResDTO;
//import id.co.adira.partner.order.dto.kafka.PurchaseOrderDistributedDTO;
//import id.co.adira.partner.order.dto.kafka.TrackingOrderDistributeDTO;
//import id.co.adira.partner.order.exception.AdiraCustomException;
//import id.co.adira.partner.order.service.LogService;
//import id.co.adira.partner.order.service.OrderService;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import brave.Tracer;
//
//@Component
//class KafkaConsumer {
//
//	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);
//
//	@Value("${spring.kafka.topic.lead-info}")
//	private String leadInfoTopic;
//
//	@Value("${spring.kafka.topic.lead-info-v2}")
//	private String leadInfoTopicV2;
//
//	@Value("${spring.kafka.topic.lead-tracking-distributed}")
//	private String leadTrackingDistribute;
//
//	@Value("${spring.kafka.topic.lead-application-distributed}")
//	private String leadApplicationDistribute;
//
//	@Value("${spring.kafka.topic.sales-customer-distributed}")
//	private String salesCustomerDistribute;
//
//	@Value("${spring.kafka.topic.po-distributed}")
//	private String purchesOrderDistribute;
//
//	@Autowired
//	private OrderService orderService;
//
//	@Autowired
//	private LogService logService;
//
//	@Autowired
//	private Tracer tracer;
//
//	ObjectMapper objectMapper = new BeanConfig().getObjectMapper();
//
//	private String errorMessage;
//
//	/**
//	 * @param message
//	 * @param partitions
//	 * @param topics
//	 * @param offsets
//	 * @throws JsonMappingException
//	 * @throws JsonProcessingException
//	 * @throws AdiraCustomException    update order status through kafka
//	 */
//	@KafkaListener(topics = "${spring.kafka.topic.lead-info}")
//	public void processMessage(String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) List<Integer> partitions,
//			@Header(KafkaHeaders.RECEIVED_TOPIC) List<String> topics, @Header(KafkaHeaders.OFFSET) List<Long> offsets)
//			throws JsonProcessingException, AdiraCustomException {
//
//		LOGGER.debug("%s-%d[%d] \"%s\"\n", topics.get(0), partitions.get(0), offsets.get(0), message);
//		LOGGER.info("Message Consumed {} on topic {} ", message, leadInfoTopic);
//
//		String traceid = tracer.currentSpan().context().traceIdString();
//
//		LOGGER.info(String.format("#### -> Trace Id For Update Application No : %s ", traceid));
//
//		GenerateApplNoResDTO generatApplNo = new GenerateApplNoResDTO();
//		errorMessage = "";
//
//		try {
//			LOGGER.info("Start Mapping Update Appl No");
//			generatApplNo = objectMapper.readValue(message, GenerateApplNoResDTO.class);
//			LOGGER.info(generatApplNo.getApplNo());
//			orderService.updateApplNumber(generatApplNo);
//
//		} catch (Exception ex) {
//			LOGGER.error("Error Consuming the message from topic {} ", leadInfoTopic);
//			errorMessage = ex.getMessage();
//		} finally {
//			logService.insertConsumerLogger(message, leadInfoTopic, errorMessage);
//		}
//
//	}
//
//	@KafkaListener(topics = "${spring.kafka.topic.lead-info-v2}")
//	public void processMessageV2(String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) List<Integer> partitions,
//			@Header(KafkaHeaders.RECEIVED_TOPIC) List<String> topics, @Header(KafkaHeaders.OFFSET) List<Long> offsets)
//			throws JsonProcessingException, AdiraCustomException {
//
//		LOGGER.debug("%s-%d[%d] \"%s\"\n", topics.get(0), partitions.get(0), offsets.get(0), message);
//		LOGGER.info("Message Consumed {} on topic {} ", message, leadInfoTopicV2);
//
//		String traceid = tracer.currentSpan().context().traceIdString();
//
//		LOGGER.info(String.format("#### -> Trace Id For Update Application No : %s ", traceid));
//
//		GenerateApplNoV2ResDTO generatApplNo;
//		errorMessage = "";
//
//		try {
//			LOGGER.info("Start Mapping Update Appl No");
//			generatApplNo = objectMapper.readValue(message, GenerateApplNoV2ResDTO.class);
//			LOGGER.info(generatApplNo.getApplicationPayung());
//			orderService.updateApplNumberV2(generatApplNo);
//
//		} catch (Exception ex) {
//
//			LOGGER.error("Error Consuming the message from topic {} ", leadInfoTopic);
//			errorMessage = ex.getMessage();
//		} finally {
//			logService.insertConsumerLogger(message, leadInfoTopic, errorMessage);
//		}
//
//	}
//
//	@KafkaListener(topics = "${spring.kafka.topic.lead-tracking-distributed}")
//	public void processMessageDistributeTracking(String message,
//			@Header(KafkaHeaders.RECEIVED_PARTITION_ID) List<Integer> partitions,
//			@Header(KafkaHeaders.RECEIVED_TOPIC) List<String> topics, @Header(KafkaHeaders.OFFSET) List<Long> offsets)
//			throws JsonProcessingException, AdiraCustomException {
//
//		LOGGER.debug("%s-%d[%d] \"%s\"\n", topics.get(0), partitions.get(0), offsets.get(0), message);
//		LOGGER.info("Message Consumed {} on topic {} ", message, leadTrackingDistribute);
//
//		String traceid = tracer.currentSpan().context().traceIdString();
//
//		LOGGER.info(String.format("#### -> Trace Id For Insert or Update Tabel Distributed Tracking : %s ", traceid));
//
//		var trackingOrderDistributed = new TrackingOrderDistributeDTO();
//		var errorMessageTracking = "";
//
//		try {
//			LOGGER.info("Start Mapping Distributed Tracking");
//
//			trackingOrderDistributed = objectMapper.readValue(message, TrackingOrderDistributeDTO.class);
//			LOGGER.info(trackingOrderDistributed.getApplicationTrackId());
//
//			orderService.sendNotificationTracking(trackingOrderDistributed);
//
//			orderService.distributedTrackingOrder(trackingOrderDistributed);
//
//		} catch (Exception ex) {
//
//			LOGGER.error("Error Consuming the message from topic {} ", leadTrackingDistribute);
//			errorMessageTracking = ex.getMessage();
//		} finally {
//			logService.insertConsumerLogger(message, leadTrackingDistribute, errorMessageTracking);
//		}
//
//	}
//
//	@KafkaListener(topics = "${spring.kafka.topic.lead-application-distributed}")
//	public void processMessageDistributeApplication(String message,
//			@Header(KafkaHeaders.RECEIVED_PARTITION_ID) List<Integer> partitions,
//			@Header(KafkaHeaders.RECEIVED_TOPIC) List<String> topics, @Header(KafkaHeaders.OFFSET) List<Long> offsets)
//			throws JsonProcessingException, AdiraCustomException {
//
//		LOGGER.debug("%s-%d[%d] \"%s\"\n", topics.get(0), partitions.get(0), offsets.get(0), message);
//		LOGGER.info("Message Consumed {} on topic {} ", message, leadApplicationDistribute);
//
//		String traceid = tracer.currentSpan().context().traceIdString();
//
//		LOGGER.info(String.format("#### -> Trace Id For Insert or Update Tabel Distibute Application : %s ", traceid));
//
//		var applicationDistributed = new ApplicationDistributedDTO();
//		var errorMessageApplication = "";
//		try {
//			var data = objectMapper.readValue(message, HashMap.class);
//
//			LOGGER.info("Start Mapping Distributed Application");
//
//			applicationDistributed = objectMapper.convertValue(data.get("data"), ApplicationDistributedDTO.class);
//
//			orderService.sendNotificationSurvey(applicationDistributed);
//			LOGGER.info(applicationDistributed.getApplicationNo());
//			orderService.distributedApplication(applicationDistributed);
//
//		} catch (Exception ex) {
//
//			LOGGER.error("Error Consuming the message from topic {} ", leadApplicationDistribute);
//			errorMessageApplication = ex.getMessage();
//		} finally {
//			logService.insertConsumerLogger(message, leadApplicationDistribute, errorMessageApplication);
//		}
//
//	}
//
//	@KafkaListener(topics = "${spring.kafka.topic.sales-customer-distributed}")
//	public void processMessageDistributeCustomer(String message,
//			@Header(KafkaHeaders.RECEIVED_PARTITION_ID) List<Integer> partitions,
//			@Header(KafkaHeaders.RECEIVED_TOPIC) List<String> topics, @Header(KafkaHeaders.OFFSET) List<Long> offsets)
//			throws JsonProcessingException, AdiraCustomException {
//
//		LOGGER.debug("%s-%d[%d] \"%s\"\n", topics.get(0), partitions.get(0), offsets.get(0), message);
//		LOGGER.info("Message Consumed {} on topic {} ", message, salesCustomerDistribute);
//
//		String traceid = tracer.currentSpan().context().traceIdString();
//
//		LOGGER.info(String.format("#### -> Trace Id For Insert or Update Tabel Distibute Customer : %s ", traceid));
//
//		var customerDistributed = new CustomerDistributedDTO();
//		var errorMessageCustomer = "";
//		try {
//			LOGGER.info("Start Mapping Distributed Customer");
//			customerDistributed = objectMapper.readValue(message, CustomerDistributedDTO.class);
//			LOGGER.info(customerDistributed.getCustomerId());
//			orderService.distributedCustomer(customerDistributed);
//
//		} catch (Exception ex) {
//
//			LOGGER.error("Error Consuming the message from topic {} ", salesCustomerDistribute);
//			errorMessageCustomer = ex.getMessage();
//		} finally {
//			logService.insertConsumerLogger(message, salesCustomerDistribute, errorMessageCustomer);
//		}
//
//	}
//
//	@KafkaListener(topics = "${spring.kafka.topic.po-distributed}")
//	public void processMessageDistributePO(String message,
//			@Header(KafkaHeaders.RECEIVED_PARTITION_ID) List<Integer> partitions,
//			@Header(KafkaHeaders.RECEIVED_TOPIC) List<String> topics, @Header(KafkaHeaders.OFFSET) List<Long> offsets)
//			throws JsonProcessingException, AdiraCustomException {
//
//		LOGGER.debug("%s-%d[%d] \"%s\"\n", topics.get(0), partitions.get(0), offsets.get(0), message);
//		LOGGER.info("Message Consumed {} on topic {} ", message, purchesOrderDistribute);
//
//		String traceid = tracer.currentSpan().context().traceIdString();
//
//		LOGGER.info(String.format("#### -> Trace Id For Insert or Update Tabel Distibute PO : %s ", traceid));
//
//		var purchaseOrderDistributed = new PurchaseOrderDistributedDTO();
//		errorMessage = "";
//		try {
//			LOGGER.info("Start Mapping Distributed Customer");
//			purchaseOrderDistributed = objectMapper.readValue(message, PurchaseOrderDistributedDTO.class);
//			LOGGER.info(purchaseOrderDistributed.getPoNo());
//			orderService.distributedPO(purchaseOrderDistributed);
//
//		} catch (Exception ex) {
//			LOGGER.error("Error Consuming the message from topic {} ", purchesOrderDistribute);
//			errorMessage = ex.getMessage();
//		} finally {
//			logService.insertConsumerLogger(message, purchesOrderDistribute, errorMessage);
//		}
//
//	}
//
//}
