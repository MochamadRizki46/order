package id.co.adira.partner.order.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.adira.partner.order.config.BeanConfig;
import id.co.adira.partner.order.dto.SendOrderToLeadReqDTO;
import id.co.adira.partner.order.dto.SendOrderToLeadV2ReqDTO;
import id.co.adira.partner.order.dto.kafka.UpdateStatusTrackingDTO;
import id.co.adira.partner.order.service.LogService;

@Component
public class KafkaProducer {
	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);

	private final KafkaTemplate<String, String> kafkaTemplate;

	@Value("${spring.kafka.topic.lead-generation}")
	private String topicLeadGeneration;

	@Value("${spring.kafka.topic.lead-generation-v2}")
	private String topicLeadGenerationv2;

	@Value("${spring.kafka.topic.lead-tracking-updated}")
	private String topicLeadTrackingUpdate;

	ObjectMapper objectMapper = new BeanConfig().getObjectMapper();

	KafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	@Autowired
	private LogService logService;

	/**
	 * Lead Generation
	 */
	public String sendLeadGeneration(SendOrderToLeadReqDTO message) throws JsonProcessingException {

		String response = "";

		try {
			String finalMsg = objectMapper.writeValueAsString(message);
			this.kafkaTemplate.send(topicLeadGeneration, finalMsg);
			LOGGER.info("Sent Data Order to Kafka  [' {} '] to {}", finalMsg, topicLeadGeneration);
		} catch (Exception e) {
			LOGGER.info("Error when sending Lead Generation to Kafka: {}", e.getMessage());
			response = e.toString();
		} finally {
			logService.insertProducerLogger(objectMapper.writeValueAsString(message), topicLeadGeneration, response);
		}
		return response;
	}

	public String sendLeadGenerationV2(SendOrderToLeadV2ReqDTO message) throws JsonProcessingException {

		String response = "";

		try {
			String finalMsg = objectMapper.writeValueAsString(message);
			this.kafkaTemplate.send(topicLeadGenerationv2, finalMsg);
			LOGGER.info("Sent Data Order to Kafka  [' {} '] to {}", finalMsg, topicLeadGenerationv2);
		} catch (Exception e) {
			LOGGER.info("Error when sending Lead Generation to Kafka: {}", e.getMessage());
			response = e.toString();
		} finally {
			logService.insertProducerLogger(objectMapper.writeValueAsString(message), topicLeadGenerationv2, response);
		}
		return response;
	}

	public String updateTrackingStatusLe(UpdateStatusTrackingDTO message) throws JsonProcessingException {

		var response = "";

		try {
			String finalMsg = objectMapper.writeValueAsString(message);
			this.kafkaTemplate.send(topicLeadTrackingUpdate, finalMsg);
			LOGGER.info("Sent Data Lead Update Tracking to Kafka  [' {} '] to {}", finalMsg, topicLeadTrackingUpdate);
		} catch (Exception e) {
			LOGGER.info("Error when send Lead Update Tracking to Kafka: {}", e.getMessage());
			response = e.toString();
		} finally {
			logService.insertProducerLogger(objectMapper.writeValueAsString(message), topicLeadTrackingUpdate, response);
		}
		return response;
	}

	/**
	 * @param message
	 * @throws JsonProcessingException update quantity in products through kafka
	 */

}
