/**
 * 
 */
package id.co.adira.partner.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.ApplicationDocument;

/**
 * @author Vionza
 *
 */
@Repository
public interface ApplicationDocumentRepository extends JpaRepository<ApplicationDocument, String> {

}
