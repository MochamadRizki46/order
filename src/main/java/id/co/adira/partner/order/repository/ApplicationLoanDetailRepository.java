/**
 * 
 */
package id.co.adira.partner.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.ApplicationLoanDetail;

/**
 * @author Vionza
 *
 */
@Repository
public interface ApplicationLoanDetailRepository extends JpaRepository<ApplicationLoanDetail, String> {

}
