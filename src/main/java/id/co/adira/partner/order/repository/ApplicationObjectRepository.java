/**
 * 
 */
package id.co.adira.partner.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.ApplicationObject;

/**
 * @author Vionza
 *
 */
@Repository
public interface ApplicationObjectRepository extends JpaRepository<ApplicationObject, String> {

}
