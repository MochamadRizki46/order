/**
 * 
 */
package id.co.adira.partner.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.Application;

/**
 * @author Vionza
 *
 */
@Repository
public interface ApplicationRepository extends JpaRepository<Application, String> {

	@Query("select o from Application o where o.applicationNo = :applNo")
	Application getByNoApplication(@Param("applNo") String applNo);
}
