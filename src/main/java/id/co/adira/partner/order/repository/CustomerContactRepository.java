/**
 * 
 */
package id.co.adira.partner.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.CustomerContact;

/**
 * @author Vionza
 *
 */
@Repository
public interface CustomerContactRepository extends JpaRepository<CustomerContact, String> {

}
