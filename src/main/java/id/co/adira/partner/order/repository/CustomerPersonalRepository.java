/**
 * 
 */
package id.co.adira.partner.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.CustomerPersonal;

/**
 * @author Vionza
 *
 */
@Repository
public interface CustomerPersonalRepository extends JpaRepository<CustomerPersonal, String> {

}
