/**
 * 
 */
package id.co.adira.partner.order.repository;

import javax.persistence.Tuple;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.Customer;

/**
 * @author Vionza
 *
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {

	@Query("select (case when c.customerType = 'PER' then cp.identityFullName else cc.companyName end) as customerName "
			+ "from Application a, Customer c "
			+ "left join CustomerPersonal cp on (c.skCustomer = cp.fkCustomer and cp.active = true) "
			+ "left join CustomerCompany cc on (c.skCustomer = cc.fkCustomer and cc.active = true) "
			+ "where a.fkCustomer = c.customerId "
			+ "and a.active = true "
			+ "and c.active = true "
			+ "and a.applicationNo = :applicationNo")
	Tuple findCustomerNameByApplicationNo(@Param("applicationNo") String applicationNo);
	
}

