/**
 * 
 */
package id.co.adira.partner.order.repository;


import java.util.List;

import javax.persistence.Tuple;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.dto.fcm.LatesTrackingForFCMResponsesDTO;
import id.co.adira.partner.order.entity.Application;
import id.co.adira.partner.order.entity.ApplicationTracking;
import id.co.adira.partner.order.entity.ParaApplicationStatus;

/**
 * @author Vionza
 *
 */
@Repository
public interface DistributedApplicationTrackingRepository extends JpaRepository<ApplicationTracking, String> {

	@Query("select new id.co.adira.partner.order.dto.fcm.LatesTrackingForFCMResponsesDTO(a.createdBy as userId, a.orderNo, a.applicationNo, ao.applicationNoUnit, pas.applGroupStatus, pas.applStatus) "
			+ "from Application a, ApplicationObject ao, ApplicationTracking at "
			+ "left join ParaApplicationStatus pas "
			+ "on at.groupStatus = pas.applGroupStatusId "
			+ "and at.status = pas.applStatusId "
			+ "and pas.squad = 'PARTNER'"
			+ "inner join SubmitOrder so on so.applNoPayung = a.applicationNo "
			+ "where a.skApplication = ao.fkApplication "
			+ "and a.applicationNo = at.fkApplicationId "
			+ "and at.sequence = (select max(at2.sequence) from ApplicationTracking at2 where at2.fkApplicationId = :applNo) "
			+ "and a.applicationNo = :applNo")
	List<LatesTrackingForFCMResponsesDTO> findLatesGroupStatus(@Param("applNo") String applNo);
	
	@Query("select pas from ParaApplicationStatus pas where pas.squad = 'PARTNER' and pas.applStatusId = :status and pas.applGroupStatusId = :groupStatus")
	ParaApplicationStatus findDescTrackingByGroupAndStatus(@Param("status") String status, @Param("groupStatus") String groupStatus);
	
	@Query("select o.surveyAppointmentDate as surveydate from Application o where o.applicationNo = :applNo")
	Tuple findSurveyDateByApplNo(@Param("applNo") String applNo);
	
}
