/**
 * 
 */
package id.co.adira.partner.order.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.swing.text.Document;

import id.co.adira.partner.order.entity.SubmitOrder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.DocumentDetail;

/**
 * @author Vionza
 *
 */
@Repository
public interface DocumentRepository extends JpaRepository<DocumentDetail, String> {

	@Query("select t from DocumentDetail t where t.submitOrder = :submitOrder")
	List<DocumentDetail> findAllBySubmitOrder(SubmitOrder submitOrder);

	@Query("SELECT dt FROM DocumentDetail dt WHERE dt.submitOrder = :submitOrder")
	DocumentDetail findDocumentDetailBySubmitOrder(SubmitOrder submitOrder);

	@Query("SELECT dt FROM DocumentDetail dt WHERE dt.id = :id")
	DocumentDetail findDocumentDetailById(String id);

}
