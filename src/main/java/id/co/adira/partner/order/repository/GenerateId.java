/**
 * 
 */
package id.co.adira.partner.order.repository;


import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import id.co.adira.partner.order.dto.GenerateOrderReq;


/**
 * @author 10999943
 *
 */
@Component
public class GenerateId {
	
	private SessionFactory sessionFactory;
	

	@Autowired
	public GenerateId(EntityManagerFactory entityManagerFactory) {
		if (entityManagerFactory.unwrap(SessionFactory.class) == null) {
			throw new NullPointerException("Not session factory class");
		}
		sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
	}

	@Transactional
	public String generateOrderId(GenerateOrderReq orderReq) {
		Session session = sessionFactory.openSession();
		String result = "";
		try {
			ProcedureCall call = session.createStoredProcedureCall("generateorderid");
			call.registerParameter("ROLE_ID", String.class, ParameterMode.IN).bindValue(orderReq.getRoleId());
			call.registerParameter("id", String.class, ParameterMode.OUT);
			result =  call.getOutputParameterValue("id").toString();
		}catch (Exception e) {
//			e.printStackTrace();
		}finally {
			session.close();
		}
		return result;
	}
}
