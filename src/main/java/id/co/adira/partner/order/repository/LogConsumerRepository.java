/**
 * 
 */
package id.co.adira.partner.order.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import id.co.adira.partner.order.entity.LogConsumer;

/**
 * @author Vionza
 *
 */
public interface LogConsumerRepository extends JpaRepository<LogConsumer, Integer> {

	@Query("select o from LogConsumer o where o.status='ERROR' and o.flagStatus<=:count and o.topic =:topic and to_char(o.createdDate,'yyyy-MM-dd') >= :date order by id asc")
	public List<LogConsumer> getMsgLogErrorByTopic(Integer count, String topic, String date);

	@Modifying
	@Query("update LogConsumer o set o.status=:status, o.message=:err, o.flagStatus=:count, o.modifiedDate=now() where o.id=:id")
	public void updateData(Long id, String status, String err, Integer count);
}
