/**
 * 
 */
package id.co.adira.partner.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.adira.partner.order.entity.LogProducer;

/**
 * @author Vionza
 *
 */
public interface LogProducerRepository extends JpaRepository<LogProducer, Integer> {

}
