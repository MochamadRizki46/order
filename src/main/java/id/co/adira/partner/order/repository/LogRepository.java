package id.co.adira.partner.order.repository;

import id.co.adira.partner.order.entity.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface LogRepository extends JpaRepository<Log, String> {
    @Query("select min(a) FROM Log a where a.active = 0 group by a.processDate order by a.processDate desc")
    List<Log> getLogData();

    @Modifying
    @Query("update Log a set a.active = 1 where a.logId = :logId")
    void inactivePreviousLogData(String logId);

    @Query("select a from Log a where a.userId = :pic")
    Log getLogByUserId(String pic);
}
