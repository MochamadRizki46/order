/**
 * 
 */
package id.co.adira.partner.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.adira.partner.order.entity.PurchaseOrder;

/**
 * @author Vionza
 *
 */
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, String> {

}
