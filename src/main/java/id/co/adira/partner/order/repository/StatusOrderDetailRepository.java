package id.co.adira.partner.order.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import id.co.adira.partner.order.dto.TrackingOrderHistoryResDTO;
import id.co.adira.partner.order.entity.StatusOrderDetail;

@Repository
public interface StatusOrderDetailRepository extends JpaRepository<StatusOrderDetail, String> {

	@Query("select new id.co.adira.partner.order.dto.TrackingOrderHistoryResDTO(o.applNoUnit, o.orderNo, to_char(o.statusDate, 'dd-MON-yyyy'), o.statusTime, o.status, o.substatus, o.idSeq) from StatusOrderDetail o where trim(o.applNoUnit) = :applNoUnit and o.userType = :userType order by o.idSeq desc")
	List<TrackingOrderHistoryResDTO> getHistoryByApplNo(@Param("applNoUnit") String applNoUnit, @Param("userType") String userType);
	
	@Query("select o from StatusOrderDetail o where trim(o.applNoUnit) = :applNoUnit and o.userType = :userType order by o.idSeq desc")
	List<StatusOrderDetail> findAllByApplNo(@Param("applNoUnit") String applNoUnit, @Param("userType") String userType);
	
	@Transactional
	@Modifying
	@Query("update StatusOrderDetail o set o.applNoUnit = :applNoUnit where o.orderNo = :orderNo and o.idSeq = 1")
	void updateApplNoUnit(@Param("applNoUnit") String applNoUnit, @Param("orderNo") String orderNo);

	@Transactional
	@Modifying
	@Query("update StatusOrderDetail o set o.substatus = 'Selesai' where o.orderNo = :orderNo and o.idSeq = 2")
	void updateSubStatusVer(@Param("orderNo") String orderNo);
}
