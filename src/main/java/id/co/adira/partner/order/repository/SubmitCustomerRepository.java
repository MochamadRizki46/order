/**
 * 
 */
package id.co.adira.partner.order.repository;

import id.co.adira.partner.order.entity.SubmitOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.SubmitCustomer;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 10999943
 *
 */
@Repository
@Transactional
public interface SubmitCustomerRepository extends JpaRepository<SubmitCustomer, String> {

	@Query("SELECT sc FROM SubmitCustomer sc WHERE sc.submitOrder = :submitOrder")
    SubmitCustomer findSubmitCustomerBySubmitOrder(SubmitOrder submitOrder);
}
