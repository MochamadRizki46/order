package id.co.adira.partner.order.repository;

import id.co.adira.partner.order.entity.SubmitOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.adira.partner.order.entity.SubmitDomisili;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface SubmitDomisiliRepository extends JpaRepository<SubmitDomisili, String> {

    @Query("SELECT sd FROM SubmitDomisili sd WHERE sd.submitOrder = :submitOrder")
    SubmitDomisili findSubmitDomisiliBySubmitOrder(SubmitOrder submitOrder);
}
