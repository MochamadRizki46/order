package id.co.adira.partner.order.repository;

import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.SubmitOrder;
import id.co.adira.partner.order.dto.TrackingSumOrderSlsResDTO;
import id.co.adira.partner.order.dto.TrackingOrderDetailResDTO;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface SubmitOrderRepository extends JpaRepository<SubmitOrder, String> {

	@Query("select new id.co.adira.partner.order.dto.TrackingOrderHomeResDTO(a.orderNo, a.applNoUnit, c.customerName, to_char(a.orderDate, 'dd MON yyyy'), a.status, "
			+ "case when b.object in ('001', '002') then 'MOTOR' when b.object in ('003', '004') then 'MOBIL' when b.object = '005' then 'DURABLE' when b.object = '014' then 'JASA' else '-' end as objectDesc,"
			+ " b.objectModelDesc, a.groupStatus) from SubmitOrder a, SubmitUnit b, SubmitCustomer c where a.submitOrderId = c.submitOrder and a.submitOrderId = b.submitOrder " + "and a.active = 0 "
			+ "and a.orderDate >= to_date(:date, 'dd-MM-yyyy') " + "and UPPER(a.pic) = UPPER(:userId) " + "order by a.orderNo")
	List<TrackingOrderHomeResDTO> getListTrackingSales(@Param("date") String date, @Param("userId") String userId);

	@Query("select new id.co.adira.partner.order.dto.TrackingOrderDetailResDTO(d.orderNo, a.idNo, a.customerName, COALESCE(a.handphoneNo,'0000-000-000-000'), COALESCE(a.waNo,'0000-000-000-000'), "
			+ "b.addressDomisili, b.kelurahanDesc ||', '||b.kecamatanDesc ||', '||b.kabkotDesc ||', '||b.zipcode as addressDetail, b.kelurahanDesc as kelurahan, to_char(b.surveyDate, 'dd MON yyyy') ||' '||b.surveyTime as surveyDateTime, "
			+ "c.objectDesc ||' '||c.objectModelDesc as objectDetail, "
			+ "c.objectPrice, c.dp, c.tenor, c.installmentAmt, COALESCE(c.promo,' '), c.object, c.rate, COALESCE(c.objectYear,' '), c.tipeJaminan, c.jenisAsuransi, c.objectBrandDesc as merk ) "
			+ "from SubmitCustomer a, SubmitDomisili b, SubmitUnit c, SubmitOrder d where d.submitOrderId = a.submitOrder "
			+ "and d.submitOrderId = c.submitOrder and d.applNoUnit = :applNo and a.active = 0 and b.active = 0 "
			+ "and c.active = 0 and d.active = 0 and d.submitOrderId = b.submitOrder")

	TrackingOrderDetailResDTO getTrackingOrderDetail(@Param("applNo") String applNo);

	@Query("select new id.co.adira.partner.order.dto.TrackingSumOrderSlsResDTO(count(1), "
			+ "sum(case when tso.groupStatus in ('SVR','SVY','SCA') and COALESCE(tso.applNoPayung,'') <> '' then 1 else 0 end) as berlangsung, "
			+ "sum(case when tso.groupStatus in ('SPO','SPP','SOD') then 1 else 0 end) as po, "
			+ "sum(case when tso.groupStatus = 'SRJ' then 1 else 0 end) as ditolak, "
			+ "sum(case when tso.groupStatus = 'SCO' then 1 else 0 end) as batal) " + "from SubmitOrder tso "
			+ "where tso.active = 0 and tso.status is not null and tso.status <> 'Draft' " + "and UPPER(tso.pic) = UPPER(:userId) "
			+ "and tso.orderDate >= date_trunc('month', current_date)")
	TrackingSumOrderSlsResDTO getTrackingSumSls(@Param("userId") String userId);

	@Query("SELECT so FROM SubmitOrder so WHERE so.status = :status AND UPPER(so.pic) = UPPER(:pic) AND so.active = 0")
	List<SubmitOrder> findSubmitOrdersByStatusAndPic(String status, String pic);

	@Query("SELECT so FROM SubmitOrder so WHERE so.status = :status AND so.orderNo = :orderNo and so.active = 0")
	SubmitOrder findSubmitOrderByStatusAndOrderNo(String status, String orderNo);

	@Query("SELECT so FROM SubmitOrder so WHERE so.orderNo = :orderNo and so.active = 0")
	SubmitOrder findSubmitOrderByOrderNo(String orderNo);

	@Query("SELECT so FROM SubmitOrder so WHERE so.applNoUnit = :applNoUnit and so.active = 0")
	SubmitOrder findSubmitOrderByApplNoUnit(String applNoUnit);

	@Query("SELECT so FROM SubmitOrder so WHERE so.orderDate >= to_date(:filterDate, 'dd-MM-yyyy') AND UPPER(so.pic) = UPPER(:pic) and so.active = 0")
	List<SubmitOrder> findSubmitOrdersByFlagDateAndPic(String filterDate, String pic);
	
	@Transactional
	@Modifying
	@Query("update SubmitOrder so set so.active = 1 where so.orderNo = :orderNo")
	void deleteDraft(@Param("orderNo") String orderNo);

	@Query("SELECT so FROM SubmitOrder so WHERE so.orderDate BETWEEN :fromDate AND :currentDate AND so.status = :status AND UPPER(so.pic) = UPPER(:pic) AND so.active = 0 ORDER BY so.orderDate ASC")
	List<SubmitOrder> findSubmitOrderByFilterDateStatusAndPicOrderByOrderDateAsc(Date fromDate, Date currentDate, String status, String pic);

	@Query("SELECT so FROM SubmitOrder so WHERE so.orderDate BETWEEN :fromDate AND :currentDate AND so.status = :status AND UPPER(so.pic) = UPPER(:pic) AND so.active = 0 ORDER BY so.orderDate DESC")
	List<SubmitOrder> findSubmitOrderByFilterDateStatusAndPicOrderByOrderDateDesc(Date fromDate, Date currentDate, String status, String pic);

	class TrackingOrderHomeResDTO {
	}
}
