/**
 * 
 */
package id.co.adira.partner.order.repository;

import id.co.adira.partner.order.entity.SubmitOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.adira.partner.order.entity.SubmitUnit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author 10999943
 *
 */
@Repository
@Transactional
public interface SubmitUnitRepository extends JpaRepository<SubmitUnit, String> {

    @Query("SELECT su FROM SubmitUnit su WHERE su.submitOrder = :submitOrder")
    SubmitUnit findSubmitUnitBySubmitOrder(SubmitOrder submitOrder);

}
