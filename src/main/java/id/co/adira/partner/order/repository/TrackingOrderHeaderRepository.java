package id.co.adira.partner.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.order.entity.SubmitOrder;
import id.co.adira.partner.order.dto.TrackingOrderHeaderResDTO;

@Repository
public interface TrackingOrderHeaderRepository extends JpaRepository<SubmitOrder, String> {

	@Query("select new id.co.adira.partner.order.dto.TrackingOrderHeaderResDTO(a.orderNo, a.applNoUnit, c.customerName, to_char(a.orderDate, 'dd MON yyyy'), "
			+ "a.status, COALESCE(b.object, ' ') , COALESCE(b.objectDesc, ' '), COALESCE(b.objectModel,' '), COALESCE(b.objectModelDesc, ' '),"
			+ " d.namaSo, to_char(d.surveyDate, 'yyyy-MM-dd'), d.surveyTime ) "
			+ " from SubmitOrder a, SubmitUnit b , SubmitCustomer c, SubmitDomisili d where a.submitOrderId = b.submitOrder and a.submitOrderId = d.submitOrder "
			+ " and a.submitOrderId = c.submitOrder and a.active = 0 and a.applNoUnit = :applNoUnit")
	TrackingOrderHeaderResDTO getTrackingOrderHeader(@Param("applNoUnit") String applNoUnit);




}