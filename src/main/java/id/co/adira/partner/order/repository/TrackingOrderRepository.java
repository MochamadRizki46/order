package id.co.adira.partner.order.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import id.co.adira.partner.order.dto.TrackingDashboardSalesResDTO;
import id.co.adira.partner.order.entity.Application;


@Repository
public interface TrackingOrderRepository extends JpaRepository<Application, String> {
	@Query("select new id.co.adira.partner.order.dto.TrackingDashboardSalesResDTO("
			+ " count(1) as orderTerkirim, "
			+ " sum(case when pas.applGroupStatus in ('Verifikasi','Survey','Analisis Kredit') then 1 else 0 end) as berlangsung, "
			+ " sum(case when pas.applGroupStatus in ('PO','Proses Penagihan','Sudah Cair') then 1 else 0 end) as po, "
			+ " 0 as ditolak, "
			+ " 0 as batal )"
			+ " from Application b "
			+ " inner join ParaApplicationStatus pas "
			+ " on pas.applStatusId = b.applicationLastStatus "
			+ " and pas.applGroupStatusId = b.applicationLastGroupStatus "
			+ " and pas.squad = 'PARTNER' "
			+ " where b.createdBy = :userId "
			+ " and b.applicationNo is not null "
			+ " and b.applicationLastStatus is not null  "
			+ " and b.applicationLastGroupStatus is not null "
			+ " and b.orderDate >= date_trunc('month', to_date(:paramDate, 'dd-MM-yyyy')) "
			)
	TrackingDashboardSalesResDTO dashboardSales(@Param("paramDate") String paramDate, @Param("userId") String userId);
}
