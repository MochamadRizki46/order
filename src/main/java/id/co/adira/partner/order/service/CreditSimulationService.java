package id.co.adira.partner.order.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.order.config.BeanConfig;
import id.co.adira.partner.order.constant.RequestAndResponseEnum;
import id.co.adira.partner.order.dto.DpOrPmtRequestDTOV2;
import id.co.adira.partner.order.dto.DpOrPmtResponseDTOV2;
import id.co.adira.partner.order.dto.creditsimulation.request.CreditSimulationNewReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.apikey.APIKeyRequestDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.*;
import id.co.adira.partner.order.dto.creditsimulation.request.dporpmt.DownPaymentOrPmtReqToCommonServiceDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.apikey.APIKeyHeadResponseDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.AsuransiCashAndCreditResDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.BiayaAdminResDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.CreditSimulationDetailResDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.CreditSimulationResHeaderDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.EffectiveOrLandingRateResDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPmtHeadResDTO;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.service.resttemplate.RestTemplateService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static id.co.adira.partner.order.constant.StatusConst.*;
import static id.co.adira.partner.order.constant.RequestAndResponseEnum.*;

@Service
public class CreditSimulationService {

	private final RestTemplateService restTemplateService;

	ObjectMapper objectMapper = new BeanConfig().getObjectMapper();
	ModelMapper modelMapper = new ModelMapper();

	private static final Logger LOGGER = LoggerFactory.getLogger(CreditSimulationService.class.getName());

	@Autowired
	public CreditSimulationService(RestTemplateService restTemplateService, ObjectMapper objectMapper) {
		this.restTemplateService = restTemplateService;
		this.objectMapper = objectMapper;
	}

	public DataResponse calculateCreditSimulation(CreditSimulationDetailReqDTO creditSimulationDetailReqDTO,
			String authorization) {
		DataResponse dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());

		CreditSimulationDetailResDTO creditSimulationDetailResDTO = restTemplateService
				.callPostRestApiForCalculateCredSim(authorization, creditSimulationDetailReqDTO);

		dataResponse.setData(creditSimulationDetailResDTO);
		return dataResponse;
	}

	public DataResponse calculateCreditSimulationNew(CreditSimulationNewReqDTO creditSimulationNewReqDTO)
			throws JsonProcessingException {
		DataResponse dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setStatus(1);
		dataResponse.setMessage(THERE_IS_SOMETHING_ERROR_IN_OUR_SYSTEM.getMessage());

		CreditSimulationResHeaderDTO creditSimulationResHeaderDTO = restTemplateService
				.callPostRestApiForCalculateCredSimNew(creditSimulationNewReqDTO);
		
		if (creditSimulationResHeaderDTO.getMessage().equalsIgnoreCase("SUCCESS")) {
			dataResponse.setStatus(0);
			dataResponse.setMessage(creditSimulationResHeaderDTO.getMessage());
			dataResponse.setData(creditSimulationResHeaderDTO.getData());
		}
		
		return dataResponse;
	}

	public DpOrPmtHeadResDTO getDpOrPmtCalculationData(DpOrPaymentRequestDTO dpOrPaymentRequestDTO)
			throws JsonProcessingException {
		APIKeyRequestDTO apiKeyRequestDTO = new APIKeyRequestDTO();
		apiKeyRequestDTO.setUserName(dpOrPaymentRequestDTO.getPic());
		APIKeyHeadResponseDTO apiKeyHeadResponseDTO = restTemplateService.callPostRestApiForGetApiKey(apiKeyRequestDTO);

		BiayaAdminReqDTO biayaAdminReqDTO = new BiayaAdminReqDTO(dpOrPaymentRequestDTO.getObjectCode(),
				dpOrPaymentRequestDTO.getProgramId(), dpOrPaymentRequestDTO.getObjectModelId(),
				dpOrPaymentRequestDTO.getTenor().toString(), BigDecimal.valueOf(dpOrPaymentRequestDTO.getOtr()));
		BiayaAdminResDTO biayaAdminResponse = restTemplateService.callRestTemplateToGetBiayaAdmin(biayaAdminReqDTO);

		AsuransiCashAndCreditReqDTO asuransiCashAndCreditReqDTO = new AsuransiCashAndCreditReqDTO(
				dpOrPaymentRequestDTO.getObjectCode(), dpOrPaymentRequestDTO.getObjectModelId(),
				biayaAdminReqDTO.getTenor(), dpOrPaymentRequestDTO.getProgramId(), dpOrPaymentRequestDTO.getOtr());
		AsuransiCashAndCreditResDTO asuransiCashAndCreditResDTO = restTemplateService
				.callRestTemplateToGetAsuransiCashAndCredit(asuransiCashAndCreditReqDTO);

		EffectiveOrLandingRateReqDTO effectiveOrLandingRateReqDTO = new EffectiveOrLandingRateReqDTO(
				dpOrPaymentRequestDTO.getObjectCode(), dpOrPaymentRequestDTO.getObjectModelId(),
				dpOrPaymentRequestDTO.getObjectTypeId(), dpOrPaymentRequestDTO.getPic(),
				BigDecimal.valueOf(dpOrPaymentRequestDTO.getTenor()), dpOrPaymentRequestDTO.getProgramId());
		EffectiveOrLandingRateResDTO effectiveOrLandingRateResDTO = restTemplateService
				.callRestTemplateToGetEffectiveOrLandingRate(effectiveOrLandingRateReqDTO);

		DownPaymentOrPmtReqToCommonServiceDTO downPaymentOrPmtReqToCommonServiceDTO = new DownPaymentOrPmtReqToCommonServiceDTO();
		downPaymentOrPmtReqToCommonServiceDTO.setAdminCash(dpOrPaymentRequestDTO.getCashOrCredit() == 1 ? null
				: biayaAdminResponse.getAdminCashFee().doubleValue());
		downPaymentOrPmtReqToCommonServiceDTO.setAdminCredit(dpOrPaymentRequestDTO.getCashOrCredit() == 0 ? null
				: biayaAdminResponse.getAdminCreditFee().doubleValue());
		downPaymentOrPmtReqToCommonServiceDTO.setTenor(dpOrPaymentRequestDTO.getTenor());
		downPaymentOrPmtReqToCommonServiceDTO.setOtr(dpOrPaymentRequestDTO.getOtr());
		downPaymentOrPmtReqToCommonServiceDTO.setUsername(dpOrPaymentRequestDTO.getPic());
		downPaymentOrPmtReqToCommonServiceDTO.setAsuransiCashRate(
				dpOrPaymentRequestDTO.getCashOrCredit() == 0 ? asuransiCashAndCreditResDTO.getAsuransiCashAmt()
						: BigDecimal.ZERO);
		downPaymentOrPmtReqToCommonServiceDTO.setAsuransiCreditRate(
				dpOrPaymentRequestDTO.getCashOrCredit() == 1 ? asuransiCashAndCreditResDTO.getAsuransiCreditAmt()
						: BigDecimal.ZERO);
		downPaymentOrPmtReqToCommonServiceDTO.setAngsuran(dpOrPaymentRequestDTO.getAngsuran());
		downPaymentOrPmtReqToCommonServiceDTO
				.setEffectiveRate(effectiveOrLandingRateResDTO.getEffectiveOrLandingRate().doubleValue() / 100);
		downPaymentOrPmtReqToCommonServiceDTO.setDpSystem(dpOrPaymentRequestDTO.getDpSystem());
		downPaymentOrPmtReqToCommonServiceDTO.setDpGross(dpOrPaymentRequestDTO.getDpGross());
		downPaymentOrPmtReqToCommonServiceDTO.setDiscount(dpOrPaymentRequestDTO.getDiscount().doubleValue());
		downPaymentOrPmtReqToCommonServiceDTO.setCreditType(dpOrPaymentRequestDTO.getCreditType());

		LOGGER.info("Lemparan ke Credsim: " + objectMapper.writeValueAsString(downPaymentOrPmtReqToCommonServiceDTO));
		return restTemplateService.callPostRestApiForGetCalculationDpOrPmt(downPaymentOrPmtReqToCommonServiceDTO,
				apiKeyHeadResponseDTO.getData().getApiKey());
	}

	public DataResponse getDpOrPmtCalculationDataV2(DpOrPmtRequestDTOV2 dpOrPmtRequestDTOV2) {
		DataResponse dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setStatus(1);
		dataResponse.setMessage(OK.getMessage());

		try {
			calculateDpOrPmtV2(dpOrPmtRequestDTOV2, dataResponse);
		} catch (Exception e) {
			insertErrorResponse(dataResponse, ERROR_WHILE_HITTING_DP_OR_PMT_API.getMessage(), e);
		}
		return dataResponse;
	}

	private void insertErrorResponse(DataResponse dataResponse, String message, Exception e) {
		dataResponse.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		dataResponse.setMessage(THERE_IS_SOMETHING_ERROR_IN_OUR_SYSTEM.getMessage());
		dataResponse.setData(null);

		LOGGER.error(message, e.getMessage());
	}

	private void calculateDpOrPmtV2(DpOrPmtRequestDTOV2 dpOrPmtRequestDTOV2, DataResponse dataResponse)
			throws JsonProcessingException {
		String requestBodyInString = objectMapper.writeValueAsString(dpOrPmtRequestDTOV2);
		String motorBaruCredit = RequestAndResponseEnum.MOTOR_BARU_CREDIT_REQUEST_BODY_FOR_CREDSIM.getMessage();

		if (requestBodyInString.equals(MOTOR_BARU_CREDIT_REQUEST_BODY_FOR_CREDSIM.getMessage()))
			dataResponse.setData(objectMapper.readValue(MOTOR_BARU_RESPONSE_BODY_FOR_CREDSIM.getMessage(),
					DpOrPmtResponseDTOV2.class));

		else if (requestBodyInString.equals(MOTOR_BARU_CASH_REQUEST_BODY_FOR_CREDSIM.getMessage()))
			dataResponse.setData(objectMapper.readValue(MOTOR_BARU_RESPONSE_BODY_FOR_CREDSIM.getMessage(),
					DpOrPmtResponseDTOV2.class));

		else if (requestBodyInString.equals(MOTOR_BEKAS_REQUEST_BODY_FOR_CREDSIM.getMessage()))
			dataResponse.setData(objectMapper.readValue(MOTOR_BEKAS_RESPONSE_BODY_FOR_CREDSIM.getMessage(),
					DpOrPmtResponseDTOV2.class));

		else if (requestBodyInString.equals(MOBIL_BEKAS_REQUEST_BODY_FOR_CREDSIM.getMessage()))
			dataResponse.setData(objectMapper.readValue(MOBIL_BEKAS_RESPONSE_BODY_FOR_CREDSIM.getMessage(),
					DpOrPmtResponseDTOV2.class));

		else if (requestBodyInString.equals(MOBIL_BARU_REQUEST_BODY_FOR_CREDSIM.getMessage()))
			dataResponse.setData(objectMapper.readValue(MOBIL_BARU_RESPONSE_BODY_FOR_CREDSIM.getMessage(),
					DpOrPmtResponseDTOV2.class));

		else if (requestBodyInString.equals(DURABLE_REQUEST_BODY_FOR_CREDSIM.getMessage()))
			dataResponse.setData(
					objectMapper.readValue(DURABLE_RESPONSE_BODY_FOR_CREDSIM.getMessage(), DpOrPmtResponseDTOV2.class));

		else
			dataResponse.setData(INVALID_REQUEST_DATA_FROM_CREDSIM.getMessage());
	}

}
