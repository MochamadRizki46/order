package id.co.adira.partner.order.service;

import id.co.adira.partner.order.constant.StatusConst;
import id.co.adira.partner.order.dto.*;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.entity.SubmitCustomer;
import id.co.adira.partner.order.entity.SubmitDomisili;
import id.co.adira.partner.order.entity.SubmitOrder;
import id.co.adira.partner.order.entity.SubmitUnit;
import id.co.adira.partner.order.repository.SubmitCustomerRepository;
import id.co.adira.partner.order.repository.SubmitDomisiliRepository;
import id.co.adira.partner.order.repository.SubmitOrderRepository;
import id.co.adira.partner.order.repository.SubmitUnitRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static id.co.adira.partner.order.constant.AppConst.DRAFT;
import static id.co.adira.partner.order.constant.FormattingConst.SECOND_SIMPLE_DATE_FORMAT;
import static id.co.adira.partner.order.constant.StatusConst.*;

@Service
public class DraftSubmitPartialService {

	private final SubmitOrderRepository submitOrderRepository;
	private final SubmitCustomerRepository submitCustomerRepository;
	private final SubmitDomisiliRepository submitDomisiliRepository;
	private final SubmitUnitRepository submitUnitRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(DraftSubmitPartialService.class);

	SimpleDateFormat secondSdf = new SimpleDateFormat(SECOND_SIMPLE_DATE_FORMAT.getMessage());

	@Autowired
	public DraftSubmitPartialService(SubmitOrderRepository submitOrderRepository,
			SubmitCustomerRepository submitCustomerRepository, SubmitDomisiliRepository submitDomisiliRepository,
			SubmitUnitRepository submitUnitRepository) {
		this.submitOrderRepository = submitOrderRepository;
		this.submitCustomerRepository = submitCustomerRepository;
		this.submitDomisiliRepository = submitDomisiliRepository;
		this.submitUnitRepository = submitUnitRepository;
	}

	public DataResponse getDraftSubmitPartialOrderByPic(String pic) {
		DataResponse dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());

		List<SubmitOrder> submitOrders = submitOrderRepository.findSubmitOrdersByStatusAndPic(DRAFT.getMessage(), pic);

		if (submitOrders.isEmpty())
			dataResponse.setData(new ArrayList<>());
		else
			dataResponse.setData(insertDraftSubmitOrders(submitOrders));

		return dataResponse;
	}

	private Object insertDraftSubmitOrders(List<SubmitOrder> submitOrders) {
		List<DraftSubmitOrderResDTO> draftSubmitOrderResDTOS = new ArrayList<>();

		for (SubmitOrder submitOrder : submitOrders) {
			SubmitCustomer submitCustomer = submitCustomerRepository.findSubmitCustomerBySubmitOrder(submitOrder);
			SubmitUnit submitUnit = submitUnitRepository.findSubmitUnitBySubmitOrder(submitOrder);

			DraftSubmitOrderResDTO draftSubmitOrderResDTO = new DraftSubmitOrderResDTO();

			draftSubmitOrderResDTO.setOrderNo(submitOrder.getOrderNo());

			draftSubmitOrderResDTO.setOrderDate(secondSdf.format(submitOrder.getOrderDate()));
			draftSubmitOrderResDTO.setCustName(submitCustomer != null ? submitCustomer.getCustomerName() : "");
			draftSubmitOrderResDTO.setStatus(submitOrder.getStatus());
			draftSubmitOrderResDTO.setObjectDesc(submitUnit != null ? submitUnit.getObjectDesc() : "");
			draftSubmitOrderResDTO.setObjectModelDesc(submitUnit != null ? submitUnit.getObjectModelDesc() : "");

			draftSubmitOrderResDTOS.add(draftSubmitOrderResDTO);
		}

		return draftSubmitOrderResDTOS;
	}

	public DataResponse deleteDraft(String orderNo) {
		DataResponse dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());
		try {
			submitOrderRepository.deleteDraft(orderNo);
			dataResponse.setData(SUCCESS.getMessage().toLowerCase());
		} catch (Exception e) {
			LOGGER.error(FAILED_TO_DELETE_DRAFT.getMessage() + e.getMessage());
			dataResponse.setData(null);
			dataResponse.setMessage(StatusConst.THERE_IS_SOMETHING_ERROR_IN_OUR_SYSTEM.getMessage());
		}
		return dataResponse;
	}

	public DataResponse getDraftSubmitPartialDetailByOrderNo(String orderNo) {
		DataResponse dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());

		SubmitPartialDetailResponse submitPartialDetailResponse = new SubmitPartialDetailResponse();

		SubmitOrder submitOrder = submitOrderRepository.findSubmitOrderByStatusAndOrderNo(DRAFT.getMessage(), orderNo);
		if (submitOrder == null)
			dataResponse.setData(new SubmitPartialDetailResponse());
		else
			dataResponse.setData(insertDetailSubmitPartial(submitPartialDetailResponse, submitOrder, orderNo));

		return dataResponse;
	}

	private Object insertDetailSubmitPartial(SubmitPartialDetailResponse submitPartialDetailResponse,
			SubmitOrder submitOrder, String orderNo) {
		SubmitCustomer submitCustomer = submitCustomerRepository.findSubmitCustomerBySubmitOrder(submitOrder);

		if (submitCustomer == null)
			submitPartialDetailResponse.setDraftDataKonsumen(new SubmitCustomerResDTO());
		else
			insertDraftDataKonsumenToResponse(submitCustomer, submitPartialDetailResponse);

		SubmitDomisili submitDomisili = submitDomisiliRepository.findSubmitDomisiliBySubmitOrder(submitOrder);
		
		if (submitDomisili == null)
			submitPartialDetailResponse.setDraftDataDomisili(new SubmitDomisiliResDTO());
		else
			insertDraftDataDomisiliToResponse(submitDomisili, submitPartialDetailResponse);

		SubmitUnit submitUnit = submitUnitRepository.findSubmitUnitBySubmitOrder(submitOrder);
		
		if (submitUnit == null)
			submitPartialDetailResponse.setDraftDataUnit(new SubmitUnitResDTO());
		else
			insertDraftDataUnitToResponse(submitUnit, submitPartialDetailResponse);

		submitPartialDetailResponse.setStatus(submitOrder.getStatus());

		return submitPartialDetailResponse;
	}

	private void insertDraftDataUnitToResponse(SubmitUnit submitUnit,
			SubmitPartialDetailResponse submitPartialDetailResponse) {
		SubmitUnitResDTO submitUnitResDTO = new SubmitUnitResDTO();

		submitUnitResDTO.setTipeJaminan(submitUnit.getTipeJaminan());
		submitUnitResDTO.setObjectPrice(submitUnit.getObjectPrice());
		submitUnitResDTO.setNotes(submitUnit.getNotes());
		submitUnitResDTO.setObject(submitUnit.getObject());
		submitUnitResDTO.setPromo(submitUnit.getPromo());
		submitUnitResDTO.setDp(submitUnit.getDp());
		submitUnitResDTO.setRate(submitUnit.getRate());
		submitUnitResDTO.setModelDetail(submitUnit.getModelDetail());
		submitUnitResDTO.setObjectYear(submitUnit.getObjectYear());
		submitUnitResDTO.setObjectDesc(submitUnit.getObjectDesc());
		submitUnitResDTO.setObjectModelDesc(submitUnit.getObjectModelDesc());
		submitUnitResDTO.setObjectType(submitUnit.getObjectType());
		submitUnitResDTO.setObjectModel(submitUnit.getObjectModel());
		submitUnitResDTO.setObjectBrand(submitUnit.getObjectBrand());
		submitUnitResDTO.setObjectBrandDesc(submitUnit.getObjectBrandDesc());
		submitUnitResDTO.setOrderNo(submitUnit.getSubmitOrder().getOrderNo());
		submitUnitResDTO.setTenor(submitUnit.getTenor());
		submitUnitResDTO.setActive(submitUnit.getActive());
		submitUnitResDTO.setObjectTypeDesc(submitUnit.getObjectTypeDesc());
		submitUnitResDTO.setJenisAsuransi(submitUnit.getJenisAsuransi());
		submitUnitResDTO.setKotaDomisili(submitUnit.getKotaDomisili());
		submitUnitResDTO.setInstallmentAmt(submitUnit.getInstallmentAmt());
		submitUnitResDTO.setCreatedBy(submitUnit.getCreatedBy());
		submitUnitResDTO.setModifiedBy(submitUnit.getModifiedBy());
		submitUnitResDTO.setFlagCredsim(submitUnit.getFlagCredsim());
		submitUnitResDTO.setInstallmentTypeId(submitUnit.getInstallmentTypeId());
		submitUnitResDTO.setInstallmentTypeDesc(submitUnit.getInstallmentTypeDesc());
		submitUnitResDTO.setProgramId(submitUnit.getProgramId());
		submitUnitResDTO.setProgramDesc(submitUnit.getProgramDesc());
		submitUnitResDTO.setPaymentTypeId(submitUnit.getPaymentTypeId());
		submitUnitResDTO.setPaymentTypeDesc(submitUnit.getPaymentTypeDesc());
		submitUnitResDTO.setFlagInsrAdmin(submitUnit.getFlagInsrAdmin());
		submitUnitResDTO.setAcuanHitung(submitUnit.getAcuanHitung());
		submitUnitResDTO.setKotaDomisiliId(submitUnit.getKotaDomisiliId());
		submitUnitResDTO.setModelDetailId(submitUnit.getModelDetailId());
		submitUnitResDTO.setInsrTloCoverage(submitUnit.getInsrTloCoverage());
		submitUnitResDTO.setInsrCompreCoverage(submitUnit.getInsrCompreCoverage());
		submitUnitResDTO.setInsrTypeId(submitUnit.getInsrTypeId());

		submitPartialDetailResponse.setDraftDataUnit(submitUnitResDTO);
	}

	private void insertDraftDataDomisiliToResponse(SubmitDomisili submitDomisili,
			SubmitPartialDetailResponse submitPartialDetailResponse) {
		SubmitDomisiliResDTO submitDomisiliResDTO = new SubmitDomisiliResDTO();
		submitDomisiliResDTO.setOrderNo(submitDomisili.getSubmitOrder().getOrderNo());
		submitDomisiliResDTO.setFlagAddressId(submitDomisili.getFlagAddressId());
		submitDomisiliResDTO.setAddressDomisili(submitDomisili.getAddressDomisili());
		submitDomisiliResDTO.setSurveyDate(submitDomisili.getSurveyDate());
		submitDomisiliResDTO.setSurveyTime(submitDomisili.getSurveyTime());
		submitDomisiliResDTO.setRtrw(submitDomisili.getRtrw());
		submitDomisiliResDTO.setKelurahan(submitDomisili.getKelurahan());
		submitDomisiliResDTO.setKelurahanDesc(submitDomisili.getKelurahanDesc());
		submitDomisiliResDTO.setKecamatan(submitDomisili.getKecamatan());
		submitDomisiliResDTO.setKecamatanDesc(submitDomisili.getKecamatanDesc());
		submitDomisiliResDTO.setKabkot(submitDomisili.getKabkot());
		submitDomisiliResDTO.setKabkotDesc(submitDomisili.getKabkotDesc());
		submitDomisiliResDTO.setProvinsi(submitDomisili.getProvinsi());
		submitDomisiliResDTO.setProvinsiDesc(submitDomisili.getProvinsiDesc());
		submitDomisiliResDTO.setZipcode(submitDomisili.getZipcode());
		submitDomisiliResDTO.setCreatedBy(submitDomisili.getCreatedBy());
		submitDomisiliResDTO.setModifiedBy(submitDomisili.getModifiedBy());
		submitDomisiliResDTO.setActive(submitDomisili.getActive());
		submitDomisiliResDTO.setFlagKelurahan(submitDomisili.getFlagKelurahan());
		submitDomisiliResDTO.setSalesNotes(submitDomisili.getSalesNotes());

		submitPartialDetailResponse.setDraftDataDomisili(submitDomisiliResDTO);
	}

	private void insertDraftDataKonsumenToResponse(SubmitCustomer submitCustomer,
			SubmitPartialDetailResponse submitPartialDetailResponse) {
		SubmitCustomerResDTO submitCustomerResDTO = new SubmitCustomerResDTO();

		submitCustomerResDTO.setOrderNo(submitCustomer.getSubmitOrder().getOrderNo());
		submitCustomerResDTO.setIdNo(submitCustomer.getIdNo());
		submitCustomerResDTO.setCustomerName(submitCustomer.getCustomerName());
		submitCustomerResDTO.setHandphoneNo(submitCustomer.getHandphoneNo());
		submitCustomerResDTO.setWaNo(submitCustomer.getWaNo());
		submitCustomerResDTO.setFlagWa(submitCustomer.getFlagWa());
		submitCustomerResDTO.setIdName(submitCustomer.getIdName());
		submitCustomerResDTO.setBirthPlace(submitCustomer.getBirthPlace());
		submitCustomerResDTO.setBirthDate(submitCustomer.getBirthDate());
		submitCustomerResDTO.setIdAddress(submitCustomer.getIdAddress());
		submitCustomerResDTO.setIdRtrw(submitCustomer.getIdRtrw());
		submitCustomerResDTO.setIdType(submitCustomer.getIdType());
		submitCustomerResDTO.setIdKelurahan(submitCustomer.getIdKelurahan());
		submitCustomerResDTO.setIdKelurahanDesc(submitCustomer.getIdKelurahanDesc());
		submitCustomerResDTO.setIdKecamatan(submitCustomer.getIdKecamatan());
		submitCustomerResDTO.setIdKecamatanDesc(submitCustomer.getIdKecamatanDesc());
		submitCustomerResDTO.setIdKabkot(submitCustomer.getIdKabkot());
		submitCustomerResDTO.setIdKabkotDesc(submitCustomer.getIdKabkotDesc());
		submitCustomerResDTO.setIdProvinsi(submitCustomer.getIdProvinsi());
		submitCustomerResDTO.setIdProvinsiDesc(submitCustomer.getIdProvinsiDesc());
		submitCustomerResDTO.setZipcode(submitCustomer.getZipcode());
		submitCustomerResDTO.setCreatedBy(submitCustomer.getCreatedBy());
		submitCustomerResDTO.setCreatedDate(submitCustomer.getCreatedDate());
		submitCustomerResDTO.setModifiedBy(submitCustomer.getModifiedBy());
		submitCustomerResDTO.setModifiedDate(submitCustomer.getModifiedDate());
		submitCustomerResDTO.setActive(submitCustomer.getActive());
		submitCustomerResDTO.setFinType(String.valueOf(submitCustomer.getFinType()));
		submitCustomerResDTO.setJsonIdp(submitCustomer.getJsonIdp());
		submitCustomerResDTO.setPic(submitCustomer.getPic());
		submitCustomerResDTO.setCustType(submitCustomer.getCustType());

		submitPartialDetailResponse.setDraftDataKonsumen(submitCustomerResDTO);
	}
}
