package id.co.adira.partner.order.service;

import id.co.adira.partner.order.dto.*;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.entity.SubmitCustomer;
import id.co.adira.partner.order.entity.SubmitDomisili;
import id.co.adira.partner.order.entity.SubmitOrder;
import id.co.adira.partner.order.entity.SubmitUnit;
import id.co.adira.partner.order.exception.AdiraCustomException;
import id.co.adira.partner.order.repository.SubmitCustomerRepository;
import id.co.adira.partner.order.repository.SubmitDomisiliRepository;
import id.co.adira.partner.order.repository.SubmitOrderRepository;
import id.co.adira.partner.order.repository.SubmitUnitRepository;
import id.co.adira.partner.order.utils.DateUtil;
import id.co.adira.partner.order.utils.GroupStatusTracking;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static id.co.adira.partner.order.constant.AppConst.*;
import static id.co.adira.partner.order.constant.FormattingConst.THIRD_SIMPLE_DATE_FORMAT;
import static id.co.adira.partner.order.constant.StatusConst.*;


@Service
public class DraftSubmitPartialServiceV2 {

    private final SubmitCustomerRepository submitCustomerRepository;
    private final SubmitDomisiliRepository submitDomisiliRepository;
    private final SubmitUnitRepository submitUnitRepository;
    private final SubmitOrderRepository submitOrderRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(DraftSubmitPartialServiceV2.class.getName());
    ZoneId defaultZoneId = ZoneId.systemDefault();
    SimpleDateFormat thirdSdf = new SimpleDateFormat(THIRD_SIMPLE_DATE_FORMAT.getMessage());


    @Autowired
    public DraftSubmitPartialServiceV2(SubmitCustomerRepository submitCustomerRepository, 
                                       SubmitDomisiliRepository submitDomisiliRepository, 
                                       SubmitUnitRepository submitUnitRepository,
                                       SubmitOrderRepository submitOrderRepository
                                       ) {
        this.submitCustomerRepository = submitCustomerRepository;
        this.submitDomisiliRepository = submitDomisiliRepository;
        this.submitUnitRepository = submitUnitRepository;
        this.submitOrderRepository = submitOrderRepository;
    }

    public DataResponse getDraftSubmitPartialOrderByPic(Integer numberOfPrevDay, String pic, int sort) throws ParseException {
        DataResponse dataResponse = new DataResponse();
        dataResponse.setHttpStatus(HttpStatus.OK);
        dataResponse.setMessage(OK.getMessage());

        List<DraftSubmitOrderResDTO> draftSubmitOrderResDTOS = new ArrayList<>();
        List<SubmitOrder> submitOrderList = new ArrayList<>();

        LocalDate previousDateInLocalDate =  DateUtil.getDateBasedOnInterval(numberOfPrevDay);
        Date previousDateInDate = Date.from(previousDateInLocalDate.atStartOfDay(defaultZoneId).toInstant());

        if(sort == 0 || sort == 1)
            submitOrderList = submitOrderRepository.findSubmitOrderByFilterDateStatusAndPicOrderByOrderDateAsc(previousDateInDate, new Date(), DRAFT.getMessage(), pic);
        else
            submitOrderList = submitOrderRepository.findSubmitOrderByFilterDateStatusAndPicOrderByOrderDateDesc(previousDateInDate, new Date(), DRAFT.getMessage(), pic);

        if(submitOrderList.isEmpty()){
            dataResponse.setData(new ArrayList<>());
            LOGGER.info("Submit Order List is empty!");
        } else
            dataResponse.setData(insertSubmitOrderList(submitOrderList, draftSubmitOrderResDTOS));

        return dataResponse;
    }

    private Object insertSubmitOrderList(List<SubmitOrder> submitOrderList, List<DraftSubmitOrderResDTO> draftSubmitOrderResDTOS) {

        for(SubmitOrder submitOrder : submitOrderList){
            SubmitCustomer submitCustomer = submitCustomerRepository.findSubmitCustomerBySubmitOrder(submitOrder);
            SubmitUnit submitUnit = submitUnitRepository.findSubmitUnitBySubmitOrder(submitOrder);

            DraftSubmitOrderResDTO draftSubmitOrderResDTO = new DraftSubmitOrderResDTO();
            draftSubmitOrderResDTO.setOrderNo(submitOrder.getOrderNo());
            draftSubmitOrderResDTO.setCustName(submitCustomer != null ? submitCustomer.getCustomerName() : "");
            draftSubmitOrderResDTO.setOrderDate(thirdSdf.format(submitOrder.getOrderDate()));
            draftSubmitOrderResDTO.setStatus(submitOrder.getStatus());
            draftSubmitOrderResDTO.setObjectDesc(submitUnit != null ? submitUnit.getObjectDesc() : "");
            draftSubmitOrderResDTO.setObjectModelDesc(submitUnit != null ? submitUnit.getObjectModelDesc() : "");

            draftSubmitOrderResDTOS.add(draftSubmitOrderResDTO);
            LOGGER.info(CURRENT_ORDER_NO.getMessage(), draftSubmitOrderResDTO.getOrderNo());
        }
        return draftSubmitOrderResDTOS;
    }

}
