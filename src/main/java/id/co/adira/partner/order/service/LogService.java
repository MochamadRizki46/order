package id.co.adira.partner.order.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.adira.partner.order.config.BeanConfig;
import id.co.adira.partner.order.entity.Log;
import id.co.adira.partner.order.entity.LogConsumer;
import id.co.adira.partner.order.entity.LogProducer;
import id.co.adira.partner.order.entity.SubmitOrder;
import id.co.adira.partner.order.repository.LogConsumerRepository;
import id.co.adira.partner.order.repository.LogProducerRepository;
import id.co.adira.partner.order.repository.LogRepository;
import id.co.adira.partner.order.repository.SubmitOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class LogService {
	private final LogRepository logRepository;

	private final SubmitOrderRepository submitOrderRepository;

	private final LogConsumerRepository logConsumerRepository;

	private final LogProducerRepository logProducerRepository;

	public static final String COMPLETED 	= "DONE";
	public static final String ERR 			= "ERROR";

	ObjectMapper objectMapper = new BeanConfig().getObjectMapper();

	@Autowired
	public LogService(LogRepository logRepository, SubmitOrderRepository submitOrderRepository,
			LogConsumerRepository logConsumerRepository, LogProducerRepository logProducerRepository) {
		this.logRepository = logRepository;
		this.submitOrderRepository = submitOrderRepository;
		this.logConsumerRepository = logConsumerRepository;
		this.logProducerRepository = logProducerRepository;
	}

	public void insertConsumerLogger(String jsonValue, String topic, String message) {

		var logConsumer = new LogConsumer();

		logConsumer.setJsonFile(jsonValue);
		logConsumer.setTopic(topic);
		logConsumer.setStatus(message.equalsIgnoreCase("") ? COMPLETED : ERR);
		logConsumer.setMessage(message);
		logConsumer.setFlagStatus(0);
		logConsumer.setCreatedDate(new Date());
		logConsumer.setModifiedDate(new Date());

		logConsumerRepository.save(logConsumer);

	}

	public void insertProducerLogger(String jsonValue, String topic, String message) {

		var logProducer = new LogProducer();

		logProducer.setJsonFile(jsonValue);
		logProducer.setTopic(topic);
		logProducer.setStatus(message.equalsIgnoreCase("") ? COMPLETED : ERR);
		logProducer.setMessage(message);
		logProducer.setFlagStatus(0);
		logProducer.setCreatedDate(new Date());

		logProducerRepository.save(logProducer);

	}

	public void insertToLogTable(Object request, Object response, String logMessage, double totalTimeMillis,
			String orderNo) throws JsonProcessingException {
		Log newLog = new Log();

		SubmitOrder submitOrder = submitOrderRepository.findSubmitOrderByOrderNo(orderNo);
		String requestJsonInString = objectMapper.writeValueAsString(request);
		String responseJsonInString = objectMapper.writeValueAsString(response);

		newLog.setLogId(generateUUID());
		newLog.setOrderNo(orderNo == null ? "" : orderNo);
		newLog.setDuration((int) totalTimeMillis);
		newLog.setLogName(logMessage);
		newLog.setProcessDate(new Date());
		newLog.setJsonRequest(request == null ? "" : requestJsonInString);
		newLog.setJsonResponses(response == null ? "" : responseJsonInString);
		newLog.setUserId(submitOrder != null ? submitOrder.getPic() : "");
		newLog.setActive(0);

		logRepository.save(newLog);
		inactivePreviousLog();
	}

	public void insertCredSimToLog(Object request, Object response, String logMessage, String pic,
			double totalTimeMillis) throws JsonProcessingException {
		Log existingLog = logRepository.getLogByUserId(pic);

		String requestJsonInString = objectMapper.writeValueAsString(request);
		String responseJsonInString = objectMapper.writeValueAsString(response);

		if (existingLog == null) {
			Log newLog = new Log();

			newLog.setLogId(generateUUID());
			newLog.setOrderNo("");
			newLog.setDuration((int) totalTimeMillis);
			newLog.setLogName(logMessage);
			newLog.setProcessDate(new Date());
			newLog.setJsonRequest(request == null ? "" : requestJsonInString);
			newLog.setJsonResponses(response == null ? "" : responseJsonInString);
			newLog.setUserId(pic);
			newLog.setActive(0);

			logRepository.save(newLog);
		} else {
			existingLog.setOrderNo("");
			existingLog.setDuration((int) totalTimeMillis);
			existingLog.setLogName(logMessage);
			existingLog.setProcessDate(new Date());
			existingLog.setJsonRequest(request == null ? "" : requestJsonInString);
			existingLog.setJsonResponses(response == null ? "" : responseJsonInString);
			existingLog.setUserId(pic);
			existingLog.setActive(0);

			logRepository.save(existingLog);
		}
	}

	private void inactivePreviousLog() {
		List<Log> logs = logRepository.getLogData();

		Log previousLog = logs.get(1);
		logRepository.inactivePreviousLogData(previousLog.getLogId());
	}

	private String generateUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	public List<LogConsumer> getLogByTopicAndCount(Integer count, String topic, String date) {
		return logConsumerRepository.getMsgLogErrorByTopic(count, topic, date);
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void updateLogConsumer(Long id, String status, String err, Integer count) throws Exception {

		if (id == null)
			throw new Exception("id cant null");

		if (status == null)
			throw new Exception("status cant null");
		
		logConsumerRepository.updateData(id, status, err, count);

	}
}
