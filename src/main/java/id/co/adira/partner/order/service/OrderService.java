package id.co.adira.partner.order.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.adira.partner.order.config.BeanConfig;
import id.co.adira.partner.order.dto.*;
import id.co.adira.partner.order.dto.fcm.FcmNotificationRequestDTO;
import id.co.adira.partner.order.dto.fcm.LatesTrackingForFCMResponsesDTO;
import id.co.adira.partner.order.dto.kafka.*;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.dto.response.SubmitOrderResponseDTO;
import id.co.adira.partner.order.entity.*;
import id.co.adira.partner.order.exception.AdiraCustomException;
import id.co.adira.partner.order.kafka.KafkaProducer;
import id.co.adira.partner.order.repository.*;
import id.co.adira.partner.order.service.resttemplate.RestTemplateService;
import id.co.adira.partner.order.utils.GroupStatusTracking;

import org.apache.commons.lang3.SystemUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StopWatch;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static id.co.adira.partner.order.constant.AppConst.*;
import static id.co.adira.partner.order.constant.FormattingConst.*;
import static id.co.adira.partner.order.constant.StatusConst.*;

@Service
public class OrderService {

	@Autowired
	private SubmitOrderRepository submitOrderRepository;

	@Autowired
	private SubmitCustomerRepository submitCustomerRepository;

	@Autowired
	private SubmitDomisiliRepository submitDomisiliRepository;

	@Autowired
	private SubmitUnitRepository submitUnitRepository;

	@Autowired
	private StatusOrderDetailRepository statusOrderDetailRepository;

	@Autowired
	private DocumentRepository documentRepository;

	@Autowired
	private DistributedApplicationTrackingRepository applicationTrackingRepository;

	@Autowired
	private ApplicationRepository applicationRepository;

	@Autowired
	private ApplicationObjectRepository applicationObjectRepository;
	@Autowired
	private ApplicationInsuranceRepository applicationInsuranceRepository;
	@Autowired
	private ApplicationLoanDetailRepository applicationLoanRepository;
	@Autowired
	private ApplicationSalesInfoRepository applicationSalesInfoRepository;
	@Autowired
	private ApplicationObjectCollateralOtomotifRepository collateralOtomotifRepository;

	@Autowired
	private ApplicationDocumentRepository applicationDocumentRepository;

	@Autowired
	private ApplicationNoteRepository applicationNoteRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CustomerPersonalRepository customerPersonalRepository;

	@Autowired
	private CustomerCompanyRepository customerCompanyRepository;

	@Autowired
	private ManagementPicRepository managementPicRepository;

	@Autowired
	private CustomerAddressRepository customerAddressRepository;

	@Autowired
	private CustomerContactRepository customerContactRepository;

	@Autowired
	private PurchaseOrderRepository poRepository;

	@Autowired
	private NotificationRepository notifRepository;

	@Autowired
	private LogService logService;

	@Autowired
	private GenerateId genId;

	@Autowired
	private KafkaProducer kafkaProducer;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private RestTemplateService restTemplateService;

	HttpHeaders headers;

//	@Value("${url.api.header}")
//	private String urlHeader;
//
//	@Value("${url.api.history}")
//	private String urlHistory;
//
//	@Value("${url.api.getapplno}")
//	private String urlApplno;
//
//	@Value("${url.api.detail}")
//	private String urlDetail;
//
//	@Value("${url.api.home}")
//	private String urlHome;
//
//	@Value("${url.api.ep.tracking}")
//	private String urlEpTracking;

	@Value("${url.api.getKabkot}")
	private String urlKabkot;

	// @Value("${url.api.ep.history}")
	// private String urlEpHistory;

	@Value("${url.api.getkecamatan}")
	private String urlgetkecamatan;

	@Value("${url.api.getmodel}")
	private String urlgetmodel;

	@Value("${url.api.uploadecm}")
	private String urlUploadEcm;

	@Value("${url.api.idp}")
	private String urlIDP;

	@Value("${path.npwp}")
	private String pathNpwp;

	@Value("${path.ktp}")
	private String pathKTP;

	@Value("${key.api.ecm}")
	private String keyEcm;

	@Value("${key.api.idp}")
	private String keyIDP;

	ObjectMapper objectMapper = new BeanConfig().getObjectMapper();
	ModelMapper modelMapper = new ModelMapper();

	String dataId = "";

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

	String data = "";

	Optional<SubmitOrder> dataOrder;

	SimpleDateFormat sdf = new SimpleDateFormat(FIRST_SIMPLE_DATE_FORMAT.getMessage());
	SimpleDateFormat formatTime = new SimpleDateFormat(TIME_FORMAT.getMessage());
	SimpleDateFormat secondSdf = new SimpleDateFormat(SECOND_SIMPLE_DATE_FORMAT.getMessage());
	SimpleDateFormat thirdSdf = new SimpleDateFormat(THIRD_SIMPLE_DATE_FORMAT.getMessage());
	DateTimeFormatter formatDate = DateTimeFormatter.ofPattern(FIRST_SIMPLE_DATE_FORMAT.getMessage());
	SimpleDateFormat fourthSdf = new SimpleDateFormat(FOURTH_SIMPLE_DATE_FORMAT.getMessage());
	SimpleDateFormat dataNtimeFormat = new SimpleDateFormat(DATE_TIME_FORMAT.getMessage());

	StandardResponseDTO dataResponse = new StandardResponseDTO();

	public StandardResponseDTO savePartialOrder(SubmitOrderReqDTO submitOrderReq) {
		dataResponse.setStatus(0);

		try {
			SubmitOrder detailOrder = submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReq.getOrderNo());
			if (detailOrder == null) {
				SubmitOrder submitOrder = new SubmitOrder();
				mapSubmitOrderToDb(submitOrder, submitOrderReq);
				submitOrderRepository.save(submitOrder);

			} else {
				mapSubmitOrderToDb(detailOrder, submitOrderReq);
				submitOrderRepository.save(detailOrder);
			}

			dataResponse.setMessage(SUCCESSFULLY_SAVE_DRAFT.getMessage() + submitOrderReq.getOrderNo());

			LOGGER.info(SUCCESSFULLY_SAVE_SUBMIT_ORDER_WITH_ORDER_NO.getMessage(), submitOrderReq.getOrderNo());

		} catch (Exception e) {
			insertErrorStdResponseWhenExceptionOccurs(dataResponse, ERROR_WHILE_SAVING_SUBMIT_ORDER_TO_DB.getMessage(),
					e);
		}

		return dataResponse;
	}

	private void mapSubmitOrderToDb(SubmitOrder detailOrder, SubmitOrderReqDTO submitOrderReq) throws ParseException {
		detailOrder.setOrderNo(submitOrderReq.getOrderNo());
		detailOrder.setApplNoPayung(submitOrderReq.getApplNoPayung());
		detailOrder.setApplNoUnit(submitOrderReq.getApplNoUnit());
		detailOrder.setOrderDate(submitOrderReq.getOrderDate());
		detailOrder.setPartnerType(submitOrderReq.getPartnerType());
		detailOrder.setPic(submitOrderReq.getPic());
		detailOrder.setIdx(submitOrderReq.getIdx());
		detailOrder.setStatus(submitOrderReq.getStatus());
		detailOrder.setCreatedBy(submitOrderReq.getCreatedBy());
		detailOrder.setCreatedBy(submitOrderReq.getCreatedBy());
		detailOrder.setModifiedBy(submitOrderReq.getModifiedBy());
		detailOrder.setActive(submitOrderReq.getActive());
		detailOrder.setGroupStatus(GroupStatusTracking.SVR.name());
	}

	public StandardResponseDTO savePartialCustomer(SubmitCustomerReqDTO submitCustomerReq) {
		dataResponse.setStatus(0);

		SubmitOrder submitOrder = submitOrderRepository.findSubmitOrderByOrderNo(submitCustomerReq.getOrderNo());
		if (submitOrder == null)
			return insertStdResponseWhenSubmitOrderIsNotFound(dataResponse,
					CANNOT_FIND_SUBMIT_ORDER_WITH_ID.getMessage() + submitCustomerReq.getOrderNo());

		try {
			SubmitCustomer submitCustomer = submitCustomerRepository.findSubmitCustomerBySubmitOrder(submitOrder);
			if (submitCustomer == null) {
				SubmitCustomer newCustomer = new SubmitCustomer();
				insertCustomerDataToSubmitCustomer(newCustomer, submitCustomerReq);
				newCustomer.setSubmitOrder(submitOrder);
				submitCustomerRepository.save(newCustomer);

			} else {
				insertCustomerDataToSubmitCustomer(submitCustomer, submitCustomerReq);
				submitCustomerRepository.save(submitCustomer);

			}

			dataResponse.setMessage(SUCCESSFULLY_SAVE_DRAFT.getMessage() + submitCustomerReq.getOrderNo());

			LOGGER.info(SUCCESSFULLY_SAVE_SUBMIT_CUSTOMER_WITH_ORDER_NO.getMessage(), submitOrder.getOrderNo());

		} catch (Exception e) {
			insertErrorStdResponseWhenExceptionOccurs(dataResponse,
					ERROR_WHILE_TRYING_TO_SAVE_DRAFT_CUSTOMER.getMessage(), e);
		}
		return dataResponse;
	}

	private void insertCustomerDataToSubmitCustomer(SubmitCustomer submitCustomer,
			SubmitCustomerReqDTO submitCustomerReq) {
		submitCustomer.setIdAddress(submitCustomerReq.getIdAddress());
		submitCustomer.setIdNo(submitCustomerReq.getIdNo());
		submitCustomer.setCustomerName(submitCustomerReq.getCustomerName());
		submitCustomer.setHandphoneNo(submitCustomerReq.getHandphoneNo());
		submitCustomer.setWaNo(submitCustomerReq.getWaNo());
		submitCustomer.setFlagWa(submitCustomerReq.getFlagWa());
		submitCustomer.setIdName(submitCustomerReq.getIdName());
		submitCustomer.setBirthDate(submitCustomerReq.getBirthDate());
		submitCustomer.setBirthPlace(submitCustomerReq.getBirthPlace());
		submitCustomer.setFinType(Integer.valueOf(submitCustomerReq.getFinType()));
		submitCustomer.setCustType(submitCustomerReq.getCustType());
		submitCustomer.setJsonIdp(submitCustomerReq.getJsonIdp());
		submitCustomer.setCreatedBy(submitCustomerReq.getCreatedBy());
		submitCustomer.setModifiedBy(submitCustomerReq.getModifiedBy());
		submitCustomer.setActive(submitCustomerReq.getActive());
		submitCustomer.setIdType(submitCustomerReq.getIdType());
		submitCustomer.setPic(submitCustomerReq.getPic());

	}

	public StandardResponseDTO savePartialDomisili(SubmitDomisiliReqDTO submitDomisiliReq) {
		dataResponse.setStatus(0);
		SubmitOrder submitOrder = submitOrderRepository.findSubmitOrderByOrderNo(submitDomisiliReq.getOrderNo());
		if (submitOrder == null)
			return insertStdResponseWhenSubmitOrderIsNotFound(dataResponse,
					CANNOT_FIND_SUBMIT_ORDER_WITH_ID.getMessage() + submitDomisiliReq.getOrderNo());

		try {
			SubmitDomisili submitDomisili = submitDomisiliRepository.findSubmitDomisiliBySubmitOrder(submitOrder);

			// if data submit domisili doesn't exists,
			// it will call submitOrder object to make current submit order
			// become FK at submitDomisili
			if (submitDomisili == null) {
				SubmitDomisili newDomisili = new SubmitDomisili();
				submitOrderDomisili(newDomisili, submitDomisiliReq);
				newDomisili.setSubmitOrder(submitOrder);

				submitDomisiliRepository.save(newDomisili);

			} else {
				submitOrderDomisili(submitDomisili, submitDomisiliReq);
				submitDomisiliRepository.save(submitDomisili);
			}

			dataResponse.setMessage(SUCCESSFULLY_SAVE_DRAFT.getMessage() + submitDomisiliReq.getOrderNo());

			LOGGER.info(SUCCESSFULLY_SAVE_SUBMIT_DOMISILI_WITH_ORDER_NO.getMessage(), submitDomisiliReq.getOrderNo());

		} catch (Exception e) {
			insertErrorStdResponseWhenExceptionOccurs(dataResponse,
					ERROR_WHILE_TRYING_TO_SAVE_DRAFT_DOMISILI.getMessage(), e);
		}
		return dataResponse;
	}

	private void submitOrderDomisili(SubmitDomisili submitDomisili, SubmitDomisiliReqDTO submitDomisiliReq) {
		submitDomisili.setAddressDomisili(submitDomisiliReq.getAddressDomisili());
		submitDomisili.setFlagAddressId(submitDomisiliReq.getFlagAddressId());
		submitDomisili.setSurveyDate(submitDomisiliReq.getSurveyDate());
		submitDomisili.setSurveyTime(submitDomisiliReq.getSurveyTime());
		submitDomisili.setRtrw(submitDomisiliReq.getRtrw());
		submitDomisili.setKelurahan(submitDomisiliReq.getKelurahan());
		submitDomisili.setKelurahanDesc(submitDomisiliReq.getKelurahanDesc());
		submitDomisili.setKecamatan(submitDomisiliReq.getKecamatan());
		submitDomisili.setKecamatanDesc(submitDomisiliReq.getKecamatanDesc());
		submitDomisili.setKabkot(submitDomisiliReq.getKabkot());
		submitDomisili.setKabkotDesc(submitDomisiliReq.getKabkotDesc());
		submitDomisili.setProvinsi(submitDomisiliReq.getProvinsi());
		submitDomisili.setProvinsiDesc(submitDomisiliReq.getProvinsiDesc());
		submitDomisili.setZipcode(submitDomisiliReq.getZipcode());
		submitDomisili.setCreatedDate(submitDomisiliReq.getCreatedDate());
		submitDomisili.setCreatedBy(submitDomisiliReq.getCreatedBy());
		submitDomisili.setModifiedBy(submitDomisiliReq.getModifiedBy());
		submitDomisili.setModifiedDate(submitDomisiliReq.getModifiedDate());
		submitDomisili.setActive(submitDomisiliReq.getActive());
		submitDomisili.setFlagKelurahan(submitDomisiliReq.getFlagKelurahan());
		submitDomisili.setSalesNotes(submitDomisiliReq.getSalesNotes());
	}

	public String savePartialUnit(SubmitUnitReqDTO submitUnitReq) {
		SubmitOrder submitOrder = submitOrderRepository.findSubmitOrderByOrderNo(submitUnitReq.getOrderNo());
		if (submitOrder == null)
			return CANNOT_FIND_SUBMIT_ORDER_WITH_ID.getMessage() + submitUnitReq.getOrderNo();

		try {
			SubmitUnit detailUnit = submitUnitRepository.findSubmitUnitBySubmitOrder(submitOrder);
			if (detailUnit == null) {
				SubmitUnit newUnit = new SubmitUnit();
				submitOrderUnit(newUnit, submitUnitReq);
				newUnit.setSubmitOrder(submitOrder);

				submitUnitRepository.save(newUnit);
			} else {
				submitOrderUnit(detailUnit, submitUnitReq);
				submitUnitRepository.save(detailUnit);
			}

			data = SUCCESS.getMessage();

		} catch (Exception e) {
			LOGGER.error(FAILED_TO_SAVE_DATA_TO_DB.getMessage(), e.getMessage());
			data = e.toString();
		}
		return data;
	}

	private void submitOrderUnit(SubmitUnit detailUnit, SubmitUnitReqDTO submitUnitReq) {
		detailUnit.setObjectBrand(submitUnitReq.getObjectBrand());
		detailUnit.setObjectBrandDesc(submitUnitReq.getObjectBrandDesc());
		detailUnit.setObjectType(submitUnitReq.getObjectType());
		detailUnit.setObjectTypeDesc(submitUnitReq.getObjectTypeDesc());
		detailUnit.setObjectModel(submitUnitReq.getObjectModel());
		detailUnit.setObjectModelDesc(submitUnitReq.getObjectModelDesc());
		detailUnit.setObject(submitUnitReq.getObject());
		detailUnit.setObjectDesc(submitUnitReq.getObjectDesc());
		detailUnit.setObjectYear(submitUnitReq.getObjectYear());
		detailUnit.setTipeJaminan(submitUnitReq.getTipeJaminan());
		detailUnit.setJenisAsuransi(submitUnitReq.getJenisAsuransi());
		detailUnit.setKotaDomisili(submitUnitReq.getKotaDomisili());
		detailUnit.setObjectPrice(submitUnitReq.getObjectPrice());
		detailUnit.setTenor(submitUnitReq.getTenor());
		detailUnit.setRate(submitUnitReq.getRate());
		detailUnit.setDp(submitUnitReq.getDp());
		detailUnit.setInstallmentAmt(submitUnitReq.getInstallmentAmt());
		detailUnit.setPromo(submitUnitReq.getPromo());
		detailUnit.setCreatedBy(submitUnitReq.getCreatedBy());
		detailUnit.setModifiedBy(submitUnitReq.getModifiedBy());
		detailUnit.setActive(submitUnitReq.getActive());
		detailUnit.setModelDetail(submitUnitReq.getModelDetail());
		detailUnit.setNotes(submitUnitReq.getNotes());
		detailUnit.setTipeKombinasi(submitUnitReq.getTipeKombinasi());
	}

	public StandardResponseDTO savePartialUnitV2(SubmitUnitV2ReqDTO submitUnitReq) {
		dataResponse.setStatus(0);

		SubmitOrder submitOrder = submitOrderRepository.findSubmitOrderByOrderNo(submitUnitReq.getOrderNo());
		if (submitOrder == null)
			return insertStdResponseWhenSubmitOrderIsNotFound(dataResponse,
					CANNOT_FIND_SUBMIT_ORDER_WITH_ID.getMessage() + submitUnitReq.getOrderNo());

		try {
			SubmitUnit detailUnit = submitUnitRepository.findSubmitUnitBySubmitOrder(submitOrder);
			if (detailUnit == null) {
				SubmitUnit newUnit = new SubmitUnit();
				submitOrderUnitV2(newUnit, submitUnitReq);
				newUnit.setSubmitOrder(submitOrder);

				submitUnitRepository.save(newUnit);

//				SubmitOrderResponseDTO submitOrderResponseDTO = new SubmitOrderResponseDTO(submitOrder.getOrderNo());
//				dataResponse.setMessage(SUCCESSFULLY_SAVE_NEW_SUBMIT_UNIT.getMessage());
//				dataResponse.setData(submitOrderResponseDTO);

			} else {
				submitOrderUnitV2(detailUnit, submitUnitReq);
				submitUnitRepository.save(detailUnit);

//				SubmitOrderResponseDTO submitOrderResponseDTO = new SubmitOrderResponseDTO(submitOrder.getOrderNo());
//				dataResponse.setMessage(SUCCESSFULLY_UPDATE_AN_EXISTING_SUBMIT_UNIT.getMessage());
//				dataResponse.setData(submitOrderResponseDTO);
			}

			dataResponse.setMessage(SUCCESSFULLY_SAVE_DRAFT.getMessage() + submitUnitReq.getOrderNo());

			LOGGER.info(SUCCESSFULLY_SAVE_SUBMIT_UNIT_WITH_ORDER_NO.getMessage(), submitUnitReq.getOrderNo());

		} catch (Exception e) {
			insertErrorStdResponseWhenExceptionOccurs(dataResponse, ERROR_WHILE_TRYING_TO_SAVE_SUBMIT_UNIT.getMessage(),
					e);
		}
		return dataResponse;
	}

	private void submitOrderUnitV2(SubmitUnit detailUnit, SubmitUnitV2ReqDTO submitUnitReq) {
		detailUnit.setObjectBrand(submitUnitReq.getObjectBrand());
		detailUnit.setObjectBrandDesc(submitUnitReq.getObjectBrandDesc());
		detailUnit.setObjectType(submitUnitReq.getObjectType());
		detailUnit.setObjectTypeDesc(submitUnitReq.getObjectTypeDesc());
		detailUnit.setObjectModel(submitUnitReq.getObjectModel());
		detailUnit.setObjectModelDesc(submitUnitReq.getObjectModelDesc());
		detailUnit.setObject(submitUnitReq.getObject());
		detailUnit.setObjectDesc(submitUnitReq.getObjectDesc());
		detailUnit.setObjectYear(submitUnitReq.getObjectYear());
		detailUnit.setTipeJaminan(submitUnitReq.getTipeJaminan());
		detailUnit.setJenisAsuransi(submitUnitReq.getJenisAsuransi());
		detailUnit.setKotaDomisili(submitUnitReq.getKotaDomisili());
		detailUnit.setObjectPrice(submitUnitReq.getObjectPrice());
		detailUnit.setTenor(submitUnitReq.getTenor());
		detailUnit.setRate(submitUnitReq.getRate());
		detailUnit.setDp(submitUnitReq.getDp());
		detailUnit.setInstallmentAmt(submitUnitReq.getInstallmentAmt());
		detailUnit.setPromo(submitUnitReq.getPromo());
		detailUnit.setCreatedBy(submitUnitReq.getCreatedBy());
		detailUnit.setModifiedBy(submitUnitReq.getModifiedBy());
		detailUnit.setActive(submitUnitReq.getActive());
		detailUnit.setModelDetail(submitUnitReq.getModelDetail());
		detailUnit.setNotes(submitUnitReq.getNotes());
		detailUnit.setTipeKombinasi(submitUnitReq.getTipeKombinasi());
		detailUnit.setFlagCredsim(submitUnitReq.getFlagCredsim());
		detailUnit.setInstallmentTypeId(submitUnitReq.getInstallmentTypeId());
		detailUnit.setInstallmentTypeDesc(submitUnitReq.getInstallmentTypeDesc());
		detailUnit.setProgramId(submitUnitReq.getProgramId());
		detailUnit.setProgramDesc(submitUnitReq.getProgramDesc());
		detailUnit.setPaymentTypeId(submitUnitReq.getPaymentTypeId());
		detailUnit.setPaymentTypeDesc(submitUnitReq.getPaymentTypeDesc());
		detailUnit.setFlagInsrAdmin(submitUnitReq.getFlagInsrAdmin());
		detailUnit.setAcuanHitung(submitUnitReq.getAcuanHitung());
		detailUnit.setKotaDomisiliId(submitUnitReq.getKotaDomisiliId());
		detailUnit.setModelDetailId(submitUnitReq.getModelDetailId());
		detailUnit.setInsrTloCoverage(submitUnitReq.getInsrTloCoverage());
		detailUnit.setInsrCompreCoverage(submitUnitReq.getInsrCompreCoverage());
		detailUnit.setInsrTypeId(submitUnitReq.getInsrTypeId());

	}

	public String saveDocument(DocumentReqDTO docRequest) {
		SubmitOrder submitOrder = submitOrderRepository.findSubmitOrderByOrderNo(docRequest.getOrderNo());
		if (submitOrder == null)
			return CANNOT_FIND_SUBMIT_ORDER_WITH_ORDER_NO.getMessage() + docRequest.getOrderNo();

		try {
			DocumentDetail documentDetail = documentRepository.findDocumentDetailBySubmitOrder(submitOrder);

			// if document doesn't exists in database,
			// it will call submitOrder object to make current submit order
			// become FK at tbl_doc_dtl
			if (documentDetail == null) {
				DocumentDetail newDocument = new DocumentDetail();

				insertDocumentDetail(newDocument, docRequest, submitOrder);
				newDocument.setSubmitOrder(submitOrder);
				documentRepository.save(newDocument);
			} else {
				DocumentDetail documentDetailWithId = documentRepository
						.findDocumentDetailById(submitOrder.getOrderNo().concat(docRequest.getDocType()));

				if (documentDetailWithId != null) {
					insertDocumentDetail(documentDetailWithId, docRequest, submitOrder);
					documentRepository.save(documentDetailWithId);
				} else {
					DocumentDetail newDocumentDetailWithNewId = new DocumentDetail();
					insertDocumentDetail(newDocumentDetailWithNewId, docRequest, submitOrder);
					newDocumentDetailWithNewId.setSubmitOrder(submitOrder);
					documentRepository.save(newDocumentDetailWithNewId);
				}
			}
			data = SUCCESS.getMessage();
		} catch (Exception e) {
			LOGGER.error(FAILED_TO_SAVE_DATA_TO_DB.getMessage(), e.getMessage());
			data = e.toString();
		}
		return data;
	}

	private void insertDocumentDetail(DocumentDetail documentDetail, DocumentReqDTO docRequest,
			SubmitOrder submitOrder) {
		documentDetail.setCreatedBy(docRequest.getUserLogin());
		documentDetail.setModifiedBy(docRequest.getUserLogin());
		documentDetail.setActive(0);
		documentDetail.setDocType(docRequest.getDocType());
		documentDetail.setId(submitOrder.getOrderNo().concat(docRequest.getDocType()));
		documentDetail.setDocId(docRequest.getDocId());
		documentDetail.setDocPath(docRequest.getDocPath());
	}

	public String getOrderId(GenerateOrderReq orderReq) {

		dataId = genId.generateOrderId(orderReq);
		return dataId;

	}

	// EndPoint New Submit Order Lead Engine
	public StandardResponseDTO submitOrdertoLeadV2(SendOrderToLeadV2ReqDTO sendOrderToLeadV2ReqDTO) {
		String dataOrderSubmit = "";
		dataResponse.setStatus(0);
		var updateTracking = new UpdateStatusTrackingDTO(GROUPSTATUS_LE_TRACK.getMessage(),
				STATUS_LE_TRACK.getMessage(), sendOrderToLeadV2ReqDTO.getCredOrderNo(), new Date(), "",
				PARA_TRACKING_UPDATE_LE.getMessage(), PARA_TRACKING_UPDATE_LE.getMessage(),
				PARA_TRACKING_UPDATE_LE.getMessage(), 0, true, sendOrderToLeadV2ReqDTO.getCreatedBy().toLowerCase(),
				new Date(), sendOrderToLeadV2ReqDTO.getCreatedBy().toLowerCase(), new Date());
		try {
			// Send Order To Kafka Listener
			kafkaProducer.sendLeadGenerationV2(sendOrderToLeadV2ReqDTO);

			// Send Update Tracking To Kafka Listener
			kafkaProducer.updateTrackingStatusLe(updateTracking);

			submitListOrderDetailV2(sendOrderToLeadV2ReqDTO);
			insertDocId(sendOrderToLeadV2ReqDTO);

			dataResponse.setMessage(SUCCESSFULLY_SUBMIT_LE.getMessage() + sendOrderToLeadV2ReqDTO.getCredOrderNo());

			LOGGER.info(SUCCESSFULLY_SUBMIT_LE.getMessage() + sendOrderToLeadV2ReqDTO.getCredOrderNo());
		} catch (JsonProcessingException e) {
//			LOGGER.error(ERROR_SENDING_ORDER_TO_LEAD_ENGINE.getMessage());
			insertErrorStdResponseWhenExceptionOccurs(dataResponse, ERROR_SENDING_ORDER_TO_LEAD_ENGINE.getMessage(), e);
		}
		return dataResponse;
	}

	private void submitListOrderDetailV2(SendOrderToLeadV2ReqDTO sendOrderToLeadReqDTO) {
		// List<StatusOrderDetail> listOrderDetail = new ArrayList<>();
		SubmitOrder submitOrder = submitOrderRepository
				.findSubmitOrderByOrderNo(sendOrderToLeadReqDTO.getCredOrderNo());

		if (submitOrder != null) {
			submitOrder.setStatus(ORDER_TERKIRIM.getMessage());
			submitOrder.setGroupStatus(SVR.getMessage());
			submitOrder.setOrderDate(new Date());
			submitOrderRepository.save(submitOrder);

			// statusOrderDetailRepository.saveAll(listOrderDetail);
			LOGGER.info(INSERT_TRACKING_HISTORY_ORDER_TERKIRIM.getMessage());
		} else
			LOGGER.error(CANNOT_FIND_SUBMIT_ORDER_WITH_ID.getMessage() + sendOrderToLeadReqDTO.getCredOrderNo());

	}

	private void insertDocId(SendOrderToLeadV2ReqDTO req) {
		System.out.println("masuk");

		if (req.getCredFotoKtpNpwp() != null) {
			var docReqId = new DocumentReqDTO(req.getCredOrderNo(), req.getCredFotoKtpNpwp(), null,
					req.getCredCustType().equalsIgnoreCase("PER") ? "1" : "2", req.getCreatedBy());
			saveDocument(docReqId);
		}

		if (req.getCredFotoStnk() != null) {
			var docReqStnk = new DocumentReqDTO(req.getCredOrderNo(), req.getCredFotoStnk(), null, "3",
					req.getCreatedBy());
			saveDocument(docReqStnk);
		}
	}

	// end EndPoint Submit
	public String submitOrderToLead(SendOrderToLeadReqDTO sendOrderToLeadReqDTO) {
		String dataOrderSubmit = "";
		try {
			// Send Order To Kafka Listener
			dataOrderSubmit = kafkaProducer.sendLeadGeneration(sendOrderToLeadReqDTO);
			submitListOrderDetail(sendOrderToLeadReqDTO);
		} catch (JsonProcessingException e) {
			LOGGER.error(ERROR_SENDING_ORDER_TO_LEAD_ENGINE.getMessage());
			dataOrderSubmit = THERE_IS_SOMETHING_ERROR_IN_OUR_SYSTEM.getMessage();
		}
		return dataOrderSubmit;
	}

	private void submitListOrderDetail(SendOrderToLeadReqDTO sendOrderToLeadReqDTO) {
		List<StatusOrderDetail> listOrderDetail = new ArrayList<>();
		StatusOrderDetail orderDetail = new StatusOrderDetail();
		SubmitOrder submitOrder = submitOrderRepository
				.findSubmitOrderByOrderNo(sendOrderToLeadReqDTO.getCredOrderNo());

		if (submitOrder != null) {
			submitOrder.setStatus(ORDER_TERKIRIM.getMessage());
			submitOrder.setGroupStatus(SVR.getMessage());
			submitOrder.setOrderDate(new Date());
			submitOrderRepository.save(submitOrder);

			insertOrderDetail(sendOrderToLeadReqDTO, listOrderDetail, orderDetail);

			statusOrderDetailRepository.saveAll(listOrderDetail);
			LOGGER.info(INSERT_TRACKING_HISTORY_ORDER_TERKIRIM.getMessage());
		} else
			LOGGER.error(CANNOT_FIND_SUBMIT_ORDER_WITH_ID.getMessage() + sendOrderToLeadReqDTO.getCredOrderNo());

	}

	private void insertOrderDetail(SendOrderToLeadReqDTO sendOrderToLeadReqDTO, List<StatusOrderDetail> listOrderDetail,
			StatusOrderDetail orderDetail) {
		orderDetail.setId(sendOrderToLeadReqDTO.getCredOrderNo().trim().concat("S").concat("1"));
		orderDetail.setOrderNo(sendOrderToLeadReqDTO.getCredOrderNo());
		orderDetail.setApplNoUnit("");
		orderDetail.setStatus(ORDER_TERKIRIM.getMessage());
		orderDetail.setSubstatus(SISTEM_MENERIMA_ORDER.getMessage());
		orderDetail.setGroupStatus(GroupStatusTracking.SVR.name());
		orderDetail.setStatusDate(new Date());
		orderDetail.setStatusTime(formatTime.format(new Date()));
		orderDetail.setIdSeq(1);
		orderDetail.setUserType("1");
		orderDetail.setActive(0);
		orderDetail.setCreateBy(sendOrderToLeadReqDTO.getCredEmail());
		orderDetail.setCreateDate(new Date());

		// add first for User Sales
		listOrderDetail.add(orderDetail);

		StatusOrderDetail orderDetail2 = new StatusOrderDetail();

		orderDetail2.setId(sendOrderToLeadReqDTO.getCredOrderNo().concat("W").concat("1"));
		orderDetail2.setApplNoUnit("");
		orderDetail2.setOrderNo(sendOrderToLeadReqDTO.getCredOrderNo());
		orderDetail2.setStatus(ORDER_TERKIRIM.getMessage());
		orderDetail2.setSubstatus(SISTEM_MENERIMA_ORDER.getMessage());
		orderDetail2.setGroupStatus(GroupStatusTracking.SVR.name());
		orderDetail2.setStatusDate(new Date());
		orderDetail2.setStatusTime(formatTime.format(new Date()));
		orderDetail2.setIdSeq(1);
		orderDetail2.setUserType("2");
		orderDetail2.setActive(0);
		orderDetail2.setCreateBy(sendOrderToLeadReqDTO.getCredEmail());
		orderDetail2.setCreateDate(new Date());

		// add Second for User Owner
		listOrderDetail.add(orderDetail2);
	}

	public void updateApplNumber(GenerateApplNoResDTO generateApplNo) {

		SubmitOrder submitOrder = submitOrderRepository.findSubmitOrderByOrderNo(generateApplNo.getOrderNo());

		if (submitOrder != null) {
			try {
				LOGGER.info(GET_DATA_FROM_TABLE_SUBMIT_ORDER.getMessage());

				submitOrder.setApplNoUnit(generateApplNo.getApplNo().trim());
				submitOrder.setApplNoPayung(generateApplNo.getPayungNo().trim());
				LOGGER.info("Set appl no : {} and payung no : {} ", generateApplNo.getApplNo(),
						generateApplNo.getPayungNo());

				statusOrderDetailRepository.updateApplNoUnit(generateApplNo.getApplNo().trim(),
						generateApplNo.getOrderNo().trim());
				LOGGER.info("Update Application Number for Tracking History Order Terkirim {}",
						generateApplNo.getApplNo());

				if (generateApplNo.getFlagUpdate().equals("1")) {

					Date newDate = sdf.parse(generateApplNo.getSendDate());
					submitOrder.setSendDateStatus(newDate);
					submitOrder.setGroupStatus(GroupStatusTracking.SVR.name());

					LOGGER.info("newData : {}", submitOrder);
				}

				submitOrderRepository.save(submitOrder);

				LOGGER.info("Update Order NO {}", generateApplNo.getOrderNo());

			} catch (Exception e) {
				LOGGER.error("Error Update Nomor Aplikasi dengan Order no {}", generateApplNo.getOrderNo());
				LOGGER.error(e.toString());
			}

		}

	}

	public void updateApplNumberV2(GenerateApplNoV2ResDTO generateApplNo) {

		SubmitOrder submitOrder = submitOrderRepository.findSubmitOrderByOrderNo(generateApplNo.getOrderNo());

		if (submitOrder != null) {
			try {
				LOGGER.info(GET_DATA_FROM_TABLE_SUBMIT_ORDER.getMessage());

				submitOrder.setApplNoUnit(generateApplNo.getApplicationUnit().trim());
				submitOrder.setApplNoPayung(generateApplNo.getApplicationPayung().trim());
				LOGGER.info("Set appl no : {} and payung no : {} ", generateApplNo.getApplicationUnit(),
						generateApplNo.getApplicationPayung());

				submitOrderRepository.save(submitOrder);

				LOGGER.info("Update Order NO {}", generateApplNo.getOrderNo());

				// Temporary
				var sendOrderToLeadReqDTO = new SendOrderToLeadV2ReqDTO();
				sendOrderToLeadReqDTO.setCredOrderNo(submitOrder.getOrderNo());
				submitListOrderDetailV2(sendOrderToLeadReqDTO);

			} catch (Exception e) {
				LOGGER.error("Error Update Nomor Aplikasi dengan Order no {}", generateApplNo.getOrderNo());
				LOGGER.error(e.toString());
			}

		}

	}

	public Map<String, Object> uploadEcm(MultipartFile file, String orderNo, String docType, String idNo) {

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		Map<String, Object> response = new HashMap<>();

		headers = new HttpHeaders();

		headers.set(CONTENT_TYPE.getMessage(), MULTIPART_FORM_DATA.getMessage());
		headers.set(X_API_KEY.getMessage(), keyEcm);

		var convFile = new File(file.getOriginalFilename());
		try (var fos = new FileOutputStream(convFile);) {

			Boolean newFile = convFile.createNewFile();
			if (Boolean.TRUE.equals(newFile))
				LOGGER.info("Create New File");

			fos.write(file.getBytes());

		} catch (Exception e) {
			if (convFile != null) {
				convFile.deleteOnExit();
			}
			LOGGER.error(FAILED_TO_CONVERT_FILE.getMessage(), e.getMessage());
		} finally {

			var resouceFile = new FileSystemResource(convFile);

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();

			if (docType.equalsIgnoreCase("2")) {
				body.add(DOCUMENT_TYPE.getMessage(), NPWP_CUSTOMER.getMessage());
				body.add(NO_NPWP.getMessage(), idNo);

			}
			if (docType.equalsIgnoreCase("1")) {
				body.add(DOCUMENT_TYPE.getMessage(), KTP_CUSTOMER.getMessage());
				body.add(NO_KTP.getMessage(), idNo);
			}
			if (docType.equalsIgnoreCase("3")) {
				body.add(DOCUMENT_TYPE.getMessage(), STNK.getMessage());
				body.add(NO_KTP.getMessage(), idNo);
			}
			body.add(FILE.getMessage(), resouceFile);
			body.add(DOCUMENT_TITLE.getMessage(), orderNo);
			body.add(REGION.getMessage(), "");
			body.add(APPLICATION.getMessage(), A1PARTNER.getMessage());
			body.add(OBJECT_STORE.getMessage(), ADIRA_OS.getMessage());
			body.add(REQUEST_ID.getMessage(), ONE_TO_FIVE.getMessage());

			try {
				HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);
				ResponseEntity<String> responseEntity = restTemplate.postForEntity(urlUploadEcm, entity, String.class);

				response.put("status", responseEntity.getStatusCode());
				response.put("body", responseEntity.getBody());

			} catch (HttpClientErrorException e) {
				LOGGER.error("Error when saving your upload ECM: {}", e.getMessage());
				response.put("status", e.getStatusCode());
				response.put("body", e.getMessage());

			} finally {
				if (convFile != null) {
					convFile.deleteOnExit();
				}
				stopWatch.stop();
				try {
					body.set(FILE.getMessage(), Base64.getEncoder().encodeToString(file.getBytes()));
					logService.insertToLogTable(body, response, UPLOAD_ECM.getMessage(), stopWatch.getTotalTimeMillis(),
							orderNo);
				} catch (IOException e) {
					LOGGER.error("Fail Convert File For Log Service ECM {} ", e.getMessage());
				} catch (Exception e2) {
					LOGGER.error("Fail Insert Log Service ECM {} ", e2.getMessage());
				}
			}
		}

		return response;

	}

	public String compareTo(String firstObject, String secondObject) {
		String newObject = null;
		if (secondObject != null) {
			if (!firstObject.equals(secondObject)) {
				newObject = secondObject;
			} else {
				newObject = firstObject;
			}
		} else {
			newObject = firstObject;
		}

		return newObject.trim();
	}

	// Penambahan Tabel Distribute Tracking
	public void distributedTrackingOrder(TrackingOrderDistributeDTO distributeTracking) throws AdiraCustomException {
		var applicationTracking = new ApplicationTracking();

		try {
			LOGGER.info("Start Mapping Tracking Distributed {}", distributeTracking.getApplicationTrackId());
			applicationTracking = modelMapper.map(distributeTracking, ApplicationTracking.class);
			applicationTrackingRepository.save(applicationTracking);
			LOGGER.info("Save Distributed Data Tracking {}", applicationTracking.getApplicationTrackId());

		} catch (Exception e) {
			LOGGER.error("Distributed Data Tracking {}", e.toString());

			throw new AdiraCustomException(FAILED_TO_SAVE_DATA_TO_DB.getMessage() + e.getMessage());

		}

	}

	public void distributedApplication(ApplicationDistributedDTO distributeApplication) throws AdiraCustomException {
		var application = new Application();
		List<ApplicationObject> applicationObject = new ArrayList<>();
		List<ApplicationDocument> applicationDocument = new ArrayList<>();
		List<ApplicationNote> applicationNote = new ArrayList<>();
		List<ApplicationInsurance> applicationInsurance = new ArrayList<>();
		List<ApplicationLoanDetail> applicationLoan = new ArrayList<>();
		List<ApplicationSalesInfo> applicationSalesInfo = new ArrayList<>();
		List<ApplicationObjectCollateralOtomotive> applicationObjectCollOto = new ArrayList<>();

		try {
			LOGGER.info("Start Mapping Application Distributed {}", distributeApplication.getApplicationNo());

			application = modelMapper.map(distributeApplication, Application.class);

			if (distributeApplication.getApplicationObject().length > 0) {
				LOGGER.info("map appl object {}", distributeApplication.getApplicationNo());
				applicationObject = Arrays.asList(
						modelMapper.map(distributeApplication.getApplicationObject(), ApplicationObject[].class));

				Arrays.asList(distributeApplication.getApplicationObject()).stream().forEach(e -> {

					if (e.getApplicationInsurance() != null && e.getApplicationInsurance().length > 0) {

						var listInsurance = Arrays
								.asList(modelMapper.map(e.getApplicationInsurance(), ApplicationInsurance[].class));
						applicationInsurance.addAll(listInsurance);
					}
					if (e.getApplicationLoan() != null && e.getApplicationLoan().length > 0) {
						var listApplicationLoan = Arrays
								.asList(modelMapper.map(e.getApplicationLoan(), ApplicationLoanDetail[].class));
						applicationLoan.addAll(listApplicationLoan);
					}
					if (e.getApplicationSalesInfo() != null && e.getApplicationSalesInfo().length > 0) {
						var listApplicationSalesInfo = Arrays
								.asList(modelMapper.map(e.getApplicationSalesInfo(), ApplicationSalesInfo[].class));
						applicationSalesInfo.addAll(listApplicationSalesInfo);
					}
					if (e.getApplicationObjectCollOto() != null) {
						var applicationCollateral = modelMapper.map(e.getApplicationObjectCollOto(),
								ApplicationObjectCollateralOtomotive.class);
						applicationObjectCollOto.add(applicationCollateral);
					}

				});
			}

			if (distributeApplication.getApplicationDocument() != null
					&& distributeApplication.getApplicationDocument().length > 0) {

				applicationDocument = Arrays.asList(
						modelMapper.map(distributeApplication.getApplicationDocument(), ApplicationDocument[].class));
			}

			if (distributeApplication.getApplicationNote() != null
					&& distributeApplication.getApplicationNote().length > 0) {
				applicationNote = Arrays
						.asList(modelMapper.map(distributeApplication.getApplicationNote(), ApplicationNote[].class));
			}
		} catch (Exception e) {
			throw new AdiraCustomException(FAILED_TO_SAVE_DATA_TO_DB.getMessage() + e.getMessage());

		}

		try {
			LOGGER.info("Save Distributed Data Application {}", application.getApplicationNo());
			applicationRepository.save(application);
			if (!applicationObject.isEmpty()) {

				LOGGER.info("save appl objt : {}", distributeApplication.getApplicationNo());
				applicationObjectRepository.saveAll(applicationObject);

				if (!applicationInsurance.isEmpty()) {
					LOGGER.info("save appl Insurance : {}", distributeApplication.getApplicationNo());
					applicationInsuranceRepository.saveAll(applicationInsurance);
				}
				if (!applicationLoan.isEmpty()) {
					LOGGER.info("save appl Loan : {}", distributeApplication.getApplicationNo());
					applicationLoanRepository.saveAll(applicationLoan);
				}
				if (!applicationSalesInfo.isEmpty()) {
					LOGGER.info("save appl Sales : {}", distributeApplication.getApplicationNo());
					applicationSalesInfoRepository.saveAll(applicationSalesInfo);
				}
				if (!applicationObjectCollOto.isEmpty()) {
					LOGGER.info("save appl Collateral : {}", distributeApplication.getApplicationNo());
					collateralOtomotifRepository.saveAll(applicationObjectCollOto);
				}

			}
			if (!applicationDocument.isEmpty()) {
				LOGGER.info("save appl doc : {}", distributeApplication.getApplicationNo());
				applicationDocumentRepository.saveAll(applicationDocument);
			}
			if (!applicationNote.isEmpty()) {
				LOGGER.info("save appl note : {}", distributeApplication.getApplicationNo());
				applicationNoteRepository.saveAll(applicationNote);
			}
		} catch (Exception e) {
			throw new AdiraCustomException(FAILED_TO_SAVE_DATA_TO_DB.getMessage() + e.getMessage());
		}

	}

	public void distributedCustomer(CustomerDistributedDTO distributeCustomer) throws AdiraCustomException {

		var customer = new Customer();
		var customerPersonal = new CustomerPersonal();
		var customerCompany = new CustomerCompany();
		var managementPic = new ManagementPic();
		List<CustomerAddress> customerAddress = new ArrayList<>();
		List<CustomerContact> customerContact = new ArrayList<>();

		try {

			LOGGER.info("Start Mapping Customer Distributed {}", distributeCustomer.getCustomerId());
			customer = modelMapper.map(distributeCustomer, Customer.class);
			customerRepository.save(customer);

			LOGGER.info("Save Distributed Data Customer {}", customer.getCustomerId());

			if (distributeCustomer.getCustomerType().equalsIgnoreCase(CUST_PERSONAL.getMessage())) {
				customerPersonal = modelMapper.map(distributeCustomer.getCustomerPersonal(), CustomerPersonal.class);
				customerPersonalRepository.save(customerPersonal);
				LOGGER.info("Save Distributed Data Customer Personal {}", customer.getCustomerId());
			} else {

				customerCompany = modelMapper.map(distributeCustomer.getCustomerCompany(), CustomerCompany.class);
				customerCompanyRepository.save(customerCompany);
				LOGGER.info("Save Distributed Data Customer Company {}", customer.getCustomerId());

				if (distributeCustomer.getCustomerCompany().getManagementPIC() != null) {
					managementPic = modelMapper.map(distributeCustomer.getCustomerCompany().getManagementPIC(),
							ManagementPic.class);

					managementPicRepository.save(managementPic);
					LOGGER.info("Save Distributed Data Management PIC {}", customer.getCustomerId());
				}
			}

			if (distributeCustomer.getCustomerAddress().length != 0) {
				customerAddress = Arrays
						.asList(modelMapper.map(distributeCustomer.getCustomerAddress(), CustomerAddress[].class));
				customerAddressRepository.saveAll(customerAddress);
				LOGGER.info("Save Distributed Data Customer Address {}", customer.getCustomerId());
			}
			if (distributeCustomer.getCustomerContact().length != 0) {
				customerContact = Arrays
						.asList(modelMapper.map(distributeCustomer.getCustomerContact(), CustomerContact[].class));
				customerContactRepository.saveAll(customerContact);
				LOGGER.info("Save Distributed Data Customer Contact {}", customer.getCustomerId());
			}

		} catch (Exception e) {
			LOGGER.error("Distributed Data Customer {}", e.toString());

			throw new AdiraCustomException(FAILED_TO_SAVE_DATA_TO_DB.getMessage() + e.getMessage());

		}

	}

	public void distributedPO(PurchaseOrderDistributedDTO distributePO) throws AdiraCustomException {
		var purchaseOrder = new PurchaseOrder();
		try {

			LOGGER.info("Start Mapping PO Distributed {}", distributePO.getPoNo());
			purchaseOrder = modelMapper.map(distributePO, PurchaseOrder.class);

			poRepository.save(purchaseOrder);

			LOGGER.info("Save Distributed Data PO {}", distributePO.getPoNo());
		} catch (Exception e) {
			LOGGER.error("Distributed Data PO {}", e.toString());

			throw new AdiraCustomException(FAILED_TO_SAVE_DATA_TO_DB.getMessage() + e.getMessage());

		}

	}

	public DataResponse getOCR(MultipartFile file, String flag) {
		var dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());
		var url = "";

		headers = new HttpHeaders();
		headers.set(CONTENT_TYPE.getMessage(), MULTIPART_FORM_DATA.getMessage());
		headers.set(HttpHeaders.AUTHORIZATION, keyIDP);

		var convFile = new File(file.getOriginalFilename());

		try (var fos = new FileOutputStream(convFile);) {

			Boolean newFile = convFile.createNewFile();
			if (Boolean.TRUE.equals(newFile))
				LOGGER.info("Create New File");

			fos.write(file.getBytes());

		} catch (Exception e) {
			if (convFile != null) {
				convFile.deleteOnExit();
			}
			LOGGER.error(FAILED_TO_CONVERT_FILE.getMessage(), e.getMessage());
		} finally {

			var resouceFile = new FileSystemResource(convFile);

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
			body.add(FILE.getMessage(), resouceFile);
			body.add(REQ_IDP.getMessage(), new Date().getTime());

			try {
				HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);
				if (flag.equalsIgnoreCase("1")) {
					url = urlIDP.concat(pathNpwp);
					OcrNpwpResponsesDTO response = restTemplate.postForObject(url, entity, OcrNpwpResponsesDTO.class);
					dataResponse.setData(response);
				} else {
					url = urlIDP.concat(pathKTP);
					OcrKtpResponsesDTO response = restTemplate.postForObject(url, entity, OcrKtpResponsesDTO.class);
					dataResponse.setData(response);
				}
			} catch (Exception e) {
				if (convFile != null) {
					convFile.deleteOnExit();
				}
				LOGGER.info("Error when saving your upload ECM: {}", e.getMessage());
			} finally {
				if (convFile != null) {
					convFile.deleteOnExit();
				}

			}
		}

		return dataResponse;

	}

	public DataResponse getWebOCR(IdpWebReqDTO idpWebReqDTO) {

		var dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());
		var url = "";
		var fileTipe = ".jpg";
		var fileLinuxirectory = "/tmp";
		var fileWindowsirectory = "/Temp";

		headers = new HttpHeaders();
		headers.set(CONTENT_TYPE.getMessage(), MULTIPART_FORM_DATA.getMessage());
		headers.set(HttpHeaders.AUTHORIZATION, keyIDP);

		byte[] fileByte = Base64.getDecoder().decode(idpWebReqDTO.getFile());
		var convFile = new File(pathNpwp);

		try {
			if (SystemUtils.IS_OS_WINDOWS) {
				convFile = File.createTempFile("tempImage", fileTipe, new File(fileWindowsirectory));
			} else {
				convFile = File.createTempFile("tempImage", fileTipe, new File(fileLinuxirectory));
			}

		} catch (Exception e) {
			if (convFile != null) {
				convFile.deleteOnExit();
			}
			LOGGER.error(FAILED_TO_CREATE_FILE.getMessage(), e.getMessage());
		} finally {

			try (var fos = new FileOutputStream(convFile, false);) {

				Boolean newFile = convFile.createNewFile();
				if (Boolean.TRUE.equals(newFile))
					LOGGER.info("Create New File");
				fos.write(fileByte);

			} catch (Exception e) {

				if (convFile != null) {
					convFile.deleteOnExit();
				}

				LOGGER.error(FAILED_TO_CONVERT_FILE.getMessage(), e.getMessage());
			} finally {

				try {
					var resouceFile = new FileSystemResource(convFile);

					MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
					body.add(FILE.getMessage(), resouceFile);
					body.add(REQ_IDP.getMessage(), new Date().getTime());

					HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);
					if (idpWebReqDTO.getFlag().equalsIgnoreCase("1")) {
						url = urlIDP.concat(pathNpwp);
						OcrNpwpResponsesDTO response = restTemplate.postForObject(url, entity,
								OcrNpwpResponsesDTO.class);
						dataResponse.setData(response);
					} else {
						url = urlIDP.concat(pathKTP);
						OcrKtpResponsesDTO response = restTemplate.postForObject(url, entity, OcrKtpResponsesDTO.class);
						dataResponse.setData(response);
					}
				} catch (Exception e2) {
					if (convFile != null) {
						convFile.deleteOnExit();
					}
					LOGGER.error("Error when saving your upload ECM: {}", e2.getMessage());
				} finally {
					if (convFile != null) {
						convFile.deleteOnExit();
					}
				}
			}
		}
		return dataResponse;
	}

	public void sendNotificationTracking(TrackingOrderDistributeDTO request) throws AdiraCustomException {

		// Find Lates Status Tracking From DB
		var latesTracking = applicationTrackingRepository.findLatesGroupStatus(request.getFkApplicationId());

		// Find Description Group Status From Parameter Table on DB
		var findStatusTracking = applicationTrackingRepository.findDescTrackingByGroupAndStatus(request.getStatus(),
				request.getGroupStatus());

		if (latesTracking.isEmpty() && !request.getStatus().equalsIgnoreCase("ST0001")) {

			LOGGER.info("Skip Send Notif for {} {}", request.getFkApplicationId(), request.getApplicationTrackId());

		} else if (latesTracking.isEmpty() && request.getStatus().equalsIgnoreCase("ST0001")) {

			try {

				mapNotifPartnerSubmit(findStatusTracking, request);
				LOGGER.info("Send Notif Submit Partner Success {} {}", request.getProcessType(),
						findStatusTracking.getApplGroupStatus());

			} catch (Exception e) {
				throw new AdiraCustomException(FAILED_TO_SEND_NOTIF.getMessage() + e.getMessage());
			}
		} else if (findStatusTracking != null && !latesTracking.isEmpty()
				&& !latesTracking.get(0).getGroupStatus().equalsIgnoreCase(findStatusTracking.getApplGroupStatus())) {

			try {

				mapNotifPerubahanStatus(findStatusTracking, latesTracking.get(0));
				LOGGER.info("Send Notif Perubahan Status Success {} {} to {}", request.getFkApplicationId(),
						latesTracking.get(0).getGroupStatus(), findStatusTracking.getApplGroupStatus());

			} catch (Exception e) {
				throw new AdiraCustomException(FAILED_TO_SEND_NOTIF.getMessage() + e.getMessage());
			}
		}

	}

	public void mapNotifPartnerSubmit(ParaApplicationStatus findStatusTracking, TrackingOrderDistributeDTO request) {

		String groupStatus;
		String title;
		String body;
		String customerName = "";
		String applNo = "";
		String applNoUnit = "";
		String pic = "";

		var sendMessage = new FcmNotificationRequestDTO();

		SubmitOrder orderPartner = submitOrderRepository.findSubmitOrderByOrderNo(request.getProcessType());

		groupStatus = findStatusTracking.getApplGroupStatus();
		title = "OnProgress ".concat(groupStatus);
		if (orderPartner != null) {
			if (!orderPartner.getApplNoPayung().isBlank()) {
				applNo = orderPartner.getApplNoPayung();
				applNoUnit = orderPartner.getApplNoUnit();
			} else {
				applNo = orderPartner.getOrderNo();
				applNoUnit = orderPartner.getOrderNo();

			}
			customerName = orderPartner.getSubmitCustomer().getCustomerName().toUpperCase();
			pic = orderPartner.getPic().toLowerCase();
		}

		body = "Order dengan Nomor Aplikasi ".concat(applNo).concat(" sedang ")
				.concat(findStatusTracking.getApplStatus());

		sendMessage.setUserId(pic.replaceAll("[@]+", "%"));
		sendMessage.setTo("/topics/".concat("sub".concat(pic.replaceAll("[@]+", "%"))));
		sendMessage.setNotification(sendMessage.new Notification(title, body));
		sendMessage.setData(sendMessage.new Data(request.getProcessType(), applNo, applNoUnit,
				findStatusTracking.getApplSubStatus(), "1"));

		// Send Notification Using FCM
		restTemplateService.sendNotifToFirebaseCloudMessaging(sendMessage);

		// Save Notification Into DB
		saveNotification(new Notification(dataId, pic, request.getProcessType(),
				applNo, applNoUnit, customerName, body, groupStatus,
				new Date(), false, true));
	}

	public void mapNotifPerubahanStatus(ParaApplicationStatus findStatusTracking,
			LatesTrackingForFCMResponsesDTO latesTracking) {

		String groupStatus;
		String title;
		String body;
		String customerName;

		var sendMessage = new FcmNotificationRequestDTO();

		// Find Customer Name (For List Notification in Menu Home)
		var findCustomerName = customerRepository.findCustomerNameByApplicationNo(latesTracking.getApplicationNo());

		groupStatus = findStatusTracking.getApplGroupStatus();
		title = "OnProgress ".concat(groupStatus);
		body = "Order dengan Nomor Aplikasi ".concat(latesTracking.getApplicationNo()).concat(" sedang ")
				.concat(findStatusTracking.getApplStatus());

		customerName = findCustomerName != null ? findCustomerName.get("customerName", String.class) : "";

		sendMessage.setUserId(latesTracking.getUserId().replaceAll("[@]+", "%"));
		sendMessage.setTo("/topics/".concat("sub".concat(latesTracking.getUserId().replaceAll("[@]+", "%"))));
		sendMessage.setNotification(sendMessage.new Notification(title, body));
		sendMessage.setData(sendMessage.new Data(latesTracking.getOrderNo(), latesTracking.getApplicationNo(),
				latesTracking.getApplicationUnitNo(), findStatusTracking.getApplSubStatus(), "1"));

		// Send Notification Using FCM
		restTemplateService.sendNotifToFirebaseCloudMessaging(sendMessage);

		// Save Notification Into DB
		saveNotification(new Notification(dataId, latesTracking.getUserId(), latesTracking.getOrderNo(),
				latesTracking.getApplicationNo(), latesTracking.getApplicationUnitNo(), customerName, body, groupStatus,
				new Date(), false, true));
	}

	public void sendNotificationSurvey(ApplicationDistributedDTO request) throws AdiraCustomException {
		// find Lates SurveyDate from DB
		var surveyDate = applicationTrackingRepository.findSurveyDateByApplNo(request.getApplicationNo());

		if (surveyDate != null && request.getSurveyAppointmentDate() != null) {
			try {

				if (surveyDate.get("surveydate") != null && !dataNtimeFormat.format(surveyDate.get("surveydate"))
						.equalsIgnoreCase(dataNtimeFormat.format(request.getSurveyAppointmentDate()))) {

					var sendMessage = new FcmNotificationRequestDTO();

					// Find Customer Name (For List Notification in Menu Home)
					var findCustomerName = customerRepository
							.findCustomerNameByApplicationNo(request.getApplicationNo());

					var title = "Resurvey Jadwal";
					var body = "Order dengan Nomor Aplikasi ".concat(request.getApplicationNo())
							.concat(" Resurvey ke Tanggal ")
							.concat(secondSdf.format(request.getSurveyAppointmentDate())).concat("Jam")
							.concat(formatTime.format(request.getSurveyAppointmentDate()));
					var customerName = findCustomerName != null ? findCustomerName.get("customerName", String.class)
							: title;

					sendMessage.setUserId(request.getCreatedBy().replaceAll("[@]+", "%"));
					sendMessage.setTo("/topics/".concat("sub".concat(request.getCreatedBy().replaceAll("[@]+", "%"))));

					sendMessage.setNotification(sendMessage.new Notification(title, body));

					sendMessage.setData(sendMessage.new Data(request.getOrderNo(), request.getApplicationNo(),
							request.getApplicationObject()[0].getApplicationNoUnit(), "Resurvey", "1"));

					// Send Notification Using FCM
					restTemplateService.sendNotifToFirebaseCloudMessaging(sendMessage);

					// Save Notification Into DB
					saveNotification(new Notification(dataId, request.getCreatedBy(), request.getOrderNo(),
							request.getApplicationNo(), request.getApplicationObject()[0].getApplicationNoUnit(),
							customerName, body, "Survey", new Date(), false, true));
				}
			} catch (Exception e) {
				throw new AdiraCustomException(FAILED_TO_SEND_NOTIF.getMessage() + e.getMessage());
			}
		}

	}

	public void saveNotification(Notification notification) {

		try {
			notifRepository.save(notification);
		} catch (Exception e) {
			LOGGER.error("Saving Notification into DB {}", e.toString());
		}

	}

	private void insertErrorResponseWhenExceptionOccurs(DataResponse dataResponse, String errorMessage, Exception e) {
		dataResponse.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		dataResponse.setStatus(1);
		dataResponse.setMessage(THERE_IS_SOMETHING_ERROR_IN_OUR_SYSTEM.getMessage());
		dataResponse.setData(null);

		LOGGER.error(errorMessage, e.getMessage());

	}

	private void insertErrorStdResponseWhenExceptionOccurs(StandardResponseDTO dataResponse, String errorMessage,
			Exception e) {
		dataResponse.setStatus(1);
		dataResponse.setMessage(THERE_IS_SOMETHING_ERROR_IN_OUR_SYSTEM.getMessage());

		LOGGER.error(errorMessage, e.getMessage());

	}

	private DataResponse insertResponseWhenSubmitOrderIsNotFound(DataResponse dataResponse, String message) {
		dataResponse.setMessage(message);
		dataResponse.setStatus(1);
		dataResponse.setData(null);

		return dataResponse;

	}

	private StandardResponseDTO insertStdResponseWhenSubmitOrderIsNotFound(StandardResponseDTO dataResponse,
			String message) {
		dataResponse.setMessage(message);
		dataResponse.setStatus(1);

		return dataResponse;

	}
}
