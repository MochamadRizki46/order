package id.co.adira.partner.order.service;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import id.co.adira.partner.order.config.BeanConfig;
import id.co.adira.partner.order.dto.kafka.ApplicationDistributedDTO;
import id.co.adira.partner.order.dto.kafka.CustomerDistributedDTO;
import id.co.adira.partner.order.dto.kafka.TrackingOrderDistributeDTO;
import id.co.adira.partner.order.entity.LogConsumer;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;

@Service
public class RetryMsgService {

	@Autowired
	LogService logService;

	@Autowired
	OrderService orderService;

	private static final Logger LOGGER = LoggerFactory.getLogger(RetryMsgService.class);

	final Integer COUNT = 2;

	@Value("${spring.kafka.topic.sales-customer-distributed}")
	private String customerTopic;

	@Value("${spring.kafka.topic.lead-tracking-distributed}")
	private String trackingTopic;

	@Value("${spring.kafka.topic.lead-application-distributed}")
	private String applicationTopic;

	private String getYesterdayDateString() {
		var now = Instant.now();
		Instant date = now.minus(1, ChronoUnit.DAYS);

		return new SimpleDateFormat("yyyy-MM-dd").format(Date.from(date));
	}

	@SchedulerLock(name = "retryCustomer", lockAtMostFor = "PT5M")
	public void retryCustomer() {

		List<LogConsumer> listMsgLog = logService.getLogByTopicAndCount(COUNT, customerTopic, getYesterdayDateString());

		LOGGER.info("Execute Retry Customer");

		for (LogConsumer log : listMsgLog) {
			threadCustomer(log);
		}

	}

	private void threadCustomer(LogConsumer log) {

		try {

			CustomerDistributedDTO customerDistributed = new BeanConfig().getObjectMapper().readValue(log.getJsonFile(),
					CustomerDistributedDTO.class);

			orderService.distributedCustomer(customerDistributed);
			
			LOGGER.info("Success Retry Customer {}", String.valueOf(log.getId()));

			logService.updateLogConsumer(log.getId(), LogService.COMPLETED, null, log.getFlagStatus() + 1);

		} catch (Exception e) {

			try {
				LOGGER.info("Failed Retry Customer {}", String.valueOf(log.getId()));

				logService.updateLogConsumer(log.getId(), LogService.ERR, e.getMessage(), log.getFlagStatus() + 1);

			} catch (Exception e2) {

			}

		}
	}

	@SchedulerLock(name = "retryTracking", lockAtMostFor = "PT5M")
	public void retryTracking() {

		List<LogConsumer> listMsgLog = logService.getLogByTopicAndCount(COUNT, trackingTopic, getYesterdayDateString());

		for (LogConsumer log : listMsgLog) {
			threadTracking(log);
		}

	}

	private void threadTracking(LogConsumer log) {

		try {

			TrackingOrderDistributeDTO customerDistributed = new BeanConfig().getObjectMapper()
					.readValue(log.getJsonFile(), TrackingOrderDistributeDTO.class);

			orderService.distributedTrackingOrder(customerDistributed);
			LOGGER.info("Success Retry Tracking {}", String.valueOf(log.getId()));

			logService.updateLogConsumer(log.getId(), LogService.COMPLETED, null, log.getFlagStatus() + 1);

		} catch (Exception e) {

			try {
				LOGGER.info("Failed Retry Tracking {}", String.valueOf(log.getId()));

				logService.updateLogConsumer(log.getId(), LogService.ERR, e.getMessage(), log.getFlagStatus() + 1);

			} catch (Exception e2) {

			}

		}
	}

	@SchedulerLock(name = "retryApplication", lockAtMostFor = "PT5M")
	public void retryApplication() {

		List<LogConsumer> listMsgLog = logService.getLogByTopicAndCount(COUNT, applicationTopic,
				getYesterdayDateString());

		for (LogConsumer log : listMsgLog) {
			threadApplication(log);
		}

	}

	private void threadApplication(LogConsumer log) {

		try {

			var data = new BeanConfig().getObjectMapper().readValue(log.getJsonFile(), HashMap.class);

			ApplicationDistributedDTO customerDistributed = new BeanConfig().getObjectMapper()
					.convertValue(data.get("data"), ApplicationDistributedDTO.class);

			orderService.distributedApplication(customerDistributed);

			LOGGER.info("Success Retry Application {}", String.valueOf(log.getId()));

			logService.updateLogConsumer(log.getId(), LogService.COMPLETED, null, log.getFlagStatus() + 1);

		} catch (Exception e) {

			try {
				LOGGER.info("Failed Retry Application {}", String.valueOf(log.getId()));

				logService.updateLogConsumer(log.getId(), LogService.ERR, e.getMessage(), log.getFlagStatus() + 1);

			} catch (Exception e2) {

			}

		}
	}

}
