package id.co.adira.partner.order.service.resttemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.NotNull;

import id.co.adira.partner.order.config.BeanConfig;
import id.co.adira.partner.order.dto.creditsimulation.CreditSimulationHeadReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.CreditSimulationNewReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.apikey.APIKeyRequestDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.AsuransiCashAndCreditReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.BiayaAdminReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.EffectiveOrLandingRateReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.dporpmt.DownPaymentOrPmtReqToCommonServiceDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.apikey.APIKeyHeadResponseDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.*;
import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPmtHeadResDTO;
import id.co.adira.partner.order.dto.fcm.FcmNotificationRequestDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;

import javax.persistence.metamodel.StaticMetamodel;

import static id.co.adira.partner.order.constant.AppConst.*;

@Service
public class RestTemplateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestTemplateService.class.getName());

	ObjectMapper objectMapper = new BeanConfig().getObjectMapper();

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	Environment env;

	public CreditSimulationDetailResDTO callPostRestApiForCalculateCredSim(String authorization,
			CreditSimulationHeadReqDTO creditSimulationHeadReqDTO) {
		HttpHeaders headers = new HttpHeaders();

		headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);
		headers.set(AUTHORIZATION.getMessage(), authorization);

		HttpEntity httpEntity = new HttpEntity(creditSimulationHeadReqDTO, headers);
		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(env.getProperty("url.calculate.credit.simulation"));

		return restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity, CreditSimulationDetailResDTO.class)
				.getBody();

	}
	
	public CreditSimulationResHeaderDTO callPostRestApiForCalculateCredSimNew(
			CreditSimulationNewReqDTO creditSimulationNewReqDTO) throws JsonProcessingException {
		APIKeyRequestDTO apiKeyRequestDTO = new APIKeyRequestDTO();
        apiKeyRequestDTO.setUserName(env.getProperty("key.credsim"));
        APIKeyHeadResponseDTO apiKeyHeadResponseDTO = callPostRestApiForGetApiKey(apiKeyRequestDTO);
		
		HttpHeaders headers = new HttpHeaders();

		headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);
		headers.set(AUTHORIZATION.getMessage(), apiKeyHeadResponseDTO.getData().getApiKey());

		HttpEntity httpEntity = new HttpEntity(creditSimulationNewReqDTO, headers);
		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(env.getProperty("url.calculate.credit.simulation.new"));

		return restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity, CreditSimulationResHeaderDTO.class)
				.getBody();

	}

	public APIKeyHeadResponseDTO callPostRestApiForGetApiKey(APIKeyRequestDTO apiKeyRequestDTO)
			throws JsonProcessingException {
		HttpHeaders headers = new HttpHeaders();

		headers.set(ACCEPT.getMessage(), MediaType.APPLICATION_JSON_VALUE);
		HttpEntity httpEntity = new HttpEntity(apiKeyRequestDTO, headers);
		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(env.getProperty("url.get.api.key"));

		ResponseEntity<String> response = restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity,
				String.class);

		APIKeyHeadResponseDTO apiKeyHeadResponseDTO = objectMapper.readValue(response.getBody(),
				APIKeyHeadResponseDTO.class);

		LOGGER.info(response.getBody());

		return apiKeyHeadResponseDTO;
	}

	public DpOrPmtHeadResDTO callPostRestApiForGetCalculationDpOrPmt(
			DownPaymentOrPmtReqToCommonServiceDTO downPaymentOrPmtReqToCommonServiceDTO, String authorization) {
		HttpHeaders headers = new HttpHeaders();

		headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);
		headers.set(AUTHORIZATION.getMessage(), authorization);

		HttpEntity httpEntity = new HttpEntity(downPaymentOrPmtReqToCommonServiceDTO, headers);
		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(env.getProperty("url.api.calculate.dp.or.pmt"));

		return restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity, DpOrPmtHeadResDTO.class).getBody();
	}

	public BiayaAdminResDTO callRestTemplateToGetBiayaAdmin(BiayaAdminReqDTO biayaAdminReqDTO)
			throws JsonProcessingException {
		HttpHeaders headers = new HttpHeaders();

		headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);

		HttpEntity httpEntity = new HttpEntity(biayaAdminReqDTO, headers);
		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(env.getProperty("url.api.get.biaya.admin"));

		ResponseEntity<String> response = restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity,
				String.class);

		BaseResponseDTO baseResponseDTO = objectMapper.readValue(response.getBody(), BaseResponseDTO.class);

		LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) baseResponseDTO.getData();
		BiayaAdminResDTO biayaAdminResDTO = new BiayaAdminResDTO(
				BigDecimal.valueOf(Double.valueOf(String.valueOf(map.get(ADMIN_CASH_FEE.getMessage())))),
				BigDecimal.valueOf(Double.valueOf(String.valueOf(map.get(ADMIN_CREDIT_FEE.getMessage())))));

		return biayaAdminResDTO;
	}

	public AsuransiCashAndCreditResDTO callRestTemplateToGetAsuransiCashAndCredit(
			AsuransiCashAndCreditReqDTO asuransiCashAndCreditReqDTO) throws JsonProcessingException {
		HttpHeaders headers = new HttpHeaders();

		headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);
		HttpEntity httpEntity = new HttpEntity(asuransiCashAndCreditReqDTO, headers);
		UriComponentsBuilder uri = UriComponentsBuilder
				.fromHttpUrl(env.getProperty("url.api.get.asuransi.cash.and.credit.rate"));

		ResponseEntity<String> response = restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity,
				String.class);

		BaseResponseDTO baseResponseDTO = objectMapper.readValue(response.getBody(), BaseResponseDTO.class);

		LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) baseResponseDTO.getData();
		AsuransiCashAndCreditResDTO asuransiCashAndCreditResDTO = new AsuransiCashAndCreditResDTO(
				BigDecimal.valueOf(Double.valueOf(String.valueOf(map.get(ASURANSI_CASH_AMT.getMessage())))).setScale(5,
						RoundingMode.HALF_UP),
				BigDecimal.valueOf(Double.valueOf(String.valueOf(map.get(ASURANSI_CREDIT_AMT.getMessage()))))
						.setScale(5, RoundingMode.HALF_UP));

		return asuransiCashAndCreditResDTO;
	}

	public EffectiveOrLandingRateResDTO callRestTemplateToGetEffectiveOrLandingRate(
			EffectiveOrLandingRateReqDTO effectiveOrLandingRateReqDTO) throws JsonProcessingException {
		HttpHeaders headers = new HttpHeaders();

		headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);
		HttpEntity httpEntity = new HttpEntity(effectiveOrLandingRateReqDTO, headers);
		UriComponentsBuilder uri = UriComponentsBuilder
				.fromHttpUrl(env.getProperty("url.api.get.effective.or.landing.rate"));

		ResponseEntity<String> response = restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity,
				String.class);

		BaseResponseDTO baseResponseDTO = objectMapper.readValue(response.getBody(), BaseResponseDTO.class);

		LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) baseResponseDTO.getData();
		EffectiveOrLandingRateResDTO effectiveOrLandingRateResDTO = new EffectiveOrLandingRateResDTO(
				BigDecimal.valueOf(Double.valueOf(String.valueOf(map.get(EFFECTIVE_OR_LANDING_RATE.getMessage()))))
						.setScale(5, RoundingMode.HALF_UP));

		return effectiveOrLandingRateResDTO;
	}

	public Integer sendNotifToFirebaseCloudMessaging(FcmNotificationRequestDTO fcmRequest) {

		var headers = new HttpHeaders();
		var response = new ResponseEntity(HttpStatus.OK);

		try {
			var body = objectMapper.writeValueAsString(fcmRequest);

			headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);
			headers.set(AUTHORIZATION.getMessage(), env.getProperty("key.api.fcm"));

			var uri = UriComponentsBuilder.fromHttpUrl(env.getProperty("url.api.fcm"));
			
			var httpEntity = new HttpEntity(body, headers);

			response = restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity, String.class);
		} catch (Exception e) {
			LOGGER.error("Error Hit Service Firebase {}", "response Code = " +response + "Desc " + e.toString());
			response = new ResponseEntity(HttpStatus.UNAUTHORIZED);
		}

		return response.getStatusCodeValue();
	}
}