package id.co.adira.partner.order.utils;

import java.sql.Timestamp;
import java.time.LocalDate;

public class DateUtil {
	
	private DateUtil() {
	    throw new IllegalStateException("Utility class");
	}

	public static Timestamp getCurrentTimestamp() {
		return new Timestamp(System.currentTimeMillis());
	}

	public static LocalDate getDateBasedOnInterval(Integer numberOfPrevDay){
		return LocalDate.now().minusDays(numberOfPrevDay);
	}
}
