/**
 * 
 */
package id.co.adira.partner.order.utils;

/**
 * @author Vionza
 *
 */
@Generated
public enum GroupStatusTracking {
	SVR,
	SVY,
	SCA,
	SPO,
	SPP,
	SOD,
	SRJ,
	SCO
}
