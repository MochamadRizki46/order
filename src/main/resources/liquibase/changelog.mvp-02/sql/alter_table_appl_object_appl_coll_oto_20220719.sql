ALTER TABLE public.application_object ALTER COLUMN object_used TYPE varchar(5) USING object_used::varchar;
ALTER TABLE public.application_object ALTER COLUMN object_purpose TYPE varchar(5) USING object_purpose::varchar;

ALTER TABLE public.application_object_coll_oto ALTER COLUMN object_purpose TYPE varchar(6) USING object_purpose::varchar;
ALTER TABLE public.application_object_coll_oto ALTER COLUMN cola_purpose TYPE varchar(5) USING cola_purpose::varchar;
