ALTER TABLE public.customer DROP CONSTRAINT customer_fk;
ALTER TABLE public.customer ADD CONSTRAINT customer_un UNIQUE (customer_id);
