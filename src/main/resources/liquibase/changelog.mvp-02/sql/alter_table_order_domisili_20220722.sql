ALTER TABLE public.tbl_order_domisili ADD flag_kelurahan int NULL;
COMMENT ON COLUMN public.tbl_order_domisili.flag_kelurahan IS 'flag keluran ditemukan, 0 = ya ; 1 = tidak';
ALTER TABLE public.tbl_order_domisili ADD sales_notes varchar(200) NULL;
COMMENT ON COLUMN public.tbl_order_domisili.sales_notes IS 'note untuk sales jika kelurahan tidak ditemukan';
