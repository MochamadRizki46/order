ALTER TABLE public.tbl_order_unit ALTER COLUMN object_brand TYPE varchar(6) USING object_brand::varchar;
ALTER TABLE public.tbl_order_unit ALTER COLUMN object_type TYPE varchar(6) USING object_type::varchar;
