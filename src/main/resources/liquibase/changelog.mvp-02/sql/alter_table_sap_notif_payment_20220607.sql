DROP TABLE public.sap_notif_payment;

CREATE TABLE public.sap_notif_payment (
	sap_notif_payment_id varchar(36) NOT NULL,
	tanggal_cair date NULL,
	jenis_cair varchar(20) NULL,
	jenis_komisi varchar(20) NULL,
	np_doc_no_invoice varchar(16) NULL,
	np_doc_no_payment varchar(16) NULL,
	np_accountholder varchar(50) NULL,
	np_norekening varchar(40) NULL,
	np_bank_code varchar(15) NULL,
	np_bank varchar(60) NULL,
	appl_no varchar(16) NULL,
	appl_contract_no varchar(18) NULL,
	appl_br_id varchar(4) NULL,
	bp_ext varchar(20) NULL,
	amt_cair numeric NULL,
	np_bp_vendor_no varchar(20) NULL,
	status_transfer varchar(20) NULL,
	job varchar(20) NULL,
	CONSTRAINT sap_notif_payment_pk PRIMARY KEY (sap_notif_payment_id)
);
