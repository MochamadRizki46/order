CREATE INDEX if not exists application_tracking_fk_application_id_idx1 ON public.application_tracking (fk_application_id);

CREATE INDEX if not exists application_tracking_sequence_indx ON public.application_tracking ("sequence");

CREATE INDEX if not exists application_object_third_party_id_idx ON public.application_object (third_party_id);

CREATE INDEX if not exists para_appl_status_squad_idx ON public.para_appl_status (squad,para_appl_status_id);

CREATE INDEX if not exists application_order_date_idx ON public.application (order_date);
