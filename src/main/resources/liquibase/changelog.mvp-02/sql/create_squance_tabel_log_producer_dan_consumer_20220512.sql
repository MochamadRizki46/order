CREATE SEQUENCE public.log_consumer_partner_seq INCREMENT 1 START 1 MINVALUE 1;
CREATE SEQUENCE public.log_producer_partner_seq INCREMENT 1 START 1 MINVALUE 1;