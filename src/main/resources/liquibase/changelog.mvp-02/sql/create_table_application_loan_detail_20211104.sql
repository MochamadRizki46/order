CREATE TABLE if not exists public.application_loan_detail (
	sk_application_loan_detail varchar(36) NOT NULL,
	installment_no numeric NULL,
	percentage numeric NULL,
	amount numeric NULL,
	active bool NULL,
	created_date timestamp NULL,
	created_by varchar(50) NULL,
	last_modified_date timestamp NULL,
	last_modified_by varchar(50) NULL,
	fk_application_object varchar(36) NULL,
	CONSTRAINT application_loan_detail_pk PRIMARY KEY (sk_application_loan_detail),
	CONSTRAINT application_loan_detail_fk FOREIGN KEY (fk_application_object) REFERENCES application_object(sk_application_object)
);