CREATE TABLE if not exists public.application_note (
	sk_application_note varchar(36) NOT NULL,
	application_no varchar(20) NULL,
	flag bool NULL,
	note varchar(500) NULL,
	active bool NULL,
	created_date timestamp NULL,
	created_by varchar(50) NULL,
	last_modified_date timestamp NULL,
	last_modified_by varchar(50) NULL,
	CONSTRAINT application_note_pk PRIMARY KEY (sk_application_note),
	CONSTRAINT application_note_fk FOREIGN KEY (application_no) REFERENCES application(fk_customer)
);