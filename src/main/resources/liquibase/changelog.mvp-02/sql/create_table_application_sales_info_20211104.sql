CREATE TABLE if not exists public.application_sales_info (
	sk_objek_sales varchar(36) NOT NULL,
	employee_id varchar(50) NULL,
	sales_type varchar(3) NULL,
	employee_head_id varchar(10) NULL,
	employee_job varchar(3) NULL,
	active bool NULL,
	created_date timestamp NULL,
	created_by varchar(50) NULL,
	last_modified_date timestamp NULL,
	last_modified_by varchar(50) NULL,
	fk_application_object varchar(36) NULL,
	employee_head_job varchar(3) NULL,
	employee_head_name varchar(100) NULL,
	employee_name varchar(100) NULL,
	CONSTRAINT application_sales_info_pk PRIMARY KEY (sk_objek_sales),
	CONSTRAINT application_sales_info_fk FOREIGN KEY (fk_application_object) REFERENCES application_object(sk_application_object)
);