CREATE TABLE if not exists public.application_tracking (
	application_track_id varchar(36) NOT NULL,
	process_type varchar(50) NULL,
	next_process varchar(20) NULL,
	status varchar(20) NULL,
	datetime_status timestamp NULL,
	fk_application_id varchar(36) NULL,
	employee_id varchar(20) NULL,
	employee_name varchar(50) NULL,
	employee_job varchar(50) NULL,
	"sequence" int4 NULL,
	active bool NULL,
	created_date timestamp NULL,
	created_by varchar(50) NULL,
	last_modified_date timestamp NULL,
	last_modified_by varchar(50) NULL,
	group_status varchar(20) NULL,
	CONSTRAINT application_tracking_pk PRIMARY KEY (application_track_id)
);