CREATE TABLE IF NOT EXISTS public.log_consumer_partner (
	id int8 NULL,
	json_file text NULL,
	topic varchar(250) NULL,
	status varchar(100) NULL,
	flag_status int4 NULL,
	created_date timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
	message text NULL
);

CREATE TABLE IF NOT EXISTS public.log_producer_partner (
	id int8 NULL,
	json_file text NULL,
	topic varchar(250) NULL,
	status varchar(100) NULL,
	flag_status int4 NULL,
	created_date timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
	message text NULL
);
