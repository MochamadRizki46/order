CREATE TABLE if not exists public.tbl_logs (
	log_id varchar(36) NOT NULL,
	order_no varchar(20) NOT NULL,
	log_name varchar(50) NULL,
	json_request jsonb NULL,
	json_responses jsonb NULL,
	process_date timestamp NULL,
	duration numeric NULL,
	user_id varchar(30) NULL,
	active int4 NULL,
	CONSTRAINT tbl_logs_pk PRIMARY KEY (log_id)
);
CREATE INDEX tbl_logs_orderno_idx ON public.tbl_logs USING btree (order_no, active);