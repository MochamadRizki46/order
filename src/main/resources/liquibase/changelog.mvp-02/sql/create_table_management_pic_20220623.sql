CREATE TABLE if not exists public.management_pic (
	sk_management_pic varchar(36) NOT NULL,
	identity_no varchar(20) NULL,
	active bool NULL,
	created_by varchar(50) NULL,
	created_date timestamp NULL,
	date_of_birth date NULL,
	email varchar(100) NULL,
	full_name varchar(100) NULL,
	identity_type varchar(2) NULL,
	last_modified_by varchar(50) NULL,
	last_modified_date timestamp NULL,
	level varchar(4) NULL,
	place_of_birth varchar(50) NULL,
	place_of_birth_city varchar(4) NULL,
	fk_customer_company varchar(36) NULL,
	CONSTRAINT management_pic_pkey PRIMARY KEY (sk_management_pic)
);