CREATE TABLE if not exists public.tbl_notification (
	order_no varchar(20) NULL,
	application_no varchar(16) NULL,
	application_no_unit varchar(20) NULL,
	title varchar(100) NULL,
	body varchar(500) NULL,
	created_date timestamp NULL,
	active bool NULL DEFAULT true,
	user_id varchar(30) NOT NULL,
	notif_id varchar(36) NOT NULL,
	status varchar(50) NULL,
	isread bool NULL DEFAULT false,
	CONSTRAINT tbl_notif_pk PRIMARY KEY (notif_id),
	CONSTRAINT tbl_notif_fk FOREIGN KEY (application_no) REFERENCES application(application_no)
);