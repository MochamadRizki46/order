CREATE TABLE IF NOT EXISTS sap_notif_payment (
    sap_notif_payment_id varchar(36) primary key not null,
    tanggal_cair date,
    jenis_pembayaran varchar(20),
    doc_no_invoice varchar(16),
    doc_no_payment varchar(16),
    nama_rekening_penerima varchar(50),
    no_rekening_penerima varchar(40),
    kode_bank_penerima varchar(15),
    nama_bank_penerima varchar(60),
    no_aplikasi varchar(16),
    no_kontrak varchar(18),
    kode_cabang varchar(4),
    kode_bp_partner_external varchar(20),
    jumlah_pencairan numeric,
    fk_application varchar(36),

    constraint fk_sap_notif_payment_application foreign key (fk_application) references public.application(sk_application)
);