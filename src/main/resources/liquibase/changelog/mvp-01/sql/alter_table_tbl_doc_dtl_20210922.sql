ALTER TABLE public.tbl_doc_dtl
    ADD COLUMN doc_type varchar(1) default '1';

comment on column public.tbl_doc_dtl.doc_type is 'Tipe Document 1:KTP, 2:NPWP, 3:STNK';
