CREATE TABLE public.para_seq_generateid (
	column_gen varchar(100) NULL,
	seq_number numeric NULL,
	seq_year varchar(4) NULL,
	create_date timestamp NULL,
	create_by varchar(100) NULL,
	modified_date timestamp NULL,
	modified_by varchar(100) NULL
);