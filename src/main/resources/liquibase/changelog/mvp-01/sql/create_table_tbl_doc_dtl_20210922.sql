CREATE TABLE public.tbl_doc_dtl (
    doc_detail_id varchar (36) primary key not null,
    id varchar(21),
	doc_id varchar(100) NULL,
	doc_path varchar(300) NULL,
	active int4 NULL,
	created_date timestamp NULL,
	created_by varchar(100) NULL,
	modified_date timestamp NULL,
	modified_by varchar(100) NULL,
	submit_order_id varchar(36),
	CONSTRAINT tbl_doc_dtl_un UNIQUE (submit_order_id),
	constraint fk_submit_order_doc_dtl foreign key (submit_order_id) references public.tbl_submit_order(submit_order_id)
);