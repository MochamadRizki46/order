create table public.tbl_order_domisili(
    order_domisili_id varchar(36) primary key not null,
    flag_address_id int,
    address_domisili varchar(150),
    survey_date date,
    survey_time varchar(50),
    rtrw varchar(10),
    kelurahan varchar(5),
    kelurahan_desc varchar(50),
    kecamatan varchar(5),
    kecamatan_desc varchar(50),
    kabkot varchar(4),
    kabkot_desc varchar(50),
    provinsi varchar(2),
    provinsi_desc varchar(50),
    zipcode varchar(5),
    created_date timestamp not null,
    created_by varchar(100)  not null,
    modified_date timestamp,
    modified_by varchar(100),
    active int,
    nama_so varchar(100),
    submit_order_id varchar(36),
	
	CONSTRAINT tbl_order_domisili_un UNIQUE (submit_order_id),
    constraint fk_domisili_submit_order foreign key (submit_order_id) references public.tbl_submit_order(submit_order_id)


);