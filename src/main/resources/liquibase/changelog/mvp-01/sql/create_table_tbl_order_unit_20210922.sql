create table public.tbl_order_unit(
    order_unit_id varchar(36) primary key not null,
    object_brand varchar(3),
    object_brand_desc varchar(50),
    object_type varchar(3),
    object_type_desc varchar(50),
    object_model varchar(3),
    object_model_desc varchar(50),
    object varchar(3),
    object_desc varchar(50),
    object_year varchar(4),
    tipe_jaminan varchar(20),
    jenis_asuransi varchar(50),
    kota_domisili varchar(100),
    object_price numeric,
    tenor numeric,
    rate numeric,
    dp numeric,
    installment_amt numeric,
    promo varchar(5),
    created_date timestamp not null,
    created_by varchar(100),
    modified_date timestamp,
    modified_by varchar(100),
    active int,
    model_detail varchar,
    notes varchar,
    submit_order_id varchar(36),
	
	CONSTRAINT tbl_order_unit_un UNIQUE (submit_order_id),
    constraint fk_unit_submit_order foreign key (submit_order_id) references public.tbl_submit_order(submit_order_id)


);