CREATE TABLE public.tbl_status_order_detail (
	order_no varchar(20) NOT NULL,
	appl_no_unit varchar(20) NOT NULL,
	id_seq int4 NULL,
	status varchar(100) NULL,
	substatus varchar(100) NULL,
	status_date date NULL,
	status_time varchar(10) NULL,
	active int4 NULL,
	create_by varchar(100) NULL,
	create_date timestamp NULL,
	id varchar(25) NULL,
	user_type varchar(1) NULL DEFAULT 0, -- 0 : Sales, 1 : Owner
	groupstatus varchar NULL -- Grup Status Tracking Order
);

-- Column comments

COMMENT ON COLUMN public.tbl_status_order_detail.user_type IS '0 : Sales, 1 : Owner';
COMMENT ON COLUMN public.tbl_status_order_detail.groupstatus IS 'Grup Status Tracking Order';
