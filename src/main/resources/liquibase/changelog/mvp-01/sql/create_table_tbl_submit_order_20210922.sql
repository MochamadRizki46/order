create table public.tbl_submit_order(
    submit_order_id varchar(36) primary key not null,
    order_no varchar(20) not null,
    appl_no_payung varchar(20),
    cust_name varchar(100),
    order_date timestamp,
    partner_type varchar(3),
    pic varchar(100),
    active int,
    idx int,
    status varchar(100),
    created_date timestamp not null,
    created_by varchar(100),
    modified_date timestamp,
    modified_by varchar(100),
    appl_no_unit varchar(20),
    send_date_status date,
    group_status varchar(50)

);