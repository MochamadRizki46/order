package id.co.adira.partner.order.controller;

import static id.co.adira.partner.order.constant.StatusConst.SUCCESS;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.DpOrPaymentRequestDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPaymentDetailResDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPmtHeadResDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.adira.partner.order.dto.creditsimulation.request.credsim.CreditSimulationDetailReqDTO;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.service.CreditSimulationService;

import java.math.BigDecimal;

@SpringBootTest
@ActiveProfiles("test")
public class CreditSimulationControllerSpringbootTest {

	
	@MockBean
	CreditSimulationService creditSimulationService;
	
//	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	WebApplicationContext context;

	
	
	@BeforeEach
	void setUp(){
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		MockitoAnnotations.initMocks(this);
	}
	
//	@Test
//	public void TestcalculateCreditSimulation() throws Exception {
//		CreditSimulationDetailReqDTO requested = new CreditSimulationDetailReqDTO();
//		requested.setCreditType(123);
//		requested.setFlatRate(30);
//		requested.setRounded(10);
//		
//		
//		String jsonRequestInString = objectMapper.writeValueAsString(requested);
//		String authorization = "test-auhtor";
//		DataResponse data = new DataResponse();
//		Object dat = new Object();
////		dat = "testinga";
//		data.setData("test");
//		
//		data.setHttpStatus(HttpStatus.OK);
//		data.setMessage("");
//		
////		CreditSimulationService creds = mock(CreditSimulationService.class , Mockito.RETURNS_DEEP_STUBS);
////		
//		when(creditSimulationService.calculateCreditSimulation(Mockito.any(),Mockito.anyString())).thenReturn(data);
//		
//		
//		RequestBuilder requestBuilder = 
//				MockMvcRequestBuilders.post("/api/order/v1/calculateCreditSimulation")
//				.header("Authorization", "authorization")
//				.contentType(MediaType.APPLICATION_JSON).content(jsonRequestInString);
//		
//		mockMvc.perform(requestBuilder).andExpect(status().isOk());
//		
//	}


	@Test
	public void testCalculateDpOrPmtAndExpectStatusOk() throws Exception {
		DpOrPaymentRequestDTO dpOrPaymentRequestDTO = new DpOrPaymentRequestDTO();
		dpOrPaymentRequestDTO.setCashOrCredit(1);
		dpOrPaymentRequestDTO.setCreditType(1);
		dpOrPaymentRequestDTO.setDiscount(20);
		dpOrPaymentRequestDTO.setObjectCode("OBJ001");
		dpOrPaymentRequestDTO.setDpGross(500000.0);
		dpOrPaymentRequestDTO.setDpSystem(500000.0);
		dpOrPaymentRequestDTO.setAngsuran(BigDecimal.ZERO);
		dpOrPaymentRequestDTO.setObjectModelId("OBM001");
		dpOrPaymentRequestDTO.setObjectTypeId("OBT001");
		dpOrPaymentRequestDTO.setOtr(15000000.0);
		dpOrPaymentRequestDTO.setPic("user@ad1gate.com");
		dpOrPaymentRequestDTO.setProgramId("PRG001");
		dpOrPaymentRequestDTO.setTenor(12);

		DpOrPmtHeadResDTO dpOrPmtHeadResDTO = new DpOrPmtHeadResDTO();
		dpOrPmtHeadResDTO.setStatus("1");
		dpOrPmtHeadResDTO.setMessage(SUCCESS.getMessage());

		DpOrPaymentDetailResDTO dpOrPaymentDetailResDTO = new DpOrPaymentDetailResDTO();
		dpOrPaymentDetailResDTO.setCreditType(dpOrPaymentRequestDTO.getCreditType());
		dpOrPaymentDetailResDTO.setAngsuran(0.0);
		dpOrPaymentDetailResDTO.setAdminCredit(800000.0);
		dpOrPaymentDetailResDTO.setAdminCash(0.0);
		dpOrPaymentDetailResDTO.setAngsuran1(0.0);
		dpOrPaymentDetailResDTO.setFlatRate(0.0);
		dpOrPaymentDetailResDTO.setAsuransiCashRate(0.0);
		dpOrPaymentDetailResDTO.setAsuransiCreditRate(0.0106);
		dpOrPaymentDetailResDTO.setDiscount(0.0);
		dpOrPaymentDetailResDTO.setDiscountAngsuran(1128000.0);
		dpOrPaymentDetailResDTO.setDiscountEffectiveRate(0.3020156);
		dpOrPaymentDetailResDTO.setDiscountFlatRate(0.1710356);
		dpOrPaymentDetailResDTO.setDpGross(5000000.0);
		dpOrPaymentDetailResDTO.setDpSystem(5000000.0);
		dpOrPaymentDetailResDTO.setEffectiveRate(0.3012);
		dpOrPaymentDetailResDTO.setFlatRate(null);
		dpOrPaymentDetailResDTO.setOtr(15000000.0);
		dpOrPaymentDetailResDTO.setPokokHutangUnit(10000000.0);
		dpOrPaymentDetailResDTO.setTenor(12);
		dpOrPaymentDetailResDTO.setTotalPokokHutangUnit(11559000.0);

		dpOrPmtHeadResDTO.setData(dpOrPaymentDetailResDTO);

		when(creditSimulationService.getDpOrPmtCalculationData(dpOrPaymentRequestDTO)).thenReturn(dpOrPmtHeadResDTO);
		String jsonStr = objectMapper.writeValueAsString(dpOrPaymentRequestDTO);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/v1/dpOrPmt")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}
}
