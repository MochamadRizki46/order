package id.co.adira.partner.order.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPmtHeadResDTO;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.service.CreditSimulationService;
import id.co.adira.partner.order.service.LogService;

@WebMvcTest(value = CreditSimulationController.class)
public class CreditSimulationControllerTest {
	
	@MockBean
	CreditSimulationService creditSimulationService;
	
	@MockBean
	LogService logService;
	
	
	@Autowired
	MockMvc mockMvc;
	
	@Autowired
	ObjectMapper objectMapper;

	
//	@Test
//	public void TestcalculateCreditSimulation() throws Exception {
//		
//		DataResponse data = new DataResponse();
//		data.setHttpStatus(HttpStatus.OK);
//		
//		when(creditSimulationService.calculateCreditSimulation(Mockito.any(),Mockito.any())).thenReturn(data);
//		
//		String json = "{\"ballonParam\":{\"ballonBy\":1}}";
//		
//		mockMvc.perform(post("/api/order/v1/calculateCreditSimulation")
//				.content(json)
//				.header("Authorization", "a")
//				.contentType(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk());
//		
//	}
	
	@Test
	public void getDataDpOrPmt() throws Exception {
		
		DpOrPmtHeadResDTO data = new DpOrPmtHeadResDTO();
		
		when(creditSimulationService.getDpOrPmtCalculationData(Mockito.any())).thenReturn(data);
		
		String json = "{\"objectCode\":\"a\"}}";
		
		mockMvc.perform(post("/api/order/v1/dpOrPmt")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
	}
	
	@Test
	public void getDataDpOrPmtExp() throws Exception {
		
		when(creditSimulationService.getDpOrPmtCalculationData(Mockito.any())).thenThrow(NullPointerException.class);
		
		String json = "{\"objectCode\":\"a\"}}";
		
		mockMvc.perform(post("/api/order/v1/dpOrPmt")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isInternalServerError());
		
	}
	

}
