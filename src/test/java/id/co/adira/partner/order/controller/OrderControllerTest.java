/**
 *
 */
package id.co.adira.partner.order.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.order.dto.*;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.entity.Log;
import id.co.adira.partner.order.service.DraftSubmitPartialService;
import id.co.adira.partner.order.service.DraftSubmitPartialServiceV2;
import id.co.adira.partner.order.service.LogService;
import id.co.adira.partner.order.service.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static id.co.adira.partner.order.constant.AppConst.*;
import static id.co.adira.partner.order.constant.StatusConst.OK;
import static id.co.adira.partner.order.constant.StatusConst.SUCCESS;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author 10999943
 *
 */
@WebMvcTest(value = OrderController.class)
@ActiveProfiles("test")
class OrderControllerTest {

	@MockBean
	OrderService orderService;
	
	@Autowired
	OrderController oderController;

	@MockBean
	DraftSubmitPartialServiceV2 draftSubmitPartialServiceV2;

	@MockBean
	DraftSubmitPartialService draftSubmitPartialService;

	@MockBean
	LogService logService;

	Log log = new Log();

	@Autowired
	MockMvc mockMvc;

	@Autowired
	WebApplicationContext context;

	ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * @throws java.lang.Exception
	 */
//	@BeforeEach
//	void setUp(){
//		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
//		MockitoAnnotations.initMocks(this);
//
//	}

	/**
	 * Test method for {@link id.co.adira.partner.order.controller.OrderController#submitOrder(SubmitOrderReqDTO)}.
	 * @throws
	 */
	@Test
	void testInsertSubmitOrder() throws  Exception{

		SubmitOrderReqDTO submitOrderReqDTO = new SubmitOrderReqDTO();
		submitOrderReqDTO.setOrderNo("A1P0000SLD2100045003");
		submitOrderReqDTO.setActive(0);
		submitOrderReqDTO.setOrderDate(new Date());
		submitOrderReqDTO.setCreatedBy("admin");
		submitOrderReqDTO.setCreatedDate(new Date());
		submitOrderReqDTO.setModifiedBy("admin");
		submitOrderReqDTO.setModifiedDate(new Date());
		submitOrderReqDTO.setApplNoPayung("test_appl_no_payung");
		submitOrderReqDTO.setApplNoUnit("test_appl_no_unit");
		submitOrderReqDTO.setIdx(1);
		submitOrderReqDTO.setPartnerType("001");
		submitOrderReqDTO.setPic("test@ad1gate.com");
		submitOrderReqDTO.setStatus("Draft");

		String jsonRequestInString = objectMapper.writeValueAsString(submitOrderReqDTO);
//		String jsonResponseInString = objectMapper.writeValueAsString(SUCCESS.getMessage());
		StandardResponseDTO dataResponse = new StandardResponseDTO();
		dataResponse.setMessage(SUCCESS.getMessage().toLowerCase());

		log.setLogId(generateUUID());
		log.setOrderNo("A1P0000SLD2100045003");
		log.setLogName(SUBMIT_DRAFT_FOR_ORDER.getMessage());
		log.setJsonRequest(jsonRequestInString);
		log.setJsonResponses(dataResponse.toString());
		log.setProcessDate(new Date());
		log.setDuration(1);

		when(orderService.savePartialOrder(submitOrderReqDTO)).thenReturn(dataResponse);
		doNothing().when(logService).insertToLogTable(submitOrderReqDTO, SUCCESS.getMessage(), SUBMIT_DRAFT_FOR_ORDER.getMessage(), 1, "A1P0000SLD2100045003");

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/save-order")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequestInString);
		mockMvc.perform(requestBuilder).andExpect(status().isOk());

	}

	/**
	 * Test method for {@link id.co.adira.partner.order.controller.OrderController#submitCustomer(id.co.adira.partner.order.dto.SubmitCustomerReqDTO)}.
	 * @throws Exception
	 */
//	@Test
//	void testInsertSubmitCustomer() throws Exception {
//		SubmitCustomerReqDTO request = new SubmitCustomerReqDTO();
//		StandardResponseDTO dataResponse = new StandardResponseDTO();
//		
//		request.setIdAddress("string");
//		request.setOrderNo("A1P0000SLD2100045003");
//		request.setCustType("cust_type_1");
//		request.setCustomerName("TEST");
//		request.setHandphoneNo("08989");
//		request.setWaNo("08080");
//		request.setFlagWa(0);
//		request.setIdName("TEST");
//		request.setBirthDate(new Date());
//		request.setBirthPlace("TEST");
//		request.setIdRtrw("001");
//		request.setIdType("1");
//		request.setIdKelurahan("1");
//		request.setIdKelurahanDesc("TEST");
//		request.setIdKecamatan("1");
//		request.setIdKecamatanDesc("TEST");
//		request.setIdKabkot("1");
//		request.setIdKabkotDesc("TEST");
//		request.setIdProvinsi("1");
//		request.setIdProvinsiDesc("TEST");
//		request.setZipcode("TEST");
//		request.setFinType("1");
//		request.setCreatedDate(new Date());
//		request.setCreatedBy("TEST");
//		request.setModifiedDate(new Date());
//		request.setModifiedBy("TEST");
//		request.setActive(0);
//		request.setIdNo("test_id_no_12345");
//
//		String jsonRequestInString = objectMapper.writeValueAsString(request);
////		String jsonResponseInString = objectMapper.writeValueAsString(SUCCESS.getMessage());
//
//		dataResponse.setMessage(SUCCESS.getMessage().toLowerCase());
//		dataResponse.setStatus(0);
//		
//		log.setLogId(generateUUID());
//		log.setOrderNo("A1P0000SLD2100045003");
//		log.setLogName(SUBMIT_DRAFT_FOR_CUSTOMER.getMessage());
//		log.setJsonRequest(jsonRequestInString);
//		log.setJsonResponses(dataResponse.toString());
//		log.setProcessDate(new Date());
//		log.setDuration(1);
//
//		String jsonStr = objectMapper.writeValueAsString(request);
//
//		String mockOutput = "";
//
//		when(orderService.savePartialCustomer(request)).thenReturn(dataResponse);
//		doNothing().when(logService).insertToLogTable(request, SUCCESS.getMessage(), SUBMIT_DRAFT_FOR_CUSTOMER.getMessage(), 1, "A1P0000SLD2100045003");
//
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/save-customer")
//				.contentType(MediaType.APPLICATION_JSON).content(jsonStr);
//		System.out.println(status());
//
//		mockMvc.perform(requestBuilder).andExpect(status().isOk());
//
//		//fail("Not yet implemented"); // TODO
//	}


	@Test
	void testInsertSubmitDomisili() throws Exception {
		SubmitDomisiliReqDTO request = new SubmitDomisiliReqDTO();
		request.setOrderNo("A1P0000SLD2100045003");
		request.setFlagAddressId(0);
		request.setAddressDomisili("TEST");
		request.setSurveyDate(new Date());
		request.setSurveyTime("12:20");
		request.setRtrw("001");
		request.setKelurahan("000");
		request.setKelurahanDesc("TEST");
		request.setKecamatan("000");
		request.setKecamatanDesc("TEST");
		request.setKabkot("000");
		request.setKabkotDesc("TEST");
		request.setProvinsi("00");
		request.setProvinsiDesc("TEST");
		request.setZipcode("12345");
		request.setCreatedDate(new Date());
		request.setCreatedBy("TEST");
		request.setModifiedDate(new Date());
		request.setModifiedBy("TEST");
		request.setActive(0);

		String jsonRequestInString = objectMapper.writeValueAsString(request);
//		String jsonResponseInString = objectMapper.writeValueAsString(SUCCESS.getMessage());
		StandardResponseDTO dataResponse = new StandardResponseDTO();
		dataResponse.setMessage(SUCCESS.getMessage().toLowerCase());

		log.setLogId(generateUUID());
		log.setOrderNo("A1P0000SLD2100045003");
		log.setLogName(SUBMIT_DRAFT_FOR_DOMISILI.getMessage());
		log.setJsonRequest(jsonRequestInString);
		log.setJsonResponses(dataResponse.toString());
		log.setProcessDate(new Date());
		log.setDuration(1);

		String jsonStr = objectMapper.writeValueAsString(request);

		String mockOutput = "";

		when(orderService.savePartialDomisili(request)).thenReturn(dataResponse);
		doNothing().when(logService).insertToLogTable(request, SUCCESS.getMessage(), SUBMIT_DRAFT_FOR_DOMISILI.getMessage(), 1, "A1P0000SLD2100045003");

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/save-domisili")
				.contentType(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());


		//fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link id.co.adira.partner.order.controller.OrderController#submitUnit(id.co.adira.partner.order.dto.SubmitUnitReqDTO)}.
	 */
	@Test
	void testInsertSubmitUnit() throws Exception  {
		SubmitUnitReqDTO request = new SubmitUnitReqDTO();
		request.setOrderNo("A1P0000SLD2100045003");
		request.setObjectBrand("000");
		request.setObjectBrandDesc("TEST");
		request.setObjectType("000");
		request.setObjectTypeDesc("TEST");
		request.setObjectModel("000");
		request.setObjectModelDesc("TEST");
		request.setObject("001");
		request.setObjectDesc("TEST");
		request.setObjectYear("TEST");
		request.setTipeJaminan("001");
		request.setJenisAsuransi("1");
		request.setKotaDomisili("TEST");
		request.setObjectPrice( BigDecimal.ZERO);
		request.setTenor(12);
		request.setRate(BigDecimal.ZERO);
		request.setDp(BigDecimal.ZERO);
		request.setInstallmentAmt(BigDecimal.ZERO);
		request.setPromo("TEST");
		request.setModelDetail("TEST");
		request.setNotes("TEST");
		request.setCreatedDate(new Date());
		request.setCreatedBy("TEST");
		request.setModifiedDate(new Date());
		request.setModifiedBy("TEST");
		request.setActive(0);

		String jsonRequestInString = objectMapper.writeValueAsString(request);
		String jsonResponseInString = objectMapper.writeValueAsString(SUCCESS.getMessage());

		log.setLogId(generateUUID());
		log.setOrderNo("A1P0000SLD2100045003");
		log.setLogName(SUBMIT_DRAFT_FOR_UNIT.getMessage());
		log.setJsonRequest(jsonRequestInString);
		log.setJsonResponses(jsonResponseInString);
		log.setProcessDate(new Date());
		log.setDuration(1);

		String jsonStr = objectMapper.writeValueAsString(request);

		String mockOutput = "";

		when(orderService.savePartialUnit(request)).thenReturn(mockOutput);
		doNothing().when(logService).insertToLogTable(request, SUCCESS.getMessage(), SUBMIT_DRAFT_FOR_UNIT.getMessage(), 1, "A1P0000SLD2100045003");

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/save-unit")
				.contentType(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
		//fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link id.co.adira.partner.order.controller.OrderController#insertDocument(id.co.adira.partner.order.dto.DocumentReqDTO)}.
	 */
	@Test
	void testInsertDocument() throws Exception  {
		DocumentReqDTO request = new DocumentReqDTO();
		request.setDocId("0000000000");
		request.setDocPath("TEST");
		request.setOrderNo("A1P0000SLD2100045003");
		request.setUserLogin("test@adira.com");

		String jsonRequestInString = objectMapper.writeValueAsString(request);
		String jsonResponseInString = objectMapper.writeValueAsString(SUCCESS.getMessage());

		log.setLogId(generateUUID());
		log.setOrderNo("A1P0000SLD2100045003");
		log.setLogName(SUBMIT_DRAFT_FOR_UNIT.getMessage());
		log.setJsonRequest(jsonRequestInString);
		log.setJsonResponses(jsonResponseInString);
		log.setProcessDate(new Date());
		log.setDuration(1);

		String mockOutput = "";

		when(orderService.saveDocument(request)).thenReturn(mockOutput);
		doNothing().when(logService).insertToLogTable(request, SUCCESS.getMessage(), SUBMIT_FOR_DOCUMENT.getMessage(), 1, "A1P0000SLD2100045003");

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/save-document")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequestInString);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
		//fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link id.co.adira.partner.order.controller.OrderController#generateNewOrderId(id.co.adira.partner.order.dto.GenerateOrderReq)}.
	 */
	@Test
	void testGetGenerate()  throws Exception {
		GenerateOrderReq request = new GenerateOrderReq();
		request.setRoleId("SLD");

		String jsonStr = objectMapper.writeValueAsString(request);

		String mockOutput = "";

		when(orderService.getOrderId(request)).thenReturn(mockOutput);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/generate/orderid")
				.contentType(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}

	@Test
	public void getDraftSubmitPartialOrderTestAndExpectedStatusOk() throws Exception {
		DataResponse dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());

		List<DraftSubmitOrderResDTO> draftSubmitOrderResDTOS = new ArrayList<>();

		DraftSubmitOrderResDTO firstDraftSubmitOrderResDTO = new DraftSubmitOrderResDTO();
		firstDraftSubmitOrderResDTO.setOrderNo("A1P0000SLD2100045003");
		firstDraftSubmitOrderResDTO.setOrderDate(new Date().toString());
		firstDraftSubmitOrderResDTO.setCustName("first_cust_name");
		firstDraftSubmitOrderResDTO.setObjectDesc("first_object_desc");
		firstDraftSubmitOrderResDTO.setObjectModelDesc("first_object_model_desc");

		draftSubmitOrderResDTOS.add(firstDraftSubmitOrderResDTO);

		DraftSubmitOrderResDTO secondDraftSubmitOrderResDTO = new DraftSubmitOrderResDTO();
		secondDraftSubmitOrderResDTO.setOrderNo("A1P0000SLD2100045003");
		secondDraftSubmitOrderResDTO.setOrderDate(new Date().toString());
		secondDraftSubmitOrderResDTO.setCustName("first_cust_name");
		secondDraftSubmitOrderResDTO.setObjectDesc("first_object_desc");
		secondDraftSubmitOrderResDTO.setObjectModelDesc("first_object_model_desc");

		draftSubmitOrderResDTOS.add(secondDraftSubmitOrderResDTO);
		dataResponse.setData(draftSubmitOrderResDTOS);

		when(draftSubmitPartialService.getDraftSubmitPartialOrderByPic("test@ad1gate.com")).thenReturn(dataResponse);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/order/getDraftSubmitPartialOrder?pic=test@ad1gate.com").contentType(MediaType.APPLICATION_JSON);
		mockMvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
	}

	@Test
	public void testGetDraftDetailSubmitOrderAndExpectStatusIsOk() throws Exception {
		DataResponse dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());

		SubmitPartialDetailResponse submitPartialDetailResponse = new SubmitPartialDetailResponse();
		submitPartialDetailResponse.setDraftDataKonsumen(insertDummyDraftDataKonsumen());
		submitPartialDetailResponse.setDraftDataDomisili(insertDummyDraftDataDomisili());
		submitPartialDetailResponse.setDraftDataUnit(insertDummyDraftDataUnit());

		dataResponse.setData(submitPartialDetailResponse);
		when(draftSubmitPartialService.getDraftSubmitPartialDetailByOrderNo("A1P0000SLD2100045003")).thenReturn(dataResponse);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/order/getDraftDetailSubmitPartial?orderNo=A1P0000SLD2100045003")
				.contentType(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
	}

	@Test
	public void testDeleteDataDraftUnitAndExpectStatusOk() throws Exception {
		DataResponse dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());
		dataResponse.setData(SUCCESS.getMessage().toLowerCase());

		String orderNo = "A1P0000SLD2100045003";
		when(draftSubmitPartialService.deleteDraft(orderNo)).thenReturn(dataResponse);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/deleteDraft/A1P0000SLD2100045003")
				.contentType(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}

	
	@Test
	void TestdeleteDraftExcep() throws Exception {
		when(draftSubmitPartialService.deleteDraft(Mockito.any())).thenThrow(RuntimeException.class);
		
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/deleteDraft/A1P0000SLD2100045003")
				.contentType(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}
	
	@Test
	public void testDraftSubmitPartialOrderControllerV2() throws Exception {
		DataResponse dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());

		List<DraftSubmitOrderResDTO> draftSubmitOrderResDTOS = new ArrayList<>();

		DraftSubmitOrderResDTO firstDraftSubmitOrderResDTO = new DraftSubmitOrderResDTO();
		firstDraftSubmitOrderResDTO.setOrderNo("A1P0000SLD2100045003");
		firstDraftSubmitOrderResDTO.setOrderDate(new Date().toString());
		firstDraftSubmitOrderResDTO.setCustName("first_cust_name");
		firstDraftSubmitOrderResDTO.setObjectDesc("first_object_desc");
		firstDraftSubmitOrderResDTO.setObjectModelDesc("first_object_model_desc");

		draftSubmitOrderResDTOS.add(firstDraftSubmitOrderResDTO);

		DraftSubmitOrderResDTO secondDraftSubmitOrderResDTO = new DraftSubmitOrderResDTO();
		secondDraftSubmitOrderResDTO.setOrderNo("A1P0000SLD2100045003");
		secondDraftSubmitOrderResDTO.setOrderDate(new Date().toString());
		secondDraftSubmitOrderResDTO.setCustName("first_cust_name");
		secondDraftSubmitOrderResDTO.setObjectDesc("first_object_desc");
		secondDraftSubmitOrderResDTO.setObjectModelDesc("first_object_model_desc");

		draftSubmitOrderResDTOS.add(secondDraftSubmitOrderResDTO);
		dataResponse.setData(draftSubmitOrderResDTOS);

		Integer numberOfPrevDay = 3;

		when(draftSubmitPartialServiceV2.getDraftSubmitPartialOrderByPic(numberOfPrevDay, "test@ad1gate.com", 0)).thenReturn(dataResponse);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/order/v2/getDraftSubmitPartialOrder?numberOfPrevDay=3&pic=test@ad1gate.com&sort=0")
				.contentType(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}

	private SubmitUnitResDTO insertDummyDraftDataUnit() {
		SubmitUnitResDTO submitUnitResDTO = new SubmitUnitResDTO();
		submitUnitResDTO.setOrderNo("A1P0000SLD2100045003");
		submitUnitResDTO.setCreatedBy("admin");
		submitUnitResDTO.setModifiedBy("admin");
		submitUnitResDTO.setObject("obj");
		submitUnitResDTO.setObjectType("obj_type_1");
		submitUnitResDTO.setObjectTypeDesc("obj_type_desc_1");
		submitUnitResDTO.setObjectModel("obj_model");
		submitUnitResDTO.setObjectModelDesc("obj_model_desc_1");
		submitUnitResDTO.setObjectBrand("obj_brand_1");
		submitUnitResDTO.setObjectBrandDesc("obj_brand_desc_1");

		return submitUnitResDTO;
	}

	private SubmitDomisiliResDTO insertDummyDraftDataDomisili() {
		SubmitDomisiliResDTO submitDomisiliResDTO = new SubmitDomisiliResDTO();
		submitDomisiliResDTO.setOrderNo("A1P0000SLD2100045003");
		submitDomisiliResDTO.setAddressDomisili("dummy_address_domisili");
		submitDomisiliResDTO.setZipcode("11111");
		submitDomisiliResDTO.setSurveyDate(new Date());
		submitDomisiliResDTO.setActive(0);

		return submitDomisiliResDTO;
	}

	private SubmitCustomerResDTO insertDummyDraftDataKonsumen() {
		SubmitCustomerResDTO submitCustomerResDTO = new SubmitCustomerResDTO();
		submitCustomerResDTO.setCustType("PER");
		submitCustomerResDTO.setCustomerName("first_customer");
		submitCustomerResDTO.setJsonIdp("");
		submitCustomerResDTO.setFinType("1");
		submitCustomerResDTO.setCreatedDate(new Date());
		submitCustomerResDTO.setModifiedDate(new Date());
		submitCustomerResDTO.setModifiedBy("admin");
		submitCustomerResDTO.setCreatedBy("admin");
		submitCustomerResDTO.setZipcode("11111");
		submitCustomerResDTO.setIdName("first_customer");
		submitCustomerResDTO.setOrderNo("A1P0000SLD2100045003");

		return submitCustomerResDTO;
	}

	private String generateUUID(){
		return UUID.randomUUID().toString();
	}

	
	@Test
	void uploadECM() throws Exception {
		
		MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());
		
//		MockMultipartFile file = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data", inputFile);
		String orderno="123";
		String docType="csv";
		String idNo="12333";
		
		Map<String, Object> resp = new HashMap<>();
		
		when(orderService.uploadEcm(Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(resp);
		
		
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.multipart("/api/order/uploadECM")
				.file(file).param("orderNo", "123").param("docType", "csv").param("idNo", "12333");
		
		
		 
		mockMvc.perform(requestBuilder).andReturn();
		
		
	}
	
	@Test
	void uploadECMException() throws Exception {

		MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());
		
//		MockMultipartFile file = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data", inputFile);
		String orderno="123";
		String docType="csv";
		String idNo="12333";
		
		Map<String, Object> response = new HashMap<>();
		
		when(orderService.uploadEcm(Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(RuntimeException.class);
		
		
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.multipart("/api/order/uploadECM")
				.file(file).param("orderNo", "123").param("docType", "csv").param("idNo", "12333");
		
		
		 
		mockMvc.perform(requestBuilder).andReturn();
	}
	
	@Test
	void getOcrTest() throws Exception {
		
	MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());
		
//		MockMultipartFile file = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data", inputFile);
		String orderno="123";
		String docType="csv";
		String idNo="12333";
		
		DataResponse resp = new DataResponse();
		
		when(orderService.getOCR(Mockito.any(),Mockito.any())).thenReturn(resp);
		
		
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.multipart("/api/order/ocrIdp")
				.file(file).param("flag", "csv");
		
		
		 
		mockMvc.perform(requestBuilder).andExpect(status().isOk());
		
	}
	
	@Test
	void getOcrTestException() throws Exception {
	MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());
		
//		MockMultipartFile file = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data", inputFile);
		String orderno="123";
		String docType="csv";
		String idNo="12333";
		
		DataResponse resp = new DataResponse();
		
		when(orderService.getOCR(Mockito.any(),Mockito.any())).thenThrow(RuntimeException.class);
		
		
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.multipart("/api/order/ocrIdp")
				.file(file).param("flag", "csv");
		
		
		 
		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}
	
	

	
	@Test
	public void TestgetOcrWeb() throws Exception {
		IdpWebReqDTO request = new IdpWebReqDTO();
		var response = new DataResponse();
		when(orderService.getWebOCR(Mockito.any())).thenReturn(response);
		
		String jsonRequestInString = objectMapper.writeValueAsString(request);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/ocrIdpWeb")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequestInString);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}
	
	@Test
	public void TestgetOcrWebException() throws Exception {
		
		when(orderService.getWebOCR(Mockito.any())).thenThrow(RuntimeException.class);
		
		IdpWebReqDTO request = new IdpWebReqDTO();
		
		String jsonRequestInString = objectMapper.writeValueAsString(request);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/order/ocrIdpWeb")
				.contentType(MediaType.APPLICATION_JSON).content(jsonRequestInString);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
		
	}	
	
}
