/**
 *
 */
package id.co.adira.partner.order.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.adira.partner.order.service.LogService;
import id.co.adira.partner.order.service.OrderService;

/**
 * @author 10999943
 *
 */

@WebMvcTest(value = OrderSubmitController.class)
class OrderSubmitControllerTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	OrderService orderService;

	@Autowired
	ObjectMapper objectMapper;

	@MockBean
	LogService logService;

	@Test
	void testSubmitOrdertoLead1() throws Exception {

		final String json = "{\"credOrderNo\":11}";

		when(orderService.submitOrderToLead(Mockito.any())).thenReturn("OK");

		mockMvc.perform(post("/api/submit/submitorder").content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}

	@Test
	void testSubmitOrdertoLead1Exceotion() throws Exception {

		final String json = "{\"credOrderNo\":11}";

		when(orderService.submitOrderToLead(Mockito.any())).thenThrow(NullPointerException.class);

		mockMvc.perform(post("/api/submit/submitorder").content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());

	}
	
//	@Test
//	void submitOrderToLeadV2() throws Exception {
//
//		final String json = "{\"credOrderNo\":11}";
//
//		when(orderService.submitOrdertoLeadV2(Mockito.any())).thenReturn("OK");
//
//		mockMvc.perform(post("/api/submit/submitorderV2").content(json).contentType(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk());
//
//	}
	
	@Test
	void submitOrderToLeadV2Exception() throws Exception {

		final String json = "{\"credOrderNo\":11}";


		when(orderService.submitOrdertoLeadV2(Mockito.any())).thenThrow(NullPointerException.class);

		mockMvc.perform(post("/api/submit/submitorderV2").content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());

	}

}
