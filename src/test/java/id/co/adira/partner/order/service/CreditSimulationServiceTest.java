package id.co.adira.partner.order.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.adira.partner.order.dto.creditsimulation.request.apikey.APIKeyRequestDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.BiayaAdminReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.CreditSimulationDetailReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.DpOrPaymentRequestDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.dporpmt.DownPaymentOrPmtReqToCommonServiceDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.apikey.APIKeyDetailResponseDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.apikey.APIKeyHeadResponseDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.AsuransiCashAndCreditResDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.BiayaAdminResDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.CreditSimulationDetailResDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.EffectiveOrLandingRateResDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPaymentDetailResDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPmtHeadResDTO;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.service.resttemplate.RestTemplateService;

@SpringBootTest
@ActiveProfiles("test")
public class CreditSimulationServiceTest {

	@Mock
	RestTemplateService restTemplateService;
	
	@InjectMocks
	CreditSimulationService creditSimulationService;
	
	@Mock
	ObjectMapper objectMapper;
	
	@Test
	void calculateCreditSimulationTest() {
		
		CreditSimulationDetailResDTO data = new CreditSimulationDetailResDTO();
		data.setCreditType(123);
		DataResponse data1 = new DataResponse();
		data1.setData(data);
		CreditSimulationDetailReqDTO req = new CreditSimulationDetailReqDTO();
		req.setCreditType(1233);
		String Testing = "testing123";
		
		when(restTemplateService.callPostRestApiForCalculateCredSim(Mockito.any(),Mockito.any())).thenReturn(data);
		
		DataResponse dataResponse = creditSimulationService.calculateCreditSimulation(req,Testing);
		
		assertEquals(dataResponse.getData(), data1.getData());
		
		
	}
	
	@Test
	void getDpOrPmtCalculationDataTest() throws Exception {
		DpOrPaymentRequestDTO req = new DpOrPaymentRequestDTO();
		req.setPic("Test1");
		req.setObjectCode("T10");
		req.setProgramId("T10");
		req.setObjectModelId("Tm10");
		req.setTenor(12);
		req.setOtr(12000000D);
		req.setTenor(10);
		req.setCashOrCredit(1);
		req.setDiscount(10);
		APIKeyHeadResponseDTO respreq= new APIKeyHeadResponseDTO();
		APIKeyDetailResponseDTO datainput1 = new APIKeyDetailResponseDTO();
		datainput1.setApiKey("KeyTest1");
		respreq.setData(datainput1);
		
		when(restTemplateService.callPostRestApiForGetApiKey(Mockito.any())).thenReturn(respreq);
		
		BiayaAdminReqDTO data2search = new BiayaAdminReqDTO();
		data2search.setTenor("12");
//		data2search.set
		
		BiayaAdminResDTO serching2 = new BiayaAdminResDTO();
		long admincas = 1000;
		BigDecimal cash = BigDecimal.valueOf(admincas);
		serching2.setAdminCashFee(cash);
		serching2.setAdminCreditFee(cash);
		AsuransiCashAndCreditResDTO dataasuransi = new AsuransiCashAndCreditResDTO();
		dataasuransi.setAsuransiCashAmt(cash);
		dataasuransi.setAsuransiCreditAmt(cash);
		when(restTemplateService.callRestTemplateToGetAsuransiCashAndCredit(Mockito.any())).thenReturn(dataasuransi);
		
		when(restTemplateService.callRestTemplateToGetBiayaAdmin(Mockito.any())).thenReturn(serching2);
		
		
		EffectiveOrLandingRateResDTO dataEffectivelanding = new EffectiveOrLandingRateResDTO();
		dataEffectivelanding.setEffectiveOrLandingRate(cash);
		when(restTemplateService.callRestTemplateToGetEffectiveOrLandingRate(Mockito.any())).thenReturn(dataEffectivelanding);
		
		
		
		APIKeyRequestDTO search = new APIKeyRequestDTO();
		
		DownPaymentOrPmtReqToCommonServiceDTO resp1 = new DownPaymentOrPmtReqToCommonServiceDTO();
		 
		DpOrPmtHeadResDTO respservice1 = new DpOrPmtHeadResDTO();
		DpOrPaymentDetailResDTO datarespservice1 = new DpOrPaymentDetailResDTO();
		datarespservice1.setCreditType(123);
		respservice1.setData(datarespservice1);
		
		when(restTemplateService.callPostRestApiForGetCalculationDpOrPmt(Mockito.any(),Mockito.any())).thenReturn(respservice1);
		
		when(objectMapper.writeValueAsString(resp1)).thenReturn("Test");
		
		DpOrPmtHeadResDTO resservice = creditSimulationService.getDpOrPmtCalculationData(req);
	
		assertEquals(respservice1.getData(),resservice.getData());
	
	}
}
