package id.co.adira.partner.order.service;

import id.co.adira.partner.order.dto.SubmitPartialDetailResponse;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.entity.SubmitCustomer;
import id.co.adira.partner.order.entity.SubmitDomisili;
import id.co.adira.partner.order.entity.SubmitOrder;
import id.co.adira.partner.order.entity.SubmitUnit;
import id.co.adira.partner.order.repository.SubmitCustomerRepository;
import id.co.adira.partner.order.repository.SubmitDomisiliRepository;
import id.co.adira.partner.order.repository.SubmitOrderRepository;
import id.co.adira.partner.order.repository.SubmitUnitRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static id.co.adira.partner.order.constant.AppConst.DRAFT;
import static id.co.adira.partner.order.constant.StatusConst.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;


@SpringBootTest
public class DraftSubmitPartialServiceTest {
    @InjectMocks
    DraftSubmitPartialService draftSubmitPartialService;

    @Mock
    SubmitOrderRepository submitOrderRepository;

    @Mock
    SubmitCustomerRepository submitCustomerRepository;

    @Mock
    SubmitDomisiliRepository submitDomisiliRepository;

    @Mock
    SubmitUnitRepository submitUnitRepository;

    SubmitOrder firstSubmitOrder = new SubmitOrder();
    SubmitOrder secondSubmitOrder = new SubmitOrder();
    SubmitCustomer submitCustomer = new SubmitCustomer();
    SubmitDomisili submitDomisili = new SubmitDomisili();
    SubmitUnit submitUnit = new SubmitUnit();

    List<SubmitOrder> submitOrderList = new ArrayList<>();

    ModelMapper modelMapper = new ModelMapper();

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        firstSubmitOrder.setSubmitOrderId("S0001");
        firstSubmitOrder.setOrderNo("ORD001");
        firstSubmitOrder.setActive(0);
        firstSubmitOrder.setCreatedDate(new Date());
        firstSubmitOrder.setModifiedDate(new Date());
        firstSubmitOrder.setCreatedBy("admin");
        firstSubmitOrder.setModifiedBy("admin");
        firstSubmitOrder.setIdx(1);
        firstSubmitOrder.setOrderDate(new Date());
        firstSubmitOrder.setStatus("Draft");
        firstSubmitOrder.setPic("test@ad1gate.com");
        firstSubmitOrder.setPartnerType("PRT_1");
        firstSubmitOrder.setApplNoUnit("appl_no_unit_1");
        firstSubmitOrder.setApplNoPayung("appl_no_payung_1");
        firstSubmitOrder.setGroupStatus("group_status_1");
        firstSubmitOrder.setSendDateStatus(new Date());

        submitOrderList.add(firstSubmitOrder);

        secondSubmitOrder.setSubmitOrderId("S0002");
        secondSubmitOrder.setOrderNo("ORD002");
        secondSubmitOrder.setActive(0);
        secondSubmitOrder.setCreatedDate(new Date());
        secondSubmitOrder.setModifiedDate(new Date());
        secondSubmitOrder.setCreatedBy("admin_test");
        secondSubmitOrder.setModifiedBy("admin_test");
        secondSubmitOrder.setIdx(1);
        secondSubmitOrder.setOrderDate(new Date());
        secondSubmitOrder.setStatus("Draft");
        secondSubmitOrder.setPic("test@ad1gate.com");
        secondSubmitOrder.setPartnerType("PRT_2");
        secondSubmitOrder.setApplNoUnit("appl_no_unit_2");
        secondSubmitOrder.setApplNoPayung("appl_no_payung_2");
        secondSubmitOrder.setGroupStatus("group_status_2");
        secondSubmitOrder.setSendDateStatus(new Date());

        submitOrderList.add(secondSubmitOrder);

        submitCustomer.setSubmitCustomerId("SC001");
        submitCustomer.setCustType("cust_type_1");
        submitCustomer.setCustomerName("dummy_customer_name");
        submitCustomer.setActive(0);
        submitCustomer.setIdType("1");
        submitCustomer.setFinType(1);
        submitCustomer.setJsonIdp("");
        submitCustomer.setBirthPlace("some_place");
        submitCustomer.setIdType("id_type_1");
        submitCustomer.setCreatedBy("admin");
        submitCustomer.setModifiedBy("admin");
        submitCustomer.setCreatedDate(new Date());
        submitCustomer.setModifiedDate(new Date());
        submitCustomer.setBirthDate(new Date());
        submitCustomer.setIdName("dummy_customer_name");
        submitCustomer.setFlagWa(1);
        submitCustomer.setWaNo("081234567890");
        submitCustomer.setHandphoneNo("081234567890");
        submitCustomer.setZipcode("11111");
        submitCustomer.setSubmitOrder(firstSubmitOrder);

        submitDomisili.setSubmitDomisiliId("SD001");
        submitDomisili.setNamaSo("dummy_nama_so");
        submitDomisili.setProvinsi("001");
        submitDomisili.setProvinsiDesc("Jakarta");
        submitDomisili.setActive(0);
        submitDomisili.setKecamatan("001");
        submitDomisili.setKecamatanDesc("kecamatan_1");
        submitDomisili.setAddressDomisili("address_domisili_1");
        submitDomisili.setFlagAddressId(1);
        submitDomisili.setKabkot("001");
        submitDomisili.setKabkotDesc("kabkot_1");
        submitDomisili.setModifiedDate(new Date());
        submitDomisili.setCreatedDate(new Date());
        submitDomisili.setCreatedBy("admin");
        submitDomisili.setModifiedBy("admin");
        submitDomisili.setSubmitOrder(firstSubmitOrder);

        submitUnit.setSubmitUnitId("SU001");
        submitUnit.setActive(0);
        submitUnit.setPromo("first_promo");
        submitUnit.setNotes("first_notes");
        submitUnit.setModelDetail("first_model_detail");
        submitUnit.setModifiedBy("admin");
        submitUnit.setCreatedBy("admin");
        submitUnit.setCreatedDate(new Date());
        submitUnit.setModifiedDate(new Date());
        submitUnit.setRate(BigDecimal.valueOf(0));
        submitUnit.setObjectPrice(BigDecimal.valueOf(1000000));
        submitUnit.setInstallmentAmt(BigDecimal.valueOf(1000));
        submitUnit.setTenor(1);
        submitUnit.setSubmitOrder(firstSubmitOrder);

        when(submitOrderRepository.findSubmitOrdersByStatusAndPic(DRAFT.getMessage(),"test@ad1gate.com")).thenReturn(submitOrderList);
    }

    @Test
    public void testGetSubmitPartialOrderAndExpectGetData(){
        when(submitCustomerRepository.findSubmitCustomerBySubmitOrder(firstSubmitOrder)).thenReturn(submitCustomer);
        when(submitUnitRepository.findSubmitUnitBySubmitOrder(firstSubmitOrder)).thenReturn(submitUnit);

        DataResponse dataResponse = draftSubmitPartialService.getDraftSubmitPartialOrderByPic("test@ad1gate.com");
        assertTrue(dataResponse.getData() != null);

    }

    @Test
    public void testGetSubmitPartialOrderByPicAndExpectDataNotExistsAndReturnNewArrayList(){
        when(submitOrderRepository.findSubmitOrdersByStatusAndPic(DRAFT.getMessage(), "test@ad1gate.com")).thenReturn(new ArrayList<>());
        DataResponse dataResponse = draftSubmitPartialService.getDraftSubmitPartialOrderByPic("test123@ad1gate.com");

        assertTrue(dataResponse.getData().toString().equals(new ArrayList<>().toString()));

    }

    @Test
    public void testGetDetailSubmitPartialAndExpectGetData(){
        when(submitOrderRepository.findSubmitOrderByStatusAndOrderNo(DRAFT.getMessage(),"first_order_no")).thenReturn(firstSubmitOrder);
        when(submitCustomerRepository.findSubmitCustomerBySubmitOrder(firstSubmitOrder)).thenReturn(submitCustomer);
        when(submitDomisiliRepository.findSubmitDomisiliBySubmitOrder(firstSubmitOrder)).thenReturn(submitDomisili);
        when(submitUnitRepository.findSubmitUnitBySubmitOrder(firstSubmitOrder)).thenReturn(submitUnit);

        DataResponse dataResponse = draftSubmitPartialService.getDraftSubmitPartialDetailByOrderNo("first_order_no");
        assertTrue(dataResponse.getData() != null);

    }

    @Test
    public void testGetDetailSubmitPartialWhenSubmitCustomerDomisiliAndUnitIsNull(){
        when(submitOrderRepository.findSubmitOrderByStatusAndOrderNo(DRAFT.getMessage(),"first_order_no")).thenReturn(firstSubmitOrder);
        when(submitCustomerRepository.findSubmitCustomerBySubmitOrder(firstSubmitOrder)).thenReturn(null);
        when(submitDomisiliRepository.findSubmitDomisiliBySubmitOrder(firstSubmitOrder)).thenReturn(null);
        when(submitUnitRepository.findSubmitUnitBySubmitOrder(firstSubmitOrder)).thenReturn(null);

        draftSubmitPartialService.getDraftSubmitPartialDetailByOrderNo("first_order_no");
    }

    @Test
    public void testGetDetailSubmitPartialWhenSubmitOrderIsNull(){
        when(submitOrderRepository.findSubmitOrderByStatusAndOrderNo(DRAFT.getMessage(), "first_order_no")).thenReturn(null);
        DataResponse dataResponse = draftSubmitPartialService.getDraftSubmitPartialDetailByOrderNo("first_order_no");

        SubmitPartialDetailResponse submitPartialDetailResponse = modelMapper.map(dataResponse.getData(), SubmitPartialDetailResponse.class);
        assertTrue(submitPartialDetailResponse.getDraftDataKonsumen() == null);
        assertTrue(submitPartialDetailResponse.getDraftDataDomisili() == null);
        assertTrue(submitPartialDetailResponse.getDraftDataUnit() == null);

    }

    @Test
    public void testDeleteDraftAndExpectSuccess() {
        DataResponse dataResponse = draftSubmitPartialService.deleteDraft("ORD001");
        assertEquals(dataResponse.getData().toString(), SUCCESS.getMessage().toLowerCase());
    }
    
    
//   @Test
//   void TestDeleteDraftAndException() {
//	   DataResponse dataResponse = draftSubmitPartialService.deleteDraft("ORD001");
//	   Mockito.doThrow(RuntimeException.class)
//       .when(submitOrderRepository.deleteDraft(Mockito.any()))
//       .myMethod(draftSubmitPartialService.deleteDraft("ORD001"));
//      
//   	
//   }
}
