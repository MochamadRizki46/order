package id.co.adira.partner.order.service;

import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.entity.SubmitCustomer;
import id.co.adira.partner.order.entity.SubmitDomisili;
import id.co.adira.partner.order.entity.SubmitOrder;
import id.co.adira.partner.order.entity.SubmitUnit;
import id.co.adira.partner.order.repository.SubmitCustomerRepository;
import id.co.adira.partner.order.repository.SubmitDomisiliRepository;
import id.co.adira.partner.order.repository.SubmitOrderRepository;
import id.co.adira.partner.order.repository.SubmitUnitRepository;
import id.co.adira.partner.order.utils.DateUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static id.co.adira.partner.order.constant.AppConst.DRAFT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class DraftSubmitPartialServiceV2Test {

    @InjectMocks
    DraftSubmitPartialServiceV2 draftSubmitPartialServiceV2;

    @Mock
    SubmitCustomerRepository submitCustomerRepository;

    @Mock
    SubmitDomisiliRepository submitDomisiliRepository;

    @Mock
    SubmitUnitRepository submitUnitRepository;

    @Mock
    SubmitOrderRepository submitOrderRepository;

    SubmitOrder firstSubmitOrder = new SubmitOrder();

    SubmitOrder secondSubmitOrder = new SubmitOrder();

    SubmitCustomer submitCustomer = new SubmitCustomer();

    SubmitDomisili submitDomisili = new SubmitDomisili();

    SubmitUnit submitUnit = new SubmitUnit();

    List<SubmitOrder> submitOrderList = new ArrayList<>();

    ZoneId defaultZoneId = ZoneId.systemDefault();

    ModelMapper modelMapper = new ModelMapper();

    Integer previousDayInterval = 3;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        firstSubmitOrder.setSubmitOrderId("S0001");
        firstSubmitOrder.setOrderNo("ORD001");
        firstSubmitOrder.setActive(0);
        firstSubmitOrder.setCreatedDate(new Date());
        firstSubmitOrder.setModifiedDate(new Date());
        firstSubmitOrder.setCreatedBy("admin");
        firstSubmitOrder.setModifiedBy("admin");
        firstSubmitOrder.setIdx(1);
        firstSubmitOrder.setOrderDate(new Date());
        firstSubmitOrder.setStatus("Draft");
        firstSubmitOrder.setPic("test@ad1gate.com");
        firstSubmitOrder.setPartnerType("PRT_1");
        firstSubmitOrder.setApplNoUnit("appl_no_unit_1");
        firstSubmitOrder.setApplNoPayung("appl_no_payung_1");
        firstSubmitOrder.setGroupStatus("group_status_1");
        firstSubmitOrder.setSendDateStatus(new Date());

        submitOrderList.add(firstSubmitOrder);

        secondSubmitOrder.setSubmitOrderId("S0002");
        secondSubmitOrder.setOrderNo("ORD002");
        secondSubmitOrder.setActive(0);
        secondSubmitOrder.setCreatedDate(new Date());
        secondSubmitOrder.setModifiedDate(new Date());
        secondSubmitOrder.setCreatedBy("admin_test");
        secondSubmitOrder.setModifiedBy("admin_test");
        secondSubmitOrder.setIdx(1);
        secondSubmitOrder.setOrderDate(new Date());
        secondSubmitOrder.setStatus("Draft");
        secondSubmitOrder.setPic("test@ad1gate.com");
        secondSubmitOrder.setPartnerType("PRT_2");
        secondSubmitOrder.setApplNoUnit("appl_no_unit_2");
        secondSubmitOrder.setApplNoPayung("appl_no_payung_2");
        secondSubmitOrder.setGroupStatus("group_status_2");
        secondSubmitOrder.setSendDateStatus(new Date());

        submitOrderList.add(secondSubmitOrder);

        submitCustomer.setSubmitCustomerId("SC001");
        submitCustomer.setCustType("cust_type_1");
        submitCustomer.setCustomerName("dummy_customer_name");
        submitCustomer.setActive(0);
        submitCustomer.setIdType("1");
        submitCustomer.setFinType(1);
        submitCustomer.setJsonIdp("");
        submitCustomer.setBirthPlace("some_place");
        submitCustomer.setIdType("id_type_1");
        submitCustomer.setCreatedBy("admin");
        submitCustomer.setModifiedBy("admin");
        submitCustomer.setCreatedDate(new Date());
        submitCustomer.setModifiedDate(new Date());
        submitCustomer.setBirthDate(new Date());
        submitCustomer.setIdName("dummy_customer_name");
        submitCustomer.setFlagWa(1);
        submitCustomer.setWaNo("081234567890");
        submitCustomer.setHandphoneNo("081234567890");
        submitCustomer.setZipcode("11111");
        submitCustomer.setSubmitOrder(firstSubmitOrder);

        submitDomisili.setSubmitDomisiliId("SD001");
        submitDomisili.setNamaSo("dummy_nama_so");
        submitDomisili.setProvinsi("001");
        submitDomisili.setProvinsiDesc("Jakarta");
        submitDomisili.setActive(0);
        submitDomisili.setKecamatan("001");
        submitDomisili.setKecamatanDesc("kecamatan_1");
        submitDomisili.setAddressDomisili("address_domisili_1");
        submitDomisili.setFlagAddressId(1);
        submitDomisili.setKabkot("001");
        submitDomisili.setKabkotDesc("kabkot_1");
        submitDomisili.setModifiedDate(new Date());
        submitDomisili.setCreatedDate(new Date());
        submitDomisili.setCreatedBy("admin");
        submitDomisili.setModifiedBy("admin");
        submitDomisili.setSubmitOrder(firstSubmitOrder);

        submitUnit.setSubmitUnitId("SU001");
        submitUnit.setActive(0);
        submitUnit.setPromo("first_promo");
        submitUnit.setNotes("first_notes");
        submitUnit.setModelDetail("first_model_detail");
        submitUnit.setModifiedBy("admin");
        submitUnit.setCreatedBy("admin");
        submitUnit.setCreatedDate(new Date());
        submitUnit.setModifiedDate(new Date());
        submitUnit.setRate(BigDecimal.valueOf(0));
        submitUnit.setObjectPrice(BigDecimal.valueOf(1000000));
        submitUnit.setInstallmentAmt(BigDecimal.valueOf(1000));
        submitUnit.setTenor(1);
        submitUnit.setSubmitOrder(firstSubmitOrder);
        
//        ReflectionTestUtils.setField(orderService, "urlHeader", "http://localhost:8080");
        

    }

    @Test
    public void testGetSubmitPartialOrderV2AndExpectDataIsEmpty() throws ParseException {
        LocalDate localDate = DateUtil.getDateBasedOnInterval(previousDayInterval);
        Date fromDate = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());

        when(submitOrderRepository.findSubmitOrderByFilterDateStatusAndPicOrderByOrderDateAsc(fromDate, new Date(), DRAFT.getMessage(), "test@ad1gate.com")).thenReturn(new ArrayList<>());
        DataResponse dataResponse = draftSubmitPartialServiceV2.getDraftSubmitPartialOrderByPic(previousDayInterval, "test@ad1gate.com", 0);

        List<Object> datas = modelMapper.map(dataResponse.getData(), List.class);
        assertEquals(0, datas.size());
    
    
    }
    
    @Test
    public void testGetSubmitPartialOrderV2AndExpectDataIsNotEmpty() throws ParseException {
    	LocalDate localDate = DateUtil.getDateBasedOnInterval(previousDayInterval);
        Date fromDate = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
        SubmitOrder submitOrder = new SubmitOrder();
        List<SubmitOrder> submitOrderList = new ArrayList<>();
        submitOrder.setIdx(123);
        submitOrder.setActive(1);
        submitOrder.setStatus("sudah");

        submitOrder.setOrderNo("222");
        submitOrder.setOrderDate(fromDate);

        
        submitOrderList.add(submitOrder);
        
        SubmitCustomer datacustomer = new SubmitCustomer();
        datacustomer.setCustomerName("testting");
        
        when(submitOrderRepository.findSubmitOrderByFilterDateStatusAndPicOrderByOrderDateDesc(ArgumentMatchers.eq(fromDate), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(DRAFT.getMessage()), ArgumentMatchers.eq("test@ad1gate.com"))).thenReturn(Collections.singletonList(submitOrder));
        when(submitCustomerRepository.findSubmitCustomerBySubmitOrder(submitOrder)).thenReturn(submitCustomer);

        

        when(submitOrderRepository.findSubmitOrderByStatusAndOrderNo(DRAFT.getMessage(),"first_order_no")).thenReturn(firstSubmitOrder);
        when(submitCustomerRepository.findSubmitCustomerBySubmitOrder(Mockito.any())).thenReturn(datacustomer);
        when(submitDomisiliRepository.findSubmitDomisiliBySubmitOrder(firstSubmitOrder)).thenReturn(null);
        when(submitUnitRepository.findSubmitUnitBySubmitOrder(firstSubmitOrder)).thenReturn(null);

        
        
//        draftSubmitPartialServiceV2.getDraftSubmitPartialOrderByPic("first_order_no");
        
        DataResponse dataResponse = draftSubmitPartialServiceV2.getDraftSubmitPartialOrderByPic(previousDayInterval, "test@ad1gate.com", 2);

        List<Object> datas = modelMapper.map(dataResponse.getData(), List.class);
       
     

        assertEquals(1, datas.size());
    }

    
//    @Test
//    public void testGetDetailSubmitPartialWhenSubmitCustomerDomisiliAndUnitIsNull(){
//      
//    }
}
