package id.co.adira.partner.order.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.adira.partner.order.entity.Log;
import id.co.adira.partner.order.entity.SubmitOrder;
import id.co.adira.partner.order.repository.LogRepository;
import id.co.adira.partner.order.repository.SubmitOrderRepository;

@SpringBootTest
@ActiveProfiles("test")
public class LogServiceTest {
	
	@InjectMocks
	LogService logService;
	
	@Mock
	ObjectMapper objectMapper;
	
	@Mock
	LogRepository logRepository;
	
	@Mock
	SubmitOrderRepository submitOrderRepository;
	
	@Test
	void insertToLogTableTest() throws Exception {
		
		Object request = new Object();
		request = "testrequest";
		
		Object response = "testrespon";
		String logMessage="logmessage";
		
		double totalTimeMillis=34324.34d;
		String orderNo="testorderno";
		SubmitOrder order = new SubmitOrder();
		order.setPic("StefTest");
		List<Log> retuns = new ArrayList();
		Log log = new Log();
		log.setActive(1);
		log.setLogId("1");
		log.setDuration(200);
		log.setLogName("stef");
		
		Log log2 = new Log();
		log2.setActive(2);
		log2.setLogId("2");
		log2.setDuration(300);
		log2.setLogName("stef2");
		retuns.add(log2);
		retuns.add(log);
		when(submitOrderRepository.findSubmitOrderByOrderNo(orderNo)).thenReturn(order);
		
		when(objectMapper.writeValueAsString(request)).thenReturn("Yes its me");
		
		when(objectMapper.writeValueAsString(response)).thenReturn("Yes this respon");
		
		when(logRepository.save(Mockito.any())).thenReturn(log);
		
	
		when(logRepository.getLogData()).thenReturn(retuns);
		
		
		logService.insertToLogTable(request, response, logMessage, totalTimeMillis, orderNo);
		
	}
	
	
	@Test
	public void insertCredSimToLogTest() throws Exception {
		Object request = "Test";
		Object response="nope";
		String logMessage="yes";
		String pic="tes";
		double totalTimeMillis=3000000d;
	Log log = new Log();
	log.setActive(1);
	log.setDuration(200);
	log.setLogName("Test");
	log.setLogId("112");
	
	when(logRepository.getLogByUserId(Mockito.any())).thenReturn(log);
	
	when(logRepository.save(Mockito.any())).thenReturn(log);
	
	
	
	logService.insertCredSimToLog(request, response, logMessage, pic, totalTimeMillis);
	
	
	}

	@Test
	public void insertCredSimToLogTestnull() throws Exception {
		Object request = "Test";
		Object response="nope";
		String logMessage="yes";
		String pic="tes";
		double totalTimeMillis=3000000d;
	Log log = new Log();
	log.setActive(1);
	log.setDuration(200);
	log.setLogName("Test");
	log.setLogId("112");
	
	when(logRepository.getLogByUserId(Mockito.any())).thenReturn(null);
	
	when(logRepository.save(Mockito.any())).thenReturn(log);
	
	
	
	logService.insertCredSimToLog(request, response, logMessage, pic, totalTimeMillis);
	
	
	}
	
	
}

