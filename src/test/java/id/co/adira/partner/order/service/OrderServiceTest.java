package id.co.adira.partner.order.service;

import static id.co.adira.partner.order.constant.AppConst.ID;
import static id.co.adira.partner.order.constant.FormattingConst.FIRST_SIMPLE_DATE_FORMAT;
import static id.co.adira.partner.order.constant.FormattingConst.FOURTH_SIMPLE_DATE_FORMAT;
import static id.co.adira.partner.order.constant.FormattingConst.TIME_FORMAT;
import static id.co.adira.partner.order.constant.StatusConst.CANNOT_FIND_SUBMIT_ORDER_WITH_ID;
import static id.co.adira.partner.order.constant.StatusConst.OK;
import static id.co.adira.partner.order.constant.StatusConst.ORDER_TERKIRIM;
import static id.co.adira.partner.order.constant.StatusConst.SISTEM_MENERIMA_ORDER;
import static id.co.adira.partner.order.constant.StatusConst.SUCCESS;
import static id.co.adira.partner.order.constant.StatusConst.SVR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.cfg.Environment;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.adira.partner.order.dto.DashboardReqDTO;
import id.co.adira.partner.order.dto.DocumentReqDTO;
import id.co.adira.partner.order.dto.GenerateOrderReq;
import id.co.adira.partner.order.dto.IdpWebReqDTO;
import id.co.adira.partner.order.dto.KabkotResponseDTO;
import id.co.adira.partner.order.dto.OcrKtpResponsesDTO;
import id.co.adira.partner.order.dto.OcrNpwpResponsesDTO;
import id.co.adira.partner.order.dto.SendOrderToLeadReqDTO;
import id.co.adira.partner.order.dto.SendOrderToLeadV2ReqDTO;
import id.co.adira.partner.order.dto.StandardResponseDTO;
import id.co.adira.partner.order.dto.SubmitCustomerReqDTO;
import id.co.adira.partner.order.dto.SubmitDomisiliReqDTO;
import id.co.adira.partner.order.dto.SubmitOrderReqDTO;
import id.co.adira.partner.order.dto.SubmitUnitReqDTO;
import id.co.adira.partner.order.dto.TrackingDashboardSalesResDTO;
import id.co.adira.partner.order.dto.TrackingOrderDetailReqDTO;
import id.co.adira.partner.order.dto.TrackingOrderDetailResDTO;
import id.co.adira.partner.order.dto.TrackingOrderHeaderResDTO;
import id.co.adira.partner.order.dto.TrackingOrderHistoryReqDTO;
import id.co.adira.partner.order.dto.TrackingOrderHomeResDTO;
import id.co.adira.partner.order.dto.TrackingOrderListReqDTO;
import id.co.adira.partner.order.dto.TrackingSumOrderSlsResDTO;
import id.co.adira.partner.order.dto.UpdateStatusTrackingReqDTO;
import id.co.adira.partner.order.dto.ad1gate.KecamatanAd1gateDetailResDTO;
import id.co.adira.partner.order.dto.ad1gate.KecamatanAd1gateHeaderResDTO;
import id.co.adira.partner.order.dto.ad1gate.ModelAd1gateHeaderResDTO;
import id.co.adira.partner.order.dto.ad1gate.ModelAd1gateReqDTO;
import id.co.adira.partner.order.dto.ecm.TrackingOrderDetailwithECMResDTO;
import id.co.adira.partner.order.dto.entpool.TrackingHistoryHeaderResponsesDTO;
import id.co.adira.partner.order.dto.entpool.TrackingLastStatusHeaderResDTO;
import id.co.adira.partner.order.dto.kafka.ApplicationDistributedDTO;
import id.co.adira.partner.order.dto.kafka.ApplicationDocumentDistributedDTO;
import id.co.adira.partner.order.dto.kafka.ApplicationNoteDistributedDTO;
import id.co.adira.partner.order.dto.kafka.ApplicationObjectDistributedDTO;
import id.co.adira.partner.order.dto.kafka.CustomerDistributedDTO;
import id.co.adira.partner.order.dto.kafka.CustomerPersonalDistributedDTO;
import id.co.adira.partner.order.dto.kafka.GenerateApplNoResDTO;
import id.co.adira.partner.order.dto.kafka.GenerateApplNoV2ResDTO;
import id.co.adira.partner.order.dto.kafka.PurchaseOrderDistributedDTO;
import id.co.adira.partner.order.dto.kafka.TrackingOrderDistributeDTO;
import id.co.adira.partner.order.dto.kafka.UpdateStatusTrackingDTO;
import id.co.adira.partner.order.dto.le.OrderDetailLeadResDTO;
import id.co.adira.partner.order.dto.le.TrackingHeaderRequestDTO;
import id.co.adira.partner.order.dto.le.TrackingHistoryResponseDTO;
import id.co.adira.partner.order.dto.le.TrackingHomeResponseDTO;
import id.co.adira.partner.order.dto.response.DataResponse;
import id.co.adira.partner.order.dto.revampms2.KabkotHeaderDTO;
import id.co.adira.partner.order.dto.revampms2.KabkotResDTO;
import id.co.adira.partner.order.entity.Application;
import id.co.adira.partner.order.entity.ApplicationDocument;
import id.co.adira.partner.order.entity.ApplicationTracking;
import id.co.adira.partner.order.entity.Customer;
import id.co.adira.partner.order.entity.CustomerPersonal;
import id.co.adira.partner.order.entity.DocumentDetail;
import id.co.adira.partner.order.entity.PurchaseOrder;
import id.co.adira.partner.order.entity.StatusOrderDetail;
import id.co.adira.partner.order.entity.SubmitCustomer;
import id.co.adira.partner.order.entity.SubmitDomisili;
import id.co.adira.partner.order.entity.SubmitOrder;
import id.co.adira.partner.order.entity.SubmitUnit;
import id.co.adira.partner.order.exception.AdiraCustomException;
import id.co.adira.partner.order.kafka.KafkaProducer;
import id.co.adira.partner.order.repository.ApplicationRepository;
import id.co.adira.partner.order.repository.CustomerPersonalRepository;
import id.co.adira.partner.order.repository.CustomerRepository;
import id.co.adira.partner.order.repository.DistributedApplicationTrackingRepository;
import id.co.adira.partner.order.repository.DocumentRepository;
import id.co.adira.partner.order.repository.GenerateId;
import id.co.adira.partner.order.repository.PurchaseOrderRepository;
import id.co.adira.partner.order.repository.StatusOrderDetailRepository;
import id.co.adira.partner.order.repository.SubmitCustomerRepository;
import id.co.adira.partner.order.repository.SubmitDomisiliRepository;
import id.co.adira.partner.order.repository.SubmitOrderRepository;
import id.co.adira.partner.order.repository.SubmitUnitRepository;
import id.co.adira.partner.order.repository.TrackingOrderRepository;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class OrderServiceTest {

	@InjectMocks
	OrderService orderService;

	@InjectMocks
	LogService logService;

	@Mock
	SubmitOrderRepository submitOrderRepository;

	@Mock
	DistributedApplicationTrackingRepository applicationTrackingRepository;

	@Mock
	SubmitCustomerRepository submitCustomerRepository;

	@Mock
	SubmitDomisiliRepository submitDomisiliRepository;

	@Mock
	SubmitUnitRepository submitUnitRepository;

	@Mock
	StatusOrderDetailRepository statusOrderDetailRepository;

	@Mock
	TrackingOrderRepository trackingorderRepository;

	@Mock
	GenerateId generateId;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	CustomerPersonalRepository customerPersonalRepository;

	@Mock

	private RestTemplate restTemplate;

	@Mock
	DocumentRepository documentRepository;

	@Mock
	ApplicationRepository applicationRepository;

	@Mock
	KafkaProducer kafkaProducer;

	@Mock
	ObjectMapper objectmppr;

	@Mock
	PurchaseOrderRepository poRepository;

	@Autowired
	ObjectMapper objectMapper;

	@Mock
	ModelMapper modelMapper;;

//	@Autowired
//	Environment env;

	SimpleDateFormat formatTime = new SimpleDateFormat(TIME_FORMAT.getMessage());

	// String CANNOT_FIND_SUBMIT_ORDER_WITH_ID = "Can't find submit order with id:
	// ";
	HttpHeaders headers;
	SubmitOrderReqDTO submitOrderReqDTO = new SubmitOrderReqDTO();
	ApplicationDistributedDTO distributeApplication = new ApplicationDistributedDTO();
	SubmitCustomerReqDTO submitCustomerReqDTO = new SubmitCustomerReqDTO();
	SubmitDomisiliReqDTO submitDomisiliReqDTO = new SubmitDomisiliReqDTO();
	SubmitUnitReqDTO submitUnitReqDTO = new SubmitUnitReqDTO();
	DocumentReqDTO documentReqDTO = new DocumentReqDTO();
	DateTimeFormatter formatDate = DateTimeFormatter.ofPattern(FIRST_SIMPLE_DATE_FORMAT.getMessage());

	SubmitOrder submitOrder = new SubmitOrder();
	SubmitCustomer submitCustomer = new SubmitCustomer();
	SubmitDomisili submitDomisili = new SubmitDomisili();
	SubmitUnit submitUnit = new SubmitUnit();
	DocumentDetail documentDetail = new DocumentDetail();
	StatusOrderDetail firstStatusOrderDetail = new StatusOrderDetail();
	StatusOrderDetail secondStatusOrderDetail = new StatusOrderDetail();

//	@Mock
//	ResponseEntity<String> response;

	List<StatusOrderDetail> statusOrderDetailList = new ArrayList<>();
	List<Map<String, String>> response = new ArrayList<>();
	SimpleDateFormat fourthSdf = new SimpleDateFormat(FOURTH_SIMPLE_DATE_FORMAT.getMessage());

	Map<String, Object> map = new HashMap<>();

	private static final Long USER_ID = 1L;
	private static final String USERNAME = "username";
	private static final String USER_EMAIL = "abc@exampla.com";
	private static final String USER_PHONE = "+625893475984389";
	private static final String PASSWORD = "jajaja";
	private static final String ACCESS_TOKEN = "token";

	@BeforeEach
	public void setUp() {

		ModelMapper mm = new ModelMapper();

		submitOrder.setSubmitOrderId("S0001");
		submitOrder.setOrderNo("ORD001");
		submitOrder.setActive(0);
		submitOrder.setCreatedDate(new Date());
		submitOrder.setModifiedDate(new Date());
		submitOrder.setCreatedBy("admin");
		submitOrder.setModifiedBy("admin");
		submitOrder.setIdx(1);
		submitOrder.setOrderDate(new Date());
		submitOrder.setStatus("Draft");
		submitOrder.setPic("test@ad1gate.com");
		submitOrder.setPartnerType("PRT_1");
		submitOrder.setApplNoUnit("appl_no_unit_1");
		submitOrder.setApplNoPayung("appl_no_payung_1");
		submitOrder.setGroupStatus("group_status_1");
		submitOrder.setSendDateStatus(new Date());

		submitCustomer.setSubmitCustomerId("SC001");
		submitCustomer.setCustType("cust_type_1");
		submitCustomer.setCustomerName("dummy_customer_name");
		submitCustomer.setActive(0);
		submitCustomer.setIdType("1");
		submitCustomer.setFinType(1);
		submitCustomer.setJsonIdp("");
		submitCustomer.setBirthPlace("some_place");
		submitCustomer.setIdType("id_type_1");
		submitCustomer.setCreatedBy("admin");
		submitCustomer.setModifiedBy("admin");
		submitCustomer.setCreatedDate(new Date());
		submitCustomer.setModifiedDate(new Date());
		submitCustomer.setBirthDate(new Date());
		submitCustomer.setIdName("dummy_customer_name");
		submitCustomer.setFlagWa(1);
		submitCustomer.setWaNo("081234567890");
		submitCustomer.setHandphoneNo("081234567890");
		submitCustomer.setZipcode("11111");
		submitCustomer.setSubmitOrder(submitOrder);
		submitCustomer.setIdNo("Test3090");

		submitDomisili.setSubmitDomisiliId("SD001");
		submitDomisili.setNamaSo("dummy_nama_so");
		submitDomisili.setProvinsi("001");
		submitDomisili.setProvinsiDesc("Jakarta");
		submitDomisili.setActive(0);
		submitDomisili.setKecamatan("001");
		submitDomisili.setKecamatanDesc("kecamatan_1");
		submitDomisili.setAddressDomisili("address_domisili_1");
		submitDomisili.setFlagAddressId(1);
		submitDomisili.setKabkot("001");
		submitDomisili.setKabkotDesc("kabkot_1");
		submitDomisili.setModifiedDate(new Date());
		submitDomisili.setCreatedDate(new Date());
		submitDomisili.setCreatedBy("admin");
		submitDomisili.setModifiedBy("admin");
		submitDomisili.setSubmitOrder(submitOrder);
		submitDomisili.setRtrw("05/06");

		submitUnit.setSubmitUnitId("SU001");
		submitUnit.setActive(0);
		submitUnit.setPromo("first_promo");
		submitUnit.setNotes("first_notes");
		submitUnit.setModelDetail("first_model_detail");
		submitUnit.setModifiedBy("admin");
		submitUnit.setCreatedBy("admin");
		submitUnit.setCreatedDate(new Date());
		submitUnit.setModifiedDate(new Date());
		submitUnit.setRate(BigDecimal.valueOf(0));
		submitUnit.setObjectPrice(BigDecimal.valueOf(1000000));
		submitUnit.setInstallmentAmt(BigDecimal.valueOf(1000));
		submitUnit.setTenor(1);
		submitUnit.setSubmitOrder(submitOrder);

		documentDetail.setDocType("first_doc_type");
		documentDetail.setActive(0);
		documentDetail.setCreatedBy("admin");
		documentDetail.setModifiedBy("admin");
		documentDetail.setCreatedDate(new Date());
		documentDetail.setModifiedDate(new Date());
		documentDetail.setDocDtlId("DTL001");
		documentDetail.setDocId("DOC001");
		documentDetail.setDocPath("doc_path_1");
		documentDetail.setSubmitOrder(submitOrder);

		firstStatusOrderDetail.setOrderNo(submitOrder.getOrderNo());
		firstStatusOrderDetail.setStatus(submitOrder.getStatus());
		firstStatusOrderDetail.setGroupStatus(submitOrder.getGroupStatus());
		firstStatusOrderDetail.setApplNoUnit(submitOrder.getApplNoUnit());
		firstStatusOrderDetail.setStatusDate(new Date());
		firstStatusOrderDetail.setCreateBy(submitOrder.getCreatedBy());
		firstStatusOrderDetail.setUserType("user_type_1");
		firstStatusOrderDetail.setActive(0);
		firstStatusOrderDetail.setStatus(ORDER_TERKIRIM.getMessage());
		firstStatusOrderDetail.setSubstatus(SISTEM_MENERIMA_ORDER.getMessage());
		firstStatusOrderDetail.setStatusTime("");
		firstStatusOrderDetail.setIdSeq(1);

		statusOrderDetailList.add(firstStatusOrderDetail);

		secondStatusOrderDetail.setOrderNo("second_order_no");
		secondStatusOrderDetail.setStatus(submitOrder.getStatus());
		secondStatusOrderDetail.setGroupStatus(submitOrder.getGroupStatus());
		secondStatusOrderDetail.setApplNoUnit(submitOrder.getApplNoUnit());
		secondStatusOrderDetail.setStatusDate(new Date());
		secondStatusOrderDetail.setCreateBy(submitOrder.getCreatedBy());
		secondStatusOrderDetail.setUserType("user_type_2");
		secondStatusOrderDetail.setActive(0);
		secondStatusOrderDetail.setStatus(ORDER_TERKIRIM.getMessage());
		secondStatusOrderDetail.setSubstatus(SISTEM_MENERIMA_ORDER.getMessage());
		secondStatusOrderDetail.setStatusTime("");
		secondStatusOrderDetail.setIdSeq(2);

		statusOrderDetailList.add(secondStatusOrderDetail);

		submitOrder.setSubmitCustomer(submitCustomer);
		submitOrder.setSubmitDomisili(submitDomisili);
		submitOrder.setSubmitUnit(submitUnit);

		submitOrderReqDTO = mm.map(submitOrder, SubmitOrderReqDTO.class);
		submitCustomerReqDTO = mm.map(submitCustomer, SubmitCustomerReqDTO.class);
		submitDomisiliReqDTO = mm.map(submitDomisili, SubmitDomisiliReqDTO.class);
		submitUnitReqDTO = mm.map(submitUnit, SubmitUnitReqDTO.class);
		documentReqDTO = mm.map(documentDetail, DocumentReqDTO.class);

		var application = mm.map(distributeApplication, Application.class);

//		ReflectionTestUtils.setField(orderService, "urlHeader", "http://localhost:8080");
//
//		ReflectionTestUtils.setField(orderService, "urlApplno", "http://localhost:8080");
//
//		ReflectionTestUtils.setField(orderService, "urlHome", "http://localhost:8080");
//
//		ReflectionTestUtils.setField(orderService, "urlEpHistory", "http://localhost:8080");
//
//		ReflectionTestUtils.setField(orderService, "urlDetail", "http://localhost:8080");

		ReflectionTestUtils.setField(orderService, "urlgetkecamatan", "http://localhost:8080");

		ReflectionTestUtils.setField(orderService, "urlKabkot", "http://localhost:8080");

		ReflectionTestUtils.setField(orderService, "urlgetkecamatan", "http://localhost:8080");

		ReflectionTestUtils.setField(orderService, "urlgetmodel", "http://localhost:8080");

//		ReflectionTestUtils.setField(orderService, "urlEpTracking", "http://localhost:8080");

		ReflectionTestUtils.setField(orderService, "urlUploadEcm", "http://localhost:8080");

		ReflectionTestUtils.setField(orderService, "urlIDP", "http://localhost:8080");

		ReflectionTestUtils.setField(orderService, "pathNpwp", "http://pathNpwp:8080");

		ReflectionTestUtils.setField(orderService, "pathKTP", "http://pathKTP:8080");

	}

//	@Test
//	public void testSubmitNewOrderAndExpectSubmitSuccess() {
//		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(null);
//		StandardResponseDTO submitNewOrderResponse = orderService.savePartialOrder(submitOrderReqDTO);
//
//		assertEquals(submitNewOrderResponse, SUCCESS.getMessage());
//	}

//	@Test
//	public void testUpdateExistingOrderAndExpectSubmitSuccess() {
//		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(submitOrder);
//		StandardResponseDTO updateExistingOrderResponse = orderService.savePartialOrder(submitOrderReqDTO);
//
//		assertEquals(updateExistingOrderResponse, SUCCESS.getMessage());
//	}

	@Test
	public void testSubmitOrderAndExpectThrowAnException() {
//		when(submitOrderRepository.findSubmitOrderByOrderNo(Mockito.any())).thenThrow(RuntimeException.class);
//		String updateExistingOrderResponse = orderService.savePartialOrder(submitOrderReqDTO);
//
		given(submitOrderRepository.findSubmitOrderByOrderNo(Mockito.any())).willThrow(new RuntimeException());

//		when(() -> );

//		assertEquals(updateExistingOrderResponse, "java.lang.RuntimeException");
	}

//	@Test
//	public void testSubmitNewCustomerAndExpectSubmitSuccess() {
//		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(submitOrder);
//		when(submitCustomerRepository.findSubmitCustomerBySubmitOrder(submitOrder)).thenReturn(null);
//
//		StandardResponseDTO submitCustomerResponse = orderService.savePartialCustomer(submitCustomerReqDTO);
//		assertEquals(submitCustomerResponse, SUCCESS.getMessage());
//	}

//	@Test
//	public void savePartialCustomerTesterror() {
//		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(null);
//
//		StandardResponseDTO submitCustomerResponse = orderService.savePartialCustomer(submitCustomerReqDTO);
//		assertEquals(submitCustomerResponse,
//				CANNOT_FIND_SUBMIT_ORDER_WITH_ID.getMessage() + submitCustomerReqDTO.getOrderNo());
//	}

//	@Test
//	public void testUpdateExistingSubmitCustomerAndExpectSubmitSuccess() {
//		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(submitOrder);
//		when(submitCustomerRepository.findSubmitCustomerBySubmitOrder(submitOrder)).thenReturn(submitCustomer);
//
//		StandardResponseDTO updateExistingSubmitCustomerResponse = orderService.savePartialCustomer(submitCustomerReqDTO);
//		assertEquals(updateExistingSubmitCustomerResponse, SUCCESS.getMessage());
//
//	}

//	@Test
//	public void testSubmitNewDomisiliAndExpectSubmitSuccess() {
//		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(submitOrder);
//		when(submitDomisiliRepository.findSubmitDomisiliBySubmitOrder(submitOrder)).thenReturn(null);
//
//		StandardResponseDTO submitNewDomisiliResponse = orderService.savePartialDomisili(submitDomisiliReqDTO);
//		assertEquals(submitNewDomisiliResponse, SUCCESS.getMessage());
//	}

//	@Test
//	public void testUpdateExistingDomisiliAnExpectSubmitSuccess() {
//		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(submitOrder);
//		when(submitDomisiliRepository.findSubmitDomisiliBySubmitOrder(submitOrder)).thenReturn(submitDomisili);
//
//		StandardResponseDTO submitNewDomisiliResponse = orderService.savePartialDomisili(submitDomisiliReqDTO);
//		assertEquals(submitNewDomisiliResponse, SUCCESS.getMessage());
//	}

	@Test
	public void testSubmitNewUnitAndExpectSubmitSuccess() {
		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(submitOrder);
		when(submitUnitRepository.findSubmitUnitBySubmitOrder(submitOrder)).thenReturn(null);

		String submitNewUnitResponse = orderService.savePartialUnit(submitUnitReqDTO);
		assertEquals(submitNewUnitResponse, SUCCESS.getMessage());
	}

	@Test
	public void testUpdateExistingSubmitUnitAndExpectSubmitSuccess() {
		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(submitOrder);
		when(submitUnitRepository.findSubmitUnitBySubmitOrder(submitOrder)).thenReturn(submitUnit);

		String submitNewUnitResponse = orderService.savePartialUnit(submitUnitReqDTO);
		assertEquals(submitNewUnitResponse, SUCCESS.getMessage());
	}

	@Test
	public void testSubmitNewDocumentAndExpectSubmitSuccess() {
		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(submitOrder);
		when(documentRepository.findDocumentDetailBySubmitOrder(submitOrder)).thenReturn(null);

		String saveNewDocumentResponse = orderService.saveDocument(documentReqDTO);
		assertEquals(saveNewDocumentResponse, SUCCESS.getMessage());
	}

	@Test
	public void testUpdateExistingDocumentAndExpectSubmitSuccess() {
		when(submitOrderRepository.findSubmitOrderByOrderNo(submitOrderReqDTO.getOrderNo())).thenReturn(submitOrder);
		when(documentRepository.findDocumentDetailBySubmitOrder(submitOrder)).thenReturn(documentDetail);

		String saveNewDocumentResponse = orderService.saveDocument(documentReqDTO);
		assertEquals(saveNewDocumentResponse, SUCCESS.getMessage());
	}

	@Test
	public void testGenerateIdAndExpectSuccessful() {
		GenerateOrderReq generateOrderReq = new GenerateOrderReq();
		generateOrderReq.setRoleId("SLD");

		when(generateId.generateOrderId(generateOrderReq)).thenReturn("SLD001");
		String generateIdResponse = orderService.getOrderId(generateOrderReq);

		assertEquals(generateIdResponse, "SLD001");
	}

	@Test
	public void testSubmitOrderToLeadAndExpectSuccessful() throws JsonProcessingException {
		SendOrderToLeadReqDTO sendOrderToLeadReqDTO = new SendOrderToLeadReqDTO();
		sendOrderToLeadReqDTO.setCredOrderNo(submitOrder.getOrderNo());
		sendOrderToLeadReqDTO.setCredApplNoPayung(submitOrder.getApplNoPayung());
		sendOrderToLeadReqDTO.setCredApplNoUnit(submitOrder.getApplNoUnit());
		sendOrderToLeadReqDTO.setCredCustType(submitCustomer.getCustType());
		sendOrderToLeadReqDTO.setCreatedBy("admin");
		sendOrderToLeadReqDTO.setCredMobilePhNo("081234567890");
		sendOrderToLeadReqDTO.setCredBirthDate(new Date());
		sendOrderToLeadReqDTO.setCredDateSurvey(new Date());

		SubmitOrder submitOrder1 = new SubmitOrder();
		submitOrder1.setSubmitOrderId("S0001");
		submitOrder1.setOrderNo("ORD001");
		submitOrder1.setActive(0);
		submitOrder1.setCreatedDate(new Date());
		submitOrder1.setModifiedDate(new Date());
		submitOrder1.setCreatedBy("admin");
		submitOrder1.setModifiedBy("admin");
		submitOrder1.setIdx(1);
		submitOrder1.setOrderDate(new Date());
		submitOrder1.setStatus("Draft");
		submitOrder1.setPic("test@ad1gate.com");
		submitOrder1.setPartnerType("PRT_1");
		submitOrder1.setApplNoUnit("appl_no_unit_1");
		submitOrder1.setApplNoPayung("appl_no_payung_1");
		submitOrder1.setGroupStatus("group_status_1");
		submitOrder1.setSendDateStatus(new Date());

		SubmitOrder newSubmitOrder = new SubmitOrder();
		newSubmitOrder.setStatus(ORDER_TERKIRIM.getMessage());
		newSubmitOrder.setGroupStatus(SVR.getMessage());
		newSubmitOrder.setOrderDate(new Date());

		when(kafkaProducer.sendLeadGeneration(sendOrderToLeadReqDTO)).thenReturn("test");
		when(submitOrderRepository.findSubmitOrderByOrderNo(Mockito.any())).thenReturn(submitOrder1);

		String submitOrderToLeadResponse = orderService.submitOrderToLead(sendOrderToLeadReqDTO);
		assertEquals(submitOrderToLeadResponse, "test");
	}

	// line 755
	@Test
	public void testUpdateApplNumberAndExpectSuccessful() {
		GenerateApplNoResDTO generateApplNoResDTO = new GenerateApplNoResDTO();
		generateApplNoResDTO.setApplNo(submitOrder.getApplNoUnit());
		generateApplNoResDTO.setOrderNo(submitOrder.getOrderNo());
		generateApplNoResDTO.setFlagUpdate("1");
		generateApplNoResDTO.setFlagMs2("2");
		generateApplNoResDTO.setSendDate("2022-02-09");
		generateApplNoResDTO.setPayungNo(submitOrder.getApplNoPayung());

		when(submitOrderRepository.findSubmitOrderByOrderNo("ORD001")).thenReturn(submitOrder);
		orderService.updateApplNumber(generateApplNoResDTO);
	}

	@Test
	public void testUpdateApplNumberV2AndExpectSuccessful() {
		GenerateApplNoV2ResDTO generateApplNoV2ResDTO = new GenerateApplNoV2ResDTO();
		generateApplNoV2ResDTO.setOrderNo(submitOrder.getOrderNo());
		generateApplNoV2ResDTO.setApplicationUnit(submitOrder.getApplNoUnit());
		generateApplNoV2ResDTO.setApplicationPayung(submitOrder.getApplNoPayung());

		when(submitOrderRepository.findSubmitOrderByOrderNo(generateApplNoV2ResDTO.getOrderNo()))
				.thenReturn(submitOrder);
		orderService.updateApplNumberV2(generateApplNoV2ResDTO);
	}

	@Test
	public void compareToTestFirstobject() {
		String firstObject = "TEST";
		String secondObject = "TEST";

		String respons = orderService.compareTo(firstObject, secondObject);

		assertEquals(respons, secondObject);
	}

	@Test
	public void compareToTestSecondObject() {
		String firstObject = "TEST1";
		String secondObject = "TEST";

		String respons = orderService.compareTo(firstObject, secondObject);

		assertEquals(respons, secondObject);
	}

	@Test
	public void compareToTestSecondObjectnull() {
		String firstObject = "TEST1";
		String secondObject = null;

		String respons = orderService.compareTo(firstObject, secondObject);

		assertEquals(respons, firstObject);
	}

	@Test
	public void distributedTrackingOrderTest() throws AdiraCustomException {

		TrackingOrderDistributeDTO request = new TrackingOrderDistributeDTO();
		request.setApplicationTrackId("Testing3000");
		request.setApplicationTrackId("3333");
//		request.setApplicationTrackId

		ApplicationTracking applicationTracking = new ApplicationTracking();
		applicationTracking.setActive(true);
		applicationTracking.setApplicationTrackId("Tester123");
		applicationTracking.setEmployeeId("TestID1233");
		applicationTracking.setApplicationTrackId("Testing3000");
		when(modelMapper.map(request, ApplicationTracking.class)).thenReturn(applicationTracking);

//		when(applicationTrackingRepository.save(applicationTracking)).thenAnswer(applicationTracking);

		orderService.distributedTrackingOrder(request);

	}

	@Test
	public void distributedTrackingOrderTestexception() throws AdiraCustomException {
		TrackingOrderDistributeDTO request = new TrackingOrderDistributeDTO();
		request.setApplicationTrackId("Testing3000");
		request.setApplicationTrackId("3333");

		when(applicationTrackingRepository.save(Mockito.any())).thenThrow(RuntimeException.class);
		Exception throwse = assertThrows(AdiraCustomException.class,
				() -> orderService.distributedTrackingOrder(request));
	}

//	@Test
//	public void getFilterDateTest1() {
//		LocalDate localDate = LocalDate.now();
//		String fillterDate = localDate.minusWeeks(3).format(formatDate);
//		int flag =1;
//		fillterDate = localDate.minusDays(3).format(formatDate);
//
//		String test = orderService.getFilterDate(flag);
//	}

//	@Test
//	public void getFilterDateTest2() {
//		LocalDate localDate = LocalDate.now();
//		String fillterDate = localDate.minusWeeks(3).format(formatDate);
//		int flag =2;
//		fillterDate = localDate.minusDays(3).format(formatDate);
//
//		String test = orderService.getFilterDate(flag);
//	}
//
//	@Test
//	public void getFilterDateTest3() {
//		LocalDate localDate = LocalDate.now();
//		String fillterDate = localDate.minusWeeks(3).format(formatDate);
//		int flag =3;
//		fillterDate = localDate.minusDays(3).format(formatDate);
//
//		String test = orderService.getFilterDate(flag);
//	}

	@Test
	public void distributedApplicationTest() throws Exception {

		ApplicationDistributedDTO distributeApplication = new ApplicationDistributedDTO();

		Application application = new Application();
		
		distributeApplication.setApplicationObject(new ApplicationObjectDistributedDTO[0]);
		distributeApplication.setApplicationDocument(new ApplicationDocumentDistributedDTO[0]);
		distributeApplication.setApplicationNote(new ApplicationNoteDistributedDTO[0]); 
		
		application.setApplicationNo("Test123");
		when(applicationRepository.save(application)).thenReturn(application);
		when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(application);

		orderService.distributedApplication(distributeApplication);

	}

	@Test
	public void distributedApplicationTestThrowException() throws AdiraCustomException {
		ApplicationDistributedDTO distributeApplication = new ApplicationDistributedDTO();
		when(applicationRepository.save(Mockito.any())).thenThrow(RuntimeException.class);
		Exception throwse = assertThrows(AdiraCustomException.class,
				() -> orderService.distributedApplication(distributeApplication));
	}

	@Test
	public void distributedCustomerTest() throws Exception {
		CustomerDistributedDTO distributeCustomer = new CustomerDistributedDTO();
		CustomerPersonalDistributedDTO distributeCustomerPersonal = new CustomerPersonalDistributedDTO();

		distributeCustomer.setCustomerType("PER");

		distributeCustomer.setCustomerId("Testing");
		distributeCustomerPersonal.setFkCustomer("Testing");
		distributeCustomer.setCustomerPersonal(distributeCustomerPersonal);

		Customer customer = new Customer();
		CustomerPersonal custPersonal = new CustomerPersonal();

		customer.setCustomerId("TestId1");
		custPersonal.setFkCustomer("TestId1");
		when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(customer);
		when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(custPersonal);

		when(customerRepository.save(customer)).thenReturn(customer);
		when(customerPersonalRepository.save(custPersonal)).thenReturn(custPersonal);

		Exception throwse = assertThrows(AdiraCustomException.class,
				() -> orderService.distributedCustomer(distributeCustomer));
	}

	@Test
	public void distributedCustomerTestException() throws AdiraCustomException {
		CustomerDistributedDTO distributeCustomer = new CustomerDistributedDTO();
		when(customerRepository.save(Mockito.any())).thenThrow(RuntimeException.class);

		Exception throwse = assertThrows(AdiraCustomException.class,
				() -> orderService.distributedCustomer(distributeCustomer));

	}

	@Test
	public void distributedPOTest() throws Exception {
		PurchaseOrderDistributedDTO distributePO = new PurchaseOrderDistributedDTO();

		distributePO.setPoNo("PoNu123");

		PurchaseOrder purchaseOrder = new PurchaseOrder();

		purchaseOrder.setPoNo("PoNu123");

		when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(purchaseOrder);

		when(poRepository.save(purchaseOrder)).thenReturn(purchaseOrder);

		orderService.distributedPO(distributePO);
	}

	@Test
	public void distributedPOTestException() throws AdiraCustomException {
		PurchaseOrderDistributedDTO distributePO = new PurchaseOrderDistributedDTO();
		distributePO.setPoNo("PoNu123");
		when(poRepository.save(Mockito.any())).thenThrow(RuntimeException.class);

		Exception throwse = assertThrows(AdiraCustomException.class, () -> orderService.distributedPO(distributePO));

	}

	@Test
	public void updateApplNumberV2() {

		GenerateApplNoV2ResDTO generateApplNo = new GenerateApplNoV2ResDTO();
		generateApplNo.setOrderNo("3");
		generateApplNo.setApplicationPayung("payung3");
		generateApplNo.setApplicationUnit("12312");

		SubmitOrder data = new SubmitOrder();
		data.setOrderNo("3");
//		data.
		when(submitOrderRepository.findSubmitOrderByOrderNo(Mockito.any())).thenReturn(data);

		when(submitOrderRepository.save(Mockito.any())).thenReturn(data);
		orderService.updateApplNumberV2(generateApplNo);

	}

	@Test
	public void submitOrdertoLeadV2() throws Exception {
		SendOrderToLeadV2ReqDTO sendOrderToLeadV2ReqDTO = new SendOrderToLeadV2ReqDTO();
		String dataOrderSubmit = "";
		sendOrderToLeadV2ReqDTO.setCredOrderNo("TestorderNo1");
		sendOrderToLeadV2ReqDTO.setCreatedBy("Stef");
		sendOrderToLeadV2ReqDTO.setCredEmail("TEst@Gmail.com");
		when(kafkaProducer.sendLeadGenerationV2(Mockito.any())).thenReturn("sukses");

		when(kafkaProducer.updateTrackingStatusLe(Mockito.any())).thenReturn("Yes");

		SubmitOrder data = new SubmitOrder();
		data.setStatus("Pending");
		data.setGroupStatus("");

		when(submitOrderRepository.findSubmitOrderByOrderNo(Mockito.any())).thenReturn(data);

		when(statusOrderDetailRepository.saveAll(Mockito.anyList())).thenReturn(statusOrderDetailList);

		orderService.submitOrdertoLeadV2(sendOrderToLeadV2ReqDTO);

	}

	@Test
	public void getOCRTest() throws Exception {

		MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());
		String flag = "1";
		byte[] filebyte = file.getBytes();
		var dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());
		var url = "";
		OcrNpwpResponsesDTO response = new OcrNpwpResponsesDTO();

		when(restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.any(Class.class)))
				.thenReturn(response);

		orderService.getOCR(file, flag);
	}

	@Test
	public void getOCRTestflag2() throws Exception {

		MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());
		String flag = "2";
		byte[] filebyte = file.getBytes();
		var dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());
		var url = "";
		OcrNpwpResponsesDTO response = new OcrNpwpResponsesDTO();

		when(restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.any(Class.class)))
				.thenReturn(response);

		orderService.getOCR(file, flag);
	}

	@Test
	public void getOCRTesterror2() throws Exception {

		MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());
		String flag = "1";
		byte[] filebyte = file.getBytes();
		var dataResponse = new DataResponse();
		dataResponse.setHttpStatus(HttpStatus.OK);
		dataResponse.setMessage(OK.getMessage());
		var url = "";
		OcrNpwpResponsesDTO response = new OcrNpwpResponsesDTO();

//		when(restTemplate.postForObject(Mockito.anyString(),Mockito.any(),Mockito.any(Class.class))).thenReturn(response);

		orderService.getOCR(file, flag);
	}

	@Test
	public void getWebOCRTest() throws Exception {

		IdpWebReqDTO idpWebReqDTO = new IdpWebReqDTO();

		DataResponse dataResponse = new DataResponse();

		MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());
		String flag = "1";
		byte[] filebyte = file.getBytes();

		OcrNpwpResponsesDTO response = new OcrNpwpResponsesDTO();

		idpWebReqDTO.setFlag("1");
		idpWebReqDTO.setFile(file.getContentType());

		when(restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.any(Class.class)))
				.thenReturn(response);

		orderService.getWebOCR(idpWebReqDTO);
	}

	@Test
	public void getWebOCRTestelse1() {

		IdpWebReqDTO idpWebReqDTO = new IdpWebReqDTO();

		MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());

		idpWebReqDTO.setFlag("2");
		idpWebReqDTO.setFile(file.getContentType());

		OcrKtpResponsesDTO response = new OcrKtpResponsesDTO();
		when(restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.any(Class.class)))
				.thenReturn(response);

		orderService.getWebOCR(idpWebReqDTO);

	}

	@Test
	public void updateApplNumberTest() {
		GenerateApplNoResDTO generateApplNo = new GenerateApplNoResDTO();
		generateApplNo.setApplNo("Testapplno");
		generateApplNo.setPayungNo("TestPayung");
		generateApplNo.setFlagUpdate("1");
		generateApplNo.setFlagMs2("2");
		generateApplNo.setOrderNo("Testoder1");
		generateApplNo.setSendDate("22/03/2022");

		when(submitOrderRepository.findSubmitOrderByOrderNo(Mockito.any())).thenReturn(submitOrder);

		orderService.updateApplNumber(generateApplNo);
	}

	@Test
	public void uploadEcm() throws AdiraCustomException {

		MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());
		String orderNo = "Test1";
		String docType = "txt";
		String idNo = "121";

		when(restTemplate.postForEntity(Mockito.anyString(), Mockito.any(), Mockito.any(Class.class)))
				.thenReturn(new ResponseEntity<String>("", HttpStatus.OK));
		;

		orderService.uploadEcm(file, orderNo, docType, idNo);

	}

//	@Test
//	public void mapObjectDetailTest() {
//
//		OrderDetailLeadResDTO orderDetail= new OrderDetailLeadResDTO();
//		orderDetail.setAcIdno("TestAcid1");
//		orderDetail.setAcFullNameId("TestACFullnameID1");
//		orderDetail.setAcHpNo("TestAChp1");
//		orderDetail.setAcBirthPlace("TestBirth1");
//		orderDetail.setAcBirthDate("22/22/2022");
//		orderDetail.set
//
//
//
//		String orderNo = "";
//
//		when(submitCustomerRepository.findSubmitCustomerBySubmitOrder(Mockito.any())).thenReturn(submitCustomer);
//
//		when(submitDomisiliRepository.findSubmitDomisiliBySubmitOrder(Mockito.any())).thenReturn(submitDomisili);
//
//		when(submitUnitRepository.findSubmitUnitBySubmitOrder(Mockito.any())).thenReturn(submitUnit);
//
//		orderService.getTrackingOrderDetail()
//
//
//
//	}

//	@Test
//	public void insertSubmitOrderToTrackingOrderHomeResTest() {
//
//		TrackingOrderHomeResDTO trackingOrderHomeResDTO = new TrackingOrderHomeResDTO();
//		SubmitOrder submitOrder = new SubmitOrder();
//		submitOrder.setOrderNo("Test123");
//		Date dt = new Date();
//		submitOrder.setOrderDate(dt);
//		submitOrder.setApplNoUnit("199");
//		submitOrder.setGroupStatus("Testok");
//		submitOrder.setStatus("Okedeh");
//		SubmitUnit submitUnit = new SubmitUnit();
//		submitUnit.setObjectDesc("TestDESC");
//		submitUnit.setObjectModelDesc("TestModelDESC");
//
//		SubmitCustomer submitCustomer = new SubmitCustomer();
//		submitCustomer.setCustomerName("TESTing username");
//
//		when(submitUnitRepository.findSubmitUnitBySubmitOrder(Mockito.any())).thenReturn(submitUnit);
//		when(submitCustomerRepository.findSubmitCustomerBySubmitOrder(Mockito.any())).thenReturn(submitCustomer);
//
//		TrackingOrderHomeResDTO data = orderService.in
//
//
//
//	}

//		private Matcher<TrackingSumOrderSlsResDTO> getDataSumDTOMatcher() {
//	        return allOf(
//	                hasProperty("id", is(USER_ID)),
//	                hasProperty("orderTerkirim", is(USER_ID)),
//	                hasProperty("berlangsung", is(USER_ID)),
//	                hasProperty("po", is(USER_ID))
//	        );
//
//	}

//	@Test
//	public void testSubmitOrderToLeadV2AndExpectSuccessful() throws JsonProcessingException {
//		UpdateStatusTrackingReqDTO updateStatusTrackingReqDTO = new UpdateStatusTrackingReqDTO();
//		updateStatusTrackingReqDTO.setStatus("1");
//		updateStatusTrackingReqDTO.setOrderNo("ORD001");
//
//		SendOrderToLeadV2ReqDTO sendOrderToLeadV2ReqDTO = new SendOrderToLeadV2ReqDTO();
//		sendOrderToLeadV2ReqDTO.setCredOrderNo("ORD001");
//		sendOrderToLeadV2ReqDTO.setCreatedBy("admin");
//
//		SubmitOrder submitOrder = new SubmitOrder();
//		submitOrder.setSubmitOrderId("SU001");
//		submitOrder.setOrderNo("ORD001");
//
//		UpdateStatusTrackingDTO updateStatusTrackingDTO = new UpdateStatusTrackingDTO("Test", "Test",
//				sendOrderToLeadV2ReqDTO.getCredOrderNo(), new Date(), "", "Test", "Test", "Test", 0, true,
//				sendOrderToLeadV2ReqDTO.getCreatedBy().toLowerCase(), new Date(),
//				sendOrderToLeadV2ReqDTO.getCreatedBy().toLowerCase(), new Date());
//
//		when(submitOrderRepository.findSubmitOrderByOrderNo(sendOrderToLeadV2ReqDTO.getCredOrderNo()))
//				.thenReturn(submitOrder);
//		when(kafkaProducer.sendLeadGenerationV2(sendOrderToLeadV2ReqDTO)).thenReturn("");
//		when(kafkaProducer.updateTrackingStatusLe(updateStatusTrackingDTO)).thenReturn("");
//
//		String response = orderService.submitOrdertoLeadV2(sendOrderToLeadV2ReqDTO);
//		assertEquals(response, "");
//	}

}
