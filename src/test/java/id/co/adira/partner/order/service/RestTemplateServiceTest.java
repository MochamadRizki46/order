//package id.co.adira.partner.order.service;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import id.co.adira.partner.order.dto.creditsimulation.CreditSimulationHeadReqDTO;
//import id.co.adira.partner.order.dto.creditsimulation.request.apikey.APIKeyRequestDTO;
//import id.co.adira.partner.order.dto.creditsimulation.request.credsim.*;
//import id.co.adira.partner.order.dto.creditsimulation.request.dporpmt.DownPaymentOrPmtReqToCommonServiceDTO;
//import id.co.adira.partner.order.dto.creditsimulation.response.apikey.APIKeyDetailResponseDTO;
//import id.co.adira.partner.order.dto.creditsimulation.response.apikey.APIKeyHeadResponseDTO;
//import id.co.adira.partner.order.dto.creditsimulation.response.credsim.AsuransiCashAndCreditResDTO;
//import id.co.adira.partner.order.dto.creditsimulation.response.credsim.BiayaAdminResDTO;
//import id.co.adira.partner.order.dto.creditsimulation.response.credsim.CreditSimulationDetailResDTO;
//import id.co.adira.partner.order.dto.creditsimulation.response.credsim.EffectiveOrLandingRateResDTO;
//import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPaymentDetailResDTO;
//import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPmtHeadResDTO;
//import id.co.adira.partner.order.service.resttemplate.RestTemplateService;
//import org.apache.http.client.methods.HttpHead;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.ArgumentMatchers;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.env.Environment;
//import org.springframework.http.*;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.context.WebApplicationContext;
//import org.springframework.web.util.UriComponentsBuilder;
//
//import java.math.BigDecimal;
//import java.math.RoundingMode;
//
//import static id.co.adira.partner.order.constant.AppConst.*;
//import static id.co.adira.partner.order.constant.StatusConst.SUCCESS;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.when;
//
//@SpringBootTest
//@PropertySource("classpath:application.properties")
//public class RestTemplateServiceTest {
//    @InjectMocks
//    RestTemplateService restTemplateService;
//
//    @Mock
//    RestTemplate restTemplate;
//
//    MockMvc mockMvc;
//
//    @Autowired
//    WebApplicationContext context;
//
//    @Mock
//    Environment env;
//
//    APIKeyRequestDTO apiKeyRequestDTO = new APIKeyRequestDTO();
//    APIKeyHeadResponseDTO apiKeyHeadResponseDTO = new APIKeyHeadResponseDTO();
//    APIKeyDetailResponseDTO apiKeyDetailResponseDTO = new APIKeyDetailResponseDTO();
//
//    BiayaAdminReqDTO biayaAdminReqDTO = new BiayaAdminReqDTO();
//    BiayaAdminResDTO biayaAdminResDTO = new BiayaAdminResDTO();
//
//    AsuransiCashAndCreditReqDTO asuransiCashAndCreditReqDTO = new AsuransiCashAndCreditReqDTO();
//    AsuransiCashAndCreditResDTO asuransiCashAndCreditResDTO = new AsuransiCashAndCreditResDTO();
//
//    EffectiveOrLandingRateResDTO effectiveOrLandingRateResDTO = new EffectiveOrLandingRateResDTO();
//    EffectiveOrLandingRateReqDTO effectiveOrLandingRateReqDTO = new EffectiveOrLandingRateReqDTO();
//
//    DpOrPaymentRequestDTO dpOrPaymentRequestDTO = new DpOrPaymentRequestDTO();
//    DpOrPaymentDetailResDTO dpOrPaymentDetailResDTO = new DpOrPaymentDetailResDTO();
//    DpOrPmtHeadResDTO dpOrPmtHeadResDTO = new DpOrPmtHeadResDTO();
//    DownPaymentOrPmtReqToCommonServiceDTO downPaymentOrPmtReqToCommonServiceDTO = new DownPaymentOrPmtReqToCommonServiceDTO();
//
//    CreditSimulationDetailReqDTO creditSimulationDetailReqDTO = new CreditSimulationDetailReqDTO();
//    CreditSimulationHeadReqDTO creditSimulationHeadReqDTO = new CreditSimulationHeadReqDTO() {
//        @Override
//        public String getInstallmentType() {
//            return "dummy_installment_type";
//        }
//
//        @Override
//        public int getCreditType() {
//            return 1;
//        }
//
//        @Override
//        public double getEffectiveRate() {
//            return 0.0192;
//        }
//
//        @Override
//        public int getFlatRate() {
//            return 0;
//        }
//
//        @Override
//        public double getFv() {
//            return 0.1;
//        }
//
//        @Override
//        public int getRounded() {
//            return 1;
//        }
//
//        @Override
//        public int getSeasonal() {
//            return 1;
//        }
//
//        @Override
//        public double getTenor() {
//            return 12.0;
//        }
//
//        @Override
//        public double getTotalPrinciple() {
//            return 1;
//        }
//    };
//
//    CreditSimulationDetailResDTO creditSimulationDetailResDTO = new CreditSimulationDetailResDTO();
//
//    @BeforeEach
//    public void setUp(){
//        MockitoAnnotations.initMocks(this);
//
//        dpOrPaymentRequestDTO.setTenor(12);
//        dpOrPaymentRequestDTO.setAngsuran(new BigDecimal(5000000));
//        dpOrPaymentRequestDTO.setPic("test@ad1gate.com");
//        dpOrPaymentRequestDTO.setDpSystem(500000.0);
//        dpOrPaymentRequestDTO.setDpGross(500000.0);
//        dpOrPaymentRequestDTO.setOtr(15000000.0);
//        dpOrPaymentRequestDTO.setDiscount(20);
//        dpOrPaymentRequestDTO.setProgramId("PRG001");
//        dpOrPaymentRequestDTO.setCreditType(1);
//        dpOrPaymentRequestDTO.setObjectTypeId("OBT001");
//        dpOrPaymentRequestDTO.setObjectCode("OBC001");
//        dpOrPaymentRequestDTO.setObjectModelId("OBM001");
//        dpOrPaymentRequestDTO.setCashOrCredit(1);
//
//        downPaymentOrPmtReqToCommonServiceDTO.setTenor(12);
//        downPaymentOrPmtReqToCommonServiceDTO.setEffectiveRate(0.0102);
//        downPaymentOrPmtReqToCommonServiceDTO.setCreditType(1);
//        downPaymentOrPmtReqToCommonServiceDTO.setOtr(15000000.0);
//        downPaymentOrPmtReqToCommonServiceDTO.setAdminCredit(1400000.0);
//        downPaymentOrPmtReqToCommonServiceDTO.setAdminCash(200000.0);
//        downPaymentOrPmtReqToCommonServiceDTO.setDiscount(0.2);
//        downPaymentOrPmtReqToCommonServiceDTO.setAngsuran(BigDecimal.ZERO);
//        downPaymentOrPmtReqToCommonServiceDTO.setDpSystem(500000.0);
//        downPaymentOrPmtReqToCommonServiceDTO.setDpGross(500000.0);
//        downPaymentOrPmtReqToCommonServiceDTO.setAsuransiCreditRate(new BigDecimal(0.0192).setScale(5, RoundingMode.HALF_UP));
//        downPaymentOrPmtReqToCommonServiceDTO.setAsuransiCashRate(BigDecimal.ZERO);
//        downPaymentOrPmtReqToCommonServiceDTO.setUsername("test@ad1gate.com");
//
//        dpOrPaymentDetailResDTO.setTotalPokokHutangUnit(10000.0);
//        dpOrPaymentDetailResDTO.setEffectiveRate(0.0102);
//        dpOrPaymentDetailResDTO.setTenor(12);
//        dpOrPaymentDetailResDTO.setOtr(15000000.0);
//        dpOrPaymentDetailResDTO.setDpSystem(500000.0);
//        dpOrPaymentDetailResDTO.setDiscount(0.2);
//        dpOrPaymentDetailResDTO.setFlatRate(0.0);
//        dpOrPaymentDetailResDTO.setPokokHutangUnit(5000.0);
//        dpOrPaymentDetailResDTO.setAsuransiCreditRate(0.0192);
//        dpOrPaymentDetailResDTO.setAsuransiCashRate(0.0);
//        dpOrPaymentDetailResDTO.setAdminCash(200000.0);
//        dpOrPaymentDetailResDTO.setAdminCredit(1400000.0);
//        dpOrPaymentDetailResDTO.setAngsuran(0.0);
//        dpOrPaymentDetailResDTO.setAngsuran1(null);
//
//        dpOrPmtHeadResDTO.setStatus("1");
//        dpOrPmtHeadResDTO.setMessage(SUCCESS.getMessage());
//        dpOrPmtHeadResDTO.setData(dpOrPaymentDetailResDTO);
//
//        biayaAdminReqDTO.setTenor("12");
//        biayaAdminReqDTO.setObjectCode("OBJ001");
//        biayaAdminReqDTO.setObjectModelId("OBM001");
//        biayaAdminReqDTO.setProgramId("PRG001");
//
//        biayaAdminResDTO.setOtr(20902000);
//        biayaAdminResDTO.setAdminCashFee(new BigDecimal(200000.0));
//        biayaAdminResDTO.setAdminCreditFee(new BigDecimal(1400000.0));
//
//        effectiveOrLandingRateReqDTO.setTenor(new BigDecimal(12));
//        effectiveOrLandingRateReqDTO.setObjectModelId("OBM001");
//        effectiveOrLandingRateReqDTO.setObjectId("OBJ001");
//        effectiveOrLandingRateReqDTO.setPic("test@ad1gate.com");
//        effectiveOrLandingRateReqDTO.setProgramId("PRG001");
//        effectiveOrLandingRateReqDTO.setObjectTypeId("OBT001");
//
//        effectiveOrLandingRateResDTO.setEffectiveOrLandingRate(new BigDecimal(0.0102).setScale(5, RoundingMode.HALF_UP));
//
//        asuransiCashAndCreditReqDTO.setObjectCode("OBJ001");
//        asuransiCashAndCreditReqDTO.setTenor("12");
//        asuransiCashAndCreditReqDTO.setProgramId("PRG001");
//        asuransiCashAndCreditReqDTO.setObjectModelId("OBM001");
//        asuransiCashAndCreditReqDTO.setOtr(15000000.0);
//
//        asuransiCashAndCreditResDTO.setAsuransiCashAmt(BigDecimal.ZERO);
//        asuransiCashAndCreditResDTO.setAsuransiCreditAmt(new BigDecimal(0.0192).setScale(5, RoundingMode.HALF_UP));
//
//        apiKeyDetailResponseDTO.setApiKey("Bearer test");
//        apiKeyDetailResponseDTO.setApiKeyExpDate("2022-02-16");
//
//        apiKeyHeadResponseDTO.setStatus("1");
//        apiKeyHeadResponseDTO.setMessage("SUCCESS");
//        apiKeyHeadResponseDTO.setData(apiKeyDetailResponseDTO);
//
//        apiKeyRequestDTO.setUserName("test@ad1gate.com");
//
//        creditSimulationDetailResDTO.setCreditType(1);
//        creditSimulationDetailResDTO.setEffectiveRate(0.0192);
//        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
//
//        when(env.getProperty("url.api.get.biaya.admin")).thenReturn("http://10.50.7.251:4321/adiraOnePartner/params/api/credsim/param/get-biaya-admin");
//        when(env.getProperty("url.api.get.asuransi.cash.and.credit.rate")).thenReturn("http://10.50.7.251:4321/adiraOnePartner/params/api/credsim/param/get-asuransi-cash-and-credit");
//        when(env.getProperty("url.api.calculate.dp.or.pmt")).thenReturn("http://10.161.17.176:32112/api/commonservice/creditsim/v1/dpOrPmt");
//        when(env.getProperty("url.get.api.key")).thenReturn("http://10.161.17.178:32201/api/commonservice/user-mgmt/v1/createAPIKey");
//        when(env.getProperty("url.api.get.effective.or.landing.rate")).thenReturn("http://10.50.7.251:4321/adiraOnePartner/params/api/credsim/param/get-effective-or-landing-rate");
//        when(env.getProperty("url.calculate.credit.simulation")).thenReturn("http://10.161.17.176:32112/api/commonservice/creditsim/v1/calculate");
//        when(env.getProperty("url.api.get.brand.tipe.model")).thenReturn("http://10.50.7.251:4321/adiraOnePartner/params/api/common/param/get-lov-brand-tipe-model");
//
//    }
//
//    @Test
//    public void testCallPostAPIToGetBiayaAdmin() throws JsonProcessingException {
//        ResponseEntity<String> responseEntity = new ResponseEntity<String>(mapBiayaAdminResponseToString(), HttpStatus.OK);
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);
//
//        HttpEntity httpEntity = new HttpEntity(biayaAdminReqDTO, headers);
//        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(env.getProperty("url.api.get.biaya.admin"));
//
//        when(restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity, String.class)).thenReturn(responseEntity);
//        BiayaAdminResDTO response = restTemplateService.callRestTemplateToGetBiayaAdmin(biayaAdminReqDTO);
//        assertEquals(biayaAdminResDTO.getOtr(), response.getOtr());
//    }
//
//    @Test
//    public void testCallRestApiToGetAsuransiCashOrCreditRateAndExpectSuccessful() throws JsonProcessingException {
//        ResponseEntity<String> responseEntity = new ResponseEntity<String>(mapAsuransiCashAndCreditRateResponseToString(), HttpStatus.OK);
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);
//
//        HttpEntity httpEntity = new HttpEntity(asuransiCashAndCreditReqDTO, headers);
//        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(env.getProperty("url.api.get.asuransi.cash.and.credit.rate"));
//
//        when(restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity, String.class)).thenReturn(responseEntity);
//        AsuransiCashAndCreditResDTO response = restTemplateService.callRestTemplateToGetAsuransiCashAndCredit(asuransiCashAndCreditReqDTO);
//
//        assertEquals(response.getAsuransiCreditAmt(), asuransiCashAndCreditResDTO.getAsuransiCreditAmt());
//
//    }
//
//    @Test
//    public void testCallRestApiToGetEffectiveOrLandingRateAndExpectSuccessful() throws JsonProcessingException {
//        ResponseEntity<String> responseEntity = new ResponseEntity<String>(mapEffectiveOrLandingRateResponseToString(), HttpStatus.OK);
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);
//
//        HttpEntity httpEntity = new HttpEntity(effectiveOrLandingRateReqDTO, headers);
//        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(env.getProperty("url.api.get.effective.or.landing.rate"));
//
//        when(restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity, String.class)).thenReturn(responseEntity);
//        EffectiveOrLandingRateResDTO response = restTemplateService.callRestTemplateToGetEffectiveOrLandingRate(effectiveOrLandingRateReqDTO);
//
//        assertEquals(response.getEffectiveOrLandingRate(), effectiveOrLandingRateResDTO.getEffectiveOrLandingRate());
//
//    }
//
//    @Test
//    public void testCallRestApiToGetApiKeyAndExpectSuccessful() throws JsonProcessingException {
//        ResponseEntity<String> responseEntity = new ResponseEntity<String>(mapApiKeyResponseToString(), HttpStatus.OK);
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.set(ACCEPT.getMessage(), MediaType.APPLICATION_JSON_VALUE);
//
//        HttpEntity httpEntity = new HttpEntity(apiKeyRequestDTO, headers);
//        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(env.getProperty("url.get.api.key"));
//
//        when(restTemplate.exchange(uri.toUriString(), HttpMethod.POST, httpEntity, String.class)).thenReturn(responseEntity);
//        APIKeyHeadResponseDTO response = restTemplateService.callPostRestApiForGetApiKey(apiKeyRequestDTO);
//
//        assertEquals(response.getData().getApiKey(), apiKeyHeadResponseDTO.getData().getApiKey());
//    }
//
//    @Test
//    public void testCallRestApiToGetCalculationDpOrPmtAndExpectSuccessful(){
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.set(ACCEPT.getMessage(), MediaType.APPLICATION_JSON_VALUE);
//
//        when(restTemplate.exchange(ArgumentMatchers.anyString(), any(HttpMethod.class), any(), ArgumentMatchers.<Class<DpOrPmtHeadResDTO>>any())).thenReturn(new ResponseEntity<>(dpOrPmtHeadResDTO, HttpStatus.OK));
//        DpOrPmtHeadResDTO response = restTemplateService.callPostRestApiForGetCalculationDpOrPmt(downPaymentOrPmtReqToCommonServiceDTO, "Bearer test");
//        assertEquals(response.getData().getDpSystem(), dpOrPmtHeadResDTO.getData().getDpSystem());
//    }
//
//    @Test
//    public void testCallRestApiForCalculateCredsimAndExpectSuccessful(){
//        HttpHeaders headers = new HttpHeaders();
//        headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);
//        headers.set(AUTHORIZATION.getMessage(), "Bearer test");
//
//        when(restTemplate.exchange(ArgumentMatchers.anyString(), any(HttpMethod.class), any(), ArgumentMatchers.<Class<CreditSimulationDetailResDTO>>any())).thenReturn(new ResponseEntity<>(creditSimulationDetailResDTO, HttpStatus.OK));
//        CreditSimulationDetailResDTO response = restTemplateService.callPostRestApiForCalculateCredSim("Bearer test", creditSimulationDetailReqDTO);
//
//        assertEquals(response.getCreditType(), creditSimulationDetailResDTO.getCreditType());
//    }
//
//    private String mapApiKeyResponseToString() {
//        return "{\"status\":\"1\",\"message\":\"Success\",\"data\":{\"apiKey\":\"Bearer test\",\"apiKeyExpDate\":\"2022-02-16\"}}";
//    }
//
//
//    private String mapBiayaAdminResponseToString(){
//        return "{\"status\":\"OK\",\"message\":\"Success\",\"data\":{\"adminCashFee\":200000,\"adminCreditFee\":1400000,\"otr\":20902000}}";
//    }
//
//    private String mapAsuransiCashAndCreditRateResponseToString(){
//        return "{\"status\":\"OK\",\"message\":\"Success\",\"data\":{\"asuransiCashAmt\":0,\"asuransiCreditAmt\":0.0192}}";
//    }
//
//    private String mapEffectiveOrLandingRateResponseToString(){
//        return "{\"status\":\"OK\",\"message\":\"Success\",\"data\":{\"effectiveOrLandingRate\":0.0102}}";
//    }
//}
