package id.co.adira.partner.order.service.resttemplate;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import id.co.adira.partner.order.dto.creditsimulation.CreditSimulationHeadReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.apikey.APIKeyRequestDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.AsuransiCashAndCreditReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.BiayaAdminReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.credsim.CreditSimulationDetailReqDTO;
import id.co.adira.partner.order.dto.creditsimulation.request.dporpmt.DownPaymentOrPmtReqToCommonServiceDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.credsim.BaseResponseDTO;
import id.co.adira.partner.order.dto.creditsimulation.response.dporpmt.DpOrPmtHeadResDTO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



@ExtendWith(MockitoExtension.class)
@PropertySource("classpath:application.properties")
@MockitoSettings(strictness = Strictness.LENIENT)
public class RestTemlateSeviceTest {
	
	@InjectMocks
	RestTemplateService restTemplateService;
	

	@Mock
	private RestTemplate restTemplate;
	
	
	@Mock
	ObjectMapper objectmppr;
	
    @Mock
	Environment env;
	
	
	@Test
	public void callPostRestApiForCalculateCredSimTest() {
		
		String authorization="TestAuthorization";
		
		CreditSimulationHeadReqDTO creditSimulationHeadReqDTO = null;
		
		when(restTemplate.exchange(Mockito.anyString(),Mockito.<HttpMethod> any(),Mockito.any(),Mockito.any(Class.class))).thenReturn(new ResponseEntity<Object>(creditSimulationHeadReqDTO,HttpStatus.OK));
		when(env.getProperty(Mockito.anyString())).thenReturn("http://localhost:8001");
		
		restTemplateService.callPostRestApiForCalculateCredSim(authorization,creditSimulationHeadReqDTO);
		
		
	}
	
	@Test
	public void callPostRestApiForGetApiKeyTest() throws Exception {
		APIKeyRequestDTO apiKeyRequestDTO = new APIKeyRequestDTO();
		when(restTemplate.exchange(Mockito.anyString(),Mockito.<HttpMethod> any(),Mockito.any(),Mockito.any(Class.class))).thenReturn(new ResponseEntity<String>("OK Test",HttpStatus.OK));
		when(env.getProperty(Mockito.anyString())).thenReturn("http://localhost:8001");
		
		
		restTemplateService.callPostRestApiForGetApiKey(apiKeyRequestDTO);
		
		
		
	}
	
	@Test
	public void callPostRestApiForGetCalculationDpOrPmtTest() {
		DownPaymentOrPmtReqToCommonServiceDTO downPaymentOrPmtReqToCommonServiceDTO = new  DownPaymentOrPmtReqToCommonServiceDTO();
		
		String authorization = "TestAuthorization";
		DpOrPmtHeadResDTO response = new DpOrPmtHeadResDTO();
	
		when(env.getProperty(Mockito.anyString())).thenReturn("http://localhost:8001");
		
		when(restTemplate.exchange(Mockito.anyString(),Mockito.<HttpMethod> any(),Mockito.any(),Mockito.any(Class.class))).thenReturn(new ResponseEntity<Object>(response,HttpStatus.OK));
		
		restTemplateService.callPostRestApiForGetCalculationDpOrPmt(downPaymentOrPmtReqToCommonServiceDTO,authorization);
	}
	
	
//	@Test
//	public void callRestTemplateToGetBiayaAdminTest() throws Exception {
//		
//		BiayaAdminReqDTO biayaAdminReqDTO = new BiayaAdminReqDTO();
//		BaseResponseDTO response = new BaseResponseDTO();
//		
//		
//		
//		when(restTemplate.exchange(Mockito.anyString(),Mockito.<HttpMethod> any(),Mockito.any(),Mockito.any(Class.class))).thenReturn(new ResponseEntity<String>("OK Test",HttpStatus.OK));
//		
//		
//		when(objectmppr.readValue(Mockito.anyString(),Mockito.any(Class.class))).thenReturn(response);
//		
//		restTemplateService.callRestTemplateToGetBiayaAdmin(biayaAdminReqDTO);
//		
//	}
//	
//	@Test
//	public void callRestTemplateToGetAsuransiCashAndCreditTest() throws Exception {
//		AsuransiCashAndCreditReqDTO asuransiCashAndCreditReqDTO = new AsuransiCashAndCreditReqDTO();
//		BaseResponseDTO response = new BaseResponseDTO();
//		
//		when(restTemplate.exchange(Mockito.anyString(),Mockito.<HttpMethod> any(),Mockito.any(),Mockito.any(Class.class))).thenReturn(new ResponseEntity<String>("OK Test",HttpStatus.OK));
//		when(objectmppr.readValue(Mockito.anyString(),Mockito.any(Class.class))).thenReturn(response);
//		
//		restTemplateService.callRestTemplateToGetAsuransiCashAndCredit(asuransiCashAndCreditReqDTO);
//		
//	}
	
	
	
	
	
}
